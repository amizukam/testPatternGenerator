#include <string.h>
#include "TgcParameterGenerator/TgcPpsTpgDelay_Generator.hh"

int TgcPpsTpgDelay_Generator::NUM_OF_CLMN_OF_DEFAULT_LIST_FILE=7;
int TgcPpsTpgDelay_Generator::NUM_OF_CLMN_OF_LIST_FILE=2;
int TgcPpsTpgDelay_Generator::NUM_OF_CLMN_FOR_LOCAL_VALUE_DEFINITION=8;
int TgcPpsTpgDelay_Generator::NUM_OF_BIT_FINE=5;
int TgcPpsTpgDelay_Generator::NUM_OF_BIT_COARSE=3;
char* TgcPpsTpgDelay_Generator::EDGE_SELECTION=(char*)"11"; //11 for rising edge, 00 for falling edge

std::string s_TPG_DELAY_dir = std::getenv("SETUP_DIR");
std::string s_TPG_DELAY_file = "/TPG_DELAY_DEFAULT.txt";
std::string s_TPG_DELAY_list = s_TPG_DELAY_dir + s_TPG_DELAY_file;
const char* c_TPG_DELAY_list = s_TPG_DELAY_list.c_str();
char* TgcPpsTpgDelay_Generator::FILE_NAME_OF_DEFAULT_VALUE_LIST = strdup(c_TPG_DELAY_list);


TgcPpsTpgDelay_Generator::TgcPpsTpgDelay_Generator(): useDefaultConfigurationFile(true),
						nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcPpsTpgDelay_Generator::TgcPpsTpgDelay_Generator(char* fileName): useDefaultConfigurationFile(false),
							      nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcPpsTpgDelay_Generator::~TgcPpsTpgDelay_Generator(){}

bool TgcPpsTpgDelay_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcPpsSsw = new TgcPpsSsw();
  m_tgcPpsSlb = new TgcPpsSlb();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iSsw=0; iSsw<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic);
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	for(int iAsic=0; iAsic<TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG; iAsic++){
	  for(int iAorB=0; iAorB<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iAorB++){
	    valueOfTpgFineDelay  [iSsw][iPort][iJtag][iAsic][iAorB]=(iAsic<numOfPpAsic.at(iJtag)) ? 0 : -1;
	    valueOfTpgCoarseDelay[iSsw][iPort][iJtag][iAsic][iAorB]=(iAsic<numOfPpAsic.at(iJtag)) ? 0 : -1;
	  }
	}
      }
    }
  }
  
  std::ifstream inputfile(FILE_NAME_OF_DEFAULT_VALUE_LIST);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<FILE_NAME_OF_DEFAULT_VALUE_LIST
	     <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  std::string buf("");
  int sharpPoint;
  
  std::string entry[NUM_OF_CLMN_OF_DEFAULT_LIST_FILE];
  
  int cciIdReference[2]={0,0};
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    
    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_DEFAULT_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
	continue;
    }
    
    if(atoi(entry[0].c_str())==0 && entry[0]!="0") continue;
    
    
    if(iLine!=NUM_OF_CLMN_OF_DEFAULT_LIST_FILE) continue;
    //if(iLine!=(NUM_OF_CLMN_OF_DEFAULT_LIST_FILE-1)) continue;
    
    
    if(!(cciId<=cciIdReference[0] &&
	 cciId> cciIdReference[1] )) continue;
    
    int iSsw   =atoi(entry[0].c_str());
    int iPort  =atoi(entry[1].c_str());
    int iPps   =atoi(entry[2].c_str());
    int iPpAsic=atoi(entry[3].c_str());
    int iAorB  =atoi(entry[4].c_str());
    int tmp_valueOfCoarseDelay=atoi(entry[5].c_str());
    int tmp_valueOfFineDelay  =atoi(entry[6].c_str());
    
    if((iSsw   ==0 && entry[0]!="0") ||
       (iPort  ==0 && entry[1]!="0") ||
       (iPps   ==0 && entry[2]!="0") ||
       (iPpAsic==0 && entry[3]!="0") ||
       (iAorB  ==0 && entry[4]!="0") ||
       (tmp_valueOfCoarseDelay==0 && entry[5]!="0") ||
       (tmp_valueOfFineDelay  ==0 && entry[6]!="0")) continue;
    
    if(logLevel<=TGC_MSG_LEVEL::INFO)
      std::cout<<"inf> "<<buf<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    
    valueOfTpgFineDelay  [iSsw][iPort][iPps][iPpAsic][iAorB]=tmp_valueOfFineDelay;
    valueOfTpgCoarseDelay[iSsw][iPort][iPps][iPpAsic][iAorB]=tmp_valueOfCoarseDelay;
  }
  
  return true;
}

bool TgcPpsTpgDelay_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Pps/TpgDelay");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;
}

bool TgcPpsTpgDelay_Generator::readList(std::string fileName){

  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf("");
  int sharpPoint;
  
  std::string entry[NUM_OF_CLMN_FOR_LOCAL_VALUE_DEFINITION];
  bool isAbsoluteValue=false;

  
  while(getline(inputfile, buf)){
    std::stringstream line(buf);
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
        
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_FOR_LOCAL_VALUE_DEFINITION)) break;
    }    
    
    if(iLine==NUM_OF_CLMN_OF_LIST_FILE){
      
      if(entry[0]=="GLOBAL_RELATIVE_VALUE") {
	int relativeValue=atoi(entry[1].c_str());
	for(int ii=0; ii<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI; ii++){
	  for(int jj=0; jj<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; jj++){
	    for(int kk=0; kk<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; kk++){
	      for(int ll=0; ll<TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG; ll++){
		for(int mm=0; mm<TGC_HW_CONSTANT::NUM_OF_A_OR_B; mm++){
		  if(!(valueOfTpgFineDelay  [ii][jj][kk][ll][mm]!=-1 &&
		       valueOfTpgCoarseDelay[ii][jj][kk][ll][mm]!=-1)) continue;
		  
		  int valueOfDelay=(valueOfTpgFineDelay  [ii][jj][kk][ll][mm]+
			      valueOfTpgCoarseDelay[ii][jj][kk][ll][mm]*TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS);
		  
		  int fineBefore  =valueOfTpgFineDelay  [ii][jj][kk][ll][mm];
		  int coarseBefore=valueOfTpgCoarseDelay[ii][jj][kk][ll][mm];
		  
		  valueOfDelay+=relativeValue;
		  
		  valueOfTpgCoarseDelay[ii][jj][kk][ll][mm] = (valueOfDelay-valueOfDelay%TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS)/TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS;
		  valueOfTpgFineDelay  [ii][jj][kk][ll][mm] =  valueOfDelay%TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS;
		  
		  if(logLevel<=TGC_MSG_LEVEL::INFO){
		    char cciAddress[10];
		    char sswAddress[10];
		    
		    sprintf(cciAddress, "%1s", cciId );
		    sprintf(sswAddress, "%1s", ii );
		    
		    char* aOrB= (mm==0) ? (char*)"A" : (char*)"B";
		    std::cout<<"inf> Afterconversion"<<std::endl
			     <<"inf> "<<relativeValue<<" will be added to FineDelay"<<fineBefore<<" and CoarseDelay"<<coarseBefore<<std::endl
			     <<"inf> 0x"<<cciAddress<<"8"<<sswAddress<<"000"<<jj<<" Pps"<<kk<<"/TpgFine"<<aOrB<<" "<<valueOfTpgFineDelay[ii][jj][kk][ll][mm]<<std::endl
			     <<"inf> 0x"<<cciAddress<<"8"<<sswAddress<<"000"<<jj<<" Pps"<<kk<<"/TpgCoarse"<<aOrB<<" "<<valueOfTpgCoarseDelay[ii][jj][kk][ll][mm]<<std::endl
			     <<"inf> in "<<__FILE__<<" at "<<__LINE__<<std::endl;		
		  }
		}
	      }
	    }
	  }
	}
	
	continue;
      }
      else if (entry[0]=="LOCAL_VALUE_DEFINITION") {
	if(entry[1]=="ABSOLUTE") {
	  isAbsoluteValue=true;
	  continue;
	}
	else if(entry[1]=="RELATIVE") {
	  isAbsoluteValue=false;
	  continue;
	}
	else {
	  std::cerr<<"err> unexpected charactor "<<entry[1]<<" "<<__FILE__<<":"<<__LINE__<<std::endl;
	  return false;
	}
      }
    }
    
    if (iLine==NUM_OF_CLMN_FOR_LOCAL_VALUE_DEFINITION) {
      if(!(string_side==entry[0] &&
	   cciId==atoi(entry[1].c_str()) )) continue;
      
      
      
      int iSsw   =atoi(entry[2].c_str()); 
      int iPort  =atoi(entry[3].c_str());
      int iPps   =atoi(entry[4].c_str());
      int iPpAsic=atoi(entry[5].c_str());
      int iAorB  =atoi(entry[6].c_str());
      int value  =atoi(entry[7].c_str());
      
      if((iSsw   ==0 && entry[2]!="0") ||
	 (iPort  ==0 && entry[3]!="0") ||
	 (iPps   ==0 && entry[4]!="0") ||
	 (iPpAsic==0 && entry[5]!="0") ||
	 (iAorB  ==0 && entry[6]!="0") ||
	 (value  ==0 && entry[7]!="0")) continue;
      
      int valueOfDelay=(valueOfTpgFineDelay  [iSsw][iPort][iPps][iPpAsic][iAorB]+
			valueOfTpgCoarseDelay[iSsw][iPort][iPps][iPpAsic][iAorB]*TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS);
      
      if(isAbsoluteValue) {
	valueOfDelay=value;
      }
      else {
	valueOfDelay+=value;
      }
      
      valueOfTpgCoarseDelay[iSsw][iPort][iPps][iPpAsic][iAorB] = (valueOfDelay-valueOfDelay%TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS)/TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS;
      valueOfTpgFineDelay  [iSsw][iPort][iPps][iPpAsic][iAorB] =  valueOfDelay%TGC_HW_CONSTANT::PP_FINE_STEP_SIZE_FOR_25NS;
    }
  } 
  return true;
  
}
  
bool TgcPpsTpgDelay_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);

  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      char address[100];
      
      sprintf(address, "0x0%x8%x000%x",cciId,iSsw,iPort);
      
      if(!m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(numOfPpAsic.size()!=4){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	
	if(numOfPpAsic.at(iJtag)==0) continue;
	
	
	for(int iLine=0; iLine<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iLine++){
	  char* lineName;
	  (iLine==1) ? lineName=(char*)"B" : lineName=(char*)"A";
	  
	  // TPG FINE
	  outfile<<address<<" Pps"<<iJtag<<"/TpgFine"<<lineName<<" ";
	  
	  for(int iPpAsic=0; iPpAsic<numOfPpAsic.at(iJtag); iPpAsic++){
	    char value_char[BUFSIZ];
	    //if(valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]==-1)
	    //std::cerr<<"err> default value is not set correctly  in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	    
	    if(!(valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]<(1<<NUM_OF_BIT_FINE) )
	       ){
	      std::cerr<<"war> invalid value was detected "<<valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]
		       <<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iJtag:"<<iJtag<<" iPpAsic:"<<iPpAsic
		       <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	      
	      valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]=((1<<NUM_OF_BIT_FINE)-1);
	    }
	    else if(!(valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]>=0)
	       ){
	      std::cerr<<"war> invalid value was detected "<<valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]
		       <<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iJtag:"<<iJtag<<" iPpAsic:"<<iPpAsic
		       <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	      
	      valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]=0;
	    }	    

	    intToBin((valueOfTpgFineDelay[iSsw][iPort][iJtag][iPpAsic][iLine]),
		     NUM_OF_BIT_FINE, 
		     value_char,
		     sizeof(value_char));
	    outfile<<value_char;
	  }
	  outfile<<std::endl;
	  
	  // TPG COARSE
	  outfile<<address<<" Pps"<<iJtag<<"/TpgCoarse"<<lineName<<" ";
	  
	  for(int iPpAsic=0; iPpAsic<numOfPpAsic.at(iJtag); iPpAsic++){
	    char value_char[BUFSIZ];
	    if(valueOfTpgCoarseDelay[iSsw][iPort][iJtag][iPpAsic][iLine]==-1)
	      std::cerr<<"err> default value is not set correctly in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	    
	    if(!(valueOfTpgCoarseDelay[iSsw][iPort][iJtag][iPpAsic][iLine]<(1<<NUM_OF_BIT_COARSE) &&
		 valueOfTpgCoarseDelay[iSsw][iPort][iJtag][iPpAsic][iLine]>=0)
	       ){
	      std::cerr<<"err> invalid value was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	      continue;
	    }
	    
	    intToBin((valueOfTpgCoarseDelay[iSsw][iPort][iJtag][iPpAsic][iLine]),
		     NUM_OF_BIT_COARSE, 
		     value_char,
		     sizeof(value_char));
	    outfile<<EDGE_SELECTION<<value_char;
	  }
	  outfile<<std::endl;
	}
      }
    }
  }
  return true;
}

bool TgcPpsTpgDelay_Generator::intToBin(const int& num_, 
					const int& max_, 
					char* bin, 
					const size_t& sizeOfBin){
  
  std::vector<int> bin_vector;
  bin_vector.clear();
  std::string bin_string("");
  
  int num=num_;
  
  for(int ii=0; ii<max_; ii++){
    if(num%2) bin_vector.push_back(1);
    else  bin_vector.push_back(0);
    num=num>>1;
  }
  
  for(int ii=0; ii<bin_vector.size(); ii++){
    char bit[BUFSIZ];
    snprintf(bit,sizeof(bit),"%1d",bin_vector.at(max_-1-ii));
    bin_string+=bit;
  }
  
  if (snprintf(bin, sizeOfBin, "%s", bin_string.c_str())<0) {
    fprintf(stderr, "err> %s:%d failed in snprintf \n", 
	    __FILE__,
	    __LINE__);
    return false;
  }
  
  return true;
}
