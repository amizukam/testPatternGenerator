#include "TgcParameterGenerator/TgcSswEdgeSelect_Generator.hh"

int TgcSswEdgeSelect_Generator::DEFAULT_VALUE=7;

int TgcSswEdgeSelect_Generator::NUM_OF_CLMN_OF_LIST_FILE=4;
TgcSswEdgeSelect_Generator::TgcSswEdgeSelect_Generator() : useDefaultConfigurationFile(true), 
					       nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswEdgeSelect_Generator::TgcSswEdgeSelect_Generator(char* fileName) : useDefaultConfigurationFile(false), 
							     nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswEdgeSelect_Generator::~TgcSswEdgeSelect_Generator(){}

bool TgcSswEdgeSelect_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1]; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){
      bool isValid=false;
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> m_tgcSlbSsw->checkRxIdValidity("<<cciId<<", "<<iSsw<<", "<<iSlb<<", isValid) in "<<__FILE__<<" at "<<__LINE__<<std::endl; 
      
      m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, isValid);
      portValidity[iSsw][iSlb]=isValid;
      
    }
  }
  
  for(int iBC=0; iBC<TGC_HW_CONSTANT::NUM_OF_INDEXBC; iBC++){
    BC_validity[iBC]=true;
  }
  
  valueOfEdgeSelect =DEFAULT_VALUE;
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
  }  
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  return true;
}

bool TgcSswEdgeSelect_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Ssw/Edge");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcSswEdgeSelect_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);
  
  if(logLevel<TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> ssw loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
      iSsw++)
    {
      
      char address[20];
      
      sprintf(address, "0x0%x8%x0000", cciId, iSsw);
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> EdgeSelect parameter file generating starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      

      // EdgeSelect
      char value[10];
      sprintf(value, "0x%x", valueOfEdgeSelect);
      
      if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	std::cout<<address<<" "
		 <<"Tx/Edge "
		 <<value<<std::endl;
      
      outfile<<address<<" "
	     <<"Tx/Edge "
	     <<value<<std::endl;
      
    }
  return true;
}


bool TgcSswEdgeSelect_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string buf("");
  int sharpPoint;
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }

  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if (entry[0]=="EDGE_SELECT") {
      const int EDGE_GL=atoi(entry[1].c_str());
      const int EDGE_BUS0=atoi(entry[2].c_str());
      const int EDGE_BUS1=atoi(entry[3].c_str());
      
      if (EDGE_GL<2 && 
	  EDGE_BUS0 < 2 &&
	  EDGE_BUS1 < 2) {
	valueOfEdgeSelect=(EDGE_GL<<2)+(EDGE_BUS0<<1)+(EDGE_BUS1<<0);
      }
      else {
	std::cerr<< "invalid input in "<<__FILE__<<":"<<__LINE__<<std::endl;
      }
    }
  }
  return true;
}
