#ifndef TGCPPSSLB_HH
#define TGCPPSSLB_HH

#include "TgcConstId.hh"
#include "TGCcabling12/TGCmappingSvc.h"
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <vector>


class TgcPpsSlb{
  
public :
  TgcPpsSlb();
  ~TgcPpsSlb();

  bool getPPJTAGfromOnlineId(const int& subSystemId,
			     const int& rodId,
			     const int& sswId,
			     const int& rxId,
			     const int& bitPos,
			     std::vector<int>& port,
			     std::vector<int>& ppsId,
			     std::vector<std::string>& aOrB,
			     std::vector<int>& asicOrder,
			     std::vector<int>& bitOrder,
			     const bool& orChannel
			     );
  
  bool getCciIdfromOnlineId(const int& rodId,
			    const int& sswId_online,
			    int& cciId,
			    int& sswId);
			    
			    
			    
private :
  static char* PP_SLB_ID_CONVERSION_DB;
  static int NUM_OF_CLMN_PP_SLB_ID_CONVERSION_DB;
  static int ENTRY_RXID;
  static int ENTRY_SLB1;
  static int ENTRY_SLB2;
  static int ENTRY_PORT;
  static int ENTRY_PPS ;
  static int ENTRY_AORB;
  static int ENTRY_ORDER;
  static int ENTRY_ASD1;
  static int ENTRY_ASD2;
  static int ENTRY_NAME;

  
};

#endif
