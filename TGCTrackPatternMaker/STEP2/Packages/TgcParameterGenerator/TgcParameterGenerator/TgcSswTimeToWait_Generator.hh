#ifndef TGC_SSWTIMETOWAIT_GENERATOR_HH
#define TGC_SSWTIMETOWAIT_GENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include "TGCcabling12/TGCmappingSvc.h"

#include <string>
#include <vector>
#include <fstream>

class TgcSswTimeToWait_Generator : public TgcParameterGeneratorBase{
public:
  TgcSswTimeToWait_Generator();
  TgcSswTimeToWait_Generator(char* fileName);
  ~TgcSswTimeToWait_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="TimeToWait.prm");
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int DEFAULT_VALUE_TIMETOWAIT;
  static int DEFAULT_VALUE_TIMETOWAIT2;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;
  

  bool readList(std::string fileNameList);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int isCside;
  int typeOfCci;
  char* sideName;
  bool BC_validity[TGC_HW_CONSTANT::NUM_OF_INDEXBC];
  
  bool portValidity[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1][TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW];
  // For SSW for EIFI to run from 1 to 12 (not from 0 to 11)
  
  int logLevel;
  
  int valueOfTimeToWait;
  int valueOfTimeToWait2;
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
};



#endif
