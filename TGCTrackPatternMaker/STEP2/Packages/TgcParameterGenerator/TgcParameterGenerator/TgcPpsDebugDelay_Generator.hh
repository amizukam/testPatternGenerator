#ifndef TGC_PPSTPGDEBUGDELAY_HH
#define TGC_PPSTPGDEBUGDELAY_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcPpsSsw.hh"
#include "TgcPpsSlb.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include <string>
#include <vector>
#include <fstream>

class TgcPpsDebugDelay_Generator : public TgcParameterGeneratorBase{
public:
  TgcPpsDebugDelay_Generator();
  TgcPpsDebugDelay_Generator(char* fileName);
  ~TgcPpsDebugDelay_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="PpsDebugDelay.prm");
  
private:
  int logLevel;
  
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int NUM_OF_BIT;
  static int DEFAULT_VALUE;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool intToBin(const int& num, 
		const int& max, 
		char* bin,
		const size_t& sizeOfBin);
  
  TgcFileListHandler* m_tgcFileListHandler;
  TgcPpsSsw* m_tgcPpsSsw;
  TgcPpsSlb* m_tgcPpsSlb;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  int valueOfPpsDebugDelay;
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
};

#endif
