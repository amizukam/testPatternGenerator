#ifndef TGC_SLBDEPTHGENERATOR_HH
#define TGC_SLBDEPTHGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"
#include "MsgLevel.hh"

#include <string>
#include <vector>
#include <fstream>

class TgcSlbDepth_Generator : public TgcParameterGeneratorBase{
public:
  TgcSlbDepth_Generator();
  TgcSlbDepth_Generator(char* fileName);
  ~TgcSlbDepth_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="SlbDepth.prm");
  bool intToBin(const int& num, 
		const int& max, 
		char* bin,
		const size_t& sizeOfBin);
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int NUM_OF_BIT;
  static int VALUE_OF_DEPTH_FOR_BCID;
  static int DEFAULT_VALUE;
  
  int logLevel;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool setBit(std::string entry[], int bitValue);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  int valueOfSlbDepth[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1];
  
  //for SL
  // JtagId(Slb0 or1) = 0, 1 
  // portID(mouthID) = 0, 1, 2
  // 0xXX0000 Slb0/Depth = EC0(phi0)
  // 0xXX0000 Slb1/Depth = EC1(phi1)
  // 0xXX0001 Slb0/Depth = EC2(phi2)
  // 0xXX0001 Slb1/Depth = EC3(phi3)
  // 0xXX0002 Slb0/Depth = FD0(F0)
  // 0xXX0002 Slb1/Depth = FD1(F1)
  bool isSL;
  //[ssw][portID][JtagId]
  int valueOfSlbDepthForSL[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1][3][2];
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
  //for SL
  inline unsigned int GetPortId( std::string phi, std::string EF );
  inline unsigned int GetJtagId( std::string phi, std::string EF );

};
//GetPortID(entry[4],entry[5])][GetJtagId(entry[4],entry[5]
inline unsigned int TgcSlbDepth_Generator::GetPortId( std::string phi, std::string EF )
{
  unsigned int portId = 0;
  if( EF == "E" ){
    if( phi=="0" )      portId = 0;
    else if( phi=="1" ) portId = 0;
    else if( phi=="2" ) portId = 1;
    else if( phi=="3" ) portId = 1;
  }else{
    if( phi=="0" )      portId = 2;
    else if( phi=="2" ) portId = 2;
  }
  return portId;
}
inline unsigned int TgcSlbDepth_Generator::GetJtagId( std::string phi, std::string EF )
{
  unsigned int JtagId = 0;
  if( EF == "E" ){
    if( phi=="0" )      JtagId = 0;
    else if( phi=="1" ) JtagId = 1;
    else if( phi=="2" ) JtagId = 0;
    else if( phi=="3" ) JtagId = 1;
  }else{
    if( phi=="0" )      JtagId = 0;
    else if( phi=="2" ) JtagId = 1;
  }
  return JtagId;
}

#endif
