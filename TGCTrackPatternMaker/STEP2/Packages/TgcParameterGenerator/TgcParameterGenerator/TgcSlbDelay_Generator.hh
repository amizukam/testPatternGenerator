#ifndef TGC_SLBDELAYGENERATOR_HH
#define TGC_SLBDELAYGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"
#include "MsgLevel.hh"

#include <string>
#include <vector>
#include <fstream>
#include <cstdlib>


class TgcSlbDelay_Generator : public TgcParameterGeneratorBase{
public:
  TgcSlbDelay_Generator();
  TgcSlbDelay_Generator(char* fileName);
  ~TgcSlbDelay_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="SlbDelay.prm");
  bool intToBin(const int& num, 
		const int& max, 
		char* bin,
		const size_t& sizeOfBin);
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int NUM_OF_BIT;
  static int VALUE_OF_DEPTH_FOR_BCID;
  static int DEFAULT_VALUE;
  static char* NAME_OF_DEFAULT_FILE;
  static int NUM_OF_CLMN_OF_DEFAULT_FILE;
  
  int logLevel;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool setBit(std::string entry[], int bitValue);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  int valueOfSlbDelay[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1]
  [TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW][TGC_HW_CONSTANT::NUM_OF_SLB_INPUT_LAYER];
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
  
};

#endif
