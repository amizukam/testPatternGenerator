#ifndef TGCPPSSSW_HH
#define TGCPPSSSW_HH

#include <vector>
#include "TgcConstId.hh"


class TgcPpsSsw{
public :
  TgcPpsSsw();
  ~TgcPpsSsw();
  
  //bool getNumberOfPPAsicInJtagLine(int sswId, int rxId, int& numOfPPAsic[NUM_OF_PP_JTAG]);
  bool getNumberOfPPAsicInJtagLine(int cciId, int sswId, int rxId, std::vector<int>& numOfPPAsic);
  bool getWireOrStripInJtagLine(int cciId, int sswId, int rxId, std::vector<int>& wireOrStrip);
  bool getSswIdFromCciId(int cciId, int& startId, int& numOfSsw);
  
private :
  static char* FILE_NAME_PP_VALIDITY_DB;
  static int NUM_OF_CLMN_PP_VALIDITY_DB;
  
};

#endif
