#ifndef TGC_SLBTPPGENERATOR_HH
#define TGC_SLBTPPGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"
#include "MsgLevel.hh"
#include "TGCcabling12/TGCmappingSvc.h"

#include <string>
#include <vector>
#include <fstream>

class TgcSlbTpp_Generator : public TgcParameterGeneratorBase{
public:
  TgcSlbTpp_Generator();
  TgcSlbTpp_Generator(char* fileName);
  ~TgcSlbTpp_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="SlbTpp.prm");
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int DEFAULT_VALUE;
  
  int logLevel;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;
  
  bool forAllBitsTpp;

  bool readList(std::string fileNameList);
  bool setBit(std::string entry[], int bitValue);
  
  TGCmappingSvc* m_tgcMappingSvc;
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  std::vector<int> channel[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1][TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW]; // For SSW for EIFI to run from 1 to 12 (not from 0 to 11)

  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}

  
};

#endif
