#ifndef TGC_SSWSENDSYNC_GENERATOR_HH
#define TGC_SSWSENDSYNC_GENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include "TGCcabling12/TGCmappingSvc.h"

#include <string>
#include <vector>
#include <fstream>

class TgcSswSendSync_Generator : public TgcParameterGeneratorBase{
public:
  TgcSswSendSync_Generator();
  TgcSswSendSync_Generator(char* fileName);
  ~TgcSswSendSync_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="SswSendSync.prm");
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;
  
  bool readList(std::string fileNameList);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int isCside;
  int typeOfCci;
  char* sideName;
  
  bool portValidity[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1][TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW];
  // For SSW for EIFI to run from 1 to 12 (not from 0 to 11)
  
  int logLevel;
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
};



#endif
