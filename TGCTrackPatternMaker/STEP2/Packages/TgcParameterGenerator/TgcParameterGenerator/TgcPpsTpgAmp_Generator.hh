#ifndef TGC_PPSTPGAMPGENERATOR_HH
#define TGC_PPSTPGAMPGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcPpsSsw.hh"
#include "TgcPpsSlb.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include <string>
#include <vector>
#include <fstream>

class TgcPpsTpgAmp_Generator : public TgcParameterGeneratorBase{
public:
  TgcPpsTpgAmp_Generator();
  TgcPpsTpgAmp_Generator(char* fileName);
  ~TgcPpsTpgAmp_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="PpsTpgAmp.prm");
  
private:
  int logLevel;
  
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int NUM_OF_BIT;
  static int DEFAULT_VALUE;

  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool intToBin(const int& num, 
		const int& max, 
		char* bin,
		const size_t& sizeOfBin);
  
  TgcFileListHandler* m_tgcFileListHandler;
  TgcPpsSsw* m_tgcPpsSsw;
  TgcPpsSlb* m_tgcPpsSlb;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  int valueOfPpsTpgAmp;
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
};

#endif
