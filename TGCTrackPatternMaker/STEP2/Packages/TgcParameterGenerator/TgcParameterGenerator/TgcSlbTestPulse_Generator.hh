#ifndef TGC_SLBTESTPULSEGENERATOR_HH
#define TGC_SLBTESTPULSEGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"
#include "MsgLevel.hh"

#include <string>
#include <vector>
#include <fstream>

class TgcSlbTestPulse_Generator : public TgcParameterGeneratorBase{
public:
  TgcSlbTestPulse_Generator();
  TgcSlbTestPulse_Generator(char* fileName);
  ~TgcSlbTestPulse_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="SlbTestPulse.prm");
  bool intToBin(const int& num, 
		const int& max, 
		char* bin,
		const size_t& sizeOfBin);
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int NUM_OF_BIT_DELAY;
  static int NUM_OF_BIT_TESTPULSE;
  static int DEFAULT_VALUE_DELAY_BW;
  static int DEFAULT_VALUE_DELAY_SL;
  static int DEFAULT_VALUE_DELAY_EIFI;

  static int DEFAULT_VALUE_TESTPULSE;
  
  int logLevel;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool setBit(std::string entry[], int bitValue);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  int valueOfSlbTestPulseDelay;
  int valueOfSlbTestPulse;
  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
  
};

#endif
