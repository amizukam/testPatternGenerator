#ifndef __TGCCONSTID__HH
#define __TGCCONSTID__HH

#include <vector>
#include <string>

namespace TGC_HW_CONSTANT{
  
  // HARDWARE INFORMATION
  const int NUM_OF_SIDE=2;
  const int NUM_OF_SIGNAL=2; // wire or strip
  const int NUM_OF_REGION=2; //endcap or forward
  const int NUM_OF_DOUBLET_OR_TRIPLET=2; //doublet or triplet
  const int NUM_OF_BIT_IN_SLB=160;
  const int NUM_OF_SLB_IN_SSW=23;
  
  const int NUM_OF_SLB_INPUT_LAYER=4;
  
  //const int NUM_OF_SSW_IN_CCI=12;
  const int NUM_OF_INPUT_IN_SSW=10;
  const int NUM_OF_PPJTAG_IN_PORT=4;
  const int NUM_OF_PPASIC_IN_PPJTAG=3;
  const int NUM_OF_BIT_IN_PP=16;
  const int NUM_OF_CCI=15;
  
  // WHEN TO USE THIS PARAMTERS, DON'T FORGET TO USE "cciId-1" 
  const int NUM_OF_SSW_IN_CCI[NUM_OF_CCI]={8,8,8,8,8,8,8,8,8,8,8,8,2,2,12};
  const int START_ID_OF_SSW  [NUM_OF_CCI]={0,0,0,0,0,0,0,0,0,0,0,0,0,2,1};
  
  
  const int NUM_OF_ROD =12;
  const int NUM_OF_TX_MASK =2;
  const int NUM_OF_PORT_IN_TX_MASK[NUM_OF_TX_MASK] = {12, 11};
  const int START_ID_OF_PORT_IN_TX_MASK[NUM_OF_TX_MASK] = {0, 12};
  
  const int NUM_OF_PORT_IN_ONE_SSW_MOUTH=2;
  
  const int NUM_OF_SSW_RX_FPGA=6;
  const int NUM_OF_PORT_IN_RX_FPGA[NUM_OF_SSW_RX_FPGA]={4,4,4,4,4,3};
  const int NUM_OF_PORT_FOR_ONE_SSW_MOUTH[NUM_OF_SSW_RX_FPGA][NUM_OF_PORT_IN_ONE_SSW_MOUTH]
  ={{2,2},{2,2},{2,2},{2,2},{3,1},{3,0}};
  
  const int START_ID_OF_PORT_IN_RX_FPGA[NUM_OF_SSW_RX_FPGA] = {0, 4, 8, 12, 16, 20};
  
  const int SSW_EDGE_SELECTION = 1;
  const int EIFI_LAYER_START=7;
  const int NUM_OF_A_OR_B=2;
  const int MAX_OF_SSW_IN_CCI=12;
  const int NUM_OF_SSW_IN_BW_SECTOR=8;
  
  
  // ONLINE IDs
  const int ID_A_SIDE=103;
  const int ID_C_SIDE=104;
  
  const int CCI_ID_EIFI_L = 13;
  const int CCI_ID_EIFI_H = 14;
  const int CCI_ID_SL     = 15;
  
  const int SSWID_EIFI=8;
  const int SSWID_SL  =9;
  const int SSWID_MAX =9;
  const int SSWID_MIN =0;
  
  enum CCI_TYPE {BW_CCI=0, EIFI_CCI, SL_CCI, NUM_OF_CCITYPE};
  enum SLB_TYPE {SLB_TYPE_TRIPLET_WIRE=0, SLB_TYPE_TRIPLET_STRIP, SLB_TYPE_DOUBLET_WIRE, SLB_TYPE_DOUBLET_STRIP, SLB_TYPE_EIFI, SLB_TYPE_SL, SLB_TYPE_NOT_USED};
  
  // OTHERS (USEFUL CONSTANT)
  const int BIT_H=1;
  const int BIT_L=0;
  
  
  const int NUM_OF_INDEXBC=3;
  
  
  const int PP_FINE_STEP_SIZE_FOR_25NS=30;
};
#endif
