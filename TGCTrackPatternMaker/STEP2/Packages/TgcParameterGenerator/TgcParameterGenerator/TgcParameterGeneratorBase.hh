#ifndef TGC_PARAMETERGENERATORBASE_HH
#define TGC_PARAMETERGENERATORBASE_HH

#include <string>
#include <vector>
#include "TgcConstId.hh"
#include "MsgLevel.hh"

class TgcParameterGeneratorBase{
  
public:
  TgcParameterGeneratorBase();
  TgcParameterGeneratorBase(char* fileName);
  ~TgcParameterGeneratorBase();
  
  virtual bool initialize(int tmp_cciId, bool tmp_isCside);
  virtual bool setData();
  virtual bool generate(char* output_fileName=(char*)"test.prm");
  virtual bool setLogLevel(int tmp_logLevel){};
  
private:
  bool logLevel;
  
};

#endif
