#ifndef TGCFILELISTHANDLER_HH
#define TGCFILELISTHANDLER_HH

#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

class TgcFileListHandler{
public :
  TgcFileListHandler();
  ~TgcFileListHandler();
  TgcFileListHandler(const char* fileName);
  
  bool getFileList(const std::string& triggerName, 
		   std::vector<std::string>& file_name_list);
  
private :  
  std::ifstream fileList;
  bool file_initialization();
  
  static int NUM_OF_CLMN_FILE_LIST;
  char* FILE_LIST_NAME;



};

#endif
