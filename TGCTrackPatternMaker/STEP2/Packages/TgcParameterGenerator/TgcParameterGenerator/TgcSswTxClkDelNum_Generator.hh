#ifndef TGC_SSWTXCLKDELNUM_GENERATOR_HH
#define TGC_SSWTXCLKDELNUM_GENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcSlbSsw.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include "TGCcabling12/TGCmappingSvc.h"

#include <string>
#include <vector>
#include <fstream>

class TgcSswTxClkDelNum_Generator : public TgcParameterGeneratorBase{
public:
  TgcSswTxClkDelNum_Generator();
  TgcSswTxClkDelNum_Generator(char* fileName);
  ~TgcSswTxClkDelNum_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="TxClkDelNum.prm");
  
private:
  static int NUM_OF_CLMN_OF_LIST_FILE;
  static int DEFAULT_VALUE;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;
  

  bool readList(std::string fileNameList);
  
  TgcSlbSsw* m_tgcSlbSsw;
  TgcFileListHandler* m_tgcFileListHandler;
  
  int cciId;
  int isCside;
  int typeOfCci;
  char* sideName;

  
  int logLevel;
  
  int valueOfTxClkDelNum;

  
public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
};



#endif
