#ifndef TGCSLBSSW_HH
#define TGCSLBSSW_HH

#include <stdlib.h>
#include <stdio.h>
#include "TgcConstId.hh"

class TgcSlbSsw{
public :
  TgcSlbSsw();
  ~TgcSlbSsw();
  
  bool getAddressFromRxId(const int& rxId,
			  int& mouthId, 
			  int& slbJtag);

  bool getSlbTypeFromRxId(const int& cciId, 
			  const int& sswId, 
			  const int& rxId, 
			  int& slbType);

  bool checkRxIdValidity(const int& cciId, 
			 const int& sswId, 
			 const int& rxId, 
			 bool& isValid);

  bool getSswIdFromCciId(const int& cciId, 
			 int& startId, 
			 int& numOfSsw);

  bool getCciIdfromOnlineId(const int& rodId, 
			    const int& sswId_online, 
			    int& cciId, 
			    int& sswId);
  
private :
  static char* FILE_NAME_SLB_ID_DB;
  static char* FILE_NAME_SLB_VALIDITY_DB;
  //static char FILE_NAME_SLB_ID_DB[BUFSIZ];
  //static char FILE_NAME_SLB_VALIDITY_DB[BUFSIZ];
  static int NUM_OF_CLMN_SLB_VALIDITY_DB;
  static int NUM_OF_CLMN_SLB_ID_DB;
  
};

#endif
