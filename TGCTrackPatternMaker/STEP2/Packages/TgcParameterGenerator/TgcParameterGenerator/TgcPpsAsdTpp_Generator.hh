#ifndef TGC_PPSASDTPPGENERATOR_HH
#define TGC_PPSASDTPPGENERATOR_HH

#include "TgcParameterGeneratorBase.hh"
#include "TgcPpsSsw.hh"
#include "TgcPpsSlb.hh"
#include "TgcFileListHandler.hh"
#include "TgcConstId.hh"

#include "TGCcabling12/TGCmappingSvc.h"

#include <string>
#include <vector>
#include <fstream>

class TgcPpsAsdTpp_Generator : public TgcParameterGeneratorBase{
public:
  TgcPpsAsdTpp_Generator();
  TgcPpsAsdTpp_Generator(char* fileName);
  ~TgcPpsAsdTpp_Generator();
  bool initialize(int tmp_cciId, bool tmp_isCside);
  bool setData();
  bool generate(char* output_fileName="PpsAsdTpp.prm");
  
private:
  int logLevel;
  
  static int NUM_OF_CLMN_OF_LIST_FILE;
  
  const bool useDefaultConfigurationFile;
  const char* nameOfConfigurationFile;

  bool readList(std::string fileNameList);
  bool setBit(std::string entry[]);
  
  TGCmappingSvc* m_tgcMappingSvc;
  TgcFileListHandler* m_tgcFileListHandler;
  TgcPpsSsw* m_tgcPpsSsw;
  TgcPpsSlb* m_tgcPpsSlb;
  
  int cciId;
  int sectorId;
  int isCside;
  int typeOfCci;
  char* sideName;
  int subDetectorId;
  
  std::vector<int> channel[TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1][TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW][TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT][TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG][TGC_HW_CONSTANT::NUM_OF_A_OR_B];  // For SSW for EIFI to run from 1 to 12 (not from 0 to 11)

public:
  bool setLogLevel(int tmp_logLevel){logLevel=tmp_logLevel; return true;}
  
  
};

#endif
