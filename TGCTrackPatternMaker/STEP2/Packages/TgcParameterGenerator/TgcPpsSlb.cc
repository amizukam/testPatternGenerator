#include <iostream>
#include <cstdlib>
#include <string.h>

#include "TgcParameterGenerator/TgcPpsSlb.hh"

std::string s_pp_slb_id_conversion_db_dir = std::getenv("PACKAGES_DIR");
std::string s_pp_slb_id_conversion_db_file = "/DB/PP_SLB_ID_CONVERSION_DB.txt";
std::string s_pp_slb_id_conversion_db = s_pp_slb_id_conversion_db_dir + s_pp_slb_id_conversion_db_file;
const char* c_pp_slb_id_conversion_db = s_pp_slb_id_conversion_db.c_str();
char* TgcPpsSlb::PP_SLB_ID_CONVERSION_DB=strdup(c_pp_slb_id_conversion_db);

int TgcPpsSlb::NUM_OF_CLMN_PP_SLB_ID_CONVERSION_DB=10;
int TgcPpsSlb::ENTRY_RXID =0;
int TgcPpsSlb::ENTRY_SLB1 =1;
int TgcPpsSlb::ENTRY_SLB2 =2;
int TgcPpsSlb::ENTRY_PORT =3;
int TgcPpsSlb::ENTRY_PPS  =4;
int TgcPpsSlb::ENTRY_AORB =5;
int TgcPpsSlb::ENTRY_ORDER=6;
int TgcPpsSlb::ENTRY_ASD1 =7;
int TgcPpsSlb::ENTRY_ASD2 =8;
int TgcPpsSlb::ENTRY_NAME =9;



TgcPpsSlb::TgcPpsSlb(){
}

TgcPpsSlb::~TgcPpsSlb(){
}

bool TgcPpsSlb::getPPJTAGfromOnlineId(const int& subSystemId,
				      const int& rodId,
				      const int& sswId_online,
				      const int& rxId,
				      const int& bitPos,
				      std::vector<int>& port,
				      std::vector<int>& ppsId,
				      std::vector<std::string>& aOrB,
				      std::vector<int>& asicOrder,
				      std::vector<int>& bitOrder,
				      const bool& orChannel
				      ){
  
  int cciId;
  int sswId;
  
  getCciIdfromOnlineId(rodId,
		       sswId_online,
		       cciId,
		       sswId);

  std::ifstream f(PP_SLB_ID_CONVERSION_DB);
  
  std::string buf;
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_PP_SLB_ID_CONVERSION_DB];
  
  int tmp_sswId[2]={0,0};
  int tmp_cciId[2]={0,0};
  
  
  while(getline(f,buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    
    int iLine=0;
    if(buf=="") continue;
    
    std::stringstream line(buf);
    
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_PP_SLB_ID_CONVERSION_DB)) break;
    }    
    
    if(iLine==0) continue;
    
    
    if(entry[0]=="NUM_OF_CCI"){
      tmp_cciId[1]=tmp_cciId[0];
      tmp_cciId[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(!(cciId<=tmp_cciId[0] &&
	 cciId> tmp_cciId[1])) continue;

    
    if(entry[0]=="NUM_OF_SSW"){
      tmp_sswId[1]=tmp_sswId[0];
      tmp_sswId[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(atoi(entry[ENTRY_RXID].c_str())==0 && entry[ENTRY_RXID]!="0") continue;
    
    if(!(rxId==atoi(entry[ENTRY_RXID].c_str()) &&
	 sswId< tmp_sswId[0] &&
	 sswId>=tmp_sswId[1] &&
	 bitPos>=atoi(entry[ENTRY_SLB1].c_str()) &&
	 bitPos<=atoi(entry[ENTRY_SLB2].c_str()))) {
      
      continue;
    }
    port.push_back(atoi(entry[ENTRY_PORT].c_str()));
    ppsId.push_back(atoi(entry[ENTRY_PPS].c_str()));
    aOrB.push_back(entry[ENTRY_AORB].c_str());
    asicOrder.push_back(atoi(entry[ENTRY_ORDER].c_str()));
    bitOrder.push_back((atoi(entry[ENTRY_ASD1].c_str())-
			(bitPos-atoi(entry[ENTRY_SLB1].c_str()))));
    
    if(!orChannel) break; // only One candidate will be applied
  }
  
  f.close();
  return true;
}

bool TgcPpsSlb::getCciIdfromOnlineId(const int& rodId,
				     const int& sswId_online,
				     int& cciId,
				     int& sswId){
  
  if(!(rodId>0 && rodId<=TGC_HW_CONSTANT::NUM_OF_ROD)){
    std::cerr<<"err> invalid rod ID in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId_online>=TGC_HW_CONSTANT::SSWID_MIN && sswId_online<=TGC_HW_CONSTANT::SSWID_MAX)){
    std::cerr<<"err> invalid ssw ID in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }  
  
  if(sswId_online == TGC_HW_CONSTANT::SSWID_EIFI){
    switch(rodId){
    case 2:
      cciId=13;
      sswId=0;
      break;
    case 5:
      cciId=13;
      sswId=1;
      break;
    case 8: 
      cciId=14;
      sswId=2;
      break;
    case 11:
      cciId=14;
      sswId=3;
      break;
    default: 
      std::cerr<<"err> invalid rodId in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    return true;
  }
  
  if(sswId_online == TGC_HW_CONSTANT::SSWID_SL ){
    sswId = rodId;
    cciId = 15;
    return true;
  }
  
  sswId = sswId_online;
  cciId = rodId;
    
  return true;
  
}
