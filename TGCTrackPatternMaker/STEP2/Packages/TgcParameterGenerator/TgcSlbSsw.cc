#include "iostream"
#include "fstream"
#include "sstream"
#include <stdlib.h>
#include <cstdlib>
#include <string.h>

#include "TgcParameterGenerator/TgcSlbSsw.hh"

std::string s_slb_id_db_dir = std::getenv("PACKAGES_DIR");
std::string s_slb_id_db_file = "/DB/SLB_ID_DB.txt";
std::string s_slb_id_db = s_slb_id_db_dir + s_slb_id_db_file;
const char* c_slb_id_db = s_slb_id_db.c_str();
char* TgcSlbSsw::FILE_NAME_SLB_ID_DB=strdup(c_slb_id_db);

std::string s_slb_validity_db_dir = std::getenv("PACKAGES_DIR");
std::string s_slb_validity_db_file = "/DB/SSW_VALID_PORT_DB.txt";
std::string s_slb_validity_db = s_slb_validity_db_dir + s_slb_validity_db_file;
const char* c_slb_validity_db = s_slb_validity_db.c_str();
char* TgcSlbSsw::FILE_NAME_SLB_VALIDITY_DB=strdup(c_slb_validity_db);

int TgcSlbSsw::NUM_OF_CLMN_SLB_VALIDITY_DB=4;
int TgcSlbSsw::NUM_OF_CLMN_SLB_ID_DB=3;


TgcSlbSsw::TgcSlbSsw()
{
}


TgcSlbSsw::~TgcSlbSsw(){
}

bool TgcSlbSsw::getAddressFromRxId(const int& rxId, 
				   int& mouthId, int& 
				   slbJtag){
  std::ifstream f(FILE_NAME_SLB_ID_DB);
  if(!f.is_open()){
    std::cerr<<"err> DB file dose not exist in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf;
  int sharpPoint;
  std::string entry[NUM_OF_CLMN_SLB_ID_DB];
  while(getline(f,buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


    std::stringstream line(buf);

    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_SLB_ID_DB)) break;
    }    
    if(iLine==0) continue;

    if((atoi(entry[0].c_str())==0)&&(entry[0]!="0")) continue;
    
    if(!(atoi(entry[0].c_str())==rxId)) continue;

    mouthId=atoi(entry[1].c_str());
    slbJtag=atoi(entry[2].c_str());
    
    return true;
  }
  
  return false;
}

bool TgcSlbSsw::checkRxIdValidity(const int& cciId, 
				  const int& sswId, 
				  const int& rxId, 
				  bool& isValid){
  
  int startId;
  int numOfSsw;
  
  isValid=false;

  if(!(getSswIdFromCciId(cciId, startId, numOfSsw))){
    std::cerr<<"err> an error was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId>=startId && sswId<(startId+numOfSsw))){ 
    isValid=false;
    return true;
  }
  
  
  std::ifstream f(FILE_NAME_SLB_VALIDITY_DB);
  if(!f.is_open()){
    std::cerr<<"err> DB file dose not exist in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  int cciIdReference[2]={0,0};
  
  std::string buf;
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_SLB_VALIDITY_DB];

  while(getline(f,buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }

    std::stringstream line(buf);
    
    if(buf=="") continue;
    
    for(int ii=0; ii<NUM_OF_CLMN_SLB_VALIDITY_DB; ii++){
      line>>entry[ii];
    }
    
    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(!((atoi(entry[0].c_str())==sswId)&&
	 (atoi(entry[1].c_str())==rxId) &&
	 (cciId<=cciIdReference[0]) &&
	 (cciId> cciIdReference[1]))) continue;
    
    isValid=(atoi(entry[2].c_str())!=0);
    
    return true;
  }
  
  return false;
  
}

bool TgcSlbSsw::getSswIdFromCciId(const int& cciId, 
				  int& startId, 
				  int& numOfSsw){
  if(cciId<1 && cciId>TGC_HW_CONSTANT::NUM_OF_CCI){
    std::cerr<<"err> invalid cciId detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  startId=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  numOfSsw=TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  return true;
}

bool TgcSlbSsw::getCciIdfromOnlineId(const int& rodId,
				     const int& sswId_online,
				     int& cciId,
				     int& sswId){

  if(!(rodId>0 && rodId<=TGC_HW_CONSTANT::NUM_OF_ROD)){
    std::cerr<<"err> invalid rod ID in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId_online>=TGC_HW_CONSTANT::SSWID_MIN && sswId_online<=TGC_HW_CONSTANT::SSWID_MAX)){
    std::cerr<<"err> invalid ssw ID in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }  
  
  if(sswId_online == TGC_HW_CONSTANT::SSWID_EIFI){
    switch(rodId){
    case 2:
      cciId=13;
      sswId=0;
      break;
    case 5:
      cciId=13;
      sswId=1;
      break;
    case 8: 
      cciId=14;
      sswId=2;
      break;
    case 11:
      cciId=14;
      sswId=3;
      break;
    default: 
      std::cerr<<"err> invalid rodId in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    return true;
  }
  
  if(sswId_online == TGC_HW_CONSTANT::SSWID_SL ){
    sswId = rodId;
    cciId = 15;
    return true;
  }
  
  sswId = sswId_online;
  cciId = rodId;
    
  return true;
  
}

bool TgcSlbSsw::getSlbTypeFromRxId(const int& cciId, 
				   const int& sswId, 
				   const int& rxId, 
				   int& slbType){
  
  int startId;
  int numOfSsw;
  
  slbType=TGC_HW_CONSTANT::SLB_TYPE_NOT_USED;

  if(!(getSswIdFromCciId(cciId, startId, numOfSsw))){
    std::cerr<<"err> an error was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId>=startId && sswId<(startId+numOfSsw))){ 
    slbType=TGC_HW_CONSTANT::SLB_TYPE_NOT_USED;
    return true;
  }
  
  
  std::ifstream f(FILE_NAME_SLB_VALIDITY_DB);
  if(!f.is_open()){
    std::cerr<<"err> DB file dose not exist in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  int cciIdReference[2]={0,0};
  
  std::string buf;
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_SLB_VALIDITY_DB];

  while(getline(f,buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


    std::stringstream line(buf);
    
    if(buf=="") continue;
    
    for(int ii=0; ii<NUM_OF_CLMN_SLB_VALIDITY_DB; ii++){
      line>>entry[ii];
    }
    
    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(!((atoi(entry[0].c_str())==sswId)&&
	 (atoi(entry[1].c_str())==rxId) &&
	 (cciId<=cciIdReference[0])&&
	 (cciId> cciIdReference[1]))) continue;
    
    slbType=atoi(entry[3].c_str());
    
    return true;
  }
  
  return false;
  
}
