#include "TgcParameterGenerator/TgcParameterGeneratorBase.hh"

TgcParameterGeneratorBase::TgcParameterGeneratorBase(){logLevel=TGC_MSG_LEVEL::FATAL;}
TgcParameterGeneratorBase::TgcParameterGeneratorBase(char* fileName){logLevel=TGC_MSG_LEVEL::FATAL;}

TgcParameterGeneratorBase::~TgcParameterGeneratorBase(){}

bool TgcParameterGeneratorBase::initialize(int tmp_cciId, bool tmp_isCside){ return true;}
bool TgcParameterGeneratorBase::setData(){return true;}
bool TgcParameterGeneratorBase::generate(char* output_fileName){return true;}

