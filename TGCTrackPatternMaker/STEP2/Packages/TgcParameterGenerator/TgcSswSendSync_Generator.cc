#include "TgcParameterGenerator/TgcSswSendSync_Generator.hh"

int TgcSswSendSync_Generator::NUM_OF_CLMN_OF_LIST_FILE=4;

TgcSswSendSync_Generator::TgcSswSendSync_Generator() : useDefaultConfigurationFile(true), 
					       nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswSendSync_Generator::TgcSswSendSync_Generator(char* fileName) : useDefaultConfigurationFile(false), 
							     nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcSswSendSync_Generator::~TgcSswSendSync_Generator(){}


bool TgcSswSendSync_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1]; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){
      bool isValid=false;
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> m_tgcSlbSsw->checkRxIdValidity("<<cciId<<", "<<iSsw<<", "<<iSlb<<", isValid) in "<<__FILE__<<" at "<<__LINE__<<std::endl; 
      
      m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, isValid);
      portValidity[iSsw][iSlb]=isValid;

    }
  }
  
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
   }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
  }  
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  return true;
}

bool TgcSswSendSync_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Ssw/Mask");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcSswSendSync_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);
  
  if(logLevel<TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> ssw loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
      iSsw++)
    {
      
      char address[20];
      
      sprintf(address, "0x0%x8%x0000", cciId, iSsw);
      
      if(logLevel<TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> rx fpga loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      
      // RxSned
      for(int iRx=0; iRx<TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA; iRx++)
	{
	  if(logLevel<=TGC_MSG_LEVEL::INFO)
	    std::cout<<"inf> port loop starts in for iRx "<<iRx<<" "<<__FILE__<<" at "<<__LINE__<<std::endl;
	  
	  bool isUsed1=false;
	  bool isUsed2=false;

	  for(int iPort=0;
	      iPort<(TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]+TGC_HW_CONSTANT::NUM_OF_PORT_IN_RX_FPGA[iRx]-TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]);iPort++)
	    {
	      if(logLevel<=TGC_MSG_LEVEL::INFO){
		std::cout<<"inf> port loop starts in for iRx "<<portValidity[iSsw][iPort+TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]]<<" "<<__FILE__<<" at "<<__LINE__<<std::endl
			 <<"     iPort "<<iPort<<":"<<TGC_HW_CONSTANT::NUM_OF_PORT_FOR_ONE_SSW_MOUTH[iRx][0]<<" "<<TGC_HW_CONSTANT::NUM_OF_PORT_FOR_ONE_SSW_MOUTH[iRx][1]<<std::endl;
	      }
	      
	      if(iPort<TGC_HW_CONSTANT::NUM_OF_PORT_FOR_ONE_SSW_MOUTH[iRx][0]){
		isUsed1=portValidity[iSsw][iPort+TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]]|isUsed1;
		std::cout<<"inf> isUsed1 : "<<(portValidity[iSsw][iPort+TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]]|isUsed1)<<" "<<__FILE__<<" at "<<__LINE__<<std::endl;	
	      }
	      else if(iPort<
		      TGC_HW_CONSTANT::NUM_OF_PORT_FOR_ONE_SSW_MOUTH[iRx][1]+
		      TGC_HW_CONSTANT::NUM_OF_PORT_FOR_ONE_SSW_MOUTH[iRx][0]){
		if(iRx==4 || iRx==5) break;
		isUsed2=portValidity[iSsw][iPort+TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]]|isUsed2;
		std::cout<<"inf> isUsed2 : "<<(portValidity[iSsw][iPort+TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]]|isUsed2)<<" "<<__FILE__<<" at "<<__LINE__<<std::endl;
	      }
	      else{
		std::cout<<"inf> break the loop "<<__FILE__<<" at "<<__LINE__<<std::endl;
		break;
	      }
	    }
	  
	  
	  if(isUsed1){
	    if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	      std::cout<<address<<" "
		       <<"Rx/RxSendSync1Rx"<<iRx<<" 0x0"<<std::endl;
	    outfile<<address<<" "
		   <<"Rx/RxSendSync1Rx"<<iRx<<" 0x0"<<std::endl;
	  }
	  else{
	    if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	      std::cout<<address<<" "
		       <<"Rx/RxSendSync1Rx"<<iRx<<" -"<<std::endl;
	    outfile<<address<<" "
		   <<"Rx/RxSendSync1Rx"<<iRx<<" -"<<std::endl;
	  }
	  

	  if(isUsed2){
	    if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	      std::cout<<address<<" "
		       <<"Rx/RxSendSync2Rx"<<iRx<<" 0x0"<<std::endl;
	    outfile<<address<<" "
		   <<"Rx/RxSendSync2Rx"<<iRx<<" 0x0"<<std::endl;
	    
	  }
	  else{
	    if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	      std::cout<<address<<" "
		       <<"Rx/RxSendSync2Rx"<<iRx<<" -"<<std::endl;
	    outfile<<address<<" "
		   <<"Rx/RxSendSync2Rx"<<iRx<<" -"<<std::endl;
	  }
	  

	}
      
    }
  return true;
}


bool TgcSswSendSync_Generator::readList(std::string fileName){
  
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf("");
  int sharpPoint;
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  bool tmp_validity=false;
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    

    
    if(entry[0]=="VALIDITY_DEFINITION"){
      if(entry[1]=="true") tmp_validity=true;
      else if(entry[1]=="false") tmp_validity=false;
      else{
	std::cerr<<"err> DB corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      continue;
    }
    
    if(entry[0]=="BC_DEFINITION"){
      
      continue;
    }





    
    if(!(entry[0]==string_side)) continue;
    if(!(atoi(entry[1].c_str())==cciId)) continue;
    
    int sswId=atoi(entry[2].c_str());
    int rxId=atoi(entry[3].c_str());
    
    if(!(sswId>=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] && 
	 sswId<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1])){
      
      std::cerr<<"err> invalid id found in list file in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
      
    portValidity[sswId][rxId]=tmp_validity;
  }
  return true;
}
