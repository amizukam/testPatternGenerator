#include "TgcParameterGenerator/TgcPpsBcidMask_Generator.hh"

int TgcPpsBcidMask_Generator::NUM_OF_CLMN_OF_LIST_FILE=8;

TgcPpsBcidMask_Generator::TgcPpsBcidMask_Generator(): useDefaultConfigurationFile(true),
						nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcPpsBcidMask_Generator::TgcPpsBcidMask_Generator(char* fileName): useDefaultConfigurationFile(false),
							      nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcPpsBcidMask_Generator::~TgcPpsBcidMask_Generator(){}

bool TgcPpsBcidMask_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcMappingSvc = new TGCmappingSvc();
  m_tgcMappingSvc->initialize();
  m_tgcPpsSsw = new TgcPpsSsw();
  m_tgcPpsSlb = new TgcPpsSlb();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ? 
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  for(int ii=0; ii<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI; ii++){
    for(int jj=0; jj<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; jj++){
      for(int kk=0; kk<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; kk++){
	for(int ll=0; ll<TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG; ll++){
	  for(int mm=0; mm<TGC_HW_CONSTANT::NUM_OF_A_OR_B; mm++){
	    channel[ii][jj][kk][ll][mm].clear();
	  }
	}
      }
    }
  }
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  return true;
}

bool TgcPpsBcidMask_Generator::setData(){

  std::vector<std::string> fileNameList;
  std::string keyName("Pps/BcidMask");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;
}

bool TgcPpsBcidMask_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  
  std::string buf("");
  int sharpPoint;
  
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }

    std::stringstream line(buf);

    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    switch(typeOfCci){
    case TGC_HW_CONSTANT::BW_CCI:
      if(!(entry[0]==string_side)) continue;
      if(!(atoi(entry[1].c_str())==cciId)) continue;
      if(!(atoi(entry[5].c_str())<=7)) continue;
      
      setBit(entry);
      break;
      
    case TGC_HW_CONSTANT::EIFI_CCI:
      if(cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L){
	if(!(entry[0]==string_side)) continue;
	if(!(atoi(entry[1].c_str())==2 || atoi(entry[1].c_str())==5) ) continue;
	if(!(atoi(entry[5].c_str())>7)) continue;
	
	setBit(entry);
      }
      else{
	if(!(entry[0]==string_side)) continue;
	if(!(atoi(entry[1].c_str())==8 || atoi(entry[1].c_str())==11) ) continue;
	if(!(atoi(entry[5].c_str())>7)) continue;
	
	setBit(entry);
      }
      break;
      
    case TGC_HW_CONSTANT::SL_CCI:
      break;
    default:
      std::cerr<<"err> mis-initialization was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcPpsBcidMask_Generator::setBit(std::string entry[]){
  bool isValid;
  
  // output information
  int rodID;
  int sswID;
  int sbLoc;
  int bitPos;
  
  int rxId;
  
  
  // input information
  int subsystemNumber;
  int octantNumber;
  int moduleNumber;
  int layerNumber;
  int rNumber;
  int wireOrStrip;
  int channelNumber;
  int adChannel=false;
  
  int phi; // 0..3 (E), 0,2(F)
  int phi_48; // 0..47 (E), 0..23 (F)
  
  // Converting IDs,
  phi=atoi(entry[2].c_str());
  layerNumber=atoi(entry[5].c_str())-1;
  channelNumber=atoi(entry[7].c_str());
  
  if(entry[0]=="A") subsystemNumber = 1;
  else if(entry[0]=="C") subsystemNumber = -1;
  else{ 
    std::cerr<<"err> Invalid value was detected in "
	     <<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(entry[4]=="W") wireOrStrip=0;
  else if(entry[4]=="S"){
    wireOrStrip=1;
    channelNumber=channelNumber%32;
  }
  else{
    std::cerr<<"err> Invalid value was detected in "<<__FILE__
	     <<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  rNumber=atoi(entry[6].c_str());
  
  switch(typeOfCci){
  case TGC_HW_CONSTANT::BW_CCI:
    if(entry[3]=="E"){ 
      phi_48 = (sectorId-1)*4+phi;
      octantNumber = (phi_48-(phi_48%6))/6;
      
      int tmp_module=phi_48%6;
      moduleNumber=(tmp_module-tmp_module%2)/2*3 + (tmp_module%2);
    }
    
    else if(entry[3]=="F"){
      phi_48 = (sectorId-1)*2+(phi/2);
      octantNumber = (phi_48-(phi_48%3))/3;
      
      int tmp_module=phi_48%3;
      moduleNumber=tmp_module*3 +2;
    }
    break;
    
  case TGC_HW_CONSTANT::EIFI_CCI:
    phi_48 = phi;
    octantNumber = ((phi%24)-((phi%24)%3))/3;
    int id_offset;
    if(entry[3]=="E") id_offset=9;
    else if(entry[3]=="F") id_offset=12;
    else{
      std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    moduleNumber = id_offset+(phi%3);
    break;
    
  case TGC_HW_CONSTANT::SL_CCI:
    break;
    
  }
  
  isValid=m_tgcMappingSvc->getReadoutIDfromOnlineID(subDetectorId,
						    rodID,
						    sswID,
						    sbLoc,
						    bitPos,
						    //input paramters//
						    subsystemNumber, // 1(A)..-1(C)
						    octantNumber, // 0..7
						    moduleNumber, // 0..8
						    layerNumber, // 0..6
						    rNumber, //0..n
						    wireOrStrip, 
						    channelNumber,
						    adChannel
						    );
  if(!isValid){
    std::cerr<<"err> invalid input online ID in "<<__FILE__<<" at "<<__LINE__<<std::endl
	     <<"subsystemNumber: "<<subsystemNumber<<std::endl
	     <<"octantNumber: "<<octantNumber<<std::endl
	     <<"moduleNumber: "<<moduleNumber<<std::endl
	     <<"layreNumber: "<<layerNumber<<std::endl
	     <<"rNumber: "<<rNumber<<std::endl
	     <<"wireOrStrip: "<<wireOrStrip<<std::endl
	     <<"channelNumber: "<<channelNumber<<std::endl;
      
    return false;
  }
  
  m_tgcMappingSvc->getRxIDfromReadoutID(subDetectorId,
					rodID,
					sswID,
					sbLoc,
					rxId);
  
  int r_cciId;
  int r_sswId;
  
  m_tgcPpsSlb->getCciIdfromOnlineId(rodID,
				    sswID,
				    r_cciId,
				    r_sswId
				    );
  
  if(cciId!=r_cciId){
    std::cerr<<"err> id inconsistency was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::vector<int> port;
  std::vector<int> ppsId;
  std::vector<std::string> aOrB;
  std::vector<int> asicOrder;
  std::vector<int> bitOrder;
  
  port.clear();
  ppsId.clear();
  aOrB.clear();
  asicOrder.clear();
  bitOrder.clear();
  
  if(!(m_tgcPpsSlb->getPPJTAGfromOnlineId(subDetectorId,
					  rodID,
					  sswID,
					  rxId,
					  bitPos,
					  port,
					  ppsId,
					  aOrB,
					  asicOrder,
					  bitOrder,
					  true))){
    
    std::cerr<<"err> failure in id conversion in "<<__FILE__<<" at "<<__LINE__<<std::endl
	     <<" subDetectorId:"<<subDetectorId
	     <<" rodId:"<<rodID
	     <<" sswId:"<<sswID
	     <<" sbLoc:"<<sbLoc
	     <<" rxId:"<<rxId
	     <<" bitPos:"<<bitPos
	     <<std::endl;

  }
  
  
  if(logLevel<=TGC_MSG_LEVEL::DEBUG)
    std::cout<<"dbg> ID conversion results in"<<__FILE__<<" at "<<__LINE__<<std::endl
	     <<entry[0]<<" "
	     <<entry[1]<<" "
	     <<entry[2]<<" "
	     <<entry[3]<<" "
	     <<entry[4]<<" "
	     <<entry[5]<<" "
	     <<entry[6]<<" "
	     <<entry[7]<<std::endl
      
	     <<"subusystemNumber : "<<subsystemNumber
	     <<" octantNumber:"<<octantNumber
	     <<" moduleNumber:"<<moduleNumber // 0..8
	     <<" layerNumber:"<<layerNumber // 0..6
	     <<" rNumber:"<<rNumber //0..n
	     <<" wireOrStrip:"<<wireOrStrip
	     <<" channelNumber:"<<channelNumber<<std::endl
      
	     <<"subDetectorId:"<<subDetectorId
	     <<" rodId:"<<rodID
	     <<" sswId:"<<sswID
	     <<" rxId:"<<rxId
	     <<" sbLoc:"<<sbLoc
	     <<" bitPos:"<<bitPos
	     <<std::endl;
  
  
  
  int vSize=port.size();
  if(vSize==0){
    std::cerr<<"war> no candidate was found in "<<__FILE__<<" at "<<__LINE__<<std::endl
	     <<entry[0]<<" "
	     <<entry[1]<<" "
	     <<entry[2]<<" "
	     <<entry[3]<<" "
	     <<entry[4]<<" "
	     <<entry[5]<<" "
	     <<entry[6]<<" "
	     <<entry[7]<<std::endl
	     <<"subDetectorId:"<<subDetectorId
	     <<" rodId:"<<rodID
	     <<" sswId:"<<sswID
	     <<" rxId:"<<rxId
	     <<" sbLoc:"<<sbLoc
	     <<" bitPos:"<<bitPos
	     <<std::endl;
    return true;
  }

  if(vSize!=port.size() ||
     vSize!=ppsId.size() ||
     vSize!=aOrB.size() ||
     vSize!=asicOrder.size() ||
     vSize!=bitOrder.size()
     ){
    std::cerr<<"err> vector size inconsistency detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int ii=0; ii<vSize; ii++){
    bool isAchannel=(aOrB.at(ii)=="A");
    
    if(logLevel<=TGC_MSG_LEVEL::INFO){
      char char_sswId[10];
      char char_cciId[10];
      sprintf(char_sswId, "%x", r_sswId);
      sprintf(char_cciId, "%x", cciId);
      
      std::cout<<"inf> aOrB:"<<aOrB.at(ii)<<" isAchannel:"<<isAchannel<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      std::cout<<"inf> candidate "<<ii<<" 0x0"<<char_cciId<<"8"<<char_sswId<<"000"<<port.at(ii)<<" "<<"Pps"<<ppsId.at(ii)
	       <<" BcidMask"<<aOrB.at(ii)<<" asicOrder:"<<asicOrder.at(ii)<<" bitOrder:"<<bitOrder.at(ii)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    }
    
    if(logLevel<=TGC_MSG_LEVEL::INFO)
      std::cout<<"inf> push_back "<<(15-bitOrder.at(ii))<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;

    channel[r_sswId][port.at(ii)][ppsId.at(ii)][asicOrder.at(ii)][!isAchannel].push_back((15-bitOrder.at(ii)));
  
  }
  
  return true;
}

bool TgcPpsBcidMask_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);
  
  int bit_PpsBcidMask[TGC_HW_CONSTANT::NUM_OF_BIT_IN_PP];
  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];

  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      char address[100];
      
      sprintf(address, "0x0%x8%x000%x",cciId,iSsw,iPort);
      
      if(!m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(numOfPpAsic.size()!=4){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      

      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	
	if(numOfPpAsic.at(iJtag)==0) continue;
	

	for(int iLine=0; iLine<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iLine++){
	  char* lineName;
	  (iLine==1) ? lineName=(char*)"B" : lineName=(char*)"A";
	  
	  outfile<<address<<" Pps"<<iJtag<<"/BcidMask"<<lineName<<" ";
	  
	  for(int iPpAsic=0; iPpAsic<numOfPpAsic.at(iJtag); iPpAsic++){
	    
	    for(int ii=0; ii<TGC_HW_CONSTANT::NUM_OF_BIT_IN_PP; ii++){
	      bit_PpsBcidMask[ii]=1;
	    }
	    
	    for(int ii=0; ii<channel[iSsw][iPort][iJtag][iPpAsic][iLine].size(); ii++){
	      bit_PpsBcidMask[channel[iSsw][iPort][iJtag][iPpAsic][iLine].at(ii)]=0;
	    }
	    
	    for(int ii=0; ii<TGC_HW_CONSTANT::NUM_OF_BIT_IN_PP; ii++){
	      outfile<<bit_PpsBcidMask[ii];
	    }
	  }
	  outfile<<std::endl;
	}
      }

    }
  }
  return true;
}
