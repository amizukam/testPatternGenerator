#include "TgcParameterGenerator/TgcSswTxClkDelNum_Generator.hh"

int TgcSswTxClkDelNum_Generator::DEFAULT_VALUE=0;

int TgcSswTxClkDelNum_Generator::NUM_OF_CLMN_OF_LIST_FILE=4;
TgcSswTxClkDelNum_Generator::TgcSswTxClkDelNum_Generator() : useDefaultConfigurationFile(true), 
					       nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswTxClkDelNum_Generator::TgcSswTxClkDelNum_Generator(char* fileName) : useDefaultConfigurationFile(false), 
							     nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswTxClkDelNum_Generator::~TgcSswTxClkDelNum_Generator(){}

bool TgcSswTxClkDelNum_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  valueOfTxClkDelNum =DEFAULT_VALUE;
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
  }  
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  return true;
}

bool TgcSswTxClkDelNum_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Ssw/ClkDelayNum");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcSswTxClkDelNum_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);
  
  if(logLevel<TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> ssw loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
      iSsw++)
    {
      
      char address[20];
      
      sprintf(address, "0x0%x8%x0000", cciId, iSsw);
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> TxClkDelNum parameter file generating starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      

      // TxClkDelNum
      char value[10];
      sprintf(value, "0x%x", valueOfTxClkDelNum);
      
      if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	std::cout<<address<<" "
		 <<"Tx/ClkDelayNum "
		 <<value<<std::endl;
      
      outfile<<address<<" "
	     <<"Tx/ClkDelayNum "
	     <<value<<std::endl;
      
    }
  return true;
}


bool TgcSswTxClkDelNum_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string buf("");
  int sharpPoint;
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }

  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if (entry[0]=="TX_NUMBER_OF_CLOCK_DELAY") {
      const int newValueOfClockNum=atoi(entry[1].c_str());
      if (newValueOfClockNum<64) {
	valueOfTxClkDelNum=newValueOfClockNum;
      }
      else {
	std::cerr<< "invalid input in "<<__FILE__<<":"<<__LINE__<<std::endl;
      }
    }
  }
  return true;
}
