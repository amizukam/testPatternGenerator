#include <string.h>
#include "TgcParameterGenerator/TgcSlbDelay_Generator.hh"

int TgcSlbDelay_Generator::NUM_OF_CLMN_OF_LIST_FILE=8;
int TgcSlbDelay_Generator::NUM_OF_BIT=3;
int TgcSlbDelay_Generator::NUM_OF_CLMN_OF_DEFAULT_FILE=5;

std::string s_SLB_DELAY_dir = std::getenv("SETUP_DIR");
std::string s_SLB_DELAY_file = "/SLB_DELAY_DEFAULT.txt";
std::string s_SLB_DELAY_list = s_SLB_DELAY_dir + s_SLB_DELAY_file;
const char* c_SLB_DELAY_list = s_SLB_DELAY_list.c_str();
char* TgcSlbDelay_Generator::NAME_OF_DEFAULT_FILE = strdup(c_SLB_DELAY_list);


TgcSlbDelay_Generator::TgcSlbDelay_Generator(): useDefaultConfigurationFile(true),
					  nameOfConfigurationFile("")
  
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbDelay_Generator::TgcSlbDelay_Generator(char* fileName): useDefaultConfigurationFile(false),
							nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbDelay_Generator::~TgcSlbDelay_Generator()
{
}

bool TgcSlbDelay_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }

  int startId=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int numOfSsw=TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];


  for(int iSsw=startId; iSsw<startId+numOfSsw; iSsw++){

    
    std::ifstream inputfile(NAME_OF_DEFAULT_FILE);
    if(!(inputfile.is_open())){
      std::cerr<<"err> failed in read db processing to open: "<<NAME_OF_DEFAULT_FILE
	       <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    
    std::string buf("");
    int sharpPoint;

    std::string entry[NUM_OF_CLMN_OF_DEFAULT_FILE];
    
    int cciIdReference[2]={0,0};
    int sswIdReference[2]={0,0};

    while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


      std::stringstream line(buf);
      
      int iLine=0;
      while(line>>entry[iLine]){
	iLine++;
	if(!(iLine<NUM_OF_CLMN_OF_DEFAULT_FILE)) break;
      }
      if(iLine==0) continue;
      
      if(entry[0]=="NUM_OF_CCI"){
	cciIdReference[1]=cciIdReference[0];
	cciIdReference[0]+=atoi(entry[1].c_str());
	continue;
      }
      
      if(!(cciId<=cciIdReference[0] &&
	   cciId> cciIdReference[1])) continue;
      
      if(entry[0]=="NUM_OF_SSW"){
	sswIdReference[1]=sswIdReference[0];
	sswIdReference[0]+=atoi(entry[1].c_str());
	continue;
      }
      
      if((atoi(entry[0].c_str())==0)&&(entry[0]!="0")) continue;
      
      if(!(iSsw< sswIdReference[0] &&
	   iSsw>=sswIdReference[1] )) continue;
      
      
      if(iLine!=NUM_OF_CLMN_OF_DEFAULT_FILE) continue;
      
      int rxId  =atoi(entry[0].c_str());
      int valueA=atoi(entry[1].c_str());
      int valueB=atoi(entry[2].c_str());
      int valueC=atoi(entry[3].c_str());
      int valueD=atoi(entry[4].c_str());
      
      if((rxId  ==0 && entry[0]!="0")  ||
	 (valueA==0 && entry[1]!="0")  || 
	 (valueB==0 && entry[2]!="0")  ||
	 (valueC==0 && entry[3]!="0")  ||
	 (valueD==0 && entry[4]!="0"))  continue;      
      
      valueOfSlbDelay[iSsw][rxId][0]=valueA;
      valueOfSlbDelay[iSsw][rxId][1]=valueB;
      valueOfSlbDelay[iSsw][rxId][2]=valueC;
      valueOfSlbDelay[iSsw][rxId][3]=valueD;
    }
  }
  
  
  return true;
}

bool TgcSlbDelay_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Slb/Delay");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;  
}

bool TgcSlbDelay_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }

  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  std::string buf("");
  int sharpPoint;
  
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    
    if(iLine!=NUM_OF_CLMN_OF_LIST_FILE) continue;
    if(!(entry[0]==string_side)) continue;
    if(!(atoi(entry[1].c_str())==cciId)) continue;
    
    int sswId =atoi(entry[2].c_str());
    int rxId  =atoi(entry[3].c_str());
    int valueA=atoi(entry[4].c_str());
    int valueB=atoi(entry[5].c_str());
    int valueC=atoi(entry[6].c_str());
    int valueD=atoi(entry[7].c_str());
    
    
    if((sswId ==0 && entry[2]!="0")  ||
       (rxId  ==0 && entry[3]!="0")  ||
       (valueA==0 && entry[4]!="0")  || 
       (valueB==0 && entry[5]!="0")  ||
       (valueC==0 && entry[6]!="0")  ||
       (valueD==0 && entry[7]!="0"))  continue;      
    
    valueOfSlbDelay[sswId][rxId][0]=valueA;
    valueOfSlbDelay[sswId][rxId][1]=valueB;
    valueOfSlbDelay[sswId][rxId][2]=valueC;
    valueOfSlbDelay[sswId][rxId][3]=valueD;
  }
  
  return true;
}

bool TgcSlbDelay_Generator::generate(char* output_fileName){
  
  std::ofstream outfile(output_fileName);
      
  int jtagId;
  int portId;
  bool validity=false;
  
  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
 
  int sbLoc;
  int rodId;
  int sswId;
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){

      if(!m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, validity)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(!validity) continue;
      
      if(!m_tgcSlbSsw->getAddressFromRxId(iSlb, portId, jtagId)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      
      //** GENERATE **//
      char address[20];
      char tmp_char[BUFSIZ];
      
      sprintf(address, "0x0%x8%x000%x", cciId, iSsw, portId);
      //intToBin( valueOfSlbDelay[iSsw]       ,  NUM_OF_BIT, char_depth_READOUT);
      
      outfile<<address<<" "<<"Slb"<<jtagId<<"/DelayA ";
      intToBin( valueOfSlbDelay[iSsw][iSlb][0],  NUM_OF_BIT, tmp_char, sizeof(tmp_char));
      outfile<<tmp_char<<std::endl;

      outfile<<address<<" "<<"Slb"<<jtagId<<"/DelayB ";
      intToBin( valueOfSlbDelay[iSsw][iSlb][1],  NUM_OF_BIT, tmp_char, sizeof(tmp_char));
      outfile<<tmp_char<<std::endl;

      outfile<<address<<" "<<"Slb"<<jtagId<<"/DelayC ";
      intToBin( valueOfSlbDelay[iSsw][iSlb][2],  NUM_OF_BIT, tmp_char, sizeof(tmp_char));
      outfile<<tmp_char<<std::endl;

      outfile<<address<<" "<<"Slb"<<jtagId<<"/DelayD ";
      intToBin( valueOfSlbDelay[iSsw][iSlb][3],  NUM_OF_BIT, tmp_char, sizeof(tmp_char));
      outfile<<tmp_char<<std::endl;
    }  
  }
  
  return true;
}

bool TgcSlbDelay_Generator::intToBin(const int& num_, 
				     const int& max_, 
				     char* bin, 
				     const size_t& sizeOfBin){
  
  std::vector<int> bin_vector;
  bin_vector.clear();
  std::string bin_string("");
  
  int num=num_;
  
  for(int ii=0; ii<max_; ii++){
    if(num%2) bin_vector.push_back(1);
    else  bin_vector.push_back(0);
    num=num>>1;
  }
  
  for(int ii=0; ii<bin_vector.size(); ii++){
    char bit[BUFSIZ];
    snprintf(bit,sizeof(bit),"%1d",bin_vector.at(max_-1-ii));
    bin_string+=bit;
  }
  
  if (snprintf(bin, sizeOfBin, "%s", bin_string.c_str())<0) {
    fprintf(stderr, "err> %s:%d failed in snprintf \n", 
	    __FILE__,
	    __LINE__);
    return false;
  }
  
  return true;
}

