#include <string.h>

#include "TgcParameterGenerator/TgcPpsSignalDelay_Generator.hh"

int TgcPpsSignalDelay_Generator::NUM_OF_CLMN_OF_DEFAULT_LIST_FILE=6;
int TgcPpsSignalDelay_Generator::NUM_OF_CLMN_OF_LIST_FILE=8;
int TgcPpsSignalDelay_Generator::NUM_OF_BIT=5;

std::string s_SIGNAL_DELAY_dir = std::getenv("SETUP_DIR");
std::string s_SIGNAL_DELAY_file = "/SIGNAL_DELAY_DEFAULT.txt";
std::string s_SIGNAL_DELAY_list = s_SIGNAL_DELAY_dir + s_SIGNAL_DELAY_file;
const char* c_SIGNAL_DELAY_list = s_SIGNAL_DELAY_list.c_str();
char* TgcPpsSignalDelay_Generator::FILE_NAME_OF_DEFAULT_VALUE_LIST = strdup(c_SIGNAL_DELAY_list);


TgcPpsSignalDelay_Generator::TgcPpsSignalDelay_Generator(): useDefaultConfigurationFile(true),
						      nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcPpsSignalDelay_Generator::TgcPpsSignalDelay_Generator(char* fileName): useDefaultConfigurationFile(false),
								    nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcPpsSignalDelay_Generator::~TgcPpsSignalDelay_Generator(){}

bool TgcPpsSignalDelay_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcPpsSsw = new TgcPpsSsw();
  m_tgcPpsSlb = new TgcPpsSlb();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iSsw=0; iSsw<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic);
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	for(int iPps=0; iPps<TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG; iPps++){
	  for(int iAsic=0; iAsic<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iAsic++){
	    valueOfPpsSignalDelay[iSsw][iPort][iJtag][iPps][iAsic]= (iAsic<numOfPpAsic.at(iJtag)) ? 0 : -1;
	  }
	}
      }
    }
  }
  
  
  std::ifstream inputfile(FILE_NAME_OF_DEFAULT_VALUE_LIST);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<FILE_NAME_OF_DEFAULT_VALUE_LIST
	     <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  std::string buf("");
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_OF_DEFAULT_LIST_FILE];
  
  int cciIdReference[2]={0,0};
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }

    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_DEFAULT_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(atoi(entry[0].c_str())==0 && entry[0]!="0") continue;
    
    if(iLine!=NUM_OF_CLMN_OF_DEFAULT_LIST_FILE) continue;
    
    if(!(cciId<=cciIdReference[0] &&
	 cciId> cciIdReference[1] )) continue;
    
    int iSsw   =atoi(entry[0].c_str());
    int iPort  =atoi(entry[1].c_str());
    int iPps   =atoi(entry[2].c_str());
    int iPpAsic=atoi(entry[3].c_str());
    int iAorB  =atoi(entry[4].c_str());
    int value  =atoi(entry[5].c_str());
    
    if((iSsw   ==0 && entry[0]!="0") ||
       (iPort  ==0 && entry[1]!="0") ||
       (iPps   ==0 && entry[2]!="0") ||
       (iPpAsic==0 && entry[3]!="0") ||
       (iAorB  ==0 && entry[4]!="0") ||
       (value  ==0 && entry[5]!="0")) continue;
    
    
    valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]=value;
  }
  
  
  return true;
}

bool TgcPpsSignalDelay_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Pps/SignalDelay");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;
}

bool TgcPpsSignalDelay_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf("");
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  bool isAbsoluteValue=false;
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    std::stringstream line(buf);

    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    
    if(iLine==0) continue;
    
    if(entry[0]=="VALUE_DEFINITION"){
      if(entry[1]=="ABSOLUTE") isAbsoluteValue=true;
      else if(entry[1]=="RELATIVE") isAbsoluteValue=false;
      else{
	std::cout<<"err> LIST FILE ERROR in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
    }
    
    if(!(string_side==entry[0] &&
	 cciId==atoi(entry[1].c_str()) )) continue;
    
    int iSsw   =atoi(entry[2].c_str()); 
    int iPort  =atoi(entry[3].c_str());
    int iPps   =atoi(entry[4].c_str());
    int iPpAsic=atoi(entry[5].c_str());
    int iAorB  =atoi(entry[6].c_str());
    int value  =atoi(entry[7].c_str());
    
    if((iSsw   ==0 && entry[2]!="0") ||
       (iPort  ==0 && entry[3]!="0") ||
       (iPps   ==0 && entry[4]!="0") ||
       (iPpAsic==0 && entry[5]!="0") ||
       (iAorB  ==0 && entry[6]!="0") ||
       (value  ==0 && entry[7]!="0")) continue;
    
    valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]
      =(isAbsoluteValue) ? value : value+    valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB];
    
    if ( valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]>31 ) {
      std::cerr<<"war> "<<__FILE__<<":"<<__LINE__<<" signal delay overflow (> 31) and 31 is set at "<<entry[0]<<" "<<entry[1]<<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iPps:"<<iPps<<" iPpAsic"<<iPpAsic<<" iAorB"<<iAorB<<std::endl;
      valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]=31;
    }
    else if ( valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]<0 ) {
      std::cerr<<"war> "<<__FILE__<<":"<<__LINE__<<" signal delay underflow (< 0) and 0 is set at "<<entry[0]<<" "<<entry[1]<<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iPps:"<<iPps<<" iPpAsic"<<iPpAsic<<" iAorB"<<iAorB<<std::endl;
      valueOfPpsSignalDelay[iSsw][iPort][iPps][iPpAsic][iAorB]=0;
    }
    
    
    
  }
  
  return true;
  
}

bool TgcPpsSignalDelay_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);

  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      char address[100];
      
      sprintf(address, "0x0%x8%x000%x",cciId,iSsw,iPort);
      
      if(!m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(numOfPpAsic.size()!=4){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	
	if(numOfPpAsic.at(iJtag)==0) continue;
	
	for(int iLine=0; iLine<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iLine++){
	  char* lineName;
	  (iLine==1) ? lineName=(char*)"B" : lineName=(char*)"A";
	  
	  outfile<<address<<" Pps"<<iJtag<<"/SignalDelay"<<lineName<<" ";
	  
	  for(int iPpAsic=0; iPpAsic<numOfPpAsic.at(iJtag); iPpAsic++){
	    char value_char[BUFSIZ];
	    if(valueOfPpsSignalDelay[iSsw][iPort][iJtag][iPpAsic][iLine]==-1)
	      std::cerr<<"err> default value is not set correctly in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	    
	    intToBin(valueOfPpsSignalDelay[iSsw][iPort][iJtag][iPpAsic][iLine],
		     NUM_OF_BIT, 
		     value_char,
		     sizeof(value_char));
	    outfile<<value_char;
	  }
	  outfile<<std::endl;
	}
      }

    }
  }
  return true;
}

bool TgcPpsSignalDelay_Generator::intToBin(const int& num_, 
					   const int& max_, 
					   char* bin, 
					   const size_t& sizeOfBin){
  
  std::vector<int> bin_vector;
  bin_vector.clear();
  std::string bin_string("");
  
  int num=num_;
  
  for(int ii=0; ii<max_; ii++){
    if(num%2) bin_vector.push_back(1);
    else  bin_vector.push_back(0);
    num=num>>1;
  }
  
  for(int ii=0; ii<bin_vector.size(); ii++){
    char bit[BUFSIZ];
    snprintf(bit,sizeof(bit),"%1d",bin_vector.at(max_-1-ii));
    bin_string+=bit;
  }
  
  if (snprintf(bin, sizeOfBin, "%s", bin_string.c_str())<0) {
    fprintf(stderr, "err> %s:%d failed in snprintf \n", 
	    __FILE__,
	    __LINE__);
    return false;
  }
  
  return true;
}
