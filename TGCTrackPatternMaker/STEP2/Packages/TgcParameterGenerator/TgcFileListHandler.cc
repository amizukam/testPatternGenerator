#include "TgcParameterGenerator/TgcFileListHandler.hh"


int TgcFileListHandler::NUM_OF_CLMN_FILE_LIST=2;

TgcFileListHandler::TgcFileListHandler(){
  FILE_LIST_NAME = (char*) "DEFAULT_CONFIG_FILE_LIST!!!";
  fileList.open(FILE_LIST_NAME);
  
  if(!fileList.is_open()){
    std::cerr<<"err> default config file list not preferred, in "<<__FILE__<<" "<<__LINE__<<std::endl;
  }
  
}

TgcFileListHandler::TgcFileListHandler(const char* fileName){
  FILE_LIST_NAME = (char*)fileName;
  fileList.open(FILE_LIST_NAME);
  
  if(!fileList.is_open()){
    std::cerr<<"err> error happened in opening file name list in "<<__FILE__<<" "<<__LINE__<<std::endl;
  }
  
}


TgcFileListHandler::~TgcFileListHandler(){
  fileList.close();
}

bool TgcFileListHandler::getFileList(const std::string& triggerName, 
				     std::vector<std::string>& file_name_list){
  
  file_initialization();
  
  std::string buf;
  std::string entry[NUM_OF_CLMN_FILE_LIST];
  bool isGetting=false;
  int counter=0;
  int sharpPoint;
  
  while(getline(fileList,buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_FILE_LIST)) break;
    }    
    if(iLine==0) continue;
    


    if(entry[0]==":"){
      if(entry[1]==triggerName){
	isGetting=true;
	counter++;
      }
      else isGetting=false;
      continue;
    }
    
    if(!isGetting) continue;
    if(buf=="") continue;
    
    file_name_list.push_back((char*)entry[0].c_str());
  }
  
  if(counter==0){
    std::cerr<<"err> no file group "<<triggerName<<" detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;    
  }
  
  return true;
}

bool TgcFileListHandler::file_initialization(){
  fileList.clear();
  fileList.seekg(0);
}
