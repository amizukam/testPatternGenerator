#include <string.h>
#include "TgcParameterGenerator/TgcPpsBcidDelay_Generator.hh"

int TgcPpsBcidDelay_Generator::NUM_OF_CLMN_OF_DEFAULT_LIST_FILE=6;
int TgcPpsBcidDelay_Generator::NUM_OF_CLMN_OF_LIST_FILE=8;
int TgcPpsBcidDelay_Generator::NUM_OF_BIT=5;

std::string s_BCID_DELAY_dir = std::getenv("SETUP_DIR");
std::string s_BCID_DELAY_file = "/BCID_DELAY_DEFAULT.txt";
std::string s_BCID_DELAY_list = s_BCID_DELAY_dir + s_BCID_DELAY_file;
const char* c_BCID_DELAY_list = s_BCID_DELAY_list.c_str();
char* TgcPpsBcidDelay_Generator::FILE_NAME_OF_DEFAULT_VALUE_LIST = strdup(c_BCID_DELAY_list);

TgcPpsBcidDelay_Generator::TgcPpsBcidDelay_Generator(): useDefaultConfigurationFile(true),
						      nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcPpsBcidDelay_Generator::TgcPpsBcidDelay_Generator(char* fileName): useDefaultConfigurationFile(false),
								    nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcPpsBcidDelay_Generator::~TgcPpsBcidDelay_Generator(){}

bool TgcPpsBcidDelay_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcPpsSsw = new TgcPpsSsw();
  m_tgcPpsSlb = new TgcPpsSlb();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iSsw=0; iSsw<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic);
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	for(int iAsic=0; iAsic<TGC_HW_CONSTANT::NUM_OF_PPASIC_IN_PPJTAG; iAsic++){
	  
	  for(int iAorB=0; iAorB<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iAorB++){
	    valueOfPpsBcidDelay[iSsw][iPort][iJtag][iAsic][iAorB]= (iAsic<numOfPpAsic.at(iJtag)) ? 0 : -1;
	  }
	}
      }
    }
    
    std::ifstream inputfile(FILE_NAME_OF_DEFAULT_VALUE_LIST);
    
    if(!(inputfile.is_open())){
      std::cerr<<"err> failed in read db processing to open: "<<FILE_NAME_OF_DEFAULT_VALUE_LIST
	       <<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    std::string buf("");
    int sharpPoint;

    std::string entry[NUM_OF_CLMN_OF_DEFAULT_LIST_FILE];
    
    int cciIdReference[2]={0,0};
  
    while(getline(inputfile, buf)){
      
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }

      sharpPoint=buf.find("#",0);
      if(sharpPoint!=std::string::npos){
	buf=buf.substr(0,sharpPoint);
      }
      
      std::stringstream line(buf);
      
      int iLine=0;  
      while(line>>entry[iLine]){
	iLine++;
	if(!(iLine<NUM_OF_CLMN_OF_DEFAULT_LIST_FILE)) break;
      }    
      if(iLine==0) continue;
      
      if(entry[0]=="NUM_OF_CCI"){
	cciIdReference[1]=cciIdReference[0];
	cciIdReference[0]+=atoi(entry[1].c_str());
	continue;
      }
      
      //if(!(cciId<=tmp_cciId[0])) continue;
      
      if(atoi(entry[0].c_str())==0 && entry[0]!="0") continue;
      
      if(iLine!=NUM_OF_CLMN_OF_DEFAULT_LIST_FILE) continue;
    
      if(!(cciId<=cciIdReference[0] &&
	   cciId> cciIdReference[1] )) continue;
      
      int sswId   =atoi(entry[0].c_str()); 
      int portId  =atoi(entry[1].c_str());
      int ppsId   =atoi(entry[2].c_str());
      int ppAsicId=atoi(entry[3].c_str());
      int iAorB   =atoi(entry[4].c_str());
      int value   =atoi(entry[5].c_str());
      
      if((sswId   ==0 && entry[0]!="0") ||
	 (portId  ==0 && entry[1]!="0") ||
	 (ppsId   ==0 && entry[2]!="0") ||
	 (ppAsicId==0 && entry[3]!="0") ||
	 (iAorB   ==0 && entry[4]!="0") ||
	 (value   ==0 && entry[5]!="0")) continue;
      
      
      valueOfPpsBcidDelay[sswId][portId][ppsId][ppAsicId][iAorB]=value;
    }
  }
  
  return true;
}

bool TgcPpsBcidDelay_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Pps/BcidDelay");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;
}

bool TgcPpsBcidDelay_Generator::readList(std::string fileName){
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf("");
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  bool isAbsoluteValue=false;
  
  int sharpPoint;
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    
    std::stringstream line(buf);

    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    
    if(iLine==0) continue;
    
    if(entry[0]=="VALUE_DEFINITION"){
      if(entry[1]=="ABSOLUTE") isAbsoluteValue=true;
      else if(entry[1]=="RELATIVE") isAbsoluteValue=false;
      else{
	std::cout<<"err> LIST FILE ERROR in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
    }
    
    if(!(string_side==entry[0] &&
	 cciId==atoi(entry[1].c_str()) )) continue;
    
    int iSsw   =atoi(entry[2].c_str()); 
    int iPort  =atoi(entry[3].c_str());
    int iPps   =atoi(entry[4].c_str());
    int iPpAsic=atoi(entry[5].c_str());
    int iAorB  =atoi(entry[6].c_str());
    int value  =atoi(entry[7].c_str());
    
    if((iSsw   ==0 && entry[2]!="0") ||
       (iPort  ==0 && entry[3]!="0") ||
       (iPps   ==0 && entry[4]!="0") ||
       (iPpAsic==0 && entry[5]!="0") ||
       (iAorB  ==0 && entry[6]!="0") ||
       (value  ==0 && entry[7]!="0")) continue;
    
    valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB]
      =(isAbsoluteValue) ? value : value+    valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB];

    if ( valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB]>30 ) {
      std::cerr<<"inf> "<<__FILE__<<":"<<__LINE__<<" bcid delay value returns to 1 at "<<entry[0]<<" "<<entry[1]<<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iPps:"<<iPps<<" iPpAsic"<<iPpAsic<<" iAorB"<<iAorB<<std::endl;
      valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB]-=30;
    }
    else if ( valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB]<1 ) {
      std::cerr<<"inf> "<<__FILE__<<":"<<__LINE__<<" bcid delay value returns to 30 at "<<entry[0]<<" "<<entry[1]<<" iSsw:"<<iSsw<<" iPort:"<<iPort<<" iPps:"<<iPps<<" iPpAsic"<<iPpAsic<<" iAorB"<<iAorB<<std::endl;
      valueOfPpsBcidDelay[iSsw][iPort][iPps][iPpAsic][iAorB]+=30;
    }
    
  }
  
  return true;
  
}

bool TgcPpsBcidDelay_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);

  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iPort=0; iPort<TGC_HW_CONSTANT::NUM_OF_INPUT_IN_SSW; iPort++){
      
      std::vector<int> numOfPpAsic;
      numOfPpAsic.clear();
      char address[100];
      
      sprintf(address, "0x0%x8%x000%x",cciId,iSsw,iPort);
      
      if(!m_tgcPpsSsw->getNumberOfPPAsicInJtagLine(cciId, iSsw, iPort, numOfPpAsic)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(numOfPpAsic.size()!=4){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      
      for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
	
	if(numOfPpAsic.at(iJtag)==0) continue;
	
	
	for(int iLine=0; iLine<TGC_HW_CONSTANT::NUM_OF_A_OR_B; iLine++){
	  char* lineName;
	  (iLine==1) ? lineName=(char*)"B" : lineName=(char*)"A";
	  
	  outfile<<address<<" Pps"<<iJtag<<"/BcidDelay"<<lineName<<" ";
	  
	  for(int iPpAsic=0; iPpAsic<numOfPpAsic.at(iJtag); iPpAsic++){
	    char value_char[BUFSIZ];
	    if(valueOfPpsBcidDelay[iSsw][iPort][iJtag][iPpAsic][iLine]==-1)
	      std::cerr<<"err> default value is not set correctly in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	    
	    intToBin(valueOfPpsBcidDelay[iSsw][iPort][iJtag][iPpAsic][iLine],
		     NUM_OF_BIT, 
		     value_char,
		     sizeof(value_char));
	    outfile<<value_char;
	  }
	  outfile<<std::endl;
	}
      }

    }
  }
  return true;
}
bool TgcPpsBcidDelay_Generator::intToBin(const int& num_, 
				     const int& max_, 
				     char* bin, 
				     const size_t& sizeOfBin){
  
  std::vector<int> bin_vector;
  bin_vector.clear();
  std::string bin_string("");
  
  int num=num_;
  
  for(int ii=0; ii<max_; ii++){
    if(num%2) bin_vector.push_back(1);
    else  bin_vector.push_back(0);
    num=num>>1;
  }
  
  for(int ii=0; ii<bin_vector.size(); ii++){
    char bit[BUFSIZ];
    snprintf(bit,sizeof(bit),"%1d",bin_vector.at(max_-1-ii));
    bin_string+=bit;
  }
  
  if (snprintf(bin, sizeOfBin, "%s", bin_string.c_str())<0) {
    fprintf(stderr, "err> %s:%d failed in snprintf \n", 
	    __FILE__,
	    __LINE__);
    return false;
  }
  
  return true;
}
