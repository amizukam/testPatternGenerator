#include "TgcParameterGenerator/TgcParameterGeneratorBase.hh"
#include "TgcParameterGenerator/TgcSlbMask2_Generator.hh"
#include "TgcParameterGenerator/TgcSlbMask1_Generator.hh"
#include "TgcParameterGenerator/TgcPpsBcidMask_Generator.hh"
#include "TgcParameterGenerator/TgcPpsAsdTpp_Generator.hh"
#include "TgcParameterGenerator/TgcPpsTpgAmp_Generator.hh"
#include "TgcParameterGenerator/TgcPpsSignalDelay_Generator.hh"
#include "TgcParameterGenerator/TgcPpsTpgDelay_Generator.hh"
#include "TgcParameterGenerator/TgcPpsBcidDelay_Generator.hh"
#include "TgcParameterGenerator/TgcPpsBcidGate_Generator.hh"
#include "TgcParameterGenerator/TgcSlbDepth_Generator.hh"
#include "TgcParameterGenerator/TgcPpsDebugDelay_Generator.hh"
#include "TgcParameterGenerator/TgcSlbL1Veto_Generator.hh"
#include "TgcParameterGenerator/TgcSlbReset_Generator.hh"
#include "TgcParameterGenerator/TgcSlbScheme_Generator.hh"
#include "TgcParameterGenerator/TgcSlbClkInv_Generator.hh"
#include "TgcParameterGenerator/TgcSlbDcVeto_Generator.hh"
#include "TgcParameterGenerator/TgcSlbDrdRst_Generator.hh"
#include "TgcParameterGenerator/TgcSlbTpp_Generator.hh"
#include "TgcParameterGenerator/TgcSlbTestPulse_Generator.hh"
#include "TgcParameterGenerator/TgcSlbDelay_Generator.hh"
#include "TgcParameterGenerator/TgcSlbObsoleteDelay_Generator.hh"
#include "TgcParameterGenerator/TgcSswMask_Generator.hh"
#include "TgcParameterGenerator/TgcSswTimeToWait_Generator.hh"
#include "TgcParameterGenerator/TgcSswEdgeSelect_Generator.hh"
#include "TgcParameterGenerator/TgcSswSendSync_Generator.hh"
#include "TgcParameterGenerator/TgcSswTxClkDelNum_Generator.hh"
#include "TgcParameterGenerator/MsgLevel.hh"

#include <iostream>
#include <stdlib.h>
#include <string>

bool showUsage();
const int STATUS_FAILURE = 1;
const int STATUS_SUCCESS = 0;


int main(int argc, char* argv[]){
  
  std::string functionalityName("DEFAULT");
  std::string string_side("DEFAULT");
  int cciId=-1; //Default Value
  int logLevel=TGC_MSG_LEVEL::FATAL;
  std::string fileName("");
  std::string mainConfigurationFileName("");
  bool useDefaultConfigurationFile=true;
  
  for(int iArg=0; iArg<argc; iArg++){
    std::string string_arg(argv[iArg]);
    
    if(string_arg=="-c"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }
      cciId=atoi(argv[iArg]);
      continue;
    }
    
    if(string_arg=="-f"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }
    
      functionalityName=argv[iArg];
      continue;
    }
    
    if(string_arg=="-s"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }

      string_side=argv[iArg];
      continue;
    }
    
    if(string_arg=="-l"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }
      logLevel=atoi(argv[iArg]);
      std::cout<<"inf> set loglevel "<<logLevel<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      continue;
    }
    
    if(string_arg=="-n"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }

      fileName=argv[iArg];
      continue;
    }

    if(string_arg=="-m"){
      if(!(++iArg<argc)){
	showUsage();
	return STATUS_FAILURE;
      }
      
      useDefaultConfigurationFile=false;
      mainConfigurationFileName=argv[iArg];
      continue;
    }
    
    

  }
  
  bool isCside=false;
  if(string_side=="A") isCside=false;
  else if(string_side=="C") isCside=true;
  else{
    std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    showUsage();
    return STATUS_FAILURE;
  }
  
  TgcParameterGeneratorBase* m_parameterGenerator;
  
  if(logLevel<=TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> Object initialization in "<<__FILE__<<" at "<<__LINE__<<std::endl;

  // INSTANTIATION
  if     (functionalityName=="Slb/Mask2" or
	  functionalityName=="Slb_Mask2" ) 
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbMask2_Generator() :  new TgcSlbMask2_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Slb/Mask1" or
	  functionalityName=="Slb_Mask1") 
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbMask1_Generator() :  new TgcSlbMask1_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/BcidMask" or
	  functionalityName=="Pp_BcidMask")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsBcidMask_Generator() :  new TgcPpsBcidMask_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Ssw/Mask")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSswMask_Generator() :  new TgcSswMask_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Ssw/TimeToWait")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSswTimeToWait_Generator() :  new TgcSswTimeToWait_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Ssw/Edge")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSswEdgeSelect_Generator() :  new TgcSswEdgeSelect_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Ssw/TxClkDelNum")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSswTxClkDelNum_Generator() :  new TgcSswTxClkDelNum_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Ssw/SendSync")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSswSendSync_Generator() :  new TgcSswSendSync_Generator((char*)mainConfigurationFileName.c_str());

  
  else if(functionalityName=="Pps/AsdTpp")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsAsdTpp_Generator() :  new TgcPpsAsdTpp_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/TpgAmp" or
	  functionalityName=="Pp_TpgAmp")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsTpgAmp_Generator() :  new TgcPpsTpgAmp_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/SignalDelay" or
	  functionalityName=="Pp_SignalDelay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsSignalDelay_Generator() :  new TgcPpsSignalDelay_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/TpgDelay" or
	  functionalityName=="Pp_Tpdelay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsTpgDelay_Generator() :  new TgcPpsTpgDelay_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/BcidDelay" or
	  functionalityName=="Pp_BcidDelay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsBcidDelay_Generator() :  new TgcPpsBcidDelay_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Pps/BcidGate" or
	  functionalityName=="Pp_BcidGate")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsBcidGate_Generator() :  new TgcPpsBcidGate_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Slb/Depth" or
	  functionalityName=="Slb_Depth")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbDepth_Generator() :  new TgcSlbDepth_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Pps/DebugDelay" or
	  functionalityName=="Pp_DebugDelay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcPpsDebugDelay_Generator() :  new TgcPpsDebugDelay_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/L1Veto" or
	  functionalityName=="Slb_L1Veto")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbL1Veto_Generator() :  new TgcSlbL1Veto_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/Reset" or
	  functionalityName=="Slb_Reset")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbReset_Generator() :  new TgcSlbReset_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/Scheme" or
	  functionalityName=="Slb_Scheme")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbScheme_Generator() :  new TgcSlbScheme_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Slb/ClkInv" or
	  functionalityName=="Slb_ClkInv")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbClkInv_Generator() :  new TgcSlbClkInv_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/DcVeto" or
	  functionalityName=="Slb_DcVeto")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbDcVeto_Generator() :  new TgcSlbDcVeto_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/DrdRst" or
	  functionalityName=="Slb_DcVeto")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbDrdRst_Generator() :  new TgcSlbDrdRst_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/Tpp" or
	  functionalityName=="Slb_Tpp")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbTpp_Generator() :  new TgcSlbTpp_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/TestPulse" or
	  functionalityName=="Slb_TestPulse")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbTestPulse_Generator() :  new TgcSlbTestPulse_Generator((char*)mainConfigurationFileName.c_str());
  
  else if(functionalityName=="Slb/Delay" or
	  functionalityName=="Slb_Delay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbDelay_Generator() :  new TgcSlbDelay_Generator((char*)mainConfigurationFileName.c_str());

  else if(functionalityName=="Slb/ObsoleteDelay" or
	  functionalityName=="Slb_ObsoleteDelay")
    m_parameterGenerator = (useDefaultConfigurationFile) ?
      new TgcSlbObsoleteDelay_Generator() :  new TgcSlbObsoleteDelay_Generator((char*)mainConfigurationFileName.c_str());

  
  else{
    std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    showUsage();
    return STATUS_FAILURE;
  }
  
  if(logLevel<=TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> setLogLevel("<<logLevel<<") "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  m_parameterGenerator->setLogLevel(logLevel);

  if(!(m_parameterGenerator->initialize(cciId, isCside))){
    std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    showUsage();
    return STATUS_FAILURE;
  }

  if(logLevel<=TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> setData() "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  if(!(m_parameterGenerator->setData())){
    std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    showUsage();
    return STATUS_FAILURE;
  }

  if(logLevel<=TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> generate("<<fileName<<") "<<__FILE__<<" at "<<__LINE__<<std::endl;  
  
  if(fileName==""){
    if(!(m_parameterGenerator->generate())){
      std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      showUsage();
      return STATUS_FAILURE;
    }
  }
  else{
    if(!(m_parameterGenerator->generate((char*)fileName.c_str()))){
      std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      showUsage();
      return STATUS_FAILURE;
    }
  }

  delete m_parameterGenerator;
  return STATUS_SUCCESS;
}

bool showUsage(){
  std::cerr<<"usage> program. -f [Functionarity Name] -s [side(A/C)] -c [cciId(01..15)] (-n [output fileName]) (-m [main configuration fileName])"<<std::endl
	   <<std::endl
	   <<"Funtionalities"<<std::endl
	   <<"Ssw/Mask          : To generate a parameter list for RxInitialSetting, MaskH, MaskL registers on SSW BOARD"<<std::endl
	   <<"Ssw/TimeToWait    : To generate a parameter list for TimeToWait, and TimeToWait2 registers on SSW BOARD"<<std::endl
	   <<"Ssw/Edge          : To generate a parameter list for Edge registers on SSW BOARD"<<std::endl
	   <<"Ssw/TxClkDelNum   : To generate a parameter list for TxClkDelNum registers on SSW BOARD"<<std::endl
	   <<"Ssw/SendSync      : To generate a parameter list for SendSync registers on SSW BOARD"<<std::endl
	   <<"Slb/Mask2         : To generate a parameter list for Mask2 and Mask2P registers on SLB ASIC"<<std::endl
	   <<"Slb/Mask1         : To generate a parameter list for Mask1 and Mask1P registers on SLB ASIC"<<std::endl
	   <<"Slb/Depth         : To generate a parameter list for BcidGate and TpgCoarse on PP ASIC"<<std::endl
	   <<"Slb/L1Veto        : To generate a parameter list for L1Veto on SLB ASIC " <<std::endl
	   <<"Slb/Scheme        : To generate a parameter list for Scheme on SLB ASIC " <<std::endl
	   <<"Slb/ClkInv        : To generate a parameter list for ClkInv on SLB ASIC " <<std::endl
	   <<"Slb/DcVeto        : To generate a parameter list for DcVeto on SLB ASIC " <<std::endl
	   <<"Slb/DrdRst        : To generate a parameter list for DrdRst on SLB ASIC " <<std::endl
	   <<"Slb/Tpp           : To generate a parameter list for Tpp on SLB ASIC " <<std::endl
	   <<"Slb/TestPulse     : To generate a parameter list for TestPulse on SLB ASIC " <<std::endl
	   <<"Slb/ObsoleteDelay : To generate a parameter list for ObsoleteDelay on SLB ASIC " <<std::endl
	   <<"Slb/Delay         : To generate a parameter list for Delay on SLB ASIC " <<std::endl
	   <<"Pps/BcidMask      : To generate a parameter list for BcidMask registers on PP ASIC"<<std::endl
	   <<"Pps/AsdTpp        : To generate a parameter list for BcidMask registers on PP ASIC for ASD Track Parttern Test Pulse"<<std::endl
	   <<"Pps/SignalDelay   : To generate a parameter list for Signal Delay on PP ASIC "<<std::endl
	   <<"Pps/TpgDelay      : To generate a parameter list for TpgFine and TpgCoarse on PP ASIC"<<std::endl
	   <<"Pps/BcidDelay     : To generate a parameter list for BcidDelay and TpgCoarse on PP ASIC"<<std::endl
	   <<"Pps/BcidGate      : To generate a parameter list for BcidGate and TpgCoarse on PP ASIC"<<std::endl
	   <<"Pps/DebugDelay    : To generate a parameter list for DebugDelay on PP ASIC "<<std::endl;

  return true;
}

