#include "TgcParameterGenerator/TgcSswMask_Generator.hh"

int TgcSswMask_Generator::NUM_OF_CLMN_OF_LIST_FILE=4;
TgcSswMask_Generator::TgcSswMask_Generator() : useDefaultConfigurationFile(true), 
					       nameOfConfigurationFile("")
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSswMask_Generator::TgcSswMask_Generator(char* fileName) : useDefaultConfigurationFile(false), 
							     nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}


TgcSswMask_Generator::~TgcSswMask_Generator(){}


bool TgcSswMask_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1]; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){
      bool isValid=false;
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> m_tgcSlbSsw->checkRxIdValidity("<<cciId<<", "<<iSsw<<", "<<iSlb<<", isValid) in "<<__FILE__<<" at "<<__LINE__<<std::endl; 
      
      m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, isValid);
      portValidity[iSsw][iSlb]=isValid;
    }
  
    for (int iRxFpga=0; iRxFpga<TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA; iRxFpga++) {
      edge_select[iSsw][iRxFpga]=TGC_HW_CONSTANT::SSW_EDGE_SELECTION;
    }
    
  }
  
  for(int iBC=0; iBC<TGC_HW_CONSTANT::NUM_OF_INDEXBC; iBC++){
    BC_validity[iBC]=true;
  }
  
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
   }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
  }  
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  return true;
}

bool TgcSswMask_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Ssw/Mask");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcSswMask_Generator::generate(char* output_fileName){
  std::ofstream outfile(output_fileName);
  
  if(logLevel<TGC_MSG_LEVEL::INFO)
    std::cout<<"inf> ssw loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
  
  for(int iSsw=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]; 
      iSsw<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
      iSsw++)
    {
      
      char address[20];
      
      sprintf(address, "0x0%x8%x0000", cciId, iSsw);
      
      if(logLevel<TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> rx fpga loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      
      // RxInitialSettingRx
      for(int iRx=0; iRx<TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA; iRx++)
	{
	  unsigned long RxInitialSetting=0;
	  if(logLevel<=TGC_MSG_LEVEL::INFO)
	    std::cout<<"inf> port loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	  
	  for(int iPort=TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]; 
	      iPort<(TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx]+TGC_HW_CONSTANT::NUM_OF_PORT_IN_RX_FPGA[iRx]);iPort++)
	    {
	      int tmp_RxInitialSetting=0;
	      for(int iIndexBC=0; iIndexBC<TGC_HW_CONSTANT::NUM_OF_INDEXBC; iIndexBC++)
		{
		  tmp_RxInitialSetting+=((portValidity[iSsw][iPort] & BC_validity[iIndexBC])<<iIndexBC);
		  
		  if(logLevel<=TGC_MSG_LEVEL::INFO)
		    std::cout<<"inf> iSsw:"<<iSsw<<" iPort:"<<iPort<<" "<<((portValidity[iSsw][iPort] & BC_validity[iIndexBC]))<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
		}
	      
	      RxInitialSetting+=tmp_RxInitialSetting<<((iPort-TGC_HW_CONSTANT::START_ID_OF_PORT_IN_RX_FPGA[iRx])*4);
	    }
	  
	  RxInitialSetting+=(edge_select[iSsw][iRx]<<15);
	  char value[10];
	  sprintf(value, "0x%x", RxInitialSetting);
	  
	  if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	    std::cout<<address<<" "
		     <<"Rx/RxInitialSettingRx"<<iRx<<" "
		     <<value<<std::endl;
	  
	  
	  outfile<<address<<" "
		 <<"Rx/RxInitialSettingRx"<<iRx<<" "
		 <<value<<std::endl;
	}
      
      if(logLevel<=TGC_MSG_LEVEL::INFO)
	std::cout<<"inf> MaskH/MaskL loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      
      // MaskH MaskL
      for(int iHighOrLow=0; iHighOrLow<TGC_HW_CONSTANT::NUM_OF_TX_MASK; iHighOrLow++)
	{
	  if(logLevel<=TGC_MSG_LEVEL::INFO)
	    std::cout<<"inf> port loop starts in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	  
	  unsigned long TxMask=0;
	  for(int iPort=TGC_HW_CONSTANT::START_ID_OF_PORT_IN_TX_MASK[iHighOrLow]; 
	      iPort<TGC_HW_CONSTANT::START_ID_OF_PORT_IN_TX_MASK[iHighOrLow]+TGC_HW_CONSTANT::NUM_OF_PORT_IN_TX_MASK[iHighOrLow]; 
	      iPort++){
	    
	    TxMask+=portValidity[iSsw][iPort]
	      <<(iPort-TGC_HW_CONSTANT::START_ID_OF_PORT_IN_TX_MASK[iHighOrLow]);
	  }
	  
	  char value[10];
	  sprintf(value, "0x%x", TxMask);
	  
	  char* highOrLow;
	  highOrLow=(iHighOrLow==0) ? (char*)"L" : (char*)"H";
	  
	  if(logLevel<=TGC_MSG_LEVEL::DEBUG)
	    std::cout<<address<<" "
		     <<"Tx/Mask"<<highOrLow<<" "
		     <<value<<std::endl;
	  
	  outfile<<address<<" "
		 <<"Tx/Mask"<<highOrLow<<" "
		 <<value<<std::endl;
	}
    }
  return true;
}


bool TgcSswMask_Generator::readList(std::string fileName){
  
  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);
  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf("");
  int sharpPoint;
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  bool tmp_validity=false;
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }


    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if (entry[0]=="EDGE_SELECT") {
      const int sswId=atoi(entry[1].c_str());
      const int rxId=atoi(entry[2].c_str());
      const int edgeSel=atoi(entry[3].c_str());
      
      if (rxId<TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA && 
	  sswId<TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1] &&
	  edgeSel<2) {
	edge_select[sswId][rxId]=edgeSel;
      }
      else if (rxId>=TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA or
	       edgeSel>=2) 
	{
	std::cerr<< "invalid input in "<<__FILE__<<":"<<__LINE__ 
		 << " cciid:" << cciId
		 << " ssw:" << sswId << "(" << TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1] << ")" 
		 << " rx:" << rxId << "(" << TGC_HW_CONSTANT::NUM_OF_SSW_RX_FPGA << ")"
		 << " edge: " << edgeSel <<std::endl;
      }
      
    }
    
    
    if(entry[0]=="VALIDITY_DEFINITION"){
      if(entry[1]=="true") tmp_validity=true;
      else if(entry[1]=="false") tmp_validity=false;
      else{
	std::cerr<<"err> DB corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      continue;
    }
    
    if(entry[0]=="BC_DEFINITION"){
      
      for(int iIndexBC=0; iIndexBC<TGC_HW_CONSTANT::NUM_OF_INDEXBC; iIndexBC++){
	if(entry[iIndexBC+1]=="true") BC_validity[iIndexBC]=true;
	else if(entry[iIndexBC+1]=="false") BC_validity[iIndexBC]=false;
	else{
	  std::cerr<<"err> DB corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	  return false;
	}
      }
      
      continue;
    }





    
    if(!(entry[0]==string_side)) continue;
    if(!(atoi(entry[1].c_str())==cciId)) continue;
    
    int sswId=atoi(entry[2].c_str());
    int rxId=atoi(entry[3].c_str());
    
    if(!(sswId>=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] && 
	 sswId<TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1]+TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1])){
      
      std::cerr<<"err> invalid id found in list file in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
      
    portValidity[sswId][rxId]=tmp_validity;
  }
  return true;
}
