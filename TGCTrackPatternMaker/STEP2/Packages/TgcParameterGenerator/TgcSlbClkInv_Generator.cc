#include "TgcParameterGenerator/TgcSlbClkInv_Generator.hh"

int TgcSlbClkInv_Generator::NUM_OF_CLMN_OF_LIST_FILE=2;
int TgcSlbClkInv_Generator::NUM_OF_BIT=1;
int TgcSlbClkInv_Generator::DEFAULT_VALUE=1; // THIS VALUE MUST NOT BE CHANGED

TgcSlbClkInv_Generator::TgcSlbClkInv_Generator(): useDefaultConfigurationFile(true),
					  nameOfConfigurationFile("")
  
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbClkInv_Generator::TgcSlbClkInv_Generator(char* fileName): useDefaultConfigurationFile(false),
							nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbClkInv_Generator::~TgcSlbClkInv_Generator()
{
}

bool TgcSlbClkInv_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  

  valueOfSlbClkInv=DEFAULT_VALUE;

  
  return true;
}

bool TgcSlbClkInv_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Slb/ClkInv");
  
  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  
  return true;  
}

bool TgcSlbClkInv_Generator::readList(std::string fileName){
  return true;
}

bool TgcSlbClkInv_Generator::generate(char* output_fileName){
  
  std::ofstream outfile(output_fileName);
      
  int jtagId;
  int portId;
  bool validity=false;
  
  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
 
  int sbLoc;
  int rodId;
  int sswId;
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){

      if(!m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, validity)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(!validity) continue;
      
      if(!m_tgcSlbSsw->getAddressFromRxId(iSlb, portId, jtagId)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      
      //** GENERATE **//
      char address[20];
      char tmp_char[BUFSIZ];
      
      sprintf(address, "0x0%x8%x000%x", cciId, iSsw, portId );
      //intToBin( valueOfSlbClkInv[iSsw]       ,  NUM_OF_BIT, char_depth_READOUT);
      
      outfile<<address<<" "<<"Slb"<<jtagId<<"/ClkInv ";
      //<<char_depth_READOUT<<char_depth_TRIGGER<<char_depth_BCID<<std::endl;

      intToBin( valueOfSlbClkInv       ,  NUM_OF_BIT, tmp_char, sizeof(tmp_char));
      outfile<<tmp_char;
      outfile<<std::endl;
      
    }  
  }
  return true;
}


bool TgcSlbClkInv_Generator::intToBin(const int& num_, 
				      const int& max_, 
				      char* bin, 
				      const size_t& sizeOfBin){
  
  std::vector<int> bin_vector;
  bin_vector.clear();
  std::string bin_string("");
  
  int num=num_;
  
  for(int ii=0; ii<max_; ii++){
    if(num%2) bin_vector.push_back(1);
    else  bin_vector.push_back(0);
    num=num>>1;
  }
  
  for(int ii=0; ii<bin_vector.size(); ii++){
    char bit[BUFSIZ];
    snprintf(bit,sizeof(bit),"%1d",bin_vector.at(max_-1-ii));
    bin_string+=bit;
  }
  
  if (snprintf(bin, sizeOfBin, "%s", bin_string.c_str())<0) {
    fprintf(stderr, "err> %s:%d failed in snprintf \n", 
	    __FILE__,
	    __LINE__);
    return false;
  }
  
  return true;
}

