#include "TgcParameterGenerator/TgcSlbTpp_Generator.hh"

int TgcSlbTpp_Generator::NUM_OF_CLMN_OF_LIST_FILE=8;

TgcSlbTpp_Generator::TgcSlbTpp_Generator(): useDefaultConfigurationFile(true),
					  nameOfConfigurationFile("")

{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbTpp_Generator::TgcSlbTpp_Generator(char* fileName): useDefaultConfigurationFile(false),
							nameOfConfigurationFile(fileName)
{
  setLogLevel(TGC_MSG_LEVEL::FATAL);
}

TgcSlbTpp_Generator::~TgcSlbTpp_Generator(){  
}

bool TgcSlbTpp_Generator::initialize(int tmp_cciId, bool tmp_isCside){
  cciId=tmp_cciId;
  isCside=tmp_isCside;
  
  (isCside) ? sideName=(char*)"C" : sideName=(char*)"A";
  (isCside) ? subDetectorId=TGC_HW_CONSTANT::ID_C_SIDE : subDetectorId=TGC_HW_CONSTANT::ID_A_SIDE;
  
  m_tgcMappingSvc = new TGCmappingSvc();
  m_tgcMappingSvc->initialize();
  m_tgcSlbSsw = new TgcSlbSsw();
  m_tgcFileListHandler = (useDefaultConfigurationFile) ?
    new TgcFileListHandler() : new TgcFileListHandler(nameOfConfigurationFile);
  
  forAllBitsTpp=false;
  
  for(int ii=0; ii<TGC_HW_CONSTANT::MAX_OF_SSW_IN_CCI+1; ii++){
    for(int jj=0; jj<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; jj++){
      channel[ii][jj].clear();
    }
  }
  
  if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L || tmp_cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_H){
    typeOfCci=TGC_HW_CONSTANT::EIFI_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId==TGC_HW_CONSTANT::CCI_ID_SL){
    typeOfCci=TGC_HW_CONSTANT::SL_CCI;
    sectorId=0; //DUMMY
  }
  else if(tmp_cciId<=12 && tmp_cciId>=1){
    typeOfCci=TGC_HW_CONSTANT::BW_CCI;
    sectorId=tmp_cciId;
  }
  else{
    std::cerr<<"err> invalid cci id "<<typeOfCci<<" was input in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  return true;
}

bool TgcSlbTpp_Generator::setData(){
  std::vector<std::string> fileNameList;
  std::string keyName("Slb/Tpp");

  if(!m_tgcFileListHandler->getFileList(keyName, fileNameList)){
    std::cerr<<"err> error detected in getting file name list with the key "<<keyName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  for(int iFile=0; iFile<fileNameList.size(); iFile++){
    if(!(readList(fileNameList.at(iFile)))){
      std::cerr<<"err> failed in set data for "<<fileNameList.at(iFile)<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
    
  return true;
}

bool TgcSlbTpp_Generator::readList(std::string fileName){

  std::ifstream inputfile(fileName.c_str());
  std::string string_side(sideName);

  
  if(!(inputfile.is_open())){
    std::cerr<<"err> failed in read db processing to open: "<<fileName<<" in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  
  std::string buf("");
  int sharpPoint;
  
  std::string entry[NUM_OF_CLMN_OF_LIST_FILE];
  
  
  while(getline(inputfile, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    

    std::stringstream line(buf);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_OF_LIST_FILE)) break;
    }    
    if(iLine==0) continue;
    
    if(entry[0]=="ALLBIT_TESTPULSE"){
      if(entry[1]=="true") forAllBitsTpp=true;
      else if(entry[1]=="false") forAllBitsTpp=false;
      else{
	std::cerr<<"err> DB corruptino was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      continue;
    }
    
    switch(typeOfCci){
    case TGC_HW_CONSTANT::BW_CCI:
      if(!(entry[0]==string_side)) continue;
      if(!(atoi(entry[1].c_str())==cciId)) continue;
      if(!(atoi(entry[5].c_str())<=7)) continue;
      
      setBit(entry, TGC_HW_CONSTANT::BIT_H);
      break;
      
    case TGC_HW_CONSTANT::EIFI_CCI:
      if(cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L){
	if(!(entry[0]==string_side)) continue;
	if(!(atoi(entry[1].c_str())==2 || atoi(entry[1].c_str())==5) ) continue;
	if(!(atoi(entry[5].c_str())>7)) continue;
	
	setBit(entry, TGC_HW_CONSTANT::BIT_H);
      }
      else{
	if(!(entry[0]==string_side)) continue;
	if(!(atoi(entry[1].c_str())==8 || atoi(entry[1].c_str())==11) ) continue;
	if(!(atoi(entry[5].c_str())>7)) continue;
	
	setBit(entry, TGC_HW_CONSTANT::BIT_H);
      }
      break;
      
    case TGC_HW_CONSTANT::SL_CCI:
      break;
    default:
      std::cerr<<"err> mis-initialization was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
  }
  return true;
}

bool TgcSlbTpp_Generator::setBit(std::string entry[], int bitValue){
  bool isValid;
  
  // output information
  int rodID;
  int sswID;
  int sbLoc;
  int bitPos;
  
  int rxId;
  
    
  // input information
  int subsystemNumber;
  int octantNumber;
  int moduleNumber;
  int layerNumber;
  int rNumber;
  int wireOrStrip;
  int channelNumber;
  int adChannel=false;
    
  int phi; // 0..3 (E), 0,2(F)
  int phi_48; // 0..47 (E), 0..23 (F)
  
  // Converting IDs,
  phi=atoi(entry[2].c_str());
  layerNumber=atoi(entry[5].c_str())-1;
  channelNumber=atoi(entry[7].c_str());
    
  if(entry[0]=="A") subsystemNumber = 1;
  else if(entry[0]=="C") subsystemNumber = -1;
  else{ 
    std::cerr<<"err> Invalid value was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
      
  if(entry[4]=="W") wireOrStrip=0;
  else if(entry[4]=="S"){
    wireOrStrip=1;
    channelNumber=channelNumber%32;
  }
  else{
    std::cerr<<"err> Invalid value was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }

  rNumber=atoi(entry[6].c_str());
  
  switch(typeOfCci){
  case TGC_HW_CONSTANT::BW_CCI:
    if(entry[3]=="E"){ 
      phi_48 = (sectorId-1)*4+phi;
      octantNumber = (phi_48-(phi_48%6))/6;
      
      int tmp_module=phi_48%6;
      moduleNumber=(tmp_module-tmp_module%2)/2*3 + (tmp_module%2);
    }
      
    else if(entry[3]=="F"){
      phi_48 = (sectorId-1)*2+(phi/2);
      octantNumber = (phi_48-(phi_48%3))/3;
      
      int tmp_module=phi_48%3;
	moduleNumber=tmp_module*3 +2;
    }
    break;

  case TGC_HW_CONSTANT::EIFI_CCI:
    phi_48 = phi;
    octantNumber = ((phi%24)-((phi%24)%3))/3;
    int id_offset;
    if(entry[3]=="E") id_offset=9;
    else if(entry[3]=="F") id_offset=12;
    else{
      std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
      return false;
    }
    moduleNumber = id_offset+(phi%3);
    break;
    
  case TGC_HW_CONSTANT::SL_CCI:
    break;
    
  }
  
  isValid=m_tgcMappingSvc->getReadoutIDfromOnlineID(subDetectorId,
						    rodID,
						    sswID,
						    sbLoc,
						    bitPos,
						    //input paramters//
						    subsystemNumber, // 1(A)..-1(C)
						    octantNumber, // 0..7
						    moduleNumber, // 0..8
						    layerNumber, // 0..6
						    rNumber, //0..n
						    wireOrStrip, 
						    channelNumber,
						    adChannel
						    );
  if(!isValid) return false;
    
  m_tgcMappingSvc->getRxIDfromReadoutID(subDetectorId,
					rodID,
					sswID,
					sbLoc,
					rxId);

  int r_cciId;
  int r_sswId;
  
  m_tgcSlbSsw->getCciIdfromOnlineId(rodID,
				    sswID,
				    r_cciId,
				    r_sswId
				    );
  
  if(cciId!=r_cciId){
    std::cerr<<"err> id inconsistency was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  channel[r_sswId][rxId].push_back(bitPos-40);
  
  /* Ad-Channel */
  adChannel=true;
  isValid=m_tgcMappingSvc->getReadoutIDfromOnlineID(subDetectorId,
						    rodID,
						    sswID,
						    sbLoc,
						    bitPos,
						    //input paramters//
						    subsystemNumber, // 1(A)..-1(C)
						    octantNumber, // 0..7
						    moduleNumber, // 0..8
						    layerNumber, // 0..6
						    rNumber, //0..n
						    wireOrStrip, 
						    channelNumber,
						    adChannel
						    );
  if(!isValid) return true;
  
  m_tgcMappingSvc->getRxIDfromReadoutID(subDetectorId,
					rodID,
					sswID,
					sbLoc,
					rxId);
  
  m_tgcSlbSsw->getCciIdfromOnlineId(rodID,
				    sswID,
				    r_cciId,
				    r_sswId
				    );
  
  if(cciId!=r_cciId){
    std::cerr<<"err> id inconsistency was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  channel[r_sswId][rxId].push_back(bitPos-40);
  
  
  return true;
}

bool TgcSlbTpp_Generator::generate(char* output_fileName){
  
  int bit_SlbTpp[TGC_HW_CONSTANT::NUM_OF_BIT_IN_SLB];
  std::ofstream outfile(output_fileName);
      
  int jtagId;
  int portId;
  bool validity=false;
  
  int ssw_id_start=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  int ssw_id_end=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1] + TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];

  int dummyInt;
  bool dummyBool;
  int sbLoc;
  int rodId;
  int sswId;
  
  for(int iSsw=ssw_id_start; iSsw<ssw_id_end; iSsw++){
    for(int iSlb=0; iSlb<TGC_HW_CONSTANT::NUM_OF_SLB_IN_SSW; iSlb++){
      
      // initialization
      
      if(forAllBitsTpp){
	for(int iBit=0; iBit<TGC_HW_CONSTANT::NUM_OF_BIT_IN_SLB; iBit++){
	
	  switch(typeOfCci){
	  case TGC_HW_CONSTANT::BW_CCI:
	    rodId=sectorId;
	    sswId=iSsw;
	  
	    m_tgcMappingSvc->decodeId(dummyInt,
				      dummyBool,
				      dummyBool,
				      dummyInt,
				      sbLoc,
				      subDetectorId,
				      rodId,
				      iSsw,
				      iSlb
				      );
	  
	    if(!(m_tgcMappingSvc->getOnlineIDfromReadoutID(subDetectorId,
							   rodId,
							   iSsw,
							   sbLoc,
							   iBit+40,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   false
							   ))){
	    
	      bit_SlbTpp[iBit]=TGC_HW_CONSTANT::BIT_L;
	    }
	    else{
	      bit_SlbTpp[iBit]=TGC_HW_CONSTANT::BIT_H;
	    }

	    break;
	  
	  case TGC_HW_CONSTANT::EIFI_CCI:
	    (cciId==TGC_HW_CONSTANT::CCI_ID_EIFI_L) ? rodId=2+3*(iSsw%2) : rodId=8+3*(iSsw%2);
	    sswId=TGC_HW_CONSTANT::SSWID_EIFI;

	    m_tgcMappingSvc->decodeId(dummyInt,
				      dummyBool,
				      dummyBool,
				      dummyInt,
				      sbLoc,
				      subDetectorId,
				      rodId,
				      sswId,
				      iSlb
				      );
	  
	    if(!(m_tgcMappingSvc->getOnlineIDfromReadoutID(subDetectorId,
							   rodId,
							   sswId,
							   sbLoc,
							   iBit+40,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   dummyInt,
							   false
							   ))){
	    
	      bit_SlbTpp[iBit]=TGC_HW_CONSTANT::BIT_L;
	    }
	    else{
	      bit_SlbTpp[iBit]=TGC_HW_CONSTANT::BIT_H;
	    }

	    break;
	  
	  case TGC_HW_CONSTANT::SL_CCI:
	    bit_SlbTpp[iBit]=TGC_HW_CONSTANT::BIT_L;
	  
	    break;
	  
	  defalt:
	    std::cerr<<"err> mis initialization was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	    break;
	  
	  
	  }
	}
      }
      else{
	for(int iBit=0; iBit<TGC_HW_CONSTANT::NUM_OF_BIT_IN_SLB; iBit++){
	  bit_SlbTpp[iBit]=0;
	}
      }

      if(!m_tgcSlbSsw->checkRxIdValidity(cciId, iSsw, iSlb, validity)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      if(!validity) continue;
      
      if(!m_tgcSlbSsw->getAddressFromRxId(iSlb, portId, jtagId)){
	std::cerr<<"err> db corruption was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
	return false;
      }
      
      
      
      for(int ii=0; ii<channel[iSsw][iSlb].size(); ii++){
	bit_SlbTpp[channel[iSsw][iSlb].at(ii)]=TGC_HW_CONSTANT::BIT_H;
      }
      
      
      //** GENERATE **//
      char address[20];
      sprintf(address, "0x0%x8%x000%x", cciId, iSsw, portId);
      
      outfile<<address<<" "<<"Slb"<<jtagId<<"/Tpp ";
      for(int iBit=0; iBit<TGC_HW_CONSTANT::NUM_OF_BIT_IN_SLB; iBit++){
	outfile<<bit_SlbTpp[iBit];
      }
      outfile<<std::endl;
    }  
  }
  
  return true;
}
