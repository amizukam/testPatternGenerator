#include <iostream>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <cstdlib>
#include <string.h>

#include "TgcParameterGenerator/TgcPpsSsw.hh"


std::string s_pp_validity_db_dir = std::getenv("PACKAGES_DIR");
std::string s_pp_validity_db_file = "/DB/PPJTAG_VALIDITY_DB.txt";
std::string s_pp_validity_db = s_pp_validity_db_dir + s_pp_validity_db_file;
const char* c_pp_validity_db = s_pp_validity_db.c_str();
char* TgcPpsSsw::FILE_NAME_PP_VALIDITY_DB=strdup(c_pp_validity_db);

int TgcPpsSsw::NUM_OF_CLMN_PP_VALIDITY_DB=9;


TgcPpsSsw::TgcPpsSsw(){
}

TgcPpsSsw::~TgcPpsSsw(){
}

bool TgcPpsSsw::getNumberOfPPAsicInJtagLine(int cciId, int sswId, int rxId, std::vector<int>& numOfPPAsic){
  
  int startId;
  int numOfSsw;
  
  if(!(getSswIdFromCciId(cciId, startId, numOfSsw))){
    std::cerr<<"err> an error was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId>=startId && sswId<(startId+numOfSsw))){ 
    for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
      numOfPPAsic.push_back(0);
    }
    return true;
  }

  std::ifstream f(FILE_NAME_PP_VALIDITY_DB);
  if(!f.is_open()){
    std::cerr<<"err> DB file dose not exist in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf;
  int sharpPoint;

  std::string entry[NUM_OF_CLMN_PP_VALIDITY_DB];
  
  bool isCorrectlyDone=false;
  
  int cciIdReference[2]={0,0};
  int sswIdReference[2]={0,0};
  
  while(getline(f, buf)){
    sharpPoint=buf.find("#",0);
    if(sharpPoint!=std::string::npos){
      buf=buf.substr(0,sharpPoint);
    }
    

    std::stringstream line(buf);
    
    int iLine=0;
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_PP_VALIDITY_DB)) break;
    }    
    if(iLine==0) continue;
    
    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(!(cciId<=cciIdReference[0] &&
	 cciId> cciIdReference[1])) continue;
    
    if(entry[0]=="NUM_OF_SSW"){
      sswIdReference[1]=sswIdReference[0];
      sswIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if((atoi(entry[0].c_str())==0)&&(entry[0]!="0")) continue;
    
    if(!(rxId==atoi(entry[0].c_str()) &&
	 sswId< sswIdReference[0] &&
	 sswId>=sswIdReference[1] )) continue;
    

    
    for(int jj=0; jj<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; jj++){
      numOfPPAsic.push_back(atoi(entry[jj+1].c_str()));
      isCorrectlyDone=true;
    }
    break;
    
  }
  
  f.close();
  return isCorrectlyDone;
}


bool TgcPpsSsw::getWireOrStripInJtagLine(int cciId, int sswId, int rxId, std::vector<int>& wireOrStrip){
  int startId;
  int numOfSsw;
  
  if(!(getSswIdFromCciId(cciId, startId, numOfSsw))){
    std::cerr<<"err> an error was detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  if(!(sswId>=startId && sswId<(startId+numOfSsw))){ 
    for(int iJtag=0; iJtag<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; iJtag++){
      wireOrStrip.push_back(0);
    }
    return true;
  }




  std::ifstream f(FILE_NAME_PP_VALIDITY_DB);
  if(!f.is_open()){
    std::cerr<<"err> DB file dose not exist in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  std::string buf;
  std::string entry[NUM_OF_CLMN_PP_VALIDITY_DB];
  
  bool isCorrectlyDone=false;
  int cciIdReference[2]={0,0};
  int sswIdReference[2]={0,0};
  
  
  while(getline(f, buf)){
    std::stringstream line(buf);
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(!(iLine<NUM_OF_CLMN_PP_VALIDITY_DB)) break;
    }    
    if(iLine==0) continue;

    if(entry[0]=="NUM_OF_CCI"){
      cciIdReference[1]=cciIdReference[0];
      cciIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if(!(cciId<=cciIdReference[0] &&
	 cciId> cciIdReference[1] )) continue;
    
    
    if(entry[0]=="NUM_OF_SSW"){
      sswIdReference[1]=sswIdReference[0];
      sswIdReference[0]+=atoi(entry[1].c_str());
      continue;
    }
    
    if((atoi(entry[0].c_str())==0)&&(entry[0]!="0")) continue;
    
    if(!(rxId==atoi(entry[0].c_str()) &&
	 sswId< sswIdReference[0] &&
	 sswId>=sswIdReference[1] ) )continue;
    
    for(int jj=0; jj<TGC_HW_CONSTANT::NUM_OF_PPJTAG_IN_PORT; jj++){
      wireOrStrip.push_back(atoi(entry[jj+5].c_str()));
      isCorrectlyDone=true;
    }
    break;
    
  }
  
  f.close();
  return isCorrectlyDone;
}

bool TgcPpsSsw::getSswIdFromCciId(int cciId, int& startId, int& numOfSsw){
  if(cciId<1 && cciId>TGC_HW_CONSTANT::NUM_OF_CCI){
    std::cerr<<"err> invalid cciId detected in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  
  startId=TGC_HW_CONSTANT::START_ID_OF_SSW[cciId-1];
  numOfSsw=TGC_HW_CONSTANT::NUM_OF_SSW_IN_CCI[cciId-1];
  
  return true;
}

