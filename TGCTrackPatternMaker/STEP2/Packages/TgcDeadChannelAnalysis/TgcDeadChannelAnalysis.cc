#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

#include <stdlib.h>

#include <getopt.h>

#define ERROR_PRINT std::cout<<__FILE__<<":"<<__LINE__<<" "<<__PRETTY_FUNCTION__<<" "

const int inputfile_numline = 8;

static const double clusterDefinition=0.35;

static const int numberOfSides=2;
static const int numberOfSignalKinds=2;

// 2015.07.08 K.Onogi for EIFI 
//static const int numberOfLayers=7;
static const int numberOfLayers=9;

static const int numberOfAsds=20;
static const int numberOfSectorsPerSide=12;

// 2015.07.08 K.Onogi for EIFI 
//static const int numberOfPhisPerSector=12;
static const int numberOfPhisPerSector=24;

static const int numberOfRegions=2;
static const int numberOfChambers=6;

static const int deltaChannel_WD = 2;
static const int deltaChannel_SD = 2;
static const int deltaChannel_WT = 2;
static const int deltaChannel_ST = 2;
static const int deltaChannel_between_station_WT = 10;
static const int deltaChannel_between_station_ST = 4;

static const int deltaChannel_width[2][2]
= {{deltaChannel_WT, deltaChannel_ST},
   {deltaChannel_WD, deltaChannel_SD}};

static const int deltaChannel_width_between_station[2]
= {deltaChannel_between_station_WT,
   deltaChannel_between_station_ST};

static const char* SideName[numberOfSides]={"A","C"};

static const char* WireOrStrip[numberOfSignalKinds] = {"W", "S"};


// 2015.07.08 K.Onogi for EIFI
/*
static const char* ChamberName[numberOfLayers][numberOfChambers]={{"NO", "E1", "E2", "E3", "E4", "F "},
								  {"NO", "E1", "E2", "E3", "E4", "F "},
								  {"NO", "E1", "E2", "E3", "E4", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "}};
*/

static const char* ChamberName[numberOfLayers][numberOfChambers]={{"NO", "E1", "E2", "E3", "E4", "F "},
								  {"NO", "E1", "E2", "E3", "E4", "F "},
								  {"NO", "E1", "E2", "E3", "E4", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"E1", "E2", "E3", "E4", "E5", "F "},
								  {"NO", "NO", "NO", "NO", "E5", "F "},  //layer8, but This constant is not effect on generate_mask2 function. 
								  {"NO", "NO", "NO", "NO", "E5", "F "}}; //layer9


// 2015.07.08 K.Onogi for EIFI 
/*
static const int ChannelSizePerChamber[numberOfSignalKinds][numberOfLayers][numberOfChambers]={{{  0, 24, 23, 61, 92,104},
												{  0, 24, 23, 62, 91,103},
												{  0, 24, 23, 62, 91,104},
												{ 32, 32, 32,103,110,124},
												{ 32, 32, 32,102,109,124},
												{ 31, 30, 32,106, 96,121},
												{ 31, 30, 32,106, 96,121}},
											       {{  0, 32, 32, 32, 32, 32},
												{  0,  0,  0,  0,  0,  0},
												{  0, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32}}};
*/

static const int ChannelSizePerChamber[numberOfSignalKinds][numberOfLayers][numberOfChambers]={{{  0, 24, 23, 61, 92,104},
												{  0, 24, 23, 62, 91,103},
												{  0, 24, 23, 62, 91,104},
												{ 32, 32, 32,103,110,124},
												{ 32, 32, 32,102,109,124},
												{ 31, 30, 32,106, 96,121},
												{ 31, 30, 32,106, 96,121},
												{  0,  0,  0,  0, 24, 32}, // layer8, but this constant is not effect on generate_mask2 function.
												{  0,  0,  0,  0, 24, 32}},// layer9
											       {{  0, 32, 32, 32, 32, 32},
												{  0,  0,  0,  0,  0,  0},
												{  0, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32, 32, 32, 32, 32, 32},
												{ 32,  0,  0,  0,  0, 32},  //layer8
												{ 32,  0,  0,  0,  0, 32}}}; //layer9

// masked off channels for each asd
std::vector<int> channels_asd[numberOfSides][numberOfSectorsPerSide][numberOfPhisPerSector][numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfAsds];
std::vector<std::string> lines_asd[numberOfSides][numberOfSectorsPerSide][numberOfPhisPerSector][numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfAsds];

// masked off channels for each chamber
std::vector<int> channels_chamber[numberOfSides][numberOfSectorsPerSide][numberOfPhisPerSector][numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfChambers];
std::vector<std::string> lines_chamber[numberOfSides][numberOfSectorsPerSide][numberOfPhisPerSector][numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfChambers];

std::vector<std::string> outputlines_on;
std::vector<std::string> outputlines_off;

std::vector<std::string> listupMaskedChambers;

// 2015.07.08 K.Onogi for EIFI
/*
const static int 
asdBoundary[numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfAsds]=
  {
    {
      {
	// Endcap Wire
	{  7, 23, 35, 46, 60, 76, 92,107,119,135,151,167,183,199,  0,  0,  0,  0,  0,  0},
	{  7, 23, 35, 46, 61, 77, 93,108,119,135,151,167,183,199,  0,  0,  0,  0,  0,  0},
	{  7, 23, 35, 46, 61, 77, 95,102,118,134,151,167,183,199,  0,  0,  0,  0,  0,  0}, 
	{ 15, 31, 47, 63, 79, 95,102,118,134,150,166,182,198,213,229,245,261,277,293,308},
	{ 15, 31, 47, 63, 79, 95,102,118,134,150,166,182,198,213,229,245,261,277,293,308},
	{ 14, 30, 44, 60, 77, 92,102,118,134,150,166,182,198,214,230,246,262,278,294,  0}, 
	{ 14, 30, 44, 60, 77, 92,102,118,134,150,166,182,198,214,230,246,262,278,294,  0}
      },
      {
	// Endcap Strip
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}
      }
    },
    {
      {
	// Forward Wire
	{ 15, 31, 47, 63, 79, 95,104,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,103,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,104,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 12, 28, 44, 60, 76, 92,108,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 12, 28, 44, 60, 76, 92,108,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  9, 25, 41, 57, 73, 89,105,121,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  9, 25, 41, 57, 73, 89,105,121,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}
      },
      {
	// Forward Strip
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}
      }
    }
  };
*/
const static int 
asdBoundary[numberOfRegions][numberOfSignalKinds][numberOfLayers][numberOfAsds]=
  {
    {
      {
	// Endcap Wire
	{  7, 23, 35, 46, 60, 76, 92,107,119,135,151,167,183,199,  0,  0,  0,  0,  0,  0},
	{  7, 23, 35, 46, 61, 77, 93,108,119,135,151,167,183,199,  0,  0,  0,  0,  0,  0},
	{  7, 23, 35, 46, 61, 77, 95,102,118,134,151,167,183,199,  0,  0,  0,  0,  0,  0}, 
	{ 15, 31, 47, 63, 79, 95,102,118,134,150,166,182,198,213,229,245,261,277,293,308},
	{ 15, 31, 47, 63, 79, 95,102,118,134,150,166,182,198,213,229,245,261,277,293,308},
	{ 14, 30, 44, 60, 77, 92,102,118,134,150,166,182,198,214,230,246,262,278,294,  0}, 
	{ 14, 30, 44, 60, 77, 92,102,118,134,150,166,182,198,214,230,246,262,278,294,  0}, 
	{  7, 23,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //layer8
	{  7, 23,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}  //layer9
      },
      {
	// Endcap Strip
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,111,127,143,159,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //layer8
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}  //layer9
      }
    },
    {
      {
	// Forward Wire
	{ 15, 31, 47, 63, 79, 95,104,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,103,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31, 47, 63, 79, 95,104,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 12, 28, 44, 60, 76, 92,108,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 12, 28, 44, 60, 76, 92,108,124,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  9, 25, 41, 57, 73, 89,105,121,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  9, 25, 41, 57, 73, 89,105,121,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //layer8
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}  //layer9
      },
      {
	// Forward Strip
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0},
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}, //layer8
	{ 15, 31,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0}  //layer9
      }
    }
  };

void
generateMask2List();

void
generateBcidMaskList();

// 2015.07.08 K.Onogi for EIFI
/*
int
returnTotalChannelSize_PerASD(const int& isForward,
			      const int& isStrip,
			      const int& iLayer,
			      const int& iAsd)
{
  int rc = (iAsd==0) ? 
    asdBoundary[isForward][isStrip][iLayer][iAsd] + 1: 
    asdBoundary[isForward][isStrip][iLayer][iAsd]  - asdBoundary[isForward][isStrip][iLayer][iAsd-1] ;
  
  return rc;
  
}
*/
int
returnTotalChannelSize_PerASD(const int& isForward,
			      const int& isStrip,
			      const int& iLayer,
			      const int& iAsd,
                              const int& iPhi)
{
  int rc = 0;
  
  if(iLayer < 7){
    rc = (iAsd==0) ? 
      asdBoundary[isForward][isStrip][iLayer][iAsd] + 1: 
      asdBoundary[isForward][isStrip][iLayer][iAsd]  - asdBoundary[isForward][isStrip][iLayer][iAsd-1] ;
  }else if(isStrip == 1){
    rc = (iAsd==0) ? 
      asdBoundary[isForward][isStrip][iLayer][iAsd] + 1: 
      asdBoundary[isForward][isStrip][iLayer][iAsd]  - asdBoundary[isForward][isStrip][iLayer][iAsd-1] ;
  }else if(isForward == 1 && (iPhi == 2 || iPhi == 5 || iPhi == 8 || iPhi == 11 || iPhi == 17 || iPhi == 20)){
    rc = (iAsd==0) ? 
      asdBoundary[isForward][isStrip][iLayer][iAsd] + 1 - 2: // 15 - 2 = 13 channels/ASD 
      asdBoundary[isForward][isStrip][iLayer][iAsd]  - asdBoundary[isForward][isStrip][iLayer][iAsd-1] ;
  }else if(isForward == 0 && (iPhi == 0 || iPhi == 2 || iPhi == 12 || iPhi == 14 || iPhi == 15 || iPhi == 16 || iPhi == 22 || iPhi == 23)){
    rc = (iAsd==0) ? 
      asdBoundary[isForward][isStrip][iLayer][iAsd] + 1: 
      asdBoundary[isForward][isStrip][iLayer][iAsd]  - asdBoundary[isForward][isStrip][iLayer][iAsd-1] - 6; // 17 - 8 = 9channels/ASD
  }

  return rc;  
}

int
returnTotalChannelSize_PerChamber(const int& isForward,
				  const int& isStrip,
				  const int& iLayer,
				  const int& iChamber)
{
  return ChannelSizePerChamber[isStrip][iLayer][iChamber+(isForward*5)];
}

// 2015.07.08 K.Onogi for EIFI
/*
int
CheckNeighborhood(const int& iSide,
		  const int& iSector,
		  const int& iPhi,
		  const int& isForward,
		  const int& isStrip,
		  const int& iLayer,
		  const int& iAsd,
		  bool& maskStatus)
{
  // if the channel is wire, 
  // see the neighboring ASDs.
  int numberOfCheckedASDsPerLayer;
  if (isStrip) {
    numberOfCheckedASDsPerLayer = 1;
  } else {
    numberOfCheckedASDsPerLayer = 3;
  }
  static const int AsdIndexOffSet[] = {0, -1, 1};
  
  int rc=0;
  
  for (int range=0; range<numberOfCheckedASDsPerLayer; range++) {
    int indexNeighboringAsd = iAsd + AsdIndexOffSet[range];
    int nDeadLayers = 0;
    
    if (iLayer<3) { // triplet
      for (int iNeighbor=0; iNeighbor<2; iNeighbor++) {
	const int layer_id_neighbor = ((iLayer+iNeighbor+1)%3);
	if (layer_id_neighbor<0 or layer_id_neighbor>=numberOfAsds) continue;
	
	const int channelNumber=channels_asd[iSide][iSector][iPhi][isForward][isStrip][layer_id_neighbor][indexNeighboringAsd].size();
	const int totalChannelNumber=returnTotalChannelSize_PerASD(isForward,
								   isStrip,
								   layer_id_neighbor,
								   indexNeighboringAsd);
	
	double percentage=(double)channelNumber/(double)totalChannelNumber;
	
	if (percentage>clusterDefinition) {
	  nDeadLayers++;
	  maskStatus=(iLayer<layer_id_neighbor); // masked on layer is defined with layer ID
	}
      }
    }
    
    else if (iLayer<7) { // Doublet
      for (int iNeighbor=0; iNeighbor<3; iNeighbor++) {
	const int layer_id_neighbor = ((iLayer+iNeighbor-2)%4)+3;
	
	const int channelNumber=channels_asd[iSide][iSector][iPhi][isForward][isStrip][layer_id_neighbor][indexNeighboringAsd].size();
	const int totalChannelNumber=returnTotalChannelSize_PerASD(isForward,
								   isStrip,
								   layer_id_neighbor,
								   indexNeighboringAsd);
	
	double percentage=(double)channelNumber/(double)totalChannelNumber;
	
	if (percentage>clusterDefinition) {
	  nDeadLayers++;
	  maskStatus=(iLayer<layer_id_neighbor); // masked on layer is defined with layer ID
	}
      }
    }
    
    if (rc<nDeadLayers) { rc = nDeadLayers; }
  }
  
  return rc;
}
*/

int
CheckNeighborhood(const int& iSide,
		  const int& iSector,
		  const int& iPhi,
		  const int& isForward,
		  const int& isStrip,
		  const int& iLayer,
		  const int& iAsd,
		  bool& maskStatus)
{
  // if the channel is wire, 
  // see the neighboring ASDs.
  int numberOfCheckedASDsPerLayer;
  if (isStrip) {
    numberOfCheckedASDsPerLayer = 1;
  } else {
    numberOfCheckedASDsPerLayer = 3;
  }
  static const int AsdIndexOffSet[] = {0, -1, 1};
  
  int rc=0;
  
  for (int range=0; range<numberOfCheckedASDsPerLayer; range++) {
    int indexNeighboringAsd = iAsd + AsdIndexOffSet[range];
    int nDeadLayers = 0;
    
    if (iLayer<3) { // triplet
      for (int iNeighbor=0; iNeighbor<2; iNeighbor++) {
	const int layer_id_neighbor = ((iLayer+iNeighbor+1)%3);
	if (layer_id_neighbor<0 or layer_id_neighbor>=numberOfAsds) continue;
	
	const int channelNumber=channels_asd[iSide][iSector][iPhi][isForward][isStrip][layer_id_neighbor][indexNeighboringAsd].size();
	const int totalChannelNumber=returnTotalChannelSize_PerASD(isForward,
								   isStrip,
								   layer_id_neighbor,
								   indexNeighboringAsd,
                                                                   iPhi);//added iPhi
	
	double percentage=(double)channelNumber/(double)totalChannelNumber;
	
	if (percentage>clusterDefinition) {
	  nDeadLayers++;
	  maskStatus=(iLayer<layer_id_neighbor); // masked on layer is defined with layer ID
	}
      }
    }
    
    else if (iLayer<7) { // Doublet
      for (int iNeighbor=0; iNeighbor<3; iNeighbor++) {
	const int layer_id_neighbor = ((iLayer+iNeighbor-2)%4)+3;
	
	const int channelNumber=channels_asd[iSide][iSector][iPhi][isForward][isStrip][layer_id_neighbor][indexNeighboringAsd].size();
	const int totalChannelNumber=returnTotalChannelSize_PerASD(isForward,
								   isStrip,
								   layer_id_neighbor,
								   indexNeighboringAsd,
                                                                   iPhi);//added iPhi
	
	double percentage=(double)channelNumber/(double)totalChannelNumber;
	
	if (percentage>clusterDefinition) {
	  nDeadLayers++;
	  maskStatus=(iLayer<layer_id_neighbor); // masked on layer is defined with layer ID
	}
      }
    }
    
    else if (iLayer<9) { // EIFI
      for (int iNeighbor=0; iNeighbor<2; iNeighbor++) {
	const int layer_id_neighbor = ((iLayer+iNeighbor+1)%2)+8;
	
	if (layer_id_neighbor<0 or layer_id_neighbor>=numberOfAsds) continue;

	const int channelNumber=channels_asd[iSide][iSector][iPhi][isForward][isStrip][layer_id_neighbor][indexNeighboringAsd].size();
	const int totalChannelNumber=returnTotalChannelSize_PerASD(isForward,
								   isStrip,
								   layer_id_neighbor,
								   indexNeighboringAsd,
                                                                   iPhi);//added iPhi
	
	double percentage=(double)channelNumber/(double)totalChannelNumber;
	
	if (percentage>clusterDefinition) {
	  nDeadLayers++;
	  maskStatus=(iLayer<layer_id_neighbor); // masked on layer is defined with layer ID
	}
      }
    }

    if (rc<nDeadLayers) { rc = nDeadLayers; }
  }
  
  return rc;
}

int 
returnAsdId(const int& chan,
	    const int& isForward,
	    const int& isStrip,
	    const int& layerId) {
  
  for (int iAsd=0; iAsd<numberOfAsds; iAsd++) {
    if (asdBoundary[isForward][isStrip][layerId-1][iAsd]>=chan) return iAsd;
  }
  
  return -1;
}

void
showUsage()
{
  printf("prog. -i <input file> -o <output file> \n"
	 "prog. -i <input file> -o <output file> -M, to list masked off chmabers up\n");
}

bool
listUp(const std::string* inputFileName);

int
main(int argc, char* argv[])
{
  outputlines_on.push_back(std::string("BIT_DEFINITION 1"));
  outputlines_off.push_back(std::string("BIT_DEFINITION 0"));
  
  std::string* inputfileName = 0;
  std::string* outputfileName = new std::string("test.txt");
  
  bool doMask2=true;
  bool doChamberList=false;
  
  int c;
  while ((c = getopt(argc, argv, "i:o:hMC")) != -1) {
    switch (c) {
    case 'i':
      inputfileName = new std::string(optarg);
      break;
    case 'o':
      outputfileName = new std::string(optarg);
      break;
    case 'M': // Mask2
      doMask2=true;
      doChamberList=false;
      break;
    case 'C': // Chamber
      doMask2=false;
      doChamberList=true;
      break;
    case 'h':
    defalt:
      showUsage();
      return EXIT_SUCCESS;
      
    }
  }
  
  std::ofstream output_file(outputfileName->c_str());
  

  if (not (listUp(inputfileName))) return EXIT_FAILURE;  

  if (doMask2) {
    generateMask2List();
    
    for (int iline=0, output_numline=outputlines_on.size(); iline<output_numline; iline++) {
      output_file<<outputlines_on.at(iline)<<std::endl;
    }
    
    for (int iline=0, output_numline=outputlines_off.size(); iline<output_numline; iline++) {
      output_file<<outputlines_off.at(iline)<<std::endl;
    } 
  }
  if (doChamberList){
    if (not (listUp(inputfileName))) return EXIT_FAILURE;
    generateBcidMaskList();
    
    for (int iline=0, output_numline=listupMaskedChambers.size(); iline<output_numline; iline++) {
      output_file<<listupMaskedChambers.at(iline)<<std::endl;
    }
  }
  
  return EXIT_SUCCESS;
}

// 2015.07.08 K.Onogi for EIFI
/*
bool
listUp(const std::string* inputFileName)
{
  char buf[BUFSIZ];
  
  if (inputFileName==0) {
    ERROR_PRINT << " null file name is refered to opne input file." << std::endl;
    return false;
  }
  std::ifstream inputfile(inputFileName->c_str());
  
  if (not (inputfile.is_open())) {
    ERROR_PRINT << " file does not exist " <<  inputFileName << std::endl;
    return false;    
  }
  
  std::string entry[inputfile_numline];
  
  while(not (inputfile.eof())){
    inputfile.getline(buf, sizeof(buf));
    
    std::string contents(buf);
    
    int sharpPoint=contents.find("#",0);
    if(sharpPoint!=std::string::npos){
      contents=contents.substr(0,sharpPoint);
    }
    
    std::stringstream line(contents);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(not (iLine<inputfile_numline)) break;
    }    

    if(iLine!=inputfile_numline) continue; 
    
    int isCside = (entry[0]=="A") ? 0 : 1;
    int sector = strtoul(entry[1].c_str(), NULL, 10);
    int phi = strtoul(entry[2].c_str(), NULL, 10);
    int isForward = (entry[3]=="E") ? 0 : 1;
    int isStrip = (entry[4]=="W") ? 0 : 1;
    int layerId = strtoul(entry[5].c_str(), NULL, 10);
    int rId = strtoul(entry[6].c_str(), NULL, 10);
    int chamberId = strtoul(entry[6].c_str(), NULL, 10);
    if ((layerId<=3) && (isForward==0)) {
      rId=rId-1;
    }
    int chan = (isStrip==0) ? strtoul(entry[7].c_str(), NULL, 10) : rId*32 + strtoul(entry[7].c_str(), NULL, 10);
    
    int asdId=returnAsdId(chan,
			  isForward,
			  isStrip,
			  layerId);
    
    
    int nChannels=channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].size();
    
    // avoid duplication
    bool alreadyRegistered=false;
    for (int iChannel=0; iChannel<nChannels; iChannel++) {
      if (chan == channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].at(iChannel)) {
	alreadyRegistered=true;
      }
    }
    
    if (alreadyRegistered) continue;
    
    channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].push_back(chan);
    lines_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].push_back(contents);

    channels_chamber[isCside][sector-1][phi][isForward][isStrip][layerId-1][chamberId].push_back(chan);
    lines_chamber[isCside][sector-1][phi][isForward][isStrip][layerId-1][chamberId].push_back(contents);
  }
  
  return true;
}
*/

bool
listUp(const std::string* inputFileName)
{
  char buf[BUFSIZ];
  
  if (inputFileName==0) {
    ERROR_PRINT << " null file name is refered to opne input file." << std::endl;
    return false;
  }
  std::ifstream inputfile(inputFileName->c_str());
  
  if (not (inputfile.is_open())) {
    ERROR_PRINT << " file does not exist " <<  inputFileName << std::endl;
    return false;    
  }
  
  std::string entry[inputfile_numline];
  
  while(not (inputfile.eof())){
    inputfile.getline(buf, sizeof(buf));
    
    std::string contents(buf);
    
    int sharpPoint=contents.find("#",0);
    if(sharpPoint!=std::string::npos){
      contents=contents.substr(0,sharpPoint);
    }
    
    std::stringstream line(contents);
    
    int iLine=0;  
    while(line>>entry[iLine]){
      iLine++;
      if(not (iLine<inputfile_numline)) break;
    }    

    if(iLine!=inputfile_numline) continue; 
    
    int isCside = (entry[0]=="A") ? 0 : 1;
    int sector = strtoul(entry[1].c_str(), NULL, 10);
    int phi = strtoul(entry[2].c_str(), NULL, 10);
    int isForward = (entry[3]=="E") ? 0 : 1;
    int isStrip = (entry[4]=="W") ? 0 : 1;
    int layerId = strtoul(entry[5].c_str(), NULL, 10);
    int rId = strtoul(entry[6].c_str(), NULL, 10);
    int chamberId = strtoul(entry[6].c_str(), NULL, 10);
    if ((layerId<=3) && (isForward==0)) {
      rId=rId-1;
    }else if(layerId > 7){
      rId=0;
    }

    int chan = (isStrip==0) ? strtoul(entry[7].c_str(), NULL, 10) : rId*32 + strtoul(entry[7].c_str(), NULL, 10);
    
    int asdId=returnAsdId(chan,
			  isForward,
			  isStrip,
			  layerId);
    
    
    int nChannels=channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].size();
    
    // avoid duplication
    bool alreadyRegistered=false;
    for (int iChannel=0; iChannel<nChannels; iChannel++) {
      if (chan == channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].at(iChannel)) {
	alreadyRegistered=true;
      }
    }
    
    if (alreadyRegistered) continue;
    
    channels_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].push_back(chan);
    lines_asd[isCside][sector-1][phi][isForward][isStrip][layerId-1][asdId].push_back(contents);

    channels_chamber[isCside][sector-1][phi][isForward][isStrip][layerId-1][chamberId].push_back(chan);
    lines_chamber[isCside][sector-1][phi][isForward][isStrip][layerId-1][chamberId].push_back(contents);
  }
  
  return true;
}

// 2015.07.08 K.Onogi for EIFI
/*
void
generateMask2List() {
  std::cout << "################################################################" << std::endl;
  int counter = 0;
  
  for (int iSide=0; iSide<numberOfSides; iSide++) {
    for (int iSector=0; iSector<numberOfSectorsPerSide; iSector++) {
      for (int iPhi=0; iPhi<numberOfPhisPerSector; iPhi++) {
	for (int isForward=0; isForward<numberOfRegions; isForward++) {
	  for (int isStrip=0; isStrip<numberOfSignalKinds; isStrip++) {
	    for (int iLayer=0; iLayer<numberOfLayers; iLayer++) {
	      for (int iAsd=0; iAsd<numberOfAsds; iAsd++) {
		
		int nChannels=channels_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].size();
		if (nChannels==0) continue;
		
		int totalChannelSize = returnTotalChannelSize_PerASD(isForward,
								     isStrip,
								     iLayer,
								     iAsd);
		
#ifdef __DEBUG__MODE__		
		std::cout << " iSide:" << iSide << " iSector:" << iSector << " isForwrad:" << isForward 
			  << " isStrip:" << isStrip << " iLayer:" << iLayer << " iAsd:" << iAsd << " :::: " << nChannels << "/" << totalChannelSize
			  << " " << asdBoundary[isForward][isStrip][iLayer][iAsd]  << " " << asdBoundary[isForward][isStrip][iLayer][iAsd-1] << std::endl;
#endif
		
		if (((double)nChannels/(double)totalChannelSize)<clusterDefinition) continue;
		
		//if (iSide==1 && iSector==8) {
		//ERROR_PRINT << iSide << " " << iSector 
		//<< " " << iPhi << " " << iLayer << std::endl;
		//}

		int deadChannelsInASD=0;
		
		for (int iChannel=0; iChannel<nChannels; iChannel++) {
		  std::stringstream line(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		  
		  std::string entry[inputfile_numline];
		  
		  int iElement=0;
		  
		  while(line>>entry[iElement]){
		    if(not (iElement<inputfile_numline)) break;
		    iElement++;
		  }
		  
		  int channel = strtoul(entry[7].c_str(), NULL, 0);
		  bool maskStatus=false;
		  int isDoublet=(iLayer<3) ? 0 : 1;
		  
		  int coincidenceWidth=deltaChannel_width[isDoublet][isStrip];
		  
		  int numberOfMaskedNeighberhood=CheckNeighborhood(iSide,
								   iSector,
								   iPhi,
								   isForward,
								   isStrip,
								   iLayer,
								   iAsd,
								   maskStatus);
		  
		  if ( ((isDoublet==0) and (numberOfMaskedNeighberhood==2) and (isStrip==0)) or
		       ((isDoublet==0) and (numberOfMaskedNeighberhood==1) and (isStrip==1))
		       ) {
		    
		    deadChannelsInASD++;
		    counter++;
		    //std::cout << "inf> there is a dead trigger region. side=" << iSide 
		    //std::cout << "sector=" << (iSector+1) << " phi=" << iPhi << " isForward=" << isForward << " iLayer=" << (iLayer+1) << " iAsd=" << iAsd << " isStrip=" << isStrip << " counter=" << counter << std::endl;
		    
#ifdef __USE_OLD_TREATMENT_FOR_TRIPLET__
		    if (channel%deltaChannel_width_between_station[isStrip]==0) {
		      outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));		    
		    }
		    else {
		      outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		    }
#endif //#ifndef __USE_OLD_TREATMENT_FOR_TRIPLET__
		  }
		  else if (numberOfMaskedNeighberhood==1) {
		    if ((maskStatus)&&((channel%coincidenceWidth)==0)) {
		      outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		    }
		    else {
		      outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		    }		    
		  }
		  
		  else if ((isDoublet==1) and (numberOfMaskedNeighberhood>=2)) { // to handle regions with more than 3 layers dead.
		    if (iLayer%2==0) {
		      outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		    }
		    else {
		      if (channel%deltaChannel_width_between_station[isStrip]==0) {
			outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		      }
		      else {
			outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		      }
		    }
		  }
		}
		if (deadChannelsInASD!=0) {
		  std::string side_name  = (iSide==0) ? "A" : "C";
		  std::string wire_strig = (isStrip==0) ? "W" : "S";
		  std::string end_for    = (isForward==0) ? "E" : "F";
		  
		  printf("%s%02d %s %s phi=%1d layer=%1d ASD=%2d: %2d channels \n", side_name.c_str(), (iSector+1), end_for.c_str(), wire_strig.c_str(), iPhi, (iLayer+1), iAsd, deadChannelsInASD);
		  //std::cout << "sector=" <<  << " phi=" <<  << " isForward=" << isForward 
		  //<< " iLayer=" <<  << " isStrip=" << isStrip << " iAsd=" <<  << " channels." << std::endl;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  std::cout << "################################################################" << std::endl;  
  std::cout << counter << " channels are corresponding to dead trigger region." << std::endl;
  std::cout << "################################################################" << std::endl;  
  
  return;
}
*/

void
generateMask2List() {
  std::cout << "################################################################" << std::endl;
  int counter = 0;
  
  for (int iSide=0; iSide<numberOfSides; iSide++) {
    for (int iSector=0; iSector<numberOfSectorsPerSide; iSector++) {
      for (int iPhi=0; iPhi<numberOfPhisPerSector; iPhi++) {
	for (int isForward=0; isForward<numberOfRegions; isForward++) {
	  for (int isStrip=0; isStrip<numberOfSignalKinds; isStrip++) {
	    for (int iLayer=0; iLayer<numberOfLayers; iLayer++) {
	      for (int iAsd=0; iAsd<numberOfAsds; iAsd++) {
		
		int nChannels=channels_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].size();
		if (nChannels==0) continue;
		
		int totalChannelSize = returnTotalChannelSize_PerASD(isForward,
								     isStrip,
								     iLayer,
								     iAsd,
                                                                     iPhi);
		
#ifdef __DEBUG__MODE__		
		std::cout << " iSide:" << iSide << " iSector:" << iSector << " isForwrad:" << isForward 
			  << " isStrip:" << isStrip << " iLayer:" << iLayer << " iAsd:" << iAsd << " :::: " << nChannels << "/" << totalChannelSize
			  << " " << asdBoundary[isForward][isStrip][iLayer][iAsd]  << " " << asdBoundary[isForward][isStrip][iLayer][iAsd-1] << std::endl;
#endif
		
		if (((double)nChannels/(double)totalChannelSize)<clusterDefinition) continue;
		
		//if (iSide==1 && iSector==8) {
		//ERROR_PRINT << iSide << " " << iSector 
		//<< " " << iPhi << " " << iLayer << std::endl;
		//}

		int deadChannelsInASD=0;
		
		for (int iChannel=0; iChannel<nChannels; iChannel++) {
		  std::stringstream line(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		  
		  std::string entry[inputfile_numline];
		  
		  int iElement=0;
		  
		  while(line>>entry[iElement]){
		    if(not (iElement<inputfile_numline)) break;
		    iElement++;
		  }
		  
		  int channel = strtoul(entry[7].c_str(), NULL, 0);
		  bool maskStatus=false;
		  int isDoublet=(iLayer<3) ? 0 : 1;
		  
		  int coincidenceWidth=deltaChannel_width[isDoublet][isStrip];

                  if(iLayer > 6){
		    coincidenceWidth=deltaChannel_width[isDoublet][isStrip];
		  }

		  int numberOfMaskedNeighberhood=CheckNeighborhood(iSide,
								   iSector,
								   iPhi,
								   isForward,
								   isStrip,
								   iLayer,
								   iAsd,
								   maskStatus);

                  //for EIFI
                  if(iLayer > 6){
                      if (numberOfMaskedNeighberhood==1) {
                          if ((maskStatus)&&((channel%coincidenceWidth)==0)) {
                              outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
                          }else{
                              outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
                          }                   
                      }
                  // for BW (no changed)
                  }else{
		  
		      if ( ((isDoublet==0) and (numberOfMaskedNeighberhood==2) and (isStrip==0)) or
		           ((isDoublet==0) and (numberOfMaskedNeighberhood==1) and (isStrip==1))
		         ) {
		    
		        deadChannelsInASD++;
		        counter++;
		    //std::cout << "inf> there is a dead trigger region. side=" << iSide 
		    //std::cout << "sector=" << (iSector+1) << " phi=" << iPhi << " isForward=" << isForward << " iLayer=" << (iLayer+1) << " iAsd=" << iAsd << " isStrip=" << isStrip << " counter=" << counter << std::endl;
		    
#ifdef __USE_OLD_TREATMENT_FOR_TRIPLET__
		        if (channel%deltaChannel_width_between_station[isStrip]==0) {
		            outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));		    
		        }
		        else {
		            outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		        }
#endif //#ifndef __USE_OLD_TREATMENT_FOR_TRIPLET__
		      }
		      else if (numberOfMaskedNeighberhood==1) {
		        if ((maskStatus)&&((channel%coincidenceWidth)==0)) {
		            outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		        }
		        else {
		            outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		        }		    
		      }
		  
		      else if ((isDoublet==1) and (numberOfMaskedNeighberhood>=2)) { // to handle regions with more than 3 layers dead.
		        if (iLayer%2==0) {
		            outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		        }
		        else {
		            if (channel%deltaChannel_width_between_station[isStrip]==0) {
			        outputlines_on.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		            }
		            else {
			        outputlines_off.push_back(lines_asd[iSide][iSector][iPhi][isForward][isStrip][iLayer][iAsd].at(iChannel));
		            }
		        }
		      }
		   }
                }

		if (deadChannelsInASD!=0) {
		  std::string side_name  = (iSide==0) ? "A" : "C";
		  std::string wire_strig = (isStrip==0) ? "W" : "S";
		  std::string end_for    = (isForward==0) ? "E" : "F";
		  
		  printf("%s%02d %s %s phi=%1d layer=%1d ASD=%2d: %2d channels \n", side_name.c_str(), (iSector+1), end_for.c_str(), wire_strig.c_str(), iPhi, (iLayer+1), iAsd, deadChannelsInASD);
		  //std::cout << "sector=" <<  << " phi=" <<  << " isForward=" << isForward 
		  //<< " iLayer=" <<  << " isStrip=" << isStrip << " iAsd=" <<  << " channels." << std::endl;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  
  std::cout << "################################################################" << std::endl;  
  std::cout << counter << " channels are corresponding to dead trigger region." << std::endl;
  std::cout << "################################################################" << std::endl;  
  
  return;
}

void
generateBcidMaskList() {
  char buf[BUFSIZ];
  
  for (int iSide=0; iSide<numberOfSides; iSide++) {
    for (int iSector=0; iSector<numberOfSectorsPerSide; iSector++) {
      for (int iPhi=0; iPhi<numberOfPhisPerSector; iPhi++) {
	for (int iLayer=0; iLayer<numberOfLayers; iLayer++) {
	  for (int isForward=0; isForward<numberOfRegions; isForward++) {
	    for (int isStrip=0; isStrip<numberOfSignalKinds; isStrip++) {
	      for (int iChamber=0; iChamber<numberOfChambers; iChamber++) {
		
		int nChannels=channels_chamber[iSide][iSector][iPhi][isForward][isStrip][iLayer][iChamber].size();
		if (nChannels==0) continue;
		
		int totalChannelSize = returnTotalChannelSize_PerChamber(isForward,
									 isStrip,
									 iLayer,
									 iChamber);
		
#ifdef __DEBUG__MODE__		
		std::cout << " iSide:" << iSide << " iSector:" << iSector << " isForwrad:" << isForward 
			  << " isStrip:" << isStrip << " iLayer:" << iLayer << " iChamber:" << iChamber 
			  << " :::: " << nChannels << "/" << totalChannelSize << std::endl;
#endif
		
		for (int iChannel=0; iChannel<nChannels; iChannel++) {
		  std::stringstream line(lines_chamber[iSide][iSector][iPhi][isForward][isStrip][iLayer][iChamber].at(iChannel));
		  
		  std::string entry[inputfile_numline];
		  
		  int iElement=0;
		  
		  while(line>>entry[iElement]){
		    if(not (iElement<inputfile_numline)) break;
		    iElement++;
		  }
		  
		  int chamber = strtoul(entry[6].c_str(), NULL, 0);
		  int channel = strtoul(entry[7].c_str(), NULL, 0);
		  bool maskStatus=false;
		  int isDoublet=(iLayer<3) ? 0 : 1;
		  
		  
		  snprintf(buf, sizeof(buf), 
			   "%s %02d phi%d L%d %s %s %3d %3d",
			   SideName[iSide], (iSector+1), iPhi, (iLayer+1), ChamberName[iLayer][chamber+isForward*5], 
			   WireOrStrip[isStrip], nChannels, totalChannelSize);
		  listupMaskedChambers.push_back(std::string(buf));
		  break;
		}
	      }
	    }
	  }
	}
      }
    }
  }
  return;
}
