#include "TGCcabling12/TGCChannelId.h"

namespace LVL1TGCCabling12 {

bool TGCChannelId::operator ==(const TGCChannelId& channelId) const
{
  if((this->getChannelIdType()==channelId.getChannelIdType())&&
     (this->getSideType()     ==channelId.getSideType())     &&
     (this->getRegionType()   ==channelId.getRegionType())   &&
     (this->getSignalType()   ==channelId.getSignalType())   &&
     (this->getModuleType()   ==channelId.getModuleType())   &&
     (this->getSector()       ==channelId.getSector())       &&
     (this->getLayer()        ==channelId.getLayer())        &&
     (this->getChamber()      ==channelId.getChamber())      &&
     (this->getId()           ==channelId.getId())           &&
     (this->getBlock()        ==channelId.getBlock())        &&
     (this->getChannel()      ==channelId.getChannel())      )
    return true;
  return false;
}

} //end of namespace
