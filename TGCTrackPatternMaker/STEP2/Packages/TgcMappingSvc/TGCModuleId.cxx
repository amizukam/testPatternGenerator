#include "TGCcabling12/TGCModuleId.h"

namespace LVL1TGCCabling12 {

bool TGCModuleId::operator ==(const TGCModuleId& moduleId) const
{
  if((this->getModuleIdType()==moduleId.getModuleIdType())&&
     (this->getSideType()    ==moduleId.getSideType())    &&
     (this->getRegionType()  ==moduleId.getRegionType())  &&
     (this->getSignalType()  ==moduleId.getSignalType())  &&
     (this->getModuleType()  ==moduleId.getModuleType())  &&
     (this->getSector()      ==moduleId.getSector())      &&
     (this->getOctant()      ==moduleId.getOctant())      &&
     (this->getId()          ==moduleId.getId())          )
    return true;
  return false;
}

} //end of namespace
