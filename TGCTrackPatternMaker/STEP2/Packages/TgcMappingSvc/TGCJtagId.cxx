#include <string>
#include "TGCcabling12/TGCJtagId.h"

namespace LVL1TGCCabling12 {
  bool TGCJtagId::setJtagName(std::string tmp_name)
  {
    this->jtagName=tmp_name;
    return true;
  }
  
  bool TGCJtagId::setJtagId(int tmpJtagId)
  {
    this->jtagId=tmpJtagId;
    return true;    
  }
  
  bool TGCJtagId::setPsTypeId(int tmpPsTypeId)
  {
    this->psTypeId=tmpPsTypeId;
    return true;
  }
  
  bool TGCJtagId::setOrder(int tmpOrder)
  {
    this->order=tmpOrder;
    return true;
  }
  
  bool TGCJtagId::setMax(int tmpMax)
  {
    this->max=tmpMax;
    return true;    
  }
  
  bool TGCJtagId::setAorB(int tmpAorB)
  {
    this->aorB=tmpAorB;
    return true;
  }

  bool TGCJtagId::setChanMax(int tmpChanMax)
  {
    this->chanMax=tmpChanMax;
    return true;    
  }

  bool TGCJtagId::setChanMin(int tmpChanMin)
  {
    this->chanMin=tmpChanMin;
    return true;
  }
  
  bool TGCJtagId::setSide(int tmpSide)
  {
    this->side=tmpSide;
    return true;
  }
  
  bool TGCJtagId::setPhi(int tmpPhi)
  {
    this->phi=tmpPhi;
    return true;
  }

  
  bool TGCJtagId::setLayer(int tmpLayer)
  {
    this->layer=tmpLayer;
    return true;
  }
  
  bool TGCJtagId::setChamber(int tmpChamber)
  {
    this->chamber=tmpChamber;
    return true;
  }
  
  std::string TGCJtagId::getJtagName(){
    return this->jtagName;
  }
  
  int TGCJtagId::getJtagId(){
    return this->jtagId;
  }
  
  int TGCJtagId::getPsTypeId(){
    return this->psTypeId;
  }
  
  int TGCJtagId::getOrder(){
    return this->order;
  }
  
  int TGCJtagId::getMax(){
    return this->max;
  }
  
  int TGCJtagId::getAorB(){
    return this->aorB;
  }
  
  int TGCJtagId::getChanMax(){
    return this->chanMax;    
  }
  
  int TGCJtagId::getChanMin(){
    return this->chanMin;
  }  
  
  int TGCJtagId::getSide(){
    return this->side;
  }
  
  int TGCJtagId::getLayer(){
    return this->layer;
  }
  
  int TGCJtagId::getChamber(){
    return this->chamber;
  }
  
  
  
}
