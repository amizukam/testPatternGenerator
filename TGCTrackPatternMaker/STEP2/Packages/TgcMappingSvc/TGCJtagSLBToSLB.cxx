#include "TGCcabling12/TGCJtagSLBToSLB.h"

namespace LVL1TGCCabling12 {

// Constructor & Destructor
TGCJtagSLBToSLB::TGCJtagSLBToSLB (std::string filename)
{
  database[TGCIdBase::Endcap][TGCIdBase::WD] = 
    new TGCDatabaseSLBToSLBJTAG(filename,"EWD");
  database[TGCIdBase::Endcap][TGCIdBase::WT] = 
    new TGCDatabaseSLBToSLBJTAG(filename,"EWT");
  database[TGCIdBase::Endcap][TGCIdBase::SD] =
    new TGCDatabaseSLBToSLBJTAG(filename,"ESD");
  database[TGCIdBase::Endcap][TGCIdBase::ST] =
    new TGCDatabaseSLBToSLBJTAG(filename,"EST");
  database[TGCIdBase::Endcap][TGCIdBase::WI] =
    new TGCDatabaseSLBToSLBJTAG(filename,"EWI");
  database[TGCIdBase::Endcap][TGCIdBase::SI] =
    new TGCDatabaseSLBToSLBJTAG(filename,"ESI");
  database[TGCIdBase::Forward][TGCIdBase::WD] = 
    new TGCDatabaseSLBToSLBJTAG(filename,"FWD");
  database[TGCIdBase::Forward][TGCIdBase::WT] =
    new TGCDatabaseSLBToSLBJTAG(filename,"FWT");
  database[TGCIdBase::Forward][TGCIdBase::SD] =
    new TGCDatabaseSLBToSLBJTAG(filename,"FSD");
  database[TGCIdBase::Forward][TGCIdBase::ST] = 
    new TGCDatabaseSLBToSLBJTAG(filename,"FST");
  database[TGCIdBase::Forward][TGCIdBase::WI] =
    new TGCDatabaseSLBToSLBJTAG(filename,"FWI");
  database[TGCIdBase::Forward][TGCIdBase::SI] = 
    new TGCDatabaseSLBToSLBJTAG(filename,"FSI");
}
  
TGCJtagSLBToSLB::~TGCJtagSLBToSLB (void)
{
  delete database[TGCIdBase::Endcap][TGCIdBase::WD];
  delete database[TGCIdBase::Endcap][TGCIdBase::WT];
  delete database[TGCIdBase::Endcap][TGCIdBase::SD];
  delete database[TGCIdBase::Endcap][TGCIdBase::ST];
  delete database[TGCIdBase::Endcap][TGCIdBase::WI];
  delete database[TGCIdBase::Endcap][TGCIdBase::SI];
  delete database[TGCIdBase::Forward][TGCIdBase::WD];
  delete database[TGCIdBase::Forward][TGCIdBase::WT];
  delete database[TGCIdBase::Forward][TGCIdBase::SD];
  delete database[TGCIdBase::Forward][TGCIdBase::ST];
  delete database[TGCIdBase::Forward][TGCIdBase::WI];
  delete database[TGCIdBase::Forward][TGCIdBase::SI];
}

} //end of namespace
