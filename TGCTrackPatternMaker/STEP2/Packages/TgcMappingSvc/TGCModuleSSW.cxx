#include "TGCcabling12/TGCModuleSSW.h"

namespace LVL1TGCCabling12
{
 
// Constructor
TGCModuleSSW::TGCModuleSSW (TGCIdBase::SideType vside,
			    int vreadoutSector,
			    int vid)
  : TGCModuleId(TGCModuleId::SSW)
{
  setSideType(vside);
  setReadoutSector(vreadoutSector);
  setId(vid);
}
  
bool TGCModuleSSW::isValid (void) const
{
  if((getSideType()  >TGCIdBase::NoSideType)            &&
     (getSideType()  <TGCIdBase::MaxSideType)           &&
     (getReadoutSector() >=0)                       &&
     (getReadoutSector() < NumberOfReadoutSector )  &&
     (getId()        >=0)                              )
    return true;
  return false;
}

} // end of namespace
