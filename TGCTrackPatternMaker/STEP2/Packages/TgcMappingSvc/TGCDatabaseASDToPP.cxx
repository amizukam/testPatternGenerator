#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "TGCcabling12/TGCDatabaseASDToPP.h"

namespace LVL1TGCCabling12 
{

TGCDatabaseASDToPP::TGCDatabaseASDToPP (std::string filename, 
					std::string blockname)
  : TGCDatabase(TGCDatabase::ASDToPP, filename, blockname)
{
  // read out ascii file and fill database
  if(database.size()==0) readDB();
}
  
TGCDatabaseASDToPP::TGCDatabaseASDToPP (const TGCDatabaseASDToPP& right)
  : TGCDatabase(right)
{
  right.getindexDBVectorOut(indexDBOut);
  right.getNIndexDBOut(NIndexDBOut);
  right.getmaxIndexOut(maxIndexOut);
  right.getminIndexOut(minIndexOut);
}

void TGCDatabaseASDToPP::readDB (void) 
{
  std::ifstream file(filename.c_str());
  std::string buf;

  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    maxIndexOut[iIndexOut] = 0;
    minIndexOut[iIndexOut] = 9999;
  }

  while(getline(file,buf)){
    if(buf.substr(0,blockname.size())==blockname) break;
  }

  while(getline(file,buf)){
    if(buf.substr(0,1)=="E"||buf.substr(0,1)=="F") break;
    std::istringstream line(buf);
    std::vector<int> entry;
    for(int i=0; i<6; i++){
      int temp=-1;
      line >> temp; 
      entry.push_back(temp);

      if(IndexOutMin<=i && i<=IndexOutMax) {
	int j = i-IndexOutMin;
	if(maxIndexOut[j]<temp) {
	  maxIndexOut[j] = temp;
	} 
	if(minIndexOut[j]>temp) {
	  minIndexOut[j] = temp;
	}
      }
    }
    database.push_back(entry);
  }

  file.close();

  int nline = 0;
  const unsigned int database_size = database.size();
  for(unsigned int i=0; i<database_size; i++){
    // line is defined in whole sector. [0..n]
    if(i>0&&database[i].at(0)!=database[i-1].at(0)) nline=0;
    database[i].push_back(nline++);

    if(i==database_size-1||
       database[i].at(0)!=database[i+1].at(0)||
       database[i].at(1)!=database[i+1].at(1)){
      // increase with R in chamber    [0..n]
      int totline = database[i].at(2)+1;
      for(int j=0; j<totline; j++){
	database[i-j].push_back(j);
      }
    }
  }

  makeIndexDBOut();
}

TGCDatabaseASDToPP::~TGCDatabaseASDToPP (void)
{
}

bool TGCDatabaseASDToPP::update(const std::vector<int>& input)
{
  int ip = find(input);
  if (ip<0) return false;
 
  int tmpValIndexOut[NIndexOut];
  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    tmpValIndexOut[iIndexOut] = database[ip].at(iIndexOut+IndexOutMin);
  }

  bool over_range = false;
  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    if(maxIndexOut[iIndexOut]<tmpValIndexOut[iIndexOut]) {
      maxIndexOut[iIndexOut] = tmpValIndexOut[iIndexOut];
      over_range = true;
    }
    if(minIndexOut[iIndexOut]>tmpValIndexOut[iIndexOut]) {
      minIndexOut[iIndexOut] = tmpValIndexOut[iIndexOut];
      over_range = true;
    }
  }
  if(over_range) {
    indexDBOut.clear();
    makeIndexDBOut();
  }

  int converted = convertIndexDBOut(tmpValIndexOut);
  if(converted<0 || converted>=NIndexDBOut) return false;

  indexDBOut.at(converted) = ip;

  database[ip].at(3) = input.at(3);
  database[ip].at(4) = input.at(4);
  database[ip].at(5) = input.at(5);

  return true;
}
 
int  TGCDatabaseASDToPP::find(const std::vector<int>& channel) const
{
  int index=-1;
  const size_t size = database.size();
  for(size_t i=0; i<size; i++){
    if( database[i].at(2) == channel.at(2) &&
        database[i].at(1) == channel.at(1) &&
        database[i].at(0) == channel.at(0) ) {
      index = i;
      break;
    }
  }
  return index;
}

void TGCDatabaseASDToPP::makeIndexDBOut (void) 
{
  NIndexDBOut = 1;
  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    NIndexDBOut *= (maxIndexOut[iIndexOut]-minIndexOut[iIndexOut]+1);
  }
  for(int iIndexDBOut=0; iIndexDBOut<NIndexDBOut; iIndexDBOut++) {
    indexDBOut.push_back(-1);
  }

  const int size = database.size();
  for(int i=0; i<size; i++) {
    int tmpValIndexOut[NIndexOut];
    for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
      tmpValIndexOut[iIndexOut] = database.at(i).at(iIndexOut+IndexOutMin);
    }
    indexDBOut.at(convertIndexDBOut(tmpValIndexOut)) = i;
  }
}

int TGCDatabaseASDToPP::convertIndexDBOut(int* indexOut) const
{
  int converted = indexOut[0]-minIndexOut[0];
  for(int iIndexOut=1; iIndexOut<NIndexOut; iIndexOut++) {
    converted *= (maxIndexOut[iIndexOut]-minIndexOut[iIndexOut]+1);
    converted += indexOut[iIndexOut]-minIndexOut[iIndexOut];
  }
  return converted;
}

int TGCDatabaseASDToPP::getIndexDBOut(int* indexOut) 
{
  if(!indexOut) return -1;

  if(database.size()==0) readDB();

  int converted = convertIndexDBOut(indexOut);
  if(converted<0 || converted>=NIndexDBOut) return -1;

  return indexDBOut.at(converted);
}

void TGCDatabaseASDToPP::getindexDBVectorOut(std::vector<int>& tmpindexDBOut) const
{
  tmpindexDBOut = indexDBOut;
}

void TGCDatabaseASDToPP::getNIndexDBOut(int& tmpNIndexDBOut) const
{
  tmpNIndexDBOut = NIndexDBOut;
}

void TGCDatabaseASDToPP::getmaxIndexOut(int* tmpmaxIndexOut) const
{
  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    tmpmaxIndexOut[iIndexOut] = maxIndexOut[iIndexOut];
  }
}

void TGCDatabaseASDToPP::getminIndexOut(int* tmpminIndexOut) const
{
  for(int iIndexOut=0; iIndexOut<NIndexOut; iIndexOut++) {
    tmpminIndexOut[iIndexOut] = minIndexOut[iIndexOut];
  }
}

} // end of namespace
