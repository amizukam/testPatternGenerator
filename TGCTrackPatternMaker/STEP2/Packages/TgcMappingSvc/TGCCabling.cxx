#include "TGCcabling12/TGCCabling.h"

#include <iostream>

namespace LVL1TGCCabling12 {

// Constructor & Destructor
TGCCabling::TGCCabling (std::string filenameASDToPP,
			std::string filenameInPP,
			std::string filenamePPToSL,
			std::string filenameSLBToROD)
  : TGCCablingBase()
{
  cableInASD    = new TGCCableInASD(filenameASDToPP);
  cableASDToPP  = new TGCCableASDToPP(filenameASDToPP);
  cableInPP     = new TGCCableInPP(filenameInPP);
  cablePPToSLB  = new TGCCablePPToSLB(filenamePPToSL);
  cableInSLB    = new TGCCableInSLB();
  cableSLBToHPB = new TGCCableSLBToHPB(filenamePPToSL);
  cableHPBToSL  = new TGCCableHPBToSL(filenamePPToSL);
  cableSLBToSSW = new TGCCableSLBToSSW(filenameSLBToROD);
  cableSSWToROD = new TGCCableSSWToROD(filenameSLBToROD);
}

TGCCabling::~TGCCabling (void) 
{
  delete cableInASD;
  delete cableASDToPP;
  delete cableInPP;
  delete cablePPToSLB;
  delete cableInSLB;
  delete cableSLBToHPB;
  delete cableHPBToSL;
  delete cableSLBToSSW;
  delete cableSSWToROD;
}


void TGCCabling::updateCableASDToPP(std::string filename)
{
  cableASDToPP->updateDatabase(filename); 
} 

// virtual method  of TGCCabligBase   
// slbIn --> AsdOut
TGCIdBase* TGCCabling::getASDOutChannel(const TGCIdBase* in) const
{
  TGCChannelSLBIn slb_in( in->getSideType(),
			  in->getModuleType(),
			  in->getRegionType(),
			  in->getSector(),
			  in->getId(),
			  in->getChannel() );

  return getChannel (&slb_in,
		     TGCIdBase::ASDOut); 
}



// readout ID -> SLB Module
TGCModuleId* TGCCabling::getSLBFromReadout (TGCIdBase::SideType side,
					    int rodId,
					    int sswId,
					    int sbLoc) const 
{
  bool found;
  
  // ROD Module
  int readoutSector = rodId -1 ; //rodID = 1..12
  TGCModuleROD rod(side,readoutSector);

  // SSW Module
  TGCModuleMap* sswMap =  getModule(&rod,TGCModuleId::SSW);
  if(!sswMap) return 0;

  TGCModuleId* ssw = 0;
  found = false;
  const int sswMapsize = sswMap->size();
  for(int i=0; i<sswMapsize; i++){
    if((sswMap->moduleId(i))->getId()==sswId){
      ssw = sswMap->popModuleId(i);
      found = true;
      break;
    }
  }
  delete sswMap;
  if(!found || !ssw) return 0; // Do not need to delete ssw here.  
                               // We can delete ssw but nothing will be done.  

  // SLB Module
  TGCModuleMap* slbMap =  getModule(ssw,TGCModuleId::SLB);
  delete ssw;
  if(!slbMap) return 0;

  TGCModuleSLB*   slb=0; 
  found = false;
  const int slbMapsize = slbMap->size();
  for(int i=0; i<slbMapsize; i++){
    slb = dynamic_cast<TGCModuleSLB*>(slbMap->moduleId(i));
    if(slb->getSBLoc()== sbLoc){
      found = true;
      slb = dynamic_cast<TGCModuleSLB*>(slbMap->popModuleId(i));
      break;
    }
  }
  delete slbMap;

  if(!found  || !slb) return 0; // Do not delete slb here.  
  
  return slb;
}

// readout ID -> RxID
int TGCCabling::getRxIdFromReadout(TGCIdBase::SideType side,
				   int                 rodId,
				   int                 sswId,
				   int                 sbLoc) const
{
  int rxId = -1;

  // ROD Module
  int readoutSector = rodId -1 ; //rodID = 1..12
  TGCModuleROD rod(side,readoutSector);

  // SSW Module
  TGCModuleMap* sswMap =  getModule(&rod,TGCModuleId::SSW);
  if(!sswMap) return rxId;

  TGCModuleId* ssw = 0;
  bool found = false;
  const int sswMapsize = sswMap->size();
  for(int i=0; i<sswMapsize; i++){
    if((sswMap->moduleId(i))->getId()==sswId){
      ssw = sswMap->popModuleId(i);
      found = true;
      break;
    }
  }
  delete sswMap;
  if(!found || !ssw) return rxId; // Do not need to delete ssw here.  
                                  // We can delete ssw but nothing will be done.  

  // SLB Module
  TGCModuleMap* slbMap =  getModule(ssw,TGCModuleId::SLB);
  delete ssw;
  if(!slbMap) return rxId;

  TGCModuleSLB*   slb=0; 
  found = false;
  const int slbMapsize = slbMap->size();
  for(int i=0; i<slbMapsize; i++){
    slb = dynamic_cast<TGCModuleSLB*>(slbMap->moduleId(i));
    if(slb->getSBLoc()== sbLoc){
      rxId = slbMap->connector(i);
      break;
    }
  }
  delete slbMap;

  return rxId;
}

// SSW ID/Rx ID -> SLB Module
TGCModuleId* TGCCabling::getSLBFromRxId(TGCIdBase::SideType side,
					int rodId,
					int sswId,
					int rxId) const 
{
  bool found;

  // ROD Module
  int readoutSector = rodId -1 ; //rodID = 1..12
  TGCModuleROD rod(side,readoutSector);

  // SSW Module
  TGCModuleMap* sswMap =  getModule(&rod,TGCModuleId::SSW);
  if(!sswMap) return 0;

  TGCModuleId* ssw = 0;
  found = false;
  const int size = sswMap->size();
  for(int i=0; i<size; i++){
    if((sswMap->moduleId(i))->getId()==sswId){
      ssw = sswMap->popModuleId(i);
      found = true;
      break;
    }
  }
  delete sswMap;
  if(!found || !ssw) return 0; // Do not need to delete ssw here.
                               // We can delete ssw but nothing will be done.

  // SLB Module
  TGCModuleMap* slbMap =  getModule(ssw,TGCModuleId::SLB);
  delete ssw;
  if(!slbMap) return 0;

  TGCModuleSLB*   slb=0;  
  int ip = slbMap->find(rxId);
  if (ip <0 || ip >= slbMap->size()){
    delete slbMap; slbMap = 0;
    return 0;
  }
  slb =  dynamic_cast<TGCModuleSLB*>(slbMap->popModuleId(ip));
  delete slbMap;

  if(!slb) return 0; 
  
  return slb;
}
  

// SLB Module -> readout ID
bool TGCCabling::getReadoutFromSLB (const TGCModuleSLB* slb,
				    TGCIdBase::SideType & side,
				    int & rodId,
				    int & sswId,
				    int & sbLoc) const 
{
  // initialize
  side = TGCIdBase::NoSideType;
  rodId = -1;
  sswId = -1;
  sbLoc = -1;

  if(!slb) return false;
  
  // Fill side
  side = slb->getSideType();

  TGCModuleMap* sswMap = getModule(slb,TGCModuleId::SSW);

  //if(!sswMap) std::cerr << "getReadoutFromSLB  NO SSW MAP" << std::endl;
  if(!sswMap) return false;
  
  // SSW Module
  TGCModuleId* ssw = sswMap->popModuleId(0);
  delete sswMap;
  //if(!ssw) std::cerr << "getReadoutFromSLB  NO SSW " << std::endl;
  if(!ssw) return false;
 
    
  // Fill SSW ID
  sswId = ssw->getId();


  // Fill SBLoc
  sbLoc = slb->getSBLoc();
  //std::cerr << "getReadoutFromSLB  ssw=" << sswId
  //	    << "  sbLoc=" << sbLoc 
  //	    << "  ModuleType=" <<  slb->getModuleType()  
  //	    << "  Region=" << slb->getRegionType() 	    
  //	    << "  sector=" << slb->getSector()
  //        << "  ID=" << slb->getId() <<std::endl;
  

  if (sbLoc < 0 ) {
    TGCModuleMap* slbMap =  getModule(ssw,TGCModuleId::SLB);
    //if(!slbMap) {
    //  std::cerr << "No SLB MAP for SSW Id=" << ssw->getId() 
    //	          << " ROsector=" << ssw->getReadoutSector() 
    //	          << std::endl;
    //}
    if(!slbMap){
      delete ssw; ssw = 0;
      return false;
    }
    
    TGCModuleSLB*   pSlb=0;
    bool found = false;
    const int size = slbMap->size();
    for(int i=0; i<size; i++){
      pSlb = dynamic_cast<TGCModuleSLB*>(slbMap->moduleId(i));

      //std::cerr << i <<": " << pSlb->getModuleType()  << ", "
      //	<< pSlb->getRegionType() << ", "
      //	<< pSlb->getSector() << ", "
      //	<< pSlb->getId()  << std::endl;
      
      if( slb->getRegionType() == pSlb->getRegionType() &&
  	  slb->getSector() == pSlb->getSector()  &&
	  slb->getId() == pSlb->getId()    ){
	if ( slb->getModuleType() == pSlb->getModuleType()  ) {
	  sbLoc = pSlb->getSBLoc();	
	  found = true;
	  break;
	}
	// SI is connected to the SLB corrsponding WI 
	if( (slb->getModuleType()==TGCIdBase::SI) && (pSlb->getModuleType()==TGCIdBase::WI) ) {  
	  sbLoc = pSlb->getSBLoc();	
	  found = true;
	  break;
	}
      }
    }
    delete slbMap;
    if (sbLoc < 0 ) {
      delete ssw;
      return false;
    }
  }  
    

  TGCModuleMap* rodMap = getModule(ssw,TGCModuleId::ROD);
  delete ssw;
  if(!rodMap) return false;

  // ROD Module
  TGCModuleId* rod = rodMap->popModuleId(0);
  delete rodMap;
  if(!rod) return false;

  // Fill ROD ID
  rodId = rod->getId();
  delete rod;

  return true;  
}


// coincidence channel -> readout channel
bool TGCCabling::getReadoutFromHighPtID(TGCIdBase::SideType side,
					 int rodId,
					 int & sswId,
					 int & sbLoc,
					 int & channel,
					 TGCIdBase::SignalType signal,
					 TGCIdBase::RegionType region,
					 int sectorInReadout,
					 int hpbId,
					 int block,
					 int hitId,
					 int pos,
					 TGCIdBase::ModuleType moduleType,
					 bool orChannel) const 
{
  // initialize
  sswId = -1;
  sbLoc = -1;
  channel = -1;

  // get sector number
  int readoutSector = (rodId -1 ); //rod ID = 1..12
  int sector = sectorInReadout;
  if  (region==TGCIdBase::Forward) {
    sector += readoutSector*(TGCId::NumberOfForwardSector/TGCId::NumberOfReadoutSector);
  } else {
    sector += readoutSector*(TGCId::NumberOfEndcapSector/TGCId::NumberOfReadoutSector);
  }
    
  TGCChannelHPBIn hpbin(side,
			signal,
			region,
			sector,
			hpbId,
			block,
			hitId*2+pos);
  if(!hpbin.isValid()) return false;

  TGCChannelId* slbout = cableSLBToHPB->getChannelInforHPB(&hpbin,moduleType,false);
  if(!slbout) return 0;
  if(!slbout->isValid()){
    delete slbout;
    return 0;
  }
  TGCChannelId* slbin = cableInSLB->getChannel(slbout,orChannel);
  delete slbout;
  
  //TGCChannelId* slbin = getChannel(&hpbin,TGCIdBase::SLBInforHPB,false);
  if(!slbin) return false;
  channel = slbin->getChannel();
  
  TGCModuleSLB* slb = dynamic_cast<TGCModuleSLB*>(slbin->getModule());
  delete slbin;
  if(!slb) return false;
  
  // SLB Module -> readout ID
  TGCIdBase::SideType sideType;
  int rodid;
  bool status = getReadoutFromSLB (slb,
				   sideType,
				   rodid,
				   sswId,
				   sbLoc);
  
  delete slb;
  
  return status;
}

// readout channel -> coincidence channel
bool TGCCabling::getHighPtIDFromReadout(TGCIdBase::SideType side,
					 int rodId,
					 int sswId,
					 int sbLoc,
					 int channel,
					 TGCIdBase::SignalType & signal,
					 TGCIdBase::RegionType & region,
					 int & sectorInReadout,
					 int & hpbId,
					 int & block,
					 int & hitId,
					 int & pos) const 
{
  signal = TGCIdBase::NoSignalType;
  region = TGCIdBase::NoRegionType;
  sectorInReadout = -1;
  hpbId  = -1;
  block  = -1;
  hitId  = -1;
  pos    = -1;

  TGCModuleId* slb = getSLBFromReadout (side,
                                        rodId,
                                        sswId,
                                        sbLoc);
  if(!slb) return 0;
  
  TGCChannelSLBIn slbin(slb->getSideType(),
                        slb->getModuleType(),
                        slb->getRegionType(),
                        slb->getSector(),
                        slb->getId(),
                        channel);
  delete slb;
  
  TGCChannelId* hpbin = getChannel(&slbin,TGCIdBase::HPBIn,false); 
  if(!hpbin) return 0;
  if(!hpbin->isValid()) {
    delete hpbin;
    return 0;
  }
  signal = hpbin->getSignalType();
  region = hpbin->getRegionType();
  sectorInReadout = hpbin->getSectorInReadout();
  hpbId  = hpbin->getId();
  block  = hpbin->getBlock();
  pos    = hpbin->getChannel()%2;
  hitId  = (hpbin->getChannel() - pos)/2;
  
  delete hpbin;
  return true;
}


// coincidence channel -> readout channel
bool TGCCabling::getReadoutFromLowPtCoincidence(TGCIdBase::SideType side,
						int rodId,
						int sswId,
						int sbLoc,
						int & channel,
						int block,
						int pos,
						bool flag) const 
{
  bool orChannel = flag;    

  TGCModuleId* slb = getSLBFromReadout (side,
					rodId,
					sswId,
					sbLoc);
  if(!slb) return 0;

  /*
  if (slb->getModuleType()  == 3) {   //  3:ST 
    orChannel = false;
  } 
  */
  TGCChannelSLBOut slbout(slb->getSideType(),
			  slb->getModuleType(),
			  slb->getRegionType(),
			  slb->getSector(),
			  slb->getId(),
			  block,
			  pos);
  delete slb;   

  TGCChannelId* slbin = getChannel(&slbout,TGCIdBase::SLBIn, orChannel); 
  
  if(!slbin) return false;
  
  channel = slbin->getChannel();
  
  /* 
     if (slb->getModuleType()  == 3) {   //  3:ST 
     if (flag) {
     if(channel < 77)
     channel = channel + 76;
     else if(channel >= 77 && channel < 117)
     channel = channel + 84;
     }
     }
  */

  delete slbin;
  
  return true;
}


// readout channel -> coincidence channel
bool TGCCabling::getLowPtCoincidenceFromReadout(TGCIdBase::SideType side,
						int rodId,
						int sswId,
						int sbLoc,
						int channel,
						int & block,
						int & pos,
						bool middle) const {
  
  TGCModuleId* slb = getSLBFromReadout (side,
					rodId,
					sswId,
					sbLoc);
  if(!slb) return 0;
  
  TGCChannelSLBIn slbin(slb->getSideType(),
			slb->getModuleType(),
			slb->getRegionType(),
			slb->getSector(),
			slb->getId(),
			channel);
  delete slb;
  if (!slbin.isValid()) return false;
  
  TGCChannelId* slbout = getChannel(&slbin,TGCIdBase::SLBOut,middle); 
  if(!slbout) return false;
  
  block = slbout->getBlock();
  pos = slbout->getChannel();
  delete slbout;
  
  return true;
}


// readout channel -> chamber channel
TGCChannelId* TGCCabling::getASDOutFromReadout (TGCIdBase::SideType side,
						int rodId,
						int sswId,
						int sbLoc,
						int channel,
						bool orChannel) const {
  TGCModuleId* slb = getSLBFromReadout (side,
					rodId,
					sswId,
					sbLoc);
  if(!slb) return 0;
  
  //std::cerr << "TGCCabling::getASDOutFromReadout" 
  //	    << " rodId=" << rodId << " sswId=" << sswId
  //	    << " sbLoc=" << sbLoc << " channel=" << channel 
  //	    << " slb=" << slb << std::endl;
  //std::cerr << " side=" << slb->getSideType();
  //std::cerr << " module=" << slb->getModuleType();
  //std::cerr << " region=" << slb->getRegionType();
  //std::cerr << " sector=" << slb->getSector();
  //std::cerr << " id=" << slb->getId() << std::endl;


  TGCChannelSLBIn slbin(slb->getSideType(),
			slb->getModuleType(),
			slb->getRegionType(),
			slb->getSector(),
			slb->getId(),
			channel);
  delete slb;
  if (!slbin.isValid()) return 0;  
 
  return getChannel(&slbin,TGCIdBase::ASDOut,orChannel); 
}


// chamber channel -> readout channel
bool TGCCabling::getReadoutFromASDOut (const TGCChannelASDOut* asdout,
				       TGCIdBase::SideType & side,
				       int & rodId,
				       int & sswId,
				       int & sbLoc,
				       int & channel,
				       bool orChannel) const {
  // initialize
  side = TGCIdBase::NoSideType;
  rodId = -1;
  sswId = -1;
  sbLoc = -1;
  channel = -1;


  // SLBIn channel
  TGCChannelId* slbin = getChannel(asdout,TGCIdBase::SLBIn,orChannel);
  
  if(!slbin) return false;
  channel = slbin->getChannel();
  
  TGCModuleSLB* slb = dynamic_cast<TGCModuleSLB*>(slbin->getModule());
  delete slbin;
  if(!slb) return false;
  
  // SLB Module -> readout ID
  bool status = getReadoutFromSLB (slb,
				   side,
				   rodId,
				   sswId,
				   sbLoc);
  delete slb;
  
  return status;
}

TGCChannelId* TGCCabling::getChannel (const TGCChannelId* channelId,
				      TGCIdBase::ChannelIdType type,
				      bool orChannel) const {
  switch(channelId->getChannelIdType()){
  case TGCIdBase::ASDIn:
    if(type==TGCIdBase::ASDOut)
      return cableInASD->getChannel(channelId,orChannel);
    if(type==TGCIdBase::SLBIn){
      TGCChannelId* asdout = cableInASD->getChannel(channelId,false);
      if(!asdout) return 0;
      if(!asdout->isValid()){
	delete asdout;
	return 0;
      }
      TGCChannelId* ppin = cableASDToPP->getChannel(asdout,false);
      delete asdout;
      if(!ppin) return 0;
      if(!ppin->isValid()){
	delete ppin;
	return 0;
      }
      TGCChannelId* ppout = cableInPP->getChannel(ppin,orChannel);
      delete ppin;
      if(!ppout) return 0;
      if(!ppout->isValid()){
	delete ppout;
	return 0;
      }
      TGCChannelId* slbin = cablePPToSLB->getChannel(ppout,false);
      delete ppout;
      return slbin;
    } 
    break;
  case TGCIdBase::ASDOut:
    if(type==TGCIdBase::ASDIn)
      return cableInASD->getChannel(channelId,orChannel);
    if(type==TGCIdBase::PPIn)
      return cableASDToPP->getChannel(channelId,orChannel);
    if(type==TGCIdBase::SLBIn){
      TGCChannelId* ppin = cableASDToPP->getChannel(channelId,false);
      if(!ppin) return 0;
      if(!ppin->isValid()){
	//std::cerr << "TGCCabling::getChannel ASDOut: ppin invalid"  <<std::endl; 
	delete ppin;
	return 0;
      }
      TGCChannelId* ppout = cableInPP->getChannel(ppin,orChannel);
      delete ppin;
      if(!ppout) return 0;
      if(!ppout->isValid()){
	//std::cerr << "TGCCabling::getChannel ASDOut: ppout invalid"  <<std::endl; 
	delete ppout;
	return 0;
      }
      TGCChannelId* slbin = cablePPToSLB->getChannel(ppout,false);
      delete ppout;
      return slbin;
    } 
    break;
  case TGCIdBase::PPIn:
    if(type==TGCIdBase::ASDOut)
      return cableASDToPP->getChannel(channelId,orChannel);
    if(type==TGCIdBase::PPOut)
      return cableInPP->getChannel(channelId,orChannel);
    break;
  case TGCIdBase::PPOut:
    if(type==TGCIdBase::PPIn)
      return cableInPP->getChannel(channelId,orChannel);
    if(type==TGCIdBase::SLBIn)
      return cablePPToSLB->getChannel(channelId,orChannel);
    break;
  case TGCIdBase::SLBIn:
    if(type==TGCIdBase::SLBOut)
      return cableInSLB->getChannel(channelId,orChannel);
    if(type==TGCIdBase::HPBIn){
      TGCChannelId* slbout = cableInSLB->getChannel(channelId,orChannel);
      if(!slbout) return 0; 
      if(!slbout->isValid()) {
	delete slbout;
	return 0;
      } 
      TGCChannelId* hpbin = cableSLBToHPB->getChannel(slbout,false);
      delete slbout;
      return hpbin;
    }
    if(type==TGCIdBase::PPOut)
      return cablePPToSLB->getChannel(channelId,orChannel);
    if(type==TGCIdBase::ASDOut){
      TGCChannelId* ppout = cablePPToSLB->getChannel(channelId,false);
      if(!ppout) return 0;
      if(!ppout->isValid()){
	delete ppout;
	return 0;
      }
      TGCChannelId* ppin = cableInPP->getChannel(ppout,orChannel);
      delete ppout;
      if(!ppin) return 0;
      if(!ppin->isValid()){
	delete ppin;
	return 0;
      }
      TGCChannelId* asdout = cableASDToPP->getChannel(ppin,false);
      delete ppin;
      return asdout;
    } 
    if(type==TGCIdBase::ASDIn){
      TGCChannelId* ppout = cablePPToSLB->getChannel(channelId,false);
      if(!ppout) return 0;
      if(!ppout->isValid()){
	delete ppout;
	return 0;
      }
      TGCChannelId* ppin = cableInPP->getChannel(ppout,orChannel);
      delete ppout;
      if(!ppin) return 0;
      if(!ppin->isValid()){
	delete ppin;
	return 0;
      }
      TGCChannelId* asdout = cableASDToPP->getChannel(ppin,false);
      delete ppin;
      if(!asdout) return 0;
      if(!asdout->isValid()){
	delete asdout;
	return 0;
      }
      TGCChannelId* asdin = cableInASD->getChannel(asdout,false);
      delete asdout;
      return asdin;
    } 
    break;
  case TGCIdBase::SLBOut:
    if(type==TGCIdBase::SLBIn)
      return cableInSLB->getChannel(channelId,orChannel);
    if(type==TGCIdBase::HPBIn){
      return cableSLBToHPB->getChannel(channelId,orChannel);
    }
    break;
  case TGCIdBase::HPBIn:
    if(type==TGCIdBase::SLBIn){
      TGCChannelId* slbout = cableSLBToHPB->getChannel(channelId,false);
      if(!slbout) return 0;
      if(!slbout->isValid()){
	delete slbout;
	return 0;
      }
      TGCChannelId* slbin = cableInSLB->getChannel(slbout,orChannel);
      delete slbout;
      return slbin;
    }
    if(type==TGCIdBase::SLBOut)
      return cableSLBToHPB->getChannel(channelId,orChannel);
    break;
  default:
    break;
  }
  return 0;
}

TGCModuleMap* TGCCabling::getModule (const TGCModuleId* moduleId,
				     TGCModuleId::ModuleIdType type) const {
  switch(moduleId->getModuleIdType()){
  case TGCModuleId::PP:
    if(type==TGCModuleId::SLB)
      return cablePPToSLB->getModule(moduleId);
    break;
  case TGCModuleId::SLB:
    if(type==TGCModuleId::PP)
      return cablePPToSLB->getModule(moduleId);
    if(type==TGCModuleId::HPB)
      return cableSLBToHPB->getModule(moduleId);
    if(type==TGCModuleId::SSW)
      return cableSLBToSSW->getModule(moduleId);
    break;
  case TGCModuleId::HPB:
    if(type==TGCModuleId::SLB)
      return cableSLBToHPB->getModule(moduleId);
    if(type==TGCModuleId::SL)
      return cableHPBToSL->getModule(moduleId);
    break;
  case TGCModuleId::SL:
    if(type==TGCModuleId::HPB)
      return cableHPBToSL->getModule(moduleId);
    break;
  case TGCModuleId::SSW:
    if(type==TGCModuleId::SLB)
      return cableSLBToSSW->getModule(moduleId);
    if(type==TGCModuleId::ROD)
      return cableSSWToROD->getModule(moduleId);
    break;
  case TGCModuleId::ROD:
    if(type==TGCModuleId::SSW)
      return cableSSWToROD->getModule(moduleId);
    break;
  default:
    break;
  }
  return 0;
} 

} //end of namespace
