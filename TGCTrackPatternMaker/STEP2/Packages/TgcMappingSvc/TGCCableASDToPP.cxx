#include "TGCcabling12/TGCCableASDToPP.h"
#include <iostream>
#include <fstream>
#include <sstream>

namespace LVL1TGCCabling12 {

// Constructor & Destructor
TGCCableASDToPP::TGCCableASDToPP (std::string  filename)
  : TGCCable(TGCCable::ASDToPP)
{
  initialize(filename);
}

TGCCableASDToPP::~TGCCableASDToPP (void)
{
  for (int side=0; side < TGCIdBase::MaxSideType; side++){
    int sector;
    for (sector=0; sector < TGCId::NumberOfForwardSector; sector++){
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::WD);
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::SD);
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::WT);
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::ST);
    }
    for (sector=0; sector < TGCId::NumberOfEndcapSector; sector++){
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::WD);
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::SD);
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::WT);
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::ST);
    }
    for (sector=0; sector < TGCId::NumberOfInnerSector; sector++){
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::WI);
      delete getDatabase(side, TGCIdBase::Forward, sector,TGCIdBase::SI);
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::WI);
      delete getDatabase(side, TGCIdBase::Endcap, sector,TGCIdBase::SI);
    } 
  }
}


void TGCCableASDToPP::initialize(std::string& filename)
{
  // DEBUG
  //std::cerr << "TGCCableASDToPP::initialize" << std::endl; 

  int sector;
  TGCDatabaseASDToPP* db;
  // FWD  
  FWDdb[0][0] = db =  new TGCDatabaseASDToPP(filename,"FWD");
  FWDdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfForwardSector; sector++){
    FWDdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FWDdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  //FSD
  FSDdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"FSD");
  FSDdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfForwardSector; sector++){
    FSDdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FSDdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // FWT  
  FWTdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"FWT");
  FWTdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfForwardSector; sector++){
    FWTdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FWTdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  //FST
  FSTdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"FST");
  FSTdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfForwardSector; sector++){
    FSTdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FSTdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // EWD  
  EWDdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"EWD");
  EWDdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfEndcapSector; sector++){
    EWDdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    EWDdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // ESD  
  ESDdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"ESD");
  ESDdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfEndcapSector; sector++){
    ESDdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    ESDdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // EWT  
  EWTdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"EWT");
  EWTdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfEndcapSector; sector++){
    EWTdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    EWTdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // EST  
  ESTdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"EST");
  ESTdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfEndcapSector; sector++){
    ESTdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    ESTdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // FWI
  FWIdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"FWI");
  FWIdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfInnerSector; sector++){
    FWIdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FWIdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // FSI
  FSIdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"FSI");
  FSIdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfInnerSector; sector++){
    FSIdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    FSIdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // EWI
  EWIdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"EWI");
  EWIdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfInnerSector; sector++){
    EWIdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    EWIdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
  // ESI
  ESIdb[0][0] =  db = new TGCDatabaseASDToPP(filename,"ESI");
  ESIdb[1][0] =  new TGCDatabaseASDToPP(*db);    
  for (sector=1; sector < TGCId::NumberOfInnerSector; sector++){
    ESIdb[0][sector] =  new TGCDatabaseASDToPP(*db); 
    ESIdb[1][sector] =  new TGCDatabaseASDToPP(*db); 
  }
}


void  TGCCableASDToPP::updateDatabase(std::string&  filename)
{
  std::string blockname;
  TGCDatabaseASDToPP* db;

  for (int side=0; side < TGCIdBase::MaxSideType; side++){
    for (int sector=0; sector < TGCId::NumberOfForwardSector; sector++){
      std::vector< std::vector<int> > info;
      size_t info_size = 0;
      // FWD 
      blockname = "FWD";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FWDdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // FSD 
      blockname = "FSD";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FSDdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // FWT
      blockname = "FWT";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FWTdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // FST 
      blockname = "FST";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FSTdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }      
    }
    for (int sector=0; sector < TGCId::NumberOfEndcapSector; sector++){
      std::vector< std::vector<int> > info;
      size_t info_size = 0;
      // EWD 
      blockname = "EWD";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( EWDdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // ESD 
      blockname = "ESD";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( ESDdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // EWT
      blockname = "EWT";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( EWTdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // EST 
      blockname = "EST";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( ESTdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }      
    }
    for (int sector=0; sector < TGCId::NumberOfInnerSector; sector++){
      std::vector< std::vector<int> > info;
      size_t info_size = 0;
      // EWI 
      blockname = "EWI";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( EWIdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // ESI 
      blockname = "ESI";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( ESIdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // FWI
      blockname = "FWI";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FWIdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }
      // FSI
      blockname = "FSI";  
      db = dynamic_cast<TGCDatabaseASDToPP*>( FSIdb[side][sector] );
      getUpdateInfo(filename, side, sector, blockname, info);
      info_size = info.size();
      for (size_t i=0; i< info_size; i+=1) {
	db->update(info[i]);
      }      
    }
  }
}

void  TGCCableASDToPP::getUpdateInfo(std::string&  filename,
				     int side, int sector, 
				     std::string& blockname,
				     std::vector< std::vector<int> >& info)
{
  // clear info
  info.clear();
  
  // 
  std::ifstream file(filename.c_str());
  std::string buf;
  int  size = 0;
  // search block name
  while(getline(file,buf)){
    if(buf.substr(0,1)=="/"||buf.substr(0,1)=="*") continue;
    if(buf.substr(0,blockname.size())==blockname) {
      std::istringstream line(buf);
      std::string temp;
      line >> temp >> size;
      break;
    }
  }
  
  // loop over entries of specified block
  while(getline(file,buf)){
    if(buf.substr(0,1)=="/"||buf.substr(0,1)=="*") continue;
    if(buf.substr(0,1)=="E"||buf.substr(0,1)=="F") break;
    std::istringstream line(buf);
    std::vector<int> entry;
    int  t_side, t_sector;
    line >> t_side;
    line >> t_sector;
    bool isOK = false;
    if ( (t_side == side) && (t_sector == sector) ) { 
      for(int i=2; i<8; i++){
	int temp=-1;
	if (line >> temp) { 
	  entry.push_back(temp);
	} else {
	  break;
	}
	isOK = (i == 7);
      }
      if ( isOK) {
	info.push_back(entry);
      }
    }
  }

  //
  file.close();
}



TGCDatabase*  TGCCableASDToPP::getDatabase(int side, 
					   int region, 
					   int sector,
					   int module) const
{
  TGCDatabase* db=0;
  if (region == TGCIdBase::Endcap ){
    switch(module){
    case TGCIdBase::WD :
      db = EWDdb[side][sector];
      break;
    case TGCIdBase::SD :
      db = ESDdb[side][sector];
      break;
    case TGCIdBase::WT :
      db = EWTdb[side][sector];
      break;
    case TGCIdBase::ST :
      db = ESTdb[side][sector];
      break;
    case TGCIdBase::WI :
      db = EWIdb[side][sector];
      break;
    case TGCIdBase::SI :
      db = ESIdb[side][sector];
      break;
    default:
      break;
    }
  } else if (region == TGCIdBase::Forward ){
    switch(module){
    case TGCIdBase::WD :
      db = FWDdb[side][sector];
      break;
    case TGCIdBase::SD :
      db = FSDdb[side][sector];
      break;
    case TGCIdBase::WT :
      db = FWTdb[side][sector];
      break;
    case TGCIdBase::ST :
      db = FSTdb[side][sector];
      break;
    case TGCIdBase::WI :
      db = FWIdb[side][sector];
      break;
    case TGCIdBase::SI :
      db = FSIdb[side][sector];
      break;
    default:
      break;
    }
  }
  return db;
}

// reverse layers in Forward sector
const int TGCCableASDToPP::stripForward[] = {2,1,0,4,3,6,5,8,7};


TGCChannelId* TGCCableASDToPP::getChannel (const TGCChannelId* channelId,
					   bool orChannel) const {
  if(channelId){
    if(channelId->getChannelIdType()==TGCIdBase::ASDOut)
      return getChannelOut(channelId,orChannel);
    if(channelId->getChannelIdType()==TGCIdBase::PPIn)
      return getChannelIn(channelId,orChannel);
  }
  return 0;
}

TGCChannelId*TGCCableASDToPP::getChannelIn (const TGCChannelId* ppin,
					    bool orChannel) const {
  if(orChannel) return 0;
  if(ppin->isValid()==false) return 0;
  
  TGCDatabase* databaseP = 
    getDatabase(ppin->getSideType(),
		ppin->getRegionType(),
		ppin->getSector(),
		ppin->getModuleType());
  
  if(databaseP==0) return 0;

  int indexOut[TGCDatabaseASDToPP::NIndexOut] = 
    {ppin->getId(), ppin->getBlock(), ppin->getChannel()};
  int i = databaseP->getIndexDBOut(indexOut);
  if(i<0) return 0;

  // ASD2PP.db is Backward connection  
  int layer = databaseP->getEntry(i,0);
  if(ppin->isStrip()) {
    if(!(ppin->isBackward())) {
      layer = stripForward[layer];
    }
  }
  int offset = (ppin->isWire()) ? 4 : 0;
  int channel = databaseP->getEntry(i,2+offset);

  // Endcap Triplet chamberId start from 1 in ASDOut
  int chamber = databaseP->getEntry(i,1);
  if(ppin->isEndcap() && ppin->isTriplet()) {
    chamber = chamber+1;
  }
  
  TGCChannelASDOut *asdout = new TGCChannelASDOut(ppin->getSideType(),
						  ppin->getSignalType(),
						  ppin->getRegionType(),
						  ppin->getSector(),
						  layer,
						  chamber,
						  channel);
  
  return asdout;
}

TGCChannelId* TGCCableASDToPP::getChannelOut (const TGCChannelId* asdout,
					      bool orChannel) const {
  if(orChannel) return 0;
  if(asdout->isValid()==false) return 0;

  const bool asdoutisStrip = asdout->isStrip();
  const bool asdoutisBackward = asdout->isBackward();
  const bool asdoutisEndcap = asdout->isEndcap();
  const bool asdoutisTriplet = asdout->isTriplet();
  const int asdoutLayer = asdout->getLayer();
  const int asdoutChamber = asdout->getChamber();
  const int asdoutChannel = asdout->getChannel();

  TGCDatabase* databaseP =
    getDatabase(asdout->getSideType(),
		asdout->getRegionType(),
		asdout->getSector(),
		asdout->getModuleType() );

  if (databaseP ==0) return 0;
  
  TGCChannelPPIn* ppin = 0;
  const int MaxEntry = databaseP->getMaxEntry();
  for(int i=0; i<MaxEntry; i++){
    // ASD2PP.db is Backward connection
    int layer = asdoutLayer;
    if( asdoutisStrip) {
      if ( !asdoutisBackward ) {
	layer = stripForward[layer];
      }
    }
    
    int elecChannel = asdoutChannel;

    // Endcap Triplet chamberId start from 1 in ASDOut
    int chamber = asdoutChamber;
    if(asdoutisEndcap&&asdoutisTriplet)
      chamber = chamber-1;
    int offset = (asdout->isWire())? 4 : 0;  
    if(databaseP->getEntry(i,0)==layer&&
       databaseP->getEntry(i,1)==chamber&&
       databaseP->getEntry(i,2+offset)==elecChannel)
      {
	int id = databaseP->getEntry(i,3);
	int block = databaseP->getEntry(i,4);
	int channel = databaseP->getEntry(i,5);
	
	ppin = new TGCChannelPPIn(asdout->getSideType(),
				  asdout->getModuleType(),
				  asdout->getRegionType(),
				  asdout->getSector(),
				  id,
				  block,
				  channel);
	break;
      } 
  }
  return ppin;
}  

} //end of namespace
