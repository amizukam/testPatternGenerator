/*************************************************************************         
    TGCcablingSvc.cxx
    Author  : Tadashi Maeno
              H.Kurashige        Aug. 2007
              Y.Okumura          Oct. 2007 
              Y. Horii           Mar. 2016!
    Email   : Hisaya Kurashige@cern.ch
    Description : online-offline ID mapper for TGC

***************************************************************************/

#include <iostream>
#include <cmath>
#include <fstream>
#include <string.h>

#include "TGCcabling12/TGCmappingSvc.h"

const int MSG_LEVEL = 4;

const int LEVEL_FATAL   = 4;
const int LEVEL_WARNING = 3;
const int LEVEL_DEBUG   = 2;
const int LEVEL_INFO    = 1;

TGCmappingSvc::TGCmappingSvc ()
{
  
  m_AsideId=103;
  m_CsideId=104;

  std::string s_packages_dir = std::getenv("PACKAGES_DIR");

  std::string s_ASDToPP_file = "/TgcMappingSvc/cablingSvc/ASD2PP_12.db";
  std::string s_ASDToPP = s_packages_dir + s_ASDToPP_file;
  const char* c_ASDToPP = s_ASDToPP.c_str();
  m_databaseASDToPP = strdup(c_ASDToPP);

  std::string s_InPP_file = "/TgcMappingSvc/cablingSvc/PP_12.db";
  std::string s_InPP = s_packages_dir + s_InPP_file;
  const char* c_InPP = s_InPP.c_str();
  m_databaseInPP = strdup(c_InPP);

  std::string s_PPToSL_file = "/TgcMappingSvc/cablingSvc/PP2SL_12.db";
  std::string s_PPToSL = s_packages_dir + s_PPToSL_file;
  const char* c_PPToSL = s_PPToSL.c_str();
  m_databasePPToSL = strdup(c_PPToSL);

  std::string s_SLBToROD_file = "/TgcMappingSvc/cablingSvc/SLB2ROD_12.db";
  std::string s_SLBToROD = s_packages_dir + s_SLBToROD_file;
  const char* c_SLBToROD = s_SLBToROD.c_str();
  m_databaseSLBToROD = strdup(c_SLBToROD);

}
  
TGCmappingSvc::~TGCmappingSvc (void)
{}


int TGCmappingSvc::getAsideId(){return m_AsideId;}
int TGCmappingSvc::getCsideId(){return m_CsideId;}


void TGCmappingSvc::getReadoutIDRanges( int& maxRodId,
					int& maxSswId,
					int& maxSbloc,
					int& minChannelId,
					int& maxChannelId) const
{
  maxRodId = 12;
  maxSswId = 9;
  maxSbloc = 31;
  minChannelId = 40;
  maxChannelId = 199;
}

bool TGCmappingSvc::initialize (void)
{

  std::string dbASDToPP = m_databaseASDToPP;
  std::ifstream inASDToPP;

  if (dbASDToPP != "") {
    inASDToPP.open( dbASDToPP.c_str());
  } else {
    //log << MSG::FATAL << "Could not find input file " << m_databaseASDToPP << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbASDToPP<<" Could not find input file " <<std::endl;
    return false;
  }
  if (inASDToPP.bad()) {
    //log << MSG::FATAL << "Could not open file " << dbASDToPP << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbASDToPP<<" Could not open file " <<std::endl;
    return false;
  }
  
  //std::string dbInPP = PathResolver::find_file (m_databaseInPP, "DATAPATH");
  //log << MSG::DEBUG << "found  " << dbInPP << endreq;
  std::string dbInPP = m_databaseInPP;  
  std::ifstream inInPP;
  if (dbInPP != "") {
    inInPP.open( dbInPP.c_str());
  } else {
    //log << MSG::FATAL << "Could not find input file " << m_databaseInPP <<endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbInPP<<" Could not find input file " <<std::endl;
    return false;
  }
  if (inInPP.bad()) {
    //log << MSG::FATAL << "Could not open file " << dbInPP << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbInPP<<" Could not open file " <<std::endl;
    return false;
  }
  
  //std::string dbPPToSL = PathResolver::find_file (m_databasePPToSL, "DATAPATH");
  //log << MSG::DEBUG << "found  " << dbPPToSL << endreq;
  std::string dbPPToSL = m_databasePPToSL;
  std::ifstream inPPToSL;
  if (dbPPToSL != "") {
    inPPToSL.open( dbPPToSL.c_str());
  } else {
    //log << MSG::FATAL << "Could not find input file " << m_databasePPToSL << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbPPToSL<<" Could not find input file " <<std::endl;
    return false;
  }
  if (inPPToSL.bad()) {
    //log << MSG::FATAL << "Could not open file " << dbPPToSL << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbPPToSL<<" Could not open file " <<std::endl;
    return false;
  }
  
  //std::string dbSLBToROD = PathResolver::find_file (m_databaseSLBToROD, "DATAPATH");
  //log << MSG::DEBUG << "found  " << dbSLBToROD << endreq;
  std::string dbSLBToROD = m_databaseSLBToROD;
  std::ifstream inSLBToROD;
  if (dbSLBToROD != "") {
    inSLBToROD.open( dbSLBToROD.c_str());
  } else {
    //log << MSG::FATAL << "Could not find input file " << m_databaseSLBToROD << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbSLBToROD<<" Could not find input file " <<std::endl; 
    return false;
  }
  if (inSLBToROD.bad()) {
    //log << MSG::FATAL << "Could not open file " << dbSLBToROD << endreq;
    if(MSG_LEVEL <= LEVEL_FATAL) std::cout<<dbSLBToROD<<" Could not open file " <<std::endl;
    return false;
  }
  
  // instantiate TGC cabling manager
  m_cabling = new LVL1TGCCabling12::TGCCabling(dbASDToPP,
					       dbInPP,
					       dbPPToSL,
					       dbSLBToROD);
  
  return true;
}  
  
bool TGCmappingSvc::finalize (void)
{
  delete m_cabling;

  return true;
}

// Online ID has adjacent Readout ID
bool TGCmappingSvc::hasAdjacentChannel(const int subsystemNumber,
				       const int octantNumber,
				       const int moduleNumber,
				       const int layerNumber,
				       const int rNumber,
				       const int wireOrStrip,
				       const int channelNumber) const
{
  int subDetectorID;
  int rodID;
  int sswID;
  int sbLoc;
  int channelID;
  return getReadoutIDfromOnlineID(subDetectorID,
				  rodID,
				  sswID,
				  sbLoc,
				  channelID,
				  subsystemNumber,
				  octantNumber,
				  moduleNumber,
				  layerNumber,
				  rNumber,
				  wireOrStrip,
				  channelNumber,
				  true);
}


// readout IDs -> online IDs
bool TGCmappingSvc::getOnlineIDfromReadoutID(const int subDetectorID,
					     const int rodID,
					     const int sswID,
					     const int sbLoc,
					     const int channelID,
					     int & subsystemNumber,
					     int & octantNumber,
					     int & moduleNumber,
					     int & layerNumber,
					     int & rNumber,
					     int & wireOrStrip,
					     int & channelNumber,
					     bool orChannel) const
{
  //const int stripForward[] = {2,1,0,4,3,6,5,8,7};

  // SideType
  TGCIdBase::SideType sideType = TGCIdBase::NoSideType;
  if(subDetectorID==m_AsideId) sideType = TGCIdBase::Aside;
  if(subDetectorID==m_CsideId) sideType = TGCIdBase::Cside;

  // readout channel -> chamber channel
  
  LVL1TGCCabling12::TGCChannelId* asdout =
    m_cabling->getASDOutFromReadout(sideType,
				    rodID,
				    sswID,
				    sbLoc,
				    channelID,
				    orChannel);
  if(asdout==0) {
    //std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    return false;
  }
  if(!asdout->isValid()){
    std::cerr<<"err> in "<<__FILE__<<" at "<<__LINE__<<std::endl;
    delete asdout;
    return false;
  }

  // SubsystemNumber
  subsystemNumber = (asdout->isAside())? 1 : -1;

  // OctantNumber
  octantNumber = asdout->getOctant();

  // RNumber
  rNumber = asdout->getChamber();

  // ModuleNumber
  moduleNumber = asdout->getSectorModule();

  // LayerNumber
  layerNumber = asdout->getLayer();

  /*
  if( asdout->isStrip()) {
    if ( (!asdout->isBackward() && asdout->isAside())  ||
	 (asdout->isBackward() && asdout->isCside() ) ){
      layerNumber = stripForward[layerNumber];
    }
  }
  */
  

  // WireOrStrip
  wireOrStrip = (asdout->isStrip())? 1 : 0;

  // ChannelNumber
  channelNumber = asdout->getChannel();

  delete asdout;
  return true;
}

// online IDs -> readout IDs
bool TGCmappingSvc::getReadoutIDfromOnlineID(int & subDetectorID,
					     int & rodID,
					     int & sswID,
					     int & sbLoc,
					     int & channelID,
					     const int subsystemNumber,
					     const int octantNumber,
					     const int moduleNumber,
					     const int layerNumber,
					     const int rNumber,
					     const int wireOrStrip,
					     const int channelNumber,
					     bool adChannel) const
{
  //MsgStream log( msgSvc(), name() );

  if(MSG_LEVEL<=LEVEL_DEBUG) std::cout<<"TGCmappingSvc::getReadoutIDfromOnlineID"<<std::endl;

  // SideType
  TGCIdBase::SideType sideType = 
    TGCIdBase::NoSideType;
  if(subsystemNumber==1)  sideType = TGCIdBase::Aside;
  if(subsystemNumber==-1) sideType = TGCIdBase::Cside;

  // SignalType
  TGCIdBase::SignalType signalType = 
    TGCIdBase::NoSignalType;
  if(wireOrStrip==0) signalType = TGCIdBase::Wire;
  if(wireOrStrip==1) signalType = TGCIdBase::Strip;

  if(MSG_LEVEL<=LEVEL_DEBUG) std::cout<<"\n"
				      <<"sideType               :"<<sideType<<"\n"
				      <<"signalType(wireOrStrip):"<<signalType<<"\n"
				      <<"octantNumber           :"<<octantNumber<<"\n"
				      <<"moduleNumber           :"<<moduleNumber<<"\n"
				      <<"layerNumber            :"<<layerNumber<<"\n"
				      <<"rNumber                :"<<rNumber<<"\n"
				      <<"channelNumber          :"<<channelNumber<<std::endl;
  // ASDOut
  LVL1TGCCabling12::TGCChannelASDOut asdout(sideType,
					  signalType,
					  octantNumber,
					  moduleNumber,
					  layerNumber,
					  rNumber,
					  channelNumber);  
  if (!asdout.isValid()) {
    //log << MSG::DEBUG << "getReadoutIDfromOnlineID  Invalid ASDOut" << endreq;
    if(MSG_LEVEL >= LEVEL_DEBUG) std::cout << "getReadoutIDfromOnlineID  Invalid ASDOut" << std::endl;
    return false;
  }

  if(MSG_LEVEL<=LEVEL_DEBUG) std::cout<<"Success in generation of ASDout"<<std::endl;
  
  // chamber channel -> readout channel
  bool status = m_cabling->getReadoutFromASDOut(&asdout,
						sideType,
						rodID,
						sswID,
						sbLoc,
						channelID,
						adChannel);
  if (!status) {
    //log << MSG::DEBUG << "getReadoutIDfromASDOut fails" << endreq;
    if(MSG_LEVEL <= LEVEL_INFO) std::cout << "getReadoutIDfromASDOut fails" << std::endl;
    return false;
  }


  if(MSG_LEVEL<=LEVEL_INFO) std::cout<<"Success in calcration in getReadoutFromASSDOut"<<std::endl;

  // SubDetectorID
  if(sideType==TGCIdBase::Aside) subDetectorID = m_AsideId;
  if(sideType==TGCIdBase::Cside) subDetectorID = m_CsideId;

  return status;
}

// readout ID -> SLB ID
  bool TGCmappingSvc::getSLBIDfromReadoutID(int & phi,
					    bool & isAside,
					    bool & isEndcap,
					    int & moduleType,
					    int & id,
					    const int subsectorID,
					    const int rodID,
					    const int sswID,
					    const int sbLoc) const
{
  isAside = (subsectorID==m_AsideId);

  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromReadout(side, rodID, sswID, sbLoc);
  if (!slb) return false;

  isEndcap = (slb->getRegionType()==TGCIdBase::Endcap);
  moduleType = (int)slb->getModuleType();
  bool isInner = (moduleType==TGCIdBase::WI || moduleType==TGCIdBase::SI);
  int offset, numOfSector;
  if (isInner) {
    numOfSector = LVL1TGCCabling12::TGCId::NumberOfInnerSector;
  } else {
    if (isEndcap) {
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
    }else{
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfForwardSector;
    }
  } 
  offset = numOfSector -  numOfSector/24;
  phi = (slb->getSector()+offset)%numOfSector +1;
  id = slb->getId();
  return true;
}


bool TGCmappingSvc::getSLBAddressfromReadoutID(int & slbAddr,
					       const int subsectorID,
					       const int rodID,
					       const int sswID,
					       const int sbLoc) const
{
  slbAddr = -1;
  
  bool isAside = (subsectorID==m_AsideId);
  
  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromReadout(side, rodID, sswID, sbLoc);
  if (!slb) return false;
  
  slbAddr =  ( dynamic_cast<LVL1TGCCabling12::TGCModuleSLB*>(slb) )->getSlbAddr(); 
  delete slb; slb = 0; 
  return true;
}

// ROD_ID / SSW_ID / RX_ID -> SLB ID
bool TGCmappingSvc::getSLBIDfromRxID(int &phi,
				     bool & isAside,
				     bool & isEndcap,
				     int & moduleType,
				     int & id,
				     const int subsectorID,
				     const int rodID,
				     const int sswID,
				     const int rxId) const
{
  isAside = (subsectorID==m_AsideId);

  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromRxId(side, rodID, sswID, rxId);
  if (!slb) return false;

  isEndcap = (slb->getRegionType()==TGCIdBase::Endcap);
  moduleType = (int)slb->getModuleType();
  bool isInner = (moduleType==TGCIdBase::WI || moduleType==TGCIdBase::SI);
  int offset, numOfSector;
  if (isInner) {
    numOfSector = LVL1TGCCabling12::TGCId::NumberOfInnerSector;
  } else {
    if (isEndcap) {
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
    }else{
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfForwardSector;
    }
  } 
  offset = numOfSector -  numOfSector/24;
  phi = (slb->getSector()+offset)%numOfSector +1;
  id = slb->getId();
  
  delete slb; slb = 0; 
  return true;
}

// SLB ID -> readout ID
bool TGCmappingSvc::getReadoutIDfromSLBID(const int phi,
					  const bool isAside,
					  const bool isEndcap,
					  const int moduleType,
					  const int id,
					  int & subsectorID,
					  int & rodID,
					  int & sswID,
					  int & sbLoc) const
{
  //MsgStream log( msgSvc(), name() );
  
  TGCIdBase::ModuleType module = static_cast<TGCIdBase::ModuleType>(moduleType);
  TGCIdBase::RegionType region = isEndcap ? TGCIdBase::Endcap : TGCIdBase::Forward;
  bool isInner = (module==TGCIdBase::WI || module==TGCIdBase::SI); 
  int sector ;// sector=0-47(EC), 0-23(FWD), 0-23(INNER)
  if (isInner) {
    sector = phi % LVL1TGCCabling12::TGCId::NumberOfInnerSector;
  } else if(isEndcap) {
    sector = (phi+1) % LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
  } else {
    sector = phi  % LVL1TGCCabling12::TGCId::NumberOfForwardSector;
  }
  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;

  LVL1TGCCabling12::TGCModuleSLB slb(side, module, region, sector, id);

   if (!slb.isValid()) {
     /*
       log << MSG::DEBUG
       << "getReadoutIDfromSLBID "
       << " phi=" << phi
       << " side=" << ( (isAside)? "A": "C") 
       << " region=" << ( (isEndcap)? "Endcap": "Forward")
       << " type=" << moduleType
       << " id=" << id 
       << "Invalid SLB" << endreq;
     */
     if(MSG_LEVEL <= LEVEL_INFO)
       std::cout 
		 << "getReadoutIDfromSLBID "
		 << " phi=" << phi
		 << " side=" << ( (isAside)? "A": "C") 
		 << " region=" << ( (isEndcap)? "Endcap": "Forward")
		 << " type=" << moduleType
		 << " id=" << id 
		 << "Invalid SLB" << std::endl;
  }
   
   if (!slb.isValid()) return false;
   
   subsectorID = (isAside ? m_AsideId : m_CsideId);
   
   bool status = m_cabling->getReadoutFromSLB(&slb, side, rodID, sswID, sbLoc);
   
   if (!status) {
     //log << MSG::DEBUG << " FAIL  getReadoutIDfromSLBID" << endreq;
     if(MSG_LEVEL >= LEVEL_INFO) std::cout << " FAIL  getReadoutIDfromSLBID" << std::endl;
  } else {
    if(MSG_LEVEL >= LEVEL_INFO){
      std::cout << " SUCCESS  getReadoutIDfromSLBID" << std::endl;
      std::cout	<< " phi=" << phi
		<< " side=" << ( (isAside)? "A": "C") 
		<< " region=" << ( (isEndcap)? "Endcap": "Forward")
		<< " type=" << moduleType
		<< " id=" << id 
		<< " side" << side 
		<< " rodID=" << rodID 
		<< " sswID=" << sswID
		<< " sbLoc=" << sbLoc  <<  std::endl;
    }
  } 
  if (!status) return false;
  return true;
}


// readout ID -> SL ID 
bool TGCmappingSvc::getSLIDfromReadoutID(int & phi,
			  bool & isAside,
			  bool & isEndcap,
			  const int subsectorID,
			  const int rodID,
			  const int sswID,
			  const int sbLoc) const
{
  isAside = (subsectorID==m_AsideId);
  if (!isAside && (subsectorID!=m_CsideId)) return false;
 
  int sectorInReadout = ( rodID -1 ); // rodID = 1..12 for both sides
  if (sectorInReadout>= LVL1TGCCabling12::TGCId::NumberOfReadoutSector) return false;
  
  // sswID
  // sswID for SL is fixed to 9
  if (sswID != 9) return false; 
  
  int offset, numOfSector, sector;
  if( 0<=sbLoc && sbLoc <= 3) {
    isEndcap=true;
    numOfSector = LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
    offset = numOfSector -  numOfSector/24;
    sector = numOfSector * sectorInReadout /  LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
    phi = (sector + sbLoc + offset)%numOfSector+1;

  } else if ( sbLoc==4 || sbLoc == 5){
      isEndcap=false;
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfForwardSector;
      offset = numOfSector -  numOfSector/24;
      sector = numOfSector * sectorInReadout /  LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
      phi = (sector + (sbLoc-4) + offset)% numOfSector+1;
  } else {
    return false;
  }
  return true;  
}

// SL ID -> readout ID
bool TGCmappingSvc::getReadoutIDfromSLID(const int phi,
			  const bool isAside,
			  const bool isEndcap,
			  int & subsectorID,
			  int & rodID,
			  int & sswID,
			  int & sbLoc) const
{
  if(isAside)subsectorID=m_AsideId;
  else subsectorID=m_CsideId;

  if(phi<1 || phi> 48) return false;

  int sector;
  int sectorInReadout;
  if (isEndcap) {
    sector = (phi+1)% LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
    sectorInReadout = sector %  
      (LVL1TGCCabling12::TGCId::NumberOfEndcapSector / LVL1TGCCabling12::TGCId::NumberOfReadoutSector);
    sbLoc = sectorInReadout;
    rodID = (sector-sectorInReadout)/
      (LVL1TGCCabling12::TGCId::NumberOfEndcapSector / LVL1TGCCabling12::TGCId::NumberOfReadoutSector)
      + 1 ;
  } else {
    sector = phi % LVL1TGCCabling12::TGCId::NumberOfForwardSector;
    sectorInReadout = sector % 
      (LVL1TGCCabling12::TGCId::NumberOfForwardSector / LVL1TGCCabling12::TGCId::NumberOfReadoutSector);
    sbLoc = sectorInReadout + 4;
    rodID = (sector-sectorInReadout)/
      (LVL1TGCCabling12::TGCId::NumberOfForwardSector / LVL1TGCCabling12::TGCId::NumberOfReadoutSector)
      +1;
 }
  // Fixed SSWID for SL 
  sswID = 9;
  
  return true;
}


// HPT ID -> readout ID
bool TGCmappingSvc::getReadoutIDfromHPTID(const int phi,
			   const bool isAside,
			   const bool isEndcap,
			   const bool ,
			   const int ,
			   int & subsectorID,
			   int & rodID,
			   int & sswID,
			   int & sbLoc) const
{
  return getReadoutIDfromSLID(phi, isAside, isEndcap,
			      subsectorID,
			      rodID,
			      sswID,
			      sbLoc);
}



// channel connection
LVL1TGCCabling12::TGCChannelId* 
TGCmappingSvc::getChannel (const LVL1TGCCabling12::TGCChannelId* channelId,
			   TGCIdBase::ChannelIdType type,
			   bool orChannel) const 
{
  return m_cabling->getChannel(channelId,type,orChannel);
}


// module connection
LVL1TGCCabling12::TGCModuleMap* 
TGCmappingSvc::getModule (const LVL1TGCCabling12::TGCModuleId* moduleId,
			  LVL1TGCCabling12::TGCModuleId::ModuleIdType type) const
{
  return m_cabling->getModule(moduleId,type);
}

// give phi-range which a ROD covers  
bool TGCmappingSvc::getCoveragefromRodID(
					 const int rodID,
					 double & startPhi,
					 double & endPhi
					 ) const
{
  int sectorInReadout = rodID - 1;  //rodID = 1..12
  if (sectorInReadout>= LVL1TGCCabling12::TGCId::NumberOfReadoutSector) return false;
  
  startPhi = 2.*M_PI*(sectorInReadout-0.5)/LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
  endPhi = startPhi + 2.*M_PI/LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
 
  return true; 
}

bool TGCmappingSvc::getCoveragefromRodID(
					 const int rodID,
					 int & startEndcapSector,
					 int & coverageOfEndcapSector,
					 int & startForwardSector,
					 int & coverageOfForwardSector
					 ) const
{
  int sectorInReadout = rodID - 1;  //rodID = 1..12
  if (sectorInReadout>= LVL1TGCCabling12::TGCId::NumberOfReadoutSector) return false;
  
  coverageOfEndcapSector =  
    LVL1TGCCabling12::TGCId::NumberOfEndcapSector /
    LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
  startEndcapSector = sectorInReadout *  coverageOfEndcapSector;
  coverageOfForwardSector =  
    LVL1TGCCabling12::TGCId::NumberOfForwardSector /
    LVL1TGCCabling12::TGCId::NumberOfReadoutSector;
  startForwardSector = sectorInReadout *coverageOfForwardSector;  
  
  return true; 
}

// for verifing 
bool TGCmappingSvc::writtenBy()
{
  std::cout<<"TGCmappingSvc"<<std::endl;
  std::cout<<"Yasuyuki Okumura (Nagoya Univ.) Nov. 2007"<<std::endl;
  return 0;
}



bool TGCmappingSvc::getReadoutIDfromRxID(const int subsectorID,
			  const int rodID,
			  const int sswID,
			  const int rxID,
			  int& sbLoc )
{
  int isAside = (subsectorID==m_AsideId);  
  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  
  LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromRxId(side, rodID, sswID, rxID);
  
  sbLoc = dynamic_cast<LVL1TGCCabling12::TGCModuleSLB*>(slb)->getSBLoc();
  

  //bool status = m_cabling->getReadoutFromSLB(slb, side, tmp_rodID, tmp_sswID, sbLoc);
  
  //if(!status) return false;
  
  return true;
}

bool TGCmappingSvc::getRxIDfromReadoutID(const int subsectorID,
					 const int rodID,
					 const int sswID,
					 const int sbLoc,
					 int& rxId)
{
  int isAside = (subsectorID==m_AsideId);  
  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  
  //LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromReadout(side, rodID, sswID, rxId);
  rxId = m_cabling->getRxIdFromReadout(side, rodID, sswID, sbLoc);
  //rxId = dynamic_cast<LVL1TGCCabling12::TGCModuleSLB*>(slb)->getSBLoc();
  
  return true;
}


std::string TGCmappingSvc::getPsTypefromPsTypeID(int psTypeID){
  switch(psTypeID){
  case 0:
    return "PsEWT0";
  case 1:
    return "PsEWT1";
  case 2:
    return "PsEWT2";
  case 3:
    return "PsEST";
  case 4:
    return "PsFT0";
  case 5:
    return "PsFT1";
  case 6:
    return "PsEWD0";
  case 7:
    return "PsEWD1";
  case 8:
    return "PsEWD2";
  case 9:
    return "PsEWD3";
  case 10:
    return "PsEWD4";
  case 11:
    return "PsESD0";
  case 12:
    return "PsESD1";
  case 13:
    return "PsFWD0";
  case 14:
    return "PsFWD1";
  case 15:
    return "PsFWS";
  }
  
}

// ROD_ID / SSW_ID / RX_ID -> SLB ID
bool TGCmappingSvc::decodeId(int &phi,
			     bool & isAside,
			     bool & isEndcap,
			     int & moduleType,
			     int & sbLoc,
			     const int subsectorID,
			     const int rodID,
			     const int sswID,
			     const int rxId)
{
  isAside = (subsectorID==m_AsideId);

  TGCIdBase::SideType side = isAside ? TGCIdBase::Aside :TGCIdBase::Cside;
  LVL1TGCCabling12::TGCModuleId * slb = m_cabling->getSLBFromRxId(side, rodID, sswID, rxId);
  if (!slb) return false;

  isEndcap = (slb->getRegionType()==TGCIdBase::Endcap);
  moduleType = (int)slb->getModuleType();
  bool isInner = (moduleType==TGCIdBase::WI || moduleType==TGCIdBase::SI);
  int offset, numOfSector;
  if (isInner) {
    numOfSector = LVL1TGCCabling12::TGCId::NumberOfInnerSector;
  } else {
    if (isEndcap) {
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfEndcapSector;
    }else{
      numOfSector = LVL1TGCCabling12::TGCId::NumberOfForwardSector;
    }
  } 
  offset = numOfSector -  numOfSector/24;
  phi = (slb->getSector()+offset)%numOfSector +1;

  sbLoc = dynamic_cast<LVL1TGCCabling12::TGCModuleSLB*>(slb)->getSBLoc();
  delete slb;
  return true;
}
