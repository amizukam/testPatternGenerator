#include "TGCcabling12/TGCJtagMapping.h"


#include <iostream>

namespace LVL1TGCCabling12 {

// Constructor & Destructor
TGCJtagMapping::TGCJtagMapping (std::string filenameASDToPPJTAG,
				std::string filenameSLBToSLBJTAG)
{
  jtagASDToPP   = new TGCJtagASDToPP(filenameASDToPPJTAG);
  jtagSLBToSLB  = new TGCJtagSLBToSLB(filenameSLBToSLBJTAG);
}

TGCJtagMapping::~TGCJtagMapping (void) 
{
  delete jtagASDToPP;
  delete jtagSLBToSLB;
}

bool TGCJtagMapping::getPpsInfoFromASDOUT(const TGCChannelId* asdout, int& ppsId, int& ppsOrder, int& ppsMax, int& ppsIsB, int& channelInPps){
  TGCJtagId* pps = new TGCJtagId();
  
  ppsId=-1;
  ppsOrder=-1;
  ppsMax=-1;
  ppsIsB=-1;
  
  pps=jtagASDToPP->getPps(asdout);
  
  if(!pps) return false;
  
  ppsId=pps->getJtagId();
  ppsOrder=pps->getOrder();
  ppsMax=pps->getMax();
  ppsIsB=pps->getAorB();
  
  channelInPps=(asdout->getChannel())-(pps->getChanMin())+(pps->getOrder())*8;
  
  delete pps;

  return true;
}



} //end of namespace
