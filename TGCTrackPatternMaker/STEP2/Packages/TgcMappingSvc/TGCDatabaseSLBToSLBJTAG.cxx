#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "TGCcabling12/TGCDatabaseSLBToSLBJTAG.h"

namespace LVL1TGCCabling12 
{

void TGCDatabaseSLBToSLBJTAG::readDB (void) {
  std::ifstream file(filename.c_str());
  std::string buf;

  while(getline(file,buf)){
    if(buf.substr(0,blockname.size())==blockname) break;
  }

  while(getline(file,buf)){
    if(buf.substr(0,1)=="E"||buf.substr(0,1)=="F") break;
    std::istringstream line(buf);
    std::vector<int> entry;
    for(int i=0; i<11; i++){  /// must be changed (okumura)
      int temp=-1;
      line >> temp; 
      entry.push_back(temp);
    }
    database.push_back(entry);
  }
  file.close();
}
  

} // end of namespace
