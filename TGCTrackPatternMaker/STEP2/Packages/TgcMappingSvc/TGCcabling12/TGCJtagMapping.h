
#ifndef TGCJTAGMAPPING_HH
#define TGCJTAGMAPPING_HH
  
//#include "TGCcablingInterface/TGCCablingBase.h"
#include "TGCcabling12/TGCJtagASDToPP.h"
#include "TGCcabling12/TGCJtagSLBToSLB.h"
#include "TGCcabling12/TGCJtagId.h"

#include "TGCcabling12/TGCChannelASDOut.h"

//#include "TGCcabling12/TGCCableInASD.h"
//#include "TGCcabling12/TGCCableASDToPP.h"
//#include "TGCcabling12/TGCCableInPP.h"
//#include "TGCcabling12/TGCCablePPToSLB.h"
//#include "TGCcabling12/TGCCableInSLB.h"
//#include "TGCcabling12/TGCCableSLBToHPB.h"
//#include "TGCcabling12/TGCCableHPBToSL.h"
//#include "TGCcabling12/TGCCableSLBToSSW.h"
//#include "TGCcabling12/TGCCableSSWToROD.h"


namespace LVL1TGCCabling12
{

class TGCJtagMapping
{
public:
  // Constructor & Destructor
  TGCJtagMapping (std::string filenameASDToPPJTAG,
		  std::string filenameSLBToSLBJTAG);
  
  virtual ~TGCJtagMapping (void);
  
  bool getPpsInfoFromASDOUT(const TGCChannelId* asdout, int& ppsId, int& ppsOrder, int& ppsMax, int& ppsIsB, int& channelInPps);
  

private:
  TGCJtagASDToPP*   jtagASDToPP;
  TGCJtagSLBToSLB*  jtagSLBToSLB;
  TGCJtagMapping (void) {}
  
};

} // end of namespace

#endif
