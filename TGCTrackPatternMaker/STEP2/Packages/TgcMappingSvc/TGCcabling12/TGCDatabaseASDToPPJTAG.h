#ifndef TGCDATABASEASDTOPPJTAG_HH
#define TGCDATABASEASDTOPPJTAG_HH

#include "TGCcabling12/TGCDatabase.h"

namespace LVL1TGCCabling12
{
 
class TGCDatabaseASDToPPJTAG : public TGCDatabase
{
public:
  // Constructor & Destructor
  TGCDatabaseASDToPPJTAG (std::string filename, std::string blockname)
    : TGCDatabase(TGCDatabase::ASDToPPJTAG)
  {
    this->filename=filename;
    this->blockname=blockname;
  }
  virtual ~TGCDatabaseASDToPPJTAG (void){}
  
private:
  virtual void readDB (void);
  TGCDatabaseASDToPPJTAG (void) {}
};
  
} // end of namespace
 
#endif
