#ifndef TGCJTAGSLBTOSLB_HH
#define TGCJTAGSLBTOSLB_HH
 
#include <string>
//#include "TGCcabling12/TGCCable.h"
#include "TGCcabling12/TGCId.h" //requierd
//#include "TGCcabling12/TGCDatabaseASDToPP.h" 
#include "TGCcabling12/TGCDatabaseSLBToSLBJTAG.h" 
//#include "TGCcabling12/TGCChannelASDOut.h"
//#include "TGCcabling12/TGCChannelPPIn.h"

namespace LVL1TGCCabling12
{
  
class TGCJtagSLBToSLB
{
public:
  // Constructor & Destructor
  TGCJtagSLBToSLB (std::string filename);
  virtual ~TGCJtagSLBToSLB (void);


private:
  TGCJtagSLBToSLB (void) {}
  TGCDatabase* database[TGCIdBase::MaxRegionType][TGCIdBase::MaxModuleType];
};
  
} // end of namespace
 
#endif
