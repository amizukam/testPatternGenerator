#ifndef TGCDATABASESLBTOSLBJTAG_HH
#define TGCDATABASESLBTOSLBJTAG_HH

#include "TGCcabling12/TGCDatabase.h"

namespace LVL1TGCCabling12
{
 
class TGCDatabaseSLBToSLBJTAG : public TGCDatabase
{
public:
  // Constructor & Destructor
  TGCDatabaseSLBToSLBJTAG (std::string filename, std::string blockname)
    : TGCDatabase(TGCDatabase::SLBToSLBJTAG)
  {
    this->filename=filename;
    this->blockname=blockname;
  }
  virtual ~TGCDatabaseSLBToSLBJTAG (void){}
  
private:
  virtual void readDB (void);
  TGCDatabaseSLBToSLBJTAG (void) {}
};
  
} // end of namespace
 
#endif
