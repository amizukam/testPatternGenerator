#ifndef TGCJTAGASDTOPP_HH
#define TGCJTAGASDTOPP_HH
 
#include <string>
//#include "TGCcabling12/TGCCable.h"
#include "TGCcabling12/TGCId.h" //requierd
//#include "TGCcabling12/TGCDatabaseASDToPP.h" 
#include "TGCcabling12/TGCDatabaseASDToPPJTAG.h" 
#include "TGCcabling12/TGCChannelASDOut.h"
#include "TGCcabling12/TGCJtagId.h"
//#include "TGCcabling12/TGCChannelPPIn.h"

namespace LVL1TGCCabling12
{
  
class TGCJtagASDToPP
{
public:
  // Constructor & Destructor
  TGCJtagASDToPP (std::string filename);
  virtual ~TGCJtagASDToPP (void);

  TGCJtagId* getPps(const TGCChannelId* asdout);

private:
  TGCJtagASDToPP (void) {}
  TGCDatabase* database[TGCIdBase::MaxRegionType][TGCIdBase::MaxModuleType];
};
  
} // end of namespace
 
#endif
