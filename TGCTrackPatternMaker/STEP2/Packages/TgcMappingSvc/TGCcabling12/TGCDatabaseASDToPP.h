#ifndef TGCDATABASEASDTOPP_HH
#define TGCDATABASEASDTOPP_HH

#include "TGCcabling12/TGCDatabase.h"

namespace LVL1TGCCabling12
{
 
class TGCDatabaseASDToPP : public TGCDatabase
{
public:
  // Constructor & Destructor
  TGCDatabaseASDToPP (std::string filename, std::string blockname);

  TGCDatabaseASDToPP (const TGCDatabaseASDToPP&);

  virtual ~TGCDatabaseASDToPP (void);

  virtual bool update(const std::vector<int>& );
 
  virtual int  find(const std::vector<int>&) const;

  virtual int getIndexDBOut(int* indexOut);

  virtual void getindexDBVectorOut(std::vector<int>& tmpindexDBOut) const;
  virtual void getNIndexDBOut(int& tmpNIndexDBOut) const; 
  virtual void getmaxIndexOut(int* tmpmaxIndexOut) const; 
  virtual void getminIndexOut(int* tmpminIndexOut) const; 

  enum {NIndexOut=3, IndexOutMin=3, IndexOutMax=5};
  
private:
  virtual void readDB (void);
  TGCDatabaseASDToPP (void) {}

  virtual void makeIndexDBOut(void);
  virtual int convertIndexDBOut(int* indexOut) const;

  std::vector<int> indexDBOut;
  int NIndexDBOut;
  int maxIndexOut[NIndexOut];
  int minIndexOut[NIndexOut];
};
  
} // end of namespace
 
#endif
