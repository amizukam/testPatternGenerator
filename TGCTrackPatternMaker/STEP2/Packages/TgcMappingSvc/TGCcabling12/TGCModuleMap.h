#ifndef TGCMODULEMAP_HH
#define TGCMODULEMAP_HH

#include <map>

#include "TGCcabling12/TGCModuleId.h"

namespace LVL1TGCCabling12
{
 
class TGCModuleMap
{
public:
  // Constructor & Destructor
  TGCModuleMap (void)
  {}

  virtual ~TGCModuleMap (void);
  
  int connector (int entry);

  TGCModuleId* moduleId (int entry);

  TGCModuleId* popModuleId (int entry);

  void insert (int connector, TGCModuleId* moduleId);

  int find (int connector);
  
  int size (void);

  void clear (void);

private:
  std::map<int,TGCModuleId*> moduleMap;
};
  
} // end of namespace
 
#endif
