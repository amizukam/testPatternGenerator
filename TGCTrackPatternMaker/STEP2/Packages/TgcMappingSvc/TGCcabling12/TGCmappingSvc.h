/***************************************************************************
         TGCcabling12Svc.h

    Author  : Tadashi Maeno
              H.Kurashige           Aug. 2007
    Email   : Hisaya.Kurashige@cern.ch
    Description : online-offline ID mapper for TGC
  
***************************************************************************/

#ifndef TGCCABLING_TGCMAPPINGSVC_H
#define TGCCABLING_TGCMAPPINGSVC_H

//class StatusCode;
//class TgcIdHelper;

#include <string>
#include <vector>
#include <algorithm>

//#include "GaudiKernel/Service.h"

//#include "GaudiKernel/MsgStream.h"

//#include "TGCcablingInterface/ITGCcablingSvc.h"
#include "TGCcabling12/TGCCabling.h"
#include "TGCcablingInterface/TGCIdBase.h"

class TGCmappingSvc // : public ITGCcablingSvc
{
public:
  //TGCmappingSvc (const std::string& name, ISvcLocator* svc);
  TGCmappingSvc ();
  virtual ~TGCmappingSvc (void);

  /*
  static const InterfaceID& interfaceID() 
  {
    return  IID_TGCcablingSvc;
  }
  */

  //virtual StatusCode queryInterface(const InterfaceID& riid,void** ppvIF); 
  
  //virtual StatusCode initialize (void);
  //virtual StatusCode finalize (void);

  virtual bool initialize (void);
  virtual bool finalize (void);

  //////  virtual methods of ITGCmappingSvc
 public:
  virtual TGCCablingBase* getTGCCabling() const;  


  // give max value of ReadoutID parameters
  void getReadoutIDRanges( int& maxRodId,
			   int& maxSswId,
			   int& maxSbloc,
			   int& minChannelId,
			   int& maxChannelId) const;

  // give phi-range which a ROD covers  
  bool getCoveragefromRodID(const int rodID,
			    double & startPhi,
			    double & endPhi
			    ) const;

  bool getCoveragefromRodID(const int rodID,
			    int & startEndcapSector,
			    int & coverageOfEndcapSector,
			    int & startForwardSector,
			    int & coverageOfForwardSector
			    ) const;
  
  
  // Readout ID is ored
  bool isOredChannel(const int subDetectorID,
		     const int rodID,
		     const int sswID,
		     const int sbLoc,
		     const int channelID) const;


  // Online ID has adjacent Readout ID
  bool hasAdjacentChannel(const int subsystemNumber,
			  const int octantNumber,
			  const int moduleNumber,
			  const int layerNumber,
			  const int rNumber,
			  const int wireOrStrip,
			  const int channelNumber) const;

  
  // readout IDs -> online IDs
  bool getOnlineIDfromReadoutID(const int subDetectorID,
				const int rodID,
				const int sswID,
				const int sbLoc,
				const int channelID,
				int & subsystemNumber,
				int & octantNumber,
				int & moduleNumber,
				int & layerNumber,
				int & rNumber,
				int & wireOrStrip,
				int & channelNumber,
				bool orChannel=false) const;

  // online IDs -> readout IDs
  bool getReadoutIDfromOnlineID(int & subDetectorID,
				int & rodID,
				int & sswID,
				int & sbLoc,
				int & channelID,
				const int subsystemNumber,
				const int octantNumber,
				const int moduleNumber,
				const int layerNumber,
				const int rNumber,
				const int wireOrStrip,
				const int channelNumber,
				bool adChannel=false) const;
  
  // HPT ID -> readout ID
  bool getReadoutIDfromHPTID(const int phi,
			     const bool isAside,
			     const bool isEndcap,
			     const bool isStrip,
			     const int id,
			     int & subsectorID,
			     int & rodID,
			     int & sswID,
			     int & sbLoc) const;

  // readout ID -> SLB ID
  bool getSLBIDfromReadoutID(int &phi,
			     bool & isAside,
			     bool & isEndcap,
			     int & moduleType,
			     int & id,
			     const int subsectorID,
			     const int rodID,
			     const int sswID,
			     const int sbLoc) const;

  // readout ID -> slbAddr
  bool getSLBAddressfromReadoutID(int & slbAddr,
				  const int subsectorID,
				  const int rodID,
				  const int sswID,
				  const int sbLoc) const;

  // ROD_ID / SSW_ID / RX_ID -> SLB ID
  bool getSLBIDfromRxID(int &phi,
			bool & isAside,
			bool & isEndcap,
			int & moduleType,
			int & id,
			const int subsectorID,
			const int rodID,
			const int sswID,
			const int rxId) const;
  

  // SLB ID -> readout ID
  bool getReadoutIDfromSLBID(const int phi,
			     const bool isAside,
			     const bool isEndcap,
			     const int moduleType,
			     const int id,
			     int & subsectorID,
			     int & rodID,
			     int & sswID,
			     int & sbLoc) const;
  
  // readout ID -> SL ID
  bool getSLIDfromReadoutID(int & phi,
			    bool & isAside,
			    bool & isEndcap,
			    const int subsectorID,
			    const int rodID,
			    const int sswID,
			    const int sbLoc) const;

  // SL ID -> readout ID
  bool getReadoutIDfromSLID(const int phi,
			    const bool isAside,
			    const bool isEndcap,
			    int & subsectorID,
			    int & rodID,
			    int & sswID,
			    int & sbLoc) const;

  int getAsideId();
  int getCsideId();
  

  bool getReadoutIDfromRxID(const int subDetectorID,
			    const int rodID,
			    const int sswID,
			    const int rxID,
			    int& sbLoc
			    );

  bool getRxIDfromReadoutID(const int subsectorID,
			    const int rodID,
			    const int sswID,
			    const int sbLoc,
			    int& rxId);

  bool decodeId(int &phi,
		bool & isAside,
		bool & isEndcap,
		int & moduleType,
		int & sbLoc,
		const int subsectorID,
		const int rodID,
		const int sswID,
		const int rxId);

  std::string getPsTypefromPsTypeID(int psTypeID);

  bool writtenBy();

  /////////////////////////////////////////////////////////////
  // channel connection
  LVL1TGCCabling12::TGCChannelId* 
    getChannel (const LVL1TGCCabling12::TGCChannelId* channelId,
		TGCIdBase::ChannelIdType type,
		bool orChannel=false) const;
  
  // module connection
  LVL1TGCCabling12::TGCModuleMap* 
    getModule (const LVL1TGCCabling12::TGCModuleId* moduleId,
	       LVL1TGCCabling12::TGCModuleId::ModuleIdType type ) const;	

  ///////////////////////  

private:
  //const TgcIdHelper * m_idHelper;


  int m_AsideId;
  int m_CsideId;
  int m_rodId;

  LVL1TGCCabling12::TGCCabling* m_cabling;


  
  std::string m_databaseASDToPP;
  std::string m_databaseInPP;
  std::string m_databasePPToSL;
  std::string m_databaseSLBToROD;

};

inline
 TGCCablingBase* TGCmappingSvc::getTGCCabling() const
{
  return m_cabling;
}  

#endif

