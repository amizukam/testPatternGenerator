#ifndef TGCJTAGID_HH
#define TGCJTAGID_HH

#include <string>

namespace LVL1TGCCabling12
{
  
  class TGCJtagId
    {
    public:
      // Constructor and Destructor
      TGCJtagId (){        
      }
      
      // set functions
      bool setJtagName(std::string tmp_name);
      bool setJtagId(int tmpJtagId);
      bool setPsTypeId(int tmpPsTypeId);
      bool setOrder(int tmpOrder);
      bool setMax(int tmpMax);
      bool setAorB(int tmpAorB);
      bool setChanMax(int tmpChanMax);
      bool setChanMin(int tmpChanMin);
      bool setSide(int tmpSide);
      bool setPhi(int tmpPhi);
      bool setLayer(int tmpLayer);
      bool setChamber(int tmpChamber);
      
      
      // get functions
      std::string getJtagName();
      int getJtagId();
      int getPsTypeId();
      int getOrder();
      int getMax();
      int getAorB();
      int getChanMax();
      int getChanMin();
      int getSide();
      int getPhi();
      int getLayer();
      int getChamber();

    private:
      std::string jtagName;
      int jtagId;
      int psTypeId;
      int order;
      int max;
      int aorB;
      int chanMax;
      int chanMin; 
      int psbId;
      int side;
      int phi;
      int layer;
      int chamber;
    };
  
}

#endif
