#include "TGCcabling12/TGCJtagASDToPP.h"
#include <iostream>

const int MSG_LEVEL = 1;

// 4=FATAL 3=WARNING 2=DEBUG 1=INFO
const int LEVEL_FATAL   = 4;
const int LEVEL_WARNING = 3;
const int LEVEL_DEBUG   = 2;
const int LEVEL_INFO    = 1;


bool getPhiIdSectorId(int octant);

namespace LVL1TGCCabling12 {

// Constructor & Destructor
TGCJtagASDToPP::TGCJtagASDToPP (std::string filename)
{
  database[TGCIdBase::Endcap][TGCIdBase::WD] = 
    new TGCDatabaseASDToPPJTAG(filename,"EWD");
  database[TGCIdBase::Endcap][TGCIdBase::WT] = 
    new TGCDatabaseASDToPPJTAG(filename,"EWT");
  database[TGCIdBase::Endcap][TGCIdBase::SD] =
    new TGCDatabaseASDToPPJTAG(filename,"ESD");
  database[TGCIdBase::Endcap][TGCIdBase::ST] =
    new TGCDatabaseASDToPPJTAG(filename,"EST");
  database[TGCIdBase::Endcap][TGCIdBase::WI] =
    new TGCDatabaseASDToPPJTAG(filename,"EWI");
  database[TGCIdBase::Endcap][TGCIdBase::SI] =
    new TGCDatabaseASDToPPJTAG(filename,"ESI");
  database[TGCIdBase::Forward][TGCIdBase::WD] = 
    new TGCDatabaseASDToPPJTAG(filename,"FWD");
  database[TGCIdBase::Forward][TGCIdBase::WT] =
    new TGCDatabaseASDToPPJTAG(filename,"FWT");
  database[TGCIdBase::Forward][TGCIdBase::SD] =
    new TGCDatabaseASDToPPJTAG(filename,"FSD");
  database[TGCIdBase::Forward][TGCIdBase::ST] = 
    new TGCDatabaseASDToPPJTAG(filename,"FST");
  //database[TGCIdBase::Forward][TGCIdBase::WI] =
  //  new TGCDatabaseASDToPPJTAG(filename,"FWI");
  //database[TGCIdBase::Forward][TGCIdBase::SI] = 
  //  new TGCDatabaseASDToPPJTAG(filename,"FSI");
}
  
TGCJtagASDToPP::~TGCJtagASDToPP (void)
{
  delete database[TGCIdBase::Endcap][TGCIdBase::WD];
  delete database[TGCIdBase::Endcap][TGCIdBase::WT];
  delete database[TGCIdBase::Endcap][TGCIdBase::SD];
  delete database[TGCIdBase::Endcap][TGCIdBase::ST];
  delete database[TGCIdBase::Endcap][TGCIdBase::WI];
  delete database[TGCIdBase::Endcap][TGCIdBase::SI];
  delete database[TGCIdBase::Forward][TGCIdBase::WD];
  delete database[TGCIdBase::Forward][TGCIdBase::WT];
  delete database[TGCIdBase::Forward][TGCIdBase::SD];
  delete database[TGCIdBase::Forward][TGCIdBase::ST];
  delete database[TGCIdBase::Forward][TGCIdBase::WI];
  delete database[TGCIdBase::Forward][TGCIdBase::SI];
}

TGCJtagId* TGCJtagASDToPP::getPps(const TGCChannelId* asdout){
  TGCJtagId* pps=new TGCJtagId();
  pps->setJtagName("Pps");
  
  if(asdout->isValid()==false) return 0;
  
  
  int side=asdout->getSideType();  
  int layer=asdout->getLayer();
  int chamber=asdout->getChamber();
  int channel=asdout->getChannel(); 
  int phi_48=asdout->getSector();
  int endOrFor=asdout->getRegionType();
  int modType=asdout->getModuleType();

  int sector;
  int phi;
  if(!endOrFor){ // endcap phi0/1/2/3
    phi=(phi_48)%4;  
    sector=(phi_48-phi)/4+1;  
  }
  else{ // forward phi0/2
    phi=((phi_48)%2)*2;  
    sector=(phi_48-phi)/2+1;  
  }
  

  if(MSG_LEVEL <= LEVEL_INFO){
    std::cout<<" side:"<<side
	     <<" layer:"<<layer
	     <<" channel:"<<channel
	     <<" phi_48:"<<phi_48
	     <<" phi:"<<phi
	     <<" sector:"<<sector
	     <<" endOrFor:"<<endOrFor
	     <<" modType:"<<modType
	     <<std::endl;
  }

  TGCDatabase* databaseP = database[endOrFor][modType];
  
  for(int i=0; i<databaseP->getMaxEntry(); i++){
    if(databaseP->getEntry(i,0)==side &&
       databaseP->getEntry(i,1)==phi &&
       databaseP->getEntry(i,2)==layer+1 &&
       databaseP->getEntry(i,3)==chamber &&
       channel>=databaseP->getEntry(i,4) &&
       channel<=databaseP->getEntry(i,5)       
       ){
      

      // Parameter Setting
      pps->setJtagId(databaseP->getEntry(i,7));
      pps->setPsTypeId(databaseP->getEntry(i,6));
      pps->setOrder(databaseP->getEntry(i,8));
      pps->setMax(databaseP->getEntry(i,9));
      pps->setAorB(databaseP->getEntry(i,10));
      pps->setChanMax(databaseP->getEntry(i,5));
      pps->setChanMin(databaseP->getEntry(i,4));
      
      pps->setSide(side);
      pps->setPhi(phi);
      pps->setLayer(layer+1);
      pps->setChamber(chamber);
      
      

      return pps;
    }
    
  }
  return 0;  
}
  
} //end of namespace
