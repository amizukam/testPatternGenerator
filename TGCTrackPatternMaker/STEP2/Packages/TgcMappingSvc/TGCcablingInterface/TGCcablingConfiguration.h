class TGCcablingServerSvc;
class data_type;

class TGCcablingConfiguration {

    public:
    enum data_type {NoConfiguration=-1,oldSimulation,atlas};
    ~TGCcablingConfiguration();
    
    static TGCcablingConfiguration* instance(void);
    
    data_type configuration(void) const;
    
    
    private:
    static TGCcablingConfiguration* s_instance;
    TGCcablingConfiguration();
    void setConfiguration(data_type);
    
    data_type m_configuration;
        
    
    friend class TGCcablingServerSvc;
};
