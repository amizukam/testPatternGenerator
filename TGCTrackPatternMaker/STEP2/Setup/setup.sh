# Note: "TRACK_PATTERN_DIR" should be set to ".../TGCTrackPatternMaker".
# This is normally done by "source /det/muon/TGCFE/installed/bin/TGC_env.sh".

export PACKAGES_DIR=$TRACK_PATTERN_DIR"/Packages"
export SETUP_DIR=$TRACK_PATTERN_DIR"/Setup"

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PACKAGES_DIR/TgcParameterGenerator/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PACKAGES_DIR/TgcMappingSvc/lib
