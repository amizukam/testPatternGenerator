#include "TGCArbTrack.h"
#include <stdlib.h>

TGCArbTrack::TGCArbTrack( )
{
}

TGCArbTrack::~TGCArbTrack()
{
}

int TGCArbTrack::GetrID()
{
  int rID = 0;
  return rID;
}

int TGCArbTrack::GetLayer( std::string M, int gasgap )
{
  int layer = 0;
  if( M == "M1" ){
    if( gasgap == 1) layer = 1;
    else if( gasgap == 2) layer = 2;
    else if( gasgap == 3) layer = 3;
  }else if( M == "M2" ){
    if( gasgap == 1) layer = 4;
    else if( gasgap == 2) layer = 5;
  }else{
    if( gasgap == 1) layer = 6;
    else if( gasgap == 2) layer = 7;
  }

  if( layer <= 0 || layer > 7 ){
    //std::cout<<"Error, cannot generate layer  exit " << std::endl;
    std::cout << "Bad layer argument" << std::endl;
    exit(1);
  }
  return layer;
}

int TGCArbTrack::GetTGCSector( int triggersector )
{
  int tgcsector = 0;

  if( triggersector == 1 || triggersector == 24)    tgcsector = 1;  
  else if( triggersector == 2 || triggersector == 3)   tgcsector = 2;  
  else if( triggersector == 4 || triggersector == 5)   tgcsector = 3;  
  else if( triggersector == 6 || triggersector == 7)   tgcsector = 4;  
  else if( triggersector == 8 || triggersector == 9)   tgcsector = 5;  
  else if( triggersector == 10 || triggersector == 11)   tgcsector = 6;  
  else if( triggersector == 12 || triggersector == 13)   tgcsector = 7;  
  else if( triggersector == 14 || triggersector == 15)   tgcsector = 8;  
  else if( triggersector == 16 || triggersector == 17)   tgcsector = 9;  
  else if( triggersector == 18 || triggersector == 19)   tgcsector = 10;  
  else if( triggersector == 20 || triggersector == 21)   tgcsector = 11;  
  else if( triggersector == 22 || triggersector == 23)   tgcsector = 12;  

  if( tgcsector <= 0 || tgcsector > 12 ){
    std::cout << "Bad argument" << std::endl;
    exit(1);
  }
  return tgcsector;
}

int TGCArbTrack::GetPhi( int triggersector )
{
  int phi = -1;
  
  int triggersector_tmp = triggersector;
  int phi_tmp = (triggersector_tmp%2); 
  phi = phi_tmp;
  
  if( phi < 0 || phi > 1 ){
    std::cout << "Bad phi." << std::endl;
    exit(1);
  }
  
  return phi;
}

int TGCArbTrack::GetSLBChannelID( std::string station, bool isStrip, int chamberChannel, int layer )
{
  int SLBch = 0;
  
  if( !isStrip ) { // Wire
    if( station == "M1" ){
      int F = 105;
      int F_layer2 = 104;
      
      if( layer == 2){
	SLBch = abs( chamberChannel - F_layer2); // 0-103
      } else {
	SLBch = abs( chamberChannel - F); // 0-104
      }
      
    } // end of M1
    else if(station == "M2"){ 
      int F = 125;
      SLBch = abs( chamberChannel - F );
    } // end of M2
    else { // M3
      int F = 122;
      SLBch = abs( chamberChannel - F );
    } // end of M3
  } // end of isStrip == 0 : Wire
  else { // strip
    SLBch = chamberChannel -1;
  } // end of isStrip == 1 : Strip
  
  if( SLBch <= 0 || SLBch > 400 ){
    //std::cout<<"Error, cannot get SLBchannel id, exit " << std::endl;
    exit(1);
  }
  return SLBch;
}
