#ifndef __TGCArbTrack__
#define __TGCArbTrack__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

/* #include <TROOT.h> */
/* #include <TChain.h> */
/* #include <TFile.h> */
/* #include <TH2.h> */
/* #include <TH1.h> */
/* #include <TF1.h> */
/* #include <TStyle.h> */
/* #include <TCanvas.h> */
/* #include <TMath.h> */
/* #include <TGraphErrors.h> */
/* #include <TObject.h> */
/* #include <TLorentzVector.h> */
/* #include <TColor.h> */
/* #include <THStack.h> */
/* #include <TLegend.h> */

//include my class
//==========================
//==========================
//SIDE  SEC  PHI  EoF  WoS  LAY  rID  CH 
typedef struct { 
  std::string side;
  int sector;
  int phi;
  std::string EoF;
  std::string WoS;
  int layer;
  int rID;
  int ch;
}LayerInfo;


class TGCArbTrack
{

public :
  TGCArbTrack();
  ~TGCArbTrack();

private : 

public :
  int GetrID();
  int GetLayer( std::string M, int gasgap );
  int GetTGCSector( int triggersector );
  int GetPhi( int triggersector );
  int GetSLBChannelID( std::string station, bool isStrip, int chamberChannel, int layer );
  
};

#endif
 
