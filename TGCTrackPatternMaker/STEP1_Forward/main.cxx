#include <iostream>
#include <fstream>
#include <dirent.h>
#include <vector>

#include <stdlib.h>

//#include <string>

//ROOT
//===============
// #include "TColor.h"
// #include "TString.h"
// #include "TRandom3.h"


//MyOriginal
//=========================
#include "TGCArbTrack.h"
//=========================

//my define
//=========================
std::ofstream LOGS("Analysis.log",std::ios::out);

//functions
//=========================
void ReadConfig( std::string argv );
void GetTGCTrack( std::string argv );
void GetTGCTrackFromRoI( std::string argv );
//=========================

std::vector< std::string > side;
std::vector< int > sec;
std::vector< std::string > sta;
std::vector< int > eta;
std::vector< int > lay;
std::vector< bool > ws;
std::vector< int > chan;
int RoI = 0;

int main(int argc, char **argv)
{
  
  if( argc <= 2 ) {
    LOGS << "error, worng format" << std::endl;
    exit(1);
  }  
  
  std::string tmp = argv[1];
  LOGS<<tmp<<std::endl;
  ReadConfig( tmp );

  GetTGCTrack( argv[2] );
  /*
  if( RoI != 0 ) GetTGCTrack( argv[2] );
  else GetTGCTrackFromRoI( argv[2] );
*/
  return 0;
}

void ReadConfig( std::string argv )
{
  // int count = 0;
  std::ifstream fin;

  fin.open(argv.c_str());
  if(!fin.is_open()) exit(1);

  std::string line = "";
  while ( getline(fin, line) )
  {
    if ( !line.empty() )
    {
      while ( line[0] == ' ' ) line.erase(0, 1);
    }
    // skip lines that do not start with a number, they are comments
    if ( !line.empty() && line[0] != '#' )
    {
      
      std::stringstream is(line);
      std::string side_tmp, sta_tmp;
      bool ws_tmp;
      int sec_tmp, eta_tmp, lay_tmp, chan_tmp;
      is >> side_tmp >> sec_tmp >> sta_tmp >> eta_tmp >> lay_tmp >> ws_tmp >> chan_tmp;
      LOGS << side_tmp<<" "<<sec_tmp<<" "<<sta_tmp<<" "<<eta_tmp<<" "<<lay_tmp<<" "<<ws_tmp<<" "<<chan_tmp<<std::endl;
      
      if( side_tmp == "A" || side_tmp == "C" ){
        side.push_back( side_tmp );
        sec.push_back( sec_tmp );
        sta.push_back( sta_tmp );
        eta.push_back( eta_tmp );
        lay.push_back( lay_tmp );
        ws.push_back( ws_tmp );
        chan.push_back( chan_tmp );
      }else{
        RoI = sec_tmp;
      }
    }
  }
  fin.close();
}

void GetTGCTrack( std::string argv ){

  std::ofstream ofs( argv.c_str(), std::ios::out );

  std::string SIDE;
  int SEC;
  int PHI;
  std::string EoF;
  std::string WoS;
  int LAY;
  int ETA;
  int CH;

  ofs << "#SIDE  SEC  PHI  EoF  WoS  LAY  rID  CH " <<std::endl;
  TGCArbTrack ana;
  for( unsigned int i=0; i<sta.size(); i++){
    /*
    std::cout<<" side : " << side.at(i) << std::endl;
    std::cout<<" sec : " << sec.at(i) << std::endl;
    std::cout<<" sta : " << sta.at(i) << std::endl;
    std::cout<<" eta : " << eta.at(i) << std::endl;
    std::cout<<" lay : " << lay.at(i) << std::endl;
    std::cout<<" ws : " << ws.at(i) << std::endl;
    std::cout<<" chan : " << chan.at(i) << std::endl;
    */
    SIDE = side.at(i);
    SEC  = ana.GetTGCSector( sec.at(i) );
    PHI  = ana.GetPhi( sec.at(i) );
    EoF = "F";
    if(ws.at(i) == 0) WoS = "W"; 
    else WoS = "S";
    LAY  = ana.GetLayer( sta.at(i), lay.at(i) );
    ETA  = ana.GetrID(); 
    CH   = ana.GetSLBChannelID( sta.at(i), ws.at(i), chan.at(i), LAY );
    
    ofs << SIDE <<" "<<SEC<<" "<<PHI<<" "<<EoF<<" "<<WoS<<" "<<LAY<<" "<<ETA<<" "<<CH <<std::endl;
  }
}

void GetTGCTrackFromRoI( std::string argv ){
  
  /*
  TGCArbTrack ana;
  ana.Side = "A";
  ana.triggersector = 0;
  ana.EoF = "E";
  ana.RoI = RoI;
  ana.argv = argv;
  ana.GetTGCtrackFromRoI();
  */
}
