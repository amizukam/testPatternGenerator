#ifndef __HPTClass__
#define __HPTClass__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TH1.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TColor.h>
#include <THStack.h>
#include <TLegend.h>


namespace HPT0{


}
namespace HPT1{


}
namespace HPT2{


}
namespace HPT3{


}

