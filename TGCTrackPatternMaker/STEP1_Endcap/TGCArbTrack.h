#ifndef __TGCArbTrack__
#define __TGCArbTrack__

#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

/* #include <TROOT.h> */
/* #include <TChain.h> */
/* #include <TFile.h> */
/* #include <TH2.h> */
/* #include <TH1.h> */
/* #include <TF1.h> */
/* #include <TStyle.h> */
/* #include <TCanvas.h> */
/* #include <TMath.h> */
/* #include <TGraphErrors.h> */
/* #include <TObject.h> */
/* #include <TLorentzVector.h> */
/* #include <TColor.h> */
/* #include <THStack.h> */
/* #include <TLegend.h> */

//include my class
//==========================
//==========================
//SIDE  SEC  PHI  EoF  WoS  LAY  rID  CH 
typedef struct { 
  std::string side;
  int sector;
  int phi;
  std::string EoF;
  std::string WoS;
  int layer;
  int rID;
  int ch;
}LayerInfo;


class TGCArbTrack
{

public :
  TGCArbTrack();
  ~TGCArbTrack();

private : 

public :
  int GetrID( std::string M, int etaID );
  int GetLayer( std::string M, int gasgap );
  int GetTGCSector( int triggersector, bool isEndCap );
  int GetPhi( int triggersector, bool isEndCap );
  int GetSLBChannelID( std::string station, bool isEndcap, bool isStrip, int etaID, int chamberChannel, int layer );
  
public :
  /*
  void GetTGCTrackFromRoI( std::string argv, std::string SideSector, int RoI );
  void GetDoubletInfo();
  //void GetTripletWireFromRoI();
  void SetInfo( std::string side, int tgcsector, int phi, std::string EoF );
  void GetStripChFromRoI();

public :
  std::string argv;
  std::string Side;
  int TGCSector;
  int triggersector;
  int RoI;
  int Phi;
  std::string EoF;

  LayerInfo slayer1;
  LayerInfo slayer3;
  LayerInfo slayer4;
  LayerInfo slayer5;
  LayerInfo slayer6;
  LayerInfo slayer7;
  
  LayerInfo wlayer1;
  LayerInfo wlayer2;
  LayerInfo wlayer3;
  LayerInfo wlayer4;
  LayerInfo wlayer5;
  LayerInfo wlayer6;
  LayerInfo wlayer7;
  */

};

#endif
 
