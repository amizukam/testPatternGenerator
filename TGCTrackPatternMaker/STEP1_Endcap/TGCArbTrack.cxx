#include "TGCArbTrack.h"
#include <stdlib.h>

TGCArbTrack::TGCArbTrack( )
{
}

TGCArbTrack::~TGCArbTrack()
{
}

int TGCArbTrack::GetrID( std::string M, int etaID )
{
  
  //std::cout<<M<<" "<<etaID<<std::endl;
  int rID = 0;
  if( M == "M1" ){
    if( abs(etaID) == 4 ) rID = 1;
    else if( abs(etaID) == 3 ) rID = 2;
    else if( abs(etaID) == 2 ) rID = 3;
    else if( abs(etaID) == 1 ) rID = 4;
  }else{
    if( abs(etaID) == 5 ) rID = 0;
    else if( abs(etaID) == 4 ) rID = 1;
    else if( abs(etaID) == 3 ) rID = 2;
    else if( abs(etaID) == 2 ) rID = 3;
    else if( abs(etaID) == 1 ) rID = 4;
  }
  //std::cout<<rID<<std::endl;
  
  if( rID < 0 || rID > 5 ){
    std::cout<<"Error, cannot generate rID  exit " << std::endl;
    exit(1);
  }
  return rID;
}

int TGCArbTrack::GetLayer( std::string M, int gasgap )
{
  //std::cout<<M<<" "<<gasgap<<std::endl;
  int layer = 0;
  if( M == "M1" ){
    if( gasgap == 1) layer = 1;
    else if( gasgap == 2) layer = 2;
    else if( gasgap == 3) layer = 3;
  }else if( M == "M2" ){
    if( gasgap == 1) layer = 4;
    else if( gasgap == 2) layer = 5;
  }else{
    if( gasgap == 1) layer = 6;
    else if( gasgap == 2) layer = 7;
  }

  //std::cout<<layer<<std::endl;
  if( layer <= 0 || layer > 7 ){
    std::cout<<"Error, cannot generate layer  exit " << std::endl;
    exit(1);
  }
  return layer;
}

int TGCArbTrack::GetTGCSector( int triggersector, bool isEndcap )
{
  int tgcsector = 0;
  
  //std::cout<<"triggersector "<<triggersector<<std::endl;

    if( 0 < triggersector && triggersector < 3)    tgcsector = 1;  
    else if( 2 < triggersector && triggersector < 7)   tgcsector = 2;  
    else if( 6 < triggersector && triggersector < 11)  tgcsector = 3;  
    else if( 10 < triggersector && triggersector < 15) tgcsector = 4;  
    else if( 14 < triggersector && triggersector < 19) tgcsector = 5;  
    else if( 18 < triggersector && triggersector < 23) tgcsector = 6;  
    else if( 22 < triggersector && triggersector < 27) tgcsector = 7;  
    else if( 26 < triggersector && triggersector < 31) tgcsector = 8;  
    else if( 30 < triggersector && triggersector < 35) tgcsector = 9;  
    else if( 34 < triggersector && triggersector < 39) tgcsector = 10;  
    else if( 38 < triggersector && triggersector < 43) tgcsector = 11;  
    else if( 42 < triggersector && triggersector < 47) tgcsector = 12;  
    else if( triggersector == 47 || triggersector == 48) tgcsector = 1;  

  if( tgcsector <= 0 || tgcsector > 12 ){
    std::cout<<"Error, cannot generate TGC sector from trigger sector, exit " << std::endl;
    exit(1);
  }
  return tgcsector;
}

int TGCArbTrack::GetPhi( int triggersector, bool isEndcap )
{
  int phi = -1;

  int triggersector_tmp = triggersector;
  //  std::cout << "Trigger sector : " << triggersector << std::endl;
  int phi_tmp = (triggersector_tmp+1)%4; // 0,1,2,3
  //  std::cout << "Phi : " << phi_tmp << std::endl;
  phi = phi_tmp; 


  if( phi < 0 || phi >3 ){
    std::cout<<"Error, cannot generate phi from trigger sector, exit " << std::endl;
    exit(1);
  }
  return phi;
}

int TGCArbTrack::GetSLBChannelID( std::string station, bool isEndcap, bool isStrip, int ChamberID, int chamberChannel, int layer )
{
  int SLBch = 0;
  
  if( !isEndcap ) {
    //std::cout<<"Error, not develop, exit " << std::endl;
    exit(1);
  }else{
    if( !isStrip ){ // wire
      if( station == "M1" ){
        //T3 for EndCap
          int E1 = 24;
          int E2 = 23;
          int E3 = 62;
          int E3_layer1 = 61;
          int E4 = 91;
          int E4_layer1 = 92;
          if( abs(ChamberID) == 4 ) {
            SLBch = abs( chamberChannel - E1 ) ; //0-23
          }else if( abs(ChamberID) == 3 ) { 
            SLBch = abs( chamberChannel - E2 ) + 1;//1-23
            SLBch = E1 -1 + SLBch;//23 + 1-23 -> 24-46
          }else if( abs(ChamberID) == 2 ) { 
            if( layer == 1){
              SLBch = abs( chamberChannel - E3_layer1 ) + 1;//1-61
              SLBch = E1 - 1 + E2 + SLBch; // 23 + 23 + 1-61 -> 47-107
            }else{
              SLBch = abs( chamberChannel - E3 ) + 1;//1-62
              SLBch = E1 - 1 + E2 + SLBch; // 23 + 23 + 1-62 -> 47-108
            }
          }else if( abs(ChamberID) == 1 ) {
            if( layer == 1){
              SLBch = abs( chamberChannel - E4_layer1 ) + 1;//1-92
              SLBch = E1 - 1 + E2 + E3_layer1 + SLBch; //23 + 23 + 61 + 1-92 -> 108-19
            }else{
              SLBch = abs( chamberChannel - E4 ) + 1;//1-91
              SLBch = E1 - 1 + E2 + E3 + SLBch; //23 + 23 + 62 + 1-91 -> 109-19
            }
          }
      }else if( station == "M2" ){
          int E1 = 32;
          int E2 = 32;
          int E3 = 32;
          int E4 = 103;
          int E5 = 110;
          if( abs(ChamberID) == 5 ) {
            SLBch = abs( chamberChannel - E1 ) ;//0-31
  
          }else if( abs(ChamberID) == 4 ) {
            SLBch = abs( chamberChannel - E2 ) + 1;//1-32
            SLBch = E1 - 1 + SLBch;//31 + 1-32 -> 32-63
          
          }else if( abs(ChamberID) == 3 ) { 
            SLBch = abs( chamberChannel - E3 ) + 1;//1-32
            SLBch = E1 - 1 + E2 + SLBch;//31 + 32 + 1-32 -> 64-95

          }else if( abs(ChamberID) == 2 ) { 
            SLBch = abs( chamberChannel - E4 ) + 1;//1-103
            SLBch = E1 - 1 + E2 + E3 + SLBch;//31 + 32 + 32 + 1-103 -> 96-198

          }else if( abs(ChamberID) == 1 ) { 
            SLBch = abs( chamberChannel - E5 ) + 1;//1-110
            SLBch = E1 - 1 + E2 + E3 + E4 + SLBch;//31 + 32 + 32 + 103 + 1-110 -> 199-308
          }
      }else{ // M3
          int E1 = 31;
          int E2 = 30;
          int E3 = 32;
          int E4 = 106;
          int E5 = 96;
          if( abs(ChamberID) == 5 ) {
            SLBch = abs( chamberChannel - E1 ) ;//0-30
  
          }else if( abs(ChamberID) == 4 ) {
            SLBch = abs( chamberChannel - E2 ) + 1 ;//1-30
            SLBch = E1 - 1 + SLBch;//30 + 1-30 -> 31-60
          
          }else if( abs(ChamberID) == 3 ) { 
            SLBch = abs( chamberChannel - E3 ) + 1;//1-32
            SLBch = E1 -1 + E2 + SLBch;//30 + 30 + 1-32 -> 61-92

          }else if( abs(ChamberID) == 2 ) { 
            SLBch = abs( chamberChannel - E4 ) + 1;//1-106
            SLBch = E1 - 1 + E2 + E3 + SLBch;//30 + 30 + 32 + 1-106 -> 93-198

          }else if( abs(ChamberID) == 1 ) { 
            SLBch = abs( chamberChannel - E5 ) + 1;//1-96
            SLBch = E1 - 1 + E2 + E3 + E4 + SLBch;//30 + 30 + 32 + 106 + 1-96 -> 199-294
          }
      }
    }else{ // Strip
      
      SLBch = chamberChannel - 1;
    }
  }
  std::cout << "SLBch : " << SLBch << std::endl;
  if( SLBch < 0 || SLBch > 400 ){
    std::cout<<"Error, cannot get SLBchannel id, exit " << std::endl;
    exit(1);
  }
  return SLBch;
}
