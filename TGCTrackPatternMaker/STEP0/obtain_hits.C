#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"

#include <iostream>
#include <fstream>

#include "physics.C"
#include "input.C"


void obtain_hits(const char *output_name, const int PT, const int ISFORWARD, const int PHI, const int ROI){


  // PT: TGC_coin_pt
  // ISFORWARD: TGC_coin_isForward
  // PHI: TGC_coin_phi
  // ROI: TGC_coin_roi


  // Setup

  gROOT -> SetStyle("Plain");
  gStyle -> SetOptStat(0);


  // Input

  TChain *chain = new TChain("physics");
  AddFilesToChain(chain);

  physics physics(chain);


  // Output

  ofstream output(output_name);


  // Analysis

  bool event_selection;
  uint nevents = 0;


  for(uint i = 0; i < chain->GetEntries(); i++){

    physics.GetEntry(i);

    event_selection = false;

    for(uint j = 0; j < physics.TGC_coin_pt->size(); j++){
      if(physics.TGC_coin_type->at(j) != 2) continue; // coincidence output from SL
      if(physics.TGC_coin_pt->at(j) != PT) continue;
      if(physics.TGC_coin_isForward->at(j) != ISFORWARD) continue;
      if(physics.TGC_coin_phi->at(j) != PHI) continue;
      if(physics.TGC_coin_roi->at(j) != ROI) continue;
      event_selection = true;
      nevents++;
    }

    if(event_selection == false) continue;

    output << "#side phi station eta layer w/s chann" << std::endl;

    for(uint j = 0; j < physics.TGC_prd_x->size(); j++){

      if(physics.TGC_prd_phi->at(j) != PHI) continue;

      if(ISFORWARD == 0){
	if(physics.TGC_prd_station->at(j) != 42 &&
	   physics.TGC_prd_station->at(j) != 44 &&
	   physics.TGC_prd_station->at(j) != 46) continue;
      }

      if(ISFORWARD == 1){
	if(physics.TGC_prd_station->at(j) != 41 &&
	   physics.TGC_prd_station->at(j) != 43 &&
	   physics.TGC_prd_station->at(j) != 45) continue;
      }

      if(physics.TGC_prd_eta->at(j) > 0) output << "A ";
      else output << "C ";
      output << physics.TGC_prd_phi->at(j) << " ";
      if(physics.TGC_prd_station->at(j) == 41 || physics.TGC_prd_station->at(j) == 42) output << "M1 ";
      else if(physics.TGC_prd_station->at(j) == 43 || physics.TGC_prd_station->at(j) == 44) output << "M2 ";
      else if(physics.TGC_prd_station->at(j) == 45 || physics.TGC_prd_station->at(j) == 46) output << "M3 ";
      output << TMath::Abs(physics.TGC_prd_eta->at(j)) << " ";
      output << physics.TGC_prd_gasGap->at(j) << " ";
      output << physics.TGC_prd_isStrip->at(j) << " ";
      output << physics.TGC_prd_channel->at(j);
      output << std::endl;

    }

    output << std::endl;

  }

  std::cout << "Number of selected events: " << nevents << std::endl;


  // Finalize

  gROOT -> ProcessLine(".q");


}
