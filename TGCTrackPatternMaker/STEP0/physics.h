//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug 15 16:51:34 2017 by ROOT version 5.34/32
// from TTree physics/physics
// found on file: /chai/sgt1/atlas/L1TGCD3PD/singleMuonMC/user.yhorii.SGLMU.NTUP_L1TGC_mc12_single_mu_1000gev_NominalMDTPosition.l_tgc0_EXT0.7059222/user.yhorii.4224653.EXT0._000001.L1TGC.pool.root
//////////////////////////////////////////////////////////

#ifndef physics_h
#define physics_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>
#include <vector>

// Fixed size dimensions of array or collections stored in the TTree if any.

class physics {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   Bool_t          EF_2mu10;
   Bool_t          EF_2mu10_MSonly_g10_loose;
   Bool_t          EF_2mu10_MSonly_g10_loose_EMPTY;
   Bool_t          EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO;
   Bool_t          EF_2mu13;
   Bool_t          EF_2mu13_Zmumu_IDTrkNoCut;
   Bool_t          EF_2mu13_l2muonSA;
   Bool_t          EF_2mu15;
   Bool_t          EF_2mu4T;
   Bool_t          EF_2mu4T_2e5_tight1;
   Bool_t          EF_2mu4T_Bmumu;
   Bool_t          EF_2mu4T_Bmumu_Barrel;
   Bool_t          EF_2mu4T_Bmumu_BarrelOnly;
   Bool_t          EF_2mu4T_Bmumux;
   Bool_t          EF_2mu4T_Bmumux_Barrel;
   Bool_t          EF_2mu4T_Bmumux_BarrelOnly;
   Bool_t          EF_2mu4T_DiMu;
   Bool_t          EF_2mu4T_DiMu_Barrel;
   Bool_t          EF_2mu4T_DiMu_BarrelOnly;
   Bool_t          EF_2mu4T_DiMu_L2StarB;
   Bool_t          EF_2mu4T_DiMu_L2StarC;
   Bool_t          EF_2mu4T_DiMu_e5_tight1;
   Bool_t          EF_2mu4T_DiMu_l2muonSA;
   Bool_t          EF_2mu4T_DiMu_noVtx_noOS;
   Bool_t          EF_2mu4T_Jpsimumu;
   Bool_t          EF_2mu4T_Jpsimumu_Barrel;
   Bool_t          EF_2mu4T_Jpsimumu_BarrelOnly;
   Bool_t          EF_2mu4T_Jpsimumu_IDTrkNoCut;
   Bool_t          EF_2mu4T_Upsimumu;
   Bool_t          EF_2mu4T_Upsimumu_Barrel;
   Bool_t          EF_2mu4T_Upsimumu_BarrelOnly;
   Bool_t          EF_2mu4T_xe50_tclcw;
   Bool_t          EF_2mu4T_xe60;
   Bool_t          EF_2mu4T_xe60_tclcw;
   Bool_t          EF_2mu6;
   Bool_t          EF_2mu6_Bmumu;
   Bool_t          EF_2mu6_Bmumux;
   Bool_t          EF_2mu6_DiMu;
   Bool_t          EF_2mu6_DiMu_DY20;
   Bool_t          EF_2mu6_DiMu_DY25;
   Bool_t          EF_2mu6_DiMu_noVtx_noOS;
   Bool_t          EF_2mu6_Jpsimumu;
   Bool_t          EF_2mu6_Upsimumu;
   Bool_t          EF_2mu6i_DiMu_DY;
   Bool_t          EF_2mu6i_DiMu_DY_2j25_a4tchad;
   Bool_t          EF_2mu6i_DiMu_DY_noVtx_noOS;
   Bool_t          EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad;
   Bool_t          EF_2mu8_EFxe30;
   Bool_t          EF_2mu8_EFxe30_tclcw;
   Bool_t          EF_mu10;
   Bool_t          EF_mu10_Jpsimumu;
   Bool_t          EF_mu10_MSonly;
   Bool_t          EF_mu10_Upsimumu_tight_FS;
   Bool_t          EF_mu10i_g10_loose;
   Bool_t          EF_mu10i_g10_loose_TauMass;
   Bool_t          EF_mu10i_g10_medium;
   Bool_t          EF_mu10i_g10_medium_TauMass;
   Bool_t          EF_mu10i_loose_g12Tvh_loose;
   Bool_t          EF_mu10i_loose_g12Tvh_loose_TauMass;
   Bool_t          EF_mu10i_loose_g12Tvh_medium;
   Bool_t          EF_mu10i_loose_g12Tvh_medium_TauMass;
   Bool_t          EF_mu11_empty_NoAlg;
   Bool_t          EF_mu13;
   Bool_t          EF_mu15;
   Bool_t          EF_mu18;
   Bool_t          EF_mu18_2g10_loose;
   Bool_t          EF_mu18_2g10_medium;
   Bool_t          EF_mu18_2g15_loose;
   Bool_t          EF_mu18_IDTrkNoCut_tight;
   Bool_t          EF_mu18_g20vh_loose;
   Bool_t          EF_mu18_medium;
   Bool_t          EF_mu18_tight;
   Bool_t          EF_mu18_tight_2mu4_EFFS;
   Bool_t          EF_mu18_tight_e7_medium1;
   Bool_t          EF_mu18_tight_mu8_EFFS;
   Bool_t          EF_mu18i4_tight;
   Bool_t          EF_mu18it_tight;
   Bool_t          EF_mu20i_tight_g5_loose;
   Bool_t          EF_mu20i_tight_g5_loose_TauMass;
   Bool_t          EF_mu20i_tight_g5_medium;
   Bool_t          EF_mu20i_tight_g5_medium_TauMass;
   Bool_t          EF_mu20it_tight;
   Bool_t          EF_mu22_IDTrkNoCut_tight;
   Bool_t          EF_mu24;
   Bool_t          EF_mu24_g20vh_loose;
   Bool_t          EF_mu24_g20vh_medium;
   Bool_t          EF_mu24_j65_a4tchad;
   Bool_t          EF_mu24_j65_a4tchad_EFxe40;
   Bool_t          EF_mu24_j65_a4tchad_EFxe40_tclcw;
   Bool_t          EF_mu24_j65_a4tchad_EFxe50_tclcw;
   Bool_t          EF_mu24_j65_a4tchad_EFxe60_tclcw;
   Bool_t          EF_mu24_medium;
   Bool_t          EF_mu24_muCombTag_NoEF_tight;
   Bool_t          EF_mu24_tight;
   Bool_t          EF_mu24_tight_2j35_a4tchad;
   Bool_t          EF_mu24_tight_3j35_a4tchad;
   Bool_t          EF_mu24_tight_4j35_a4tchad;
   Bool_t          EF_mu24_tight_EFxe40;
   Bool_t          EF_mu24_tight_L2StarB;
   Bool_t          EF_mu24_tight_L2StarC;
   Bool_t          EF_mu24_tight_MG;
   Bool_t          EF_mu24_tight_MuonEF;
   Bool_t          EF_mu24_tight_b35_mediumEF_j35_a4tchad;
   Bool_t          EF_mu24_tight_mu6_EFFS;
   Bool_t          EF_mu24i_tight;
   Bool_t          EF_mu24i_tight_MG;
   Bool_t          EF_mu24i_tight_MuonEF;
   Bool_t          EF_mu24i_tight_l2muonSA;
   Bool_t          EF_mu36_tight;
   Bool_t          EF_mu40_MSonly_barrel_tight;
   Bool_t          EF_mu40_muCombTag_NoEF;
   Bool_t          EF_mu40_slow_outOfTime_tight;
   Bool_t          EF_mu40_slow_tight;
   Bool_t          EF_mu40_tight;
   Bool_t          EF_mu4T;
   Bool_t          EF_mu4T_Trk_Jpsi;
   Bool_t          EF_mu4T_cosmic;
   Bool_t          EF_mu4T_j110_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j110_a4tchad_matched;
   Bool_t          EF_mu4T_j145_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j145_a4tchad_matched;
   Bool_t          EF_mu4T_j15_a4tchad_matched;
   Bool_t          EF_mu4T_j15_a4tchad_matchedZ;
   Bool_t          EF_mu4T_j180_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j180_a4tchad_matched;
   Bool_t          EF_mu4T_j220_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j220_a4tchad_matched;
   Bool_t          EF_mu4T_j25_a4tchad_matched;
   Bool_t          EF_mu4T_j25_a4tchad_matchedZ;
   Bool_t          EF_mu4T_j280_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j280_a4tchad_matched;
   Bool_t          EF_mu4T_j35_a4tchad_matched;
   Bool_t          EF_mu4T_j35_a4tchad_matchedZ;
   Bool_t          EF_mu4T_j360_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j360_a4tchad_matched;
   Bool_t          EF_mu4T_j45_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j45_a4tchad_L2FS_matchedZ;
   Bool_t          EF_mu4T_j45_a4tchad_matched;
   Bool_t          EF_mu4T_j45_a4tchad_matchedZ;
   Bool_t          EF_mu4T_j55_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j55_a4tchad_L2FS_matchedZ;
   Bool_t          EF_mu4T_j55_a4tchad_matched;
   Bool_t          EF_mu4T_j55_a4tchad_matchedZ;
   Bool_t          EF_mu4T_j65_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j65_a4tchad_matched;
   Bool_t          EF_mu4T_j65_a4tchad_xe60_tclcw_loose;
   Bool_t          EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose;
   Bool_t          EF_mu4T_j80_a4tchad_L2FS_matched;
   Bool_t          EF_mu4T_j80_a4tchad_matched;
   Bool_t          EF_mu4Ti_g20Tvh_loose;
   Bool_t          EF_mu4Ti_g20Tvh_loose_TauMass;
   Bool_t          EF_mu4Ti_g20Tvh_medium;
   Bool_t          EF_mu4Ti_g20Tvh_medium_TauMass;
   Bool_t          EF_mu4Tmu6_Bmumu;
   Bool_t          EF_mu4Tmu6_Bmumu_Barrel;
   Bool_t          EF_mu4Tmu6_Bmumux;
   Bool_t          EF_mu4Tmu6_Bmumux_Barrel;
   Bool_t          EF_mu4Tmu6_DiMu;
   Bool_t          EF_mu4Tmu6_DiMu_Barrel;
   Bool_t          EF_mu4Tmu6_DiMu_noVtx_noOS;
   Bool_t          EF_mu4Tmu6_Jpsimumu;
   Bool_t          EF_mu4Tmu6_Jpsimumu_Barrel;
   Bool_t          EF_mu4Tmu6_Jpsimumu_IDTrkNoCut;
   Bool_t          EF_mu4Tmu6_Upsimumu;
   Bool_t          EF_mu4Tmu6_Upsimumu_Barrel;
   Bool_t          EF_mu4_L1MU11_MSonly_cosmic;
   Bool_t          EF_mu4_L1MU11_cosmic;
   Bool_t          EF_mu4_empty_NoAlg;
   Bool_t          EF_mu4_firstempty_NoAlg;
   Bool_t          EF_mu4_unpaired_iso_NoAlg;
   Bool_t          EF_mu50_MSonly_barrel_tight;
   Bool_t          EF_mu6;
   Bool_t          EF_mu60_slow_outOfTime_tight1;
   Bool_t          EF_mu60_slow_tight1;
   Bool_t          EF_mu6_Jpsimumu_tight;
   Bool_t          EF_mu6_MSonly;
   Bool_t          EF_mu6_Trk_Jpsi_loose;
   Bool_t          EF_mu6i;
   Bool_t          EF_mu8;
   Bool_t          EF_mu8_4j45_a4tchad_L2FS;
   Bool_t          L1_2MU10;
   Bool_t          L1_2MU4;
   Bool_t          L1_2MU4_2EM3;
   Bool_t          L1_2MU4_BARREL;
   Bool_t          L1_2MU4_BARRELONLY;
   Bool_t          L1_2MU4_EM3;
   Bool_t          L1_2MU4_EMPTY;
   Bool_t          L1_2MU4_FIRSTEMPTY;
   Bool_t          L1_2MU4_MU6;
   Bool_t          L1_2MU4_MU6_BARREL;
   Bool_t          L1_2MU4_XE30;
   Bool_t          L1_2MU4_XE40;
   Bool_t          L1_2MU6;
   Bool_t          L1_2MU6_UNPAIRED_ISO;
   Bool_t          L1_2MU6_UNPAIRED_NONISO;
   Bool_t          L1_MU10;
   Bool_t          L1_MU10_EMPTY;
   Bool_t          L1_MU10_FIRSTEMPTY;
   Bool_t          L1_MU10_J20;
   Bool_t          L1_MU10_UNPAIRED_ISO;
   Bool_t          L1_MU10_XE20;
   Bool_t          L1_MU10_XE25;
   Bool_t          L1_MU11;
   Bool_t          L1_MU11_EMPTY;
   Bool_t          L1_MU15;
   Bool_t          L1_MU20;
   Bool_t          L1_MU20_FIRSTEMPTY;
   Bool_t          L1_MU4;
   Bool_t          L1_MU4_EMPTY;
   Bool_t          L1_MU4_FIRSTEMPTY;
   Bool_t          L1_MU4_J10;
   Bool_t          L1_MU4_J15;
   Bool_t          L1_MU4_J15_EMPTY;
   Bool_t          L1_MU4_J15_UNPAIRED_ISO;
   Bool_t          L1_MU4_J20_XE20;
   Bool_t          L1_MU4_J20_XE35;
   Bool_t          L1_MU4_J30;
   Bool_t          L1_MU4_J50;
   Bool_t          L1_MU4_J75;
   Bool_t          L1_MU4_UNPAIRED_ISO;
   Bool_t          L1_MU4_UNPAIRED_NONISO;
   Bool_t          L1_MU6;
   Bool_t          L1_MU6_2J20;
   Bool_t          L1_MU6_FIRSTEMPTY;
   Bool_t          L1_MU6_J15;
   Bool_t          L1_MUB;
   Bool_t          L1_MUE;
   Bool_t          L2_2mu10;
   Bool_t          L2_2mu10_MSonly_g10_loose;
   Bool_t          L2_2mu10_MSonly_g10_loose_EMPTY;
   Bool_t          L2_2mu10_MSonly_g10_loose_UNPAIRED_ISO;
   Bool_t          L2_2mu13;
   Bool_t          L2_2mu13_Zmumu_IDTrkNoCut;
   Bool_t          L2_2mu13_l2muonSA;
   Bool_t          L2_2mu15;
   Bool_t          L2_2mu4T;
   Bool_t          L2_2mu4T_2e5_tight1;
   Bool_t          L2_2mu4T_Bmumu;
   Bool_t          L2_2mu4T_Bmumu_Barrel;
   Bool_t          L2_2mu4T_Bmumu_BarrelOnly;
   Bool_t          L2_2mu4T_Bmumux;
   Bool_t          L2_2mu4T_Bmumux_Barrel;
   Bool_t          L2_2mu4T_Bmumux_BarrelOnly;
   Bool_t          L2_2mu4T_DiMu;
   Bool_t          L2_2mu4T_DiMu_Barrel;
   Bool_t          L2_2mu4T_DiMu_BarrelOnly;
   Bool_t          L2_2mu4T_DiMu_L2StarB;
   Bool_t          L2_2mu4T_DiMu_L2StarC;
   Bool_t          L2_2mu4T_DiMu_e5_tight1;
   Bool_t          L2_2mu4T_DiMu_l2muonSA;
   Bool_t          L2_2mu4T_DiMu_noVtx_noOS;
   Bool_t          L2_2mu4T_Jpsimumu;
   Bool_t          L2_2mu4T_Jpsimumu_Barrel;
   Bool_t          L2_2mu4T_Jpsimumu_BarrelOnly;
   Bool_t          L2_2mu4T_Jpsimumu_IDTrkNoCut;
   Bool_t          L2_2mu4T_Upsimumu;
   Bool_t          L2_2mu4T_Upsimumu_Barrel;
   Bool_t          L2_2mu4T_Upsimumu_BarrelOnly;
   Bool_t          L2_2mu4T_xe35;
   Bool_t          L2_2mu4T_xe45;
   Bool_t          L2_2mu4T_xe60;
   Bool_t          L2_2mu6;
   Bool_t          L2_2mu6_Bmumu;
   Bool_t          L2_2mu6_Bmumux;
   Bool_t          L2_2mu6_DiMu;
   Bool_t          L2_2mu6_DiMu_DY20;
   Bool_t          L2_2mu6_DiMu_DY25;
   Bool_t          L2_2mu6_DiMu_noVtx_noOS;
   Bool_t          L2_2mu6_Jpsimumu;
   Bool_t          L2_2mu6_Upsimumu;
   Bool_t          L2_2mu6i_DiMu_DY;
   Bool_t          L2_2mu6i_DiMu_DY_2j25_a4tchad;
   Bool_t          L2_2mu6i_DiMu_DY_noVtx_noOS;
   Bool_t          L2_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad;
   Bool_t          L2_2mu8_EFxe30;
   Bool_t          L2_mu10;
   Bool_t          L2_mu10_Jpsimumu;
   Bool_t          L2_mu10_MSonly;
   Bool_t          L2_mu10_Upsimumu_tight_FS;
   Bool_t          L2_mu10i_g10_loose;
   Bool_t          L2_mu10i_g10_loose_TauMass;
   Bool_t          L2_mu10i_g10_medium;
   Bool_t          L2_mu10i_g10_medium_TauMass;
   Bool_t          L2_mu10i_loose_g12Tvh_loose;
   Bool_t          L2_mu10i_loose_g12Tvh_loose_TauMass;
   Bool_t          L2_mu10i_loose_g12Tvh_medium;
   Bool_t          L2_mu10i_loose_g12Tvh_medium_TauMass;
   Bool_t          L2_mu11_empty_NoAlg;
   Bool_t          L2_mu13;
   Bool_t          L2_mu15;
   Bool_t          L2_mu15_l2cal;
   Bool_t          L2_mu18;
   Bool_t          L2_mu18_2g10_loose;
   Bool_t          L2_mu18_2g10_medium;
   Bool_t          L2_mu18_2g15_loose;
   Bool_t          L2_mu18_IDTrkNoCut_tight;
   Bool_t          L2_mu18_g20vh_loose;
   Bool_t          L2_mu18_medium;
   Bool_t          L2_mu18_tight;
   Bool_t          L2_mu18_tight_e7_medium1;
   Bool_t          L2_mu18i4_tight;
   Bool_t          L2_mu18it_tight;
   Bool_t          L2_mu20i_tight_g5_loose;
   Bool_t          L2_mu20i_tight_g5_loose_TauMass;
   Bool_t          L2_mu20i_tight_g5_medium;
   Bool_t          L2_mu20i_tight_g5_medium_TauMass;
   Bool_t          L2_mu20it_tight;
   Bool_t          L2_mu22_IDTrkNoCut_tight;
   Bool_t          L2_mu24;
   Bool_t          L2_mu24_g20vh_loose;
   Bool_t          L2_mu24_g20vh_medium;
   Bool_t          L2_mu24_j60_c4cchad_EFxe40;
   Bool_t          L2_mu24_j60_c4cchad_EFxe50;
   Bool_t          L2_mu24_j60_c4cchad_EFxe60;
   Bool_t          L2_mu24_j60_c4cchad_xe35;
   Bool_t          L2_mu24_j65_c4cchad;
   Bool_t          L2_mu24_medium;
   Bool_t          L2_mu24_muCombTag_NoEF_tight;
   Bool_t          L2_mu24_tight;
   Bool_t          L2_mu24_tight_2j35_a4tchad;
   Bool_t          L2_mu24_tight_3j35_a4tchad;
   Bool_t          L2_mu24_tight_4j35_a4tchad;
   Bool_t          L2_mu24_tight_EFxe40;
   Bool_t          L2_mu24_tight_L2StarB;
   Bool_t          L2_mu24_tight_L2StarC;
   Bool_t          L2_mu24_tight_l2muonSA;
   Bool_t          L2_mu36_tight;
   Bool_t          L2_mu40_MSonly_barrel_tight;
   Bool_t          L2_mu40_muCombTag_NoEF;
   Bool_t          L2_mu40_slow_outOfTime_tight;
   Bool_t          L2_mu40_slow_tight;
   Bool_t          L2_mu40_tight;
   Bool_t          L2_mu4T;
   Bool_t          L2_mu4T_Trk_Jpsi;
   Bool_t          L2_mu4T_cosmic;
   Bool_t          L2_mu4T_j105_c4cchad;
   Bool_t          L2_mu4T_j10_a4TTem;
   Bool_t          L2_mu4T_j140_c4cchad;
   Bool_t          L2_mu4T_j15_a4TTem;
   Bool_t          L2_mu4T_j165_c4cchad;
   Bool_t          L2_mu4T_j30_a4TTem;
   Bool_t          L2_mu4T_j40_c4cchad;
   Bool_t          L2_mu4T_j50_a4TTem;
   Bool_t          L2_mu4T_j50_c4cchad;
   Bool_t          L2_mu4T_j60_c4cchad;
   Bool_t          L2_mu4T_j60_c4cchad_xe40;
   Bool_t          L2_mu4T_j75_a4TTem;
   Bool_t          L2_mu4T_j75_c4cchad;
   Bool_t          L2_mu4Ti_g20Tvh_loose;
   Bool_t          L2_mu4Ti_g20Tvh_loose_TauMass;
   Bool_t          L2_mu4Ti_g20Tvh_medium;
   Bool_t          L2_mu4Ti_g20Tvh_medium_TauMass;
   Bool_t          L2_mu4Tmu6_Bmumu;
   Bool_t          L2_mu4Tmu6_Bmumu_Barrel;
   Bool_t          L2_mu4Tmu6_Bmumux;
   Bool_t          L2_mu4Tmu6_Bmumux_Barrel;
   Bool_t          L2_mu4Tmu6_DiMu;
   Bool_t          L2_mu4Tmu6_DiMu_Barrel;
   Bool_t          L2_mu4Tmu6_DiMu_noVtx_noOS;
   Bool_t          L2_mu4Tmu6_Jpsimumu;
   Bool_t          L2_mu4Tmu6_Jpsimumu_Barrel;
   Bool_t          L2_mu4Tmu6_Jpsimumu_IDTrkNoCut;
   Bool_t          L2_mu4Tmu6_Upsimumu;
   Bool_t          L2_mu4Tmu6_Upsimumu_Barrel;
   Bool_t          L2_mu4_L1MU11_MSonly_cosmic;
   Bool_t          L2_mu4_L1MU11_cosmic;
   Bool_t          L2_mu4_empty_NoAlg;
   Bool_t          L2_mu4_firstempty_NoAlg;
   Bool_t          L2_mu4_l2cal_empty;
   Bool_t          L2_mu4_unpaired_iso_NoAlg;
   Bool_t          L2_mu50_MSonly_barrel_tight;
   Bool_t          L2_mu6;
   Bool_t          L2_mu60_slow_outOfTime_tight1;
   Bool_t          L2_mu60_slow_tight1;
   Bool_t          L2_mu6_Jpsimumu_tight;
   Bool_t          L2_mu6_MSonly;
   Bool_t          L2_mu6_Trk_Jpsi_loose;
   Bool_t          L2_mu8;
   Bool_t          L2_mu8_4j15_a4TTem;
   UInt_t          RunNumber;
   UInt_t          EventNumber;
   UInt_t          timestamp;
   UInt_t          timestamp_ns;
   UInt_t          lbn;
   UInt_t          bcid;
   UInt_t          detmask0;
   UInt_t          detmask1;
   Float_t         actualIntPerXing;
   Float_t         averageIntPerXing;
   UInt_t          mc_channel_number;
   UInt_t          mc_event_number;
   Float_t         mc_event_weight;
   UInt_t          pixelFlags;
   UInt_t          sctFlags;
   UInt_t          trtFlags;
   UInt_t          larFlags;
   UInt_t          tileFlags;
   UInt_t          muonFlags;
   UInt_t          fwdFlags;
   UInt_t          coreFlags;
   UInt_t          pixelError;
   UInt_t          sctError;
   UInt_t          trtError;
   UInt_t          larError;
   UInt_t          tileError;
   UInt_t          muonError;
   UInt_t          fwdError;
   UInt_t          coreError;
   Bool_t          isSimulation;
   Bool_t          isCalibration;
   Bool_t          isTestBeam;
   Int_t           vxp_n;
   vector<float>   *vxp_x;
   vector<float>   *vxp_y;
   vector<float>   *vxp_z;
   vector<float>   *vxp_cov_x;
   vector<float>   *vxp_cov_y;
   vector<float>   *vxp_cov_z;
   vector<float>   *vxp_cov_xy;
   vector<float>   *vxp_cov_xz;
   vector<float>   *vxp_cov_yz;
   vector<int>     *vxp_type;
   vector<float>   *vxp_chi2;
   vector<int>     *vxp_ndof;
   vector<float>   *vxp_px;
   vector<float>   *vxp_py;
   vector<float>   *vxp_pz;
   vector<float>   *vxp_E;
   vector<float>   *vxp_m;
   vector<int>     *vxp_nTracks;
   vector<float>   *vxp_sumPt;
   vector<vector<float> > *vxp_trk_weight;
   vector<int>     *vxp_trk_n;
   vector<vector<int> > *vxp_trk_index;
   Int_t           trk_n;
   vector<float>   *trk_pt;
   vector<float>   *trk_eta;
   vector<float>   *trk_d0_wrtPV;
   vector<float>   *trk_z0_wrtPV;
   vector<float>   *trk_phi_wrtPV;
   vector<float>   *trk_theta_wrtPV;
   vector<float>   *trk_qoverp_wrtPV;
   vector<float>   *trk_cov_d0_wrtPV;
   vector<float>   *trk_cov_z0_wrtPV;
   vector<float>   *trk_cov_phi_wrtPV;
   vector<float>   *trk_cov_theta_wrtPV;
   vector<float>   *trk_cov_qoverp_wrtPV;
   vector<float>   *trk_d0_wrtBS;
   vector<float>   *trk_z0_wrtBS;
   vector<float>   *trk_phi_wrtBS;
   vector<float>   *trk_cov_d0_wrtBS;
   vector<float>   *trk_cov_z0_wrtBS;
   vector<float>   *trk_cov_phi_wrtBS;
   vector<float>   *trk_cov_theta_wrtBS;
   vector<float>   *trk_cov_qoverp_wrtBS;
   vector<float>   *trk_cov_d0_z0_wrtBS;
   vector<float>   *trk_cov_d0_phi_wrtBS;
   vector<float>   *trk_cov_d0_theta_wrtBS;
   vector<float>   *trk_cov_d0_qoverp_wrtBS;
   vector<float>   *trk_cov_z0_phi_wrtBS;
   vector<float>   *trk_cov_z0_theta_wrtBS;
   vector<float>   *trk_cov_z0_qoverp_wrtBS;
   vector<float>   *trk_cov_phi_theta_wrtBS;
   vector<float>   *trk_cov_phi_qoverp_wrtBS;
   vector<float>   *trk_cov_theta_qoverp_wrtBS;
   vector<float>   *trk_d0_wrtBL;
   vector<float>   *trk_z0_wrtBL;
   vector<float>   *trk_phi_wrtBL;
   vector<float>   *trk_d0_err_wrtBL;
   vector<float>   *trk_z0_err_wrtBL;
   vector<float>   *trk_phi_err_wrtBL;
   vector<float>   *trk_theta_err_wrtBL;
   vector<float>   *trk_qoverp_err_wrtBL;
   vector<float>   *trk_d0_z0_err_wrtBL;
   vector<float>   *trk_d0_phi_err_wrtBL;
   vector<float>   *trk_d0_theta_err_wrtBL;
   vector<float>   *trk_d0_qoverp_err_wrtBL;
   vector<float>   *trk_z0_phi_err_wrtBL;
   vector<float>   *trk_z0_theta_err_wrtBL;
   vector<float>   *trk_z0_qoverp_err_wrtBL;
   vector<float>   *trk_phi_theta_err_wrtBL;
   vector<float>   *trk_phi_qoverp_err_wrtBL;
   vector<float>   *trk_theta_qoverp_err_wrtBL;
   vector<float>   *trk_chi2;
   vector<int>     *trk_ndof;
   vector<int>     *trk_nBLHits;
   vector<int>     *trk_nPixHits;
   vector<int>     *trk_nSCTHits;
   vector<int>     *trk_nTRTHits;
   vector<int>     *trk_nTRTHighTHits;
   vector<int>     *trk_nTRTXenonHits;
   vector<int>     *trk_nPixHoles;
   vector<int>     *trk_nSCTHoles;
   vector<int>     *trk_nTRTHoles;
   vector<int>     *trk_nPixelDeadSensors;
   vector<int>     *trk_nSCTDeadSensors;
   vector<int>     *trk_nBLSharedHits;
   vector<int>     *trk_nPixSharedHits;
   vector<int>     *trk_nSCTSharedHits;
   vector<int>     *trk_nBLayerSplitHits;
   vector<int>     *trk_nPixSplitHits;
   vector<int>     *trk_expectBLayerHit;
   vector<int>     *trk_nMDTHits;
   vector<int>     *trk_nCSCEtaHits;
   vector<int>     *trk_nCSCPhiHits;
   vector<int>     *trk_nRPCEtaHits;
   vector<int>     *trk_nRPCPhiHits;
   vector<int>     *trk_nTGCEtaHits;
   vector<int>     *trk_nTGCPhiHits;
   vector<int>     *trk_nHits;
   vector<int>     *trk_nHoles;
   vector<int>     *trk_hitPattern;
   vector<float>   *trk_TRTHighTHitsRatio;
   vector<float>   *trk_TRTHighTOutliersRatio;
   vector<int>     *trk_fitter;
   vector<int>     *trk_patternReco1;
   vector<int>     *trk_patternReco2;
   vector<int>     *trk_trackProperties;
   vector<int>     *trk_particleHypothesis;
   vector<vector<float> > *trk_blayerPrediction_x;
   vector<vector<float> > *trk_blayerPrediction_y;
   vector<vector<float> > *trk_blayerPrediction_z;
   vector<vector<float> > *trk_blayerPrediction_locX;
   vector<vector<float> > *trk_blayerPrediction_locY;
   vector<vector<float> > *trk_blayerPrediction_err_locX;
   vector<vector<float> > *trk_blayerPrediction_err_locY;
   vector<vector<float> > *trk_blayerPrediction_etaDistToEdge;
   vector<vector<float> > *trk_blayerPrediction_phiDistToEdge;
   vector<vector<unsigned int> > *trk_blayerPrediction_detElementId;
   vector<vector<int> > *trk_blayerPrediction_row;
   vector<vector<int> > *trk_blayerPrediction_col;
   vector<vector<int> > *trk_blayerPrediction_type;
   vector<float>   *trk_mc_probability;
   vector<int>     *trk_mc_barcode;
   Int_t           mu_muid_n;
   vector<float>   *mu_muid_E;
   vector<float>   *mu_muid_pt;
   vector<float>   *mu_muid_m;
   vector<float>   *mu_muid_eta;
   vector<float>   *mu_muid_phi;
   vector<float>   *mu_muid_px;
   vector<float>   *mu_muid_py;
   vector<float>   *mu_muid_pz;
   vector<float>   *mu_muid_charge;
   vector<unsigned short> *mu_muid_allauthor;
   vector<int>     *mu_muid_author;
   vector<float>   *mu_muid_beta;
   vector<float>   *mu_muid_isMuonLikelihood;
   vector<float>   *mu_muid_matchchi2;
   vector<int>     *mu_muid_matchndof;
   vector<float>   *mu_muid_etcone20;
   vector<float>   *mu_muid_etcone30;
   vector<float>   *mu_muid_etcone40;
   vector<float>   *mu_muid_nucone20;
   vector<float>   *mu_muid_nucone30;
   vector<float>   *mu_muid_nucone40;
   vector<float>   *mu_muid_ptcone20;
   vector<float>   *mu_muid_ptcone30;
   vector<float>   *mu_muid_ptcone40;
   vector<float>   *mu_muid_etconeNoEm10;
   vector<float>   *mu_muid_etconeNoEm20;
   vector<float>   *mu_muid_etconeNoEm30;
   vector<float>   *mu_muid_etconeNoEm40;
   vector<float>   *mu_muid_scatteringCurvatureSignificance;
   vector<float>   *mu_muid_scatteringNeighbourSignificance;
   vector<float>   *mu_muid_momentumBalanceSignificance;
   vector<float>   *mu_muid_energyLossPar;
   vector<float>   *mu_muid_energyLossErr;
   vector<float>   *mu_muid_etCore;
   vector<float>   *mu_muid_energyLossType;
   vector<unsigned short> *mu_muid_caloMuonIdTag;
   vector<double>  *mu_muid_caloLRLikelihood;
   vector<int>     *mu_muid_bestMatch;
   vector<int>     *mu_muid_isStandAloneMuon;
   vector<int>     *mu_muid_isCombinedMuon;
   vector<int>     *mu_muid_isLowPtReconstructedMuon;
   vector<int>     *mu_muid_isSegmentTaggedMuon;
   vector<int>     *mu_muid_isCaloMuonId;
   vector<int>     *mu_muid_alsoFoundByLowPt;
   vector<int>     *mu_muid_alsoFoundByCaloMuonId;
   vector<int>     *mu_muid_isSiliconAssociatedForwardMuon;
   vector<int>     *mu_muid_loose;
   vector<int>     *mu_muid_medium;
   vector<int>     *mu_muid_tight;
   vector<float>   *mu_muid_d0_exPV;
   vector<float>   *mu_muid_z0_exPV;
   vector<float>   *mu_muid_phi_exPV;
   vector<float>   *mu_muid_theta_exPV;
   vector<float>   *mu_muid_qoverp_exPV;
   vector<float>   *mu_muid_cb_d0_exPV;
   vector<float>   *mu_muid_cb_z0_exPV;
   vector<float>   *mu_muid_cb_phi_exPV;
   vector<float>   *mu_muid_cb_theta_exPV;
   vector<float>   *mu_muid_cb_qoverp_exPV;
   vector<float>   *mu_muid_id_d0_exPV;
   vector<float>   *mu_muid_id_z0_exPV;
   vector<float>   *mu_muid_id_phi_exPV;
   vector<float>   *mu_muid_id_theta_exPV;
   vector<float>   *mu_muid_id_qoverp_exPV;
   vector<float>   *mu_muid_me_d0_exPV;
   vector<float>   *mu_muid_me_z0_exPV;
   vector<float>   *mu_muid_me_phi_exPV;
   vector<float>   *mu_muid_me_theta_exPV;
   vector<float>   *mu_muid_me_qoverp_exPV;
   vector<float>   *mu_muid_ie_d0_exPV;
   vector<float>   *mu_muid_ie_z0_exPV;
   vector<float>   *mu_muid_ie_phi_exPV;
   vector<float>   *mu_muid_ie_theta_exPV;
   vector<float>   *mu_muid_ie_qoverp_exPV;
   vector<vector<int> > *mu_muid_SpaceTime_detID;
   vector<vector<float> > *mu_muid_SpaceTime_t;
   vector<vector<float> > *mu_muid_SpaceTime_tError;
   vector<vector<float> > *mu_muid_SpaceTime_weight;
   vector<vector<float> > *mu_muid_SpaceTime_x;
   vector<vector<float> > *mu_muid_SpaceTime_y;
   vector<vector<float> > *mu_muid_SpaceTime_z;
   vector<float>   *mu_muid_cov_d0_exPV;
   vector<float>   *mu_muid_cov_z0_exPV;
   vector<float>   *mu_muid_cov_phi_exPV;
   vector<float>   *mu_muid_cov_theta_exPV;
   vector<float>   *mu_muid_cov_qoverp_exPV;
   vector<float>   *mu_muid_cov_d0_z0_exPV;
   vector<float>   *mu_muid_cov_d0_phi_exPV;
   vector<float>   *mu_muid_cov_d0_theta_exPV;
   vector<float>   *mu_muid_cov_d0_qoverp_exPV;
   vector<float>   *mu_muid_cov_z0_phi_exPV;
   vector<float>   *mu_muid_cov_z0_theta_exPV;
   vector<float>   *mu_muid_cov_z0_qoverp_exPV;
   vector<float>   *mu_muid_cov_phi_theta_exPV;
   vector<float>   *mu_muid_cov_phi_qoverp_exPV;
   vector<float>   *mu_muid_cov_theta_qoverp_exPV;
   vector<float>   *mu_muid_id_cov_d0_exPV;
   vector<float>   *mu_muid_id_cov_z0_exPV;
   vector<float>   *mu_muid_id_cov_phi_exPV;
   vector<float>   *mu_muid_id_cov_theta_exPV;
   vector<float>   *mu_muid_id_cov_qoverp_exPV;
   vector<float>   *mu_muid_id_cov_d0_z0_exPV;
   vector<float>   *mu_muid_id_cov_d0_phi_exPV;
   vector<float>   *mu_muid_id_cov_d0_theta_exPV;
   vector<float>   *mu_muid_id_cov_d0_qoverp_exPV;
   vector<float>   *mu_muid_id_cov_z0_phi_exPV;
   vector<float>   *mu_muid_id_cov_z0_theta_exPV;
   vector<float>   *mu_muid_id_cov_z0_qoverp_exPV;
   vector<float>   *mu_muid_id_cov_phi_theta_exPV;
   vector<float>   *mu_muid_id_cov_phi_qoverp_exPV;
   vector<float>   *mu_muid_id_cov_theta_qoverp_exPV;
   vector<float>   *mu_muid_me_cov_d0_exPV;
   vector<float>   *mu_muid_me_cov_z0_exPV;
   vector<float>   *mu_muid_me_cov_phi_exPV;
   vector<float>   *mu_muid_me_cov_theta_exPV;
   vector<float>   *mu_muid_me_cov_qoverp_exPV;
   vector<float>   *mu_muid_me_cov_d0_z0_exPV;
   vector<float>   *mu_muid_me_cov_d0_phi_exPV;
   vector<float>   *mu_muid_me_cov_d0_theta_exPV;
   vector<float>   *mu_muid_me_cov_d0_qoverp_exPV;
   vector<float>   *mu_muid_me_cov_z0_phi_exPV;
   vector<float>   *mu_muid_me_cov_z0_theta_exPV;
   vector<float>   *mu_muid_me_cov_z0_qoverp_exPV;
   vector<float>   *mu_muid_me_cov_phi_theta_exPV;
   vector<float>   *mu_muid_me_cov_phi_qoverp_exPV;
   vector<float>   *mu_muid_me_cov_theta_qoverp_exPV;
   vector<float>   *mu_muid_ms_d0;
   vector<float>   *mu_muid_ms_z0;
   vector<float>   *mu_muid_ms_phi;
   vector<float>   *mu_muid_ms_theta;
   vector<float>   *mu_muid_ms_qoverp;
   vector<float>   *mu_muid_id_d0;
   vector<float>   *mu_muid_id_z0;
   vector<float>   *mu_muid_id_phi;
   vector<float>   *mu_muid_id_theta;
   vector<float>   *mu_muid_id_qoverp;
   vector<float>   *mu_muid_me_d0;
   vector<float>   *mu_muid_me_z0;
   vector<float>   *mu_muid_me_phi;
   vector<float>   *mu_muid_me_theta;
   vector<float>   *mu_muid_me_qoverp;
   vector<float>   *mu_muid_ie_d0;
   vector<float>   *mu_muid_ie_z0;
   vector<float>   *mu_muid_ie_phi;
   vector<float>   *mu_muid_ie_theta;
   vector<float>   *mu_muid_ie_qoverp;
   vector<int>     *mu_muid_nOutliersOnTrack;
   vector<int>     *mu_muid_nBLHits;
   vector<int>     *mu_muid_nPixHits;
   vector<int>     *mu_muid_nSCTHits;
   vector<int>     *mu_muid_nTRTHits;
   vector<int>     *mu_muid_nTRTHighTHits;
   vector<int>     *mu_muid_nBLSharedHits;
   vector<int>     *mu_muid_nPixSharedHits;
   vector<int>     *mu_muid_nPixHoles;
   vector<int>     *mu_muid_nSCTSharedHits;
   vector<int>     *mu_muid_nSCTHoles;
   vector<int>     *mu_muid_nTRTOutliers;
   vector<int>     *mu_muid_nTRTHighTOutliers;
   vector<int>     *mu_muid_nGangedPixels;
   vector<int>     *mu_muid_nPixelDeadSensors;
   vector<int>     *mu_muid_nSCTDeadSensors;
   vector<int>     *mu_muid_nTRTDeadStraws;
   vector<int>     *mu_muid_expectBLayerHit;
   vector<int>     *mu_muid_nMDTHits;
   vector<int>     *mu_muid_nMDTHoles;
   vector<int>     *mu_muid_nCSCEtaHits;
   vector<int>     *mu_muid_nCSCEtaHoles;
   vector<int>     *mu_muid_nCSCUnspoiledEtaHits;
   vector<int>     *mu_muid_nCSCPhiHits;
   vector<int>     *mu_muid_nCSCPhiHoles;
   vector<int>     *mu_muid_nRPCEtaHits;
   vector<int>     *mu_muid_nRPCEtaHoles;
   vector<int>     *mu_muid_nRPCPhiHits;
   vector<int>     *mu_muid_nRPCPhiHoles;
   vector<int>     *mu_muid_nTGCEtaHits;
   vector<int>     *mu_muid_nTGCEtaHoles;
   vector<int>     *mu_muid_nTGCPhiHits;
   vector<int>     *mu_muid_nTGCPhiHoles;
   vector<int>     *mu_muid_nprecisionLayers;
   vector<int>     *mu_muid_nprecisionHoleLayers;
   vector<int>     *mu_muid_nphiLayers;
   vector<int>     *mu_muid_ntrigEtaLayers;
   vector<int>     *mu_muid_nphiHoleLayers;
   vector<int>     *mu_muid_ntrigEtaHoleLayers;
   vector<int>     *mu_muid_nMDTBIHits;
   vector<int>     *mu_muid_nMDTBMHits;
   vector<int>     *mu_muid_nMDTBOHits;
   vector<int>     *mu_muid_nMDTBEEHits;
   vector<int>     *mu_muid_nMDTBIS78Hits;
   vector<int>     *mu_muid_nMDTEIHits;
   vector<int>     *mu_muid_nMDTEMHits;
   vector<int>     *mu_muid_nMDTEOHits;
   vector<int>     *mu_muid_nMDTEEHits;
   vector<int>     *mu_muid_nRPCLayer1EtaHits;
   vector<int>     *mu_muid_nRPCLayer2EtaHits;
   vector<int>     *mu_muid_nRPCLayer3EtaHits;
   vector<int>     *mu_muid_nRPCLayer1PhiHits;
   vector<int>     *mu_muid_nRPCLayer2PhiHits;
   vector<int>     *mu_muid_nRPCLayer3PhiHits;
   vector<int>     *mu_muid_nTGCLayer1EtaHits;
   vector<int>     *mu_muid_nTGCLayer2EtaHits;
   vector<int>     *mu_muid_nTGCLayer3EtaHits;
   vector<int>     *mu_muid_nTGCLayer4EtaHits;
   vector<int>     *mu_muid_nTGCLayer1PhiHits;
   vector<int>     *mu_muid_nTGCLayer2PhiHits;
   vector<int>     *mu_muid_nTGCLayer3PhiHits;
   vector<int>     *mu_muid_nTGCLayer4PhiHits;
   vector<int>     *mu_muid_barrelSectors;
   vector<int>     *mu_muid_endcapSectors;
   vector<float>   *mu_muid_spec_surf_px;
   vector<float>   *mu_muid_spec_surf_py;
   vector<float>   *mu_muid_spec_surf_pz;
   vector<float>   *mu_muid_spec_surf_x;
   vector<float>   *mu_muid_spec_surf_y;
   vector<float>   *mu_muid_spec_surf_z;
   vector<float>   *mu_muid_trackd0;
   vector<float>   *mu_muid_trackz0;
   vector<float>   *mu_muid_trackphi;
   vector<float>   *mu_muid_tracktheta;
   vector<float>   *mu_muid_trackqoverp;
   vector<float>   *mu_muid_trackcov_d0;
   vector<float>   *mu_muid_trackcov_z0;
   vector<float>   *mu_muid_trackcov_phi;
   vector<float>   *mu_muid_trackcov_theta;
   vector<float>   *mu_muid_trackcov_qoverp;
   vector<float>   *mu_muid_trackcov_d0_z0;
   vector<float>   *mu_muid_trackcov_d0_phi;
   vector<float>   *mu_muid_trackcov_d0_theta;
   vector<float>   *mu_muid_trackcov_d0_qoverp;
   vector<float>   *mu_muid_trackcov_z0_phi;
   vector<float>   *mu_muid_trackcov_z0_theta;
   vector<float>   *mu_muid_trackcov_z0_qoverp;
   vector<float>   *mu_muid_trackcov_phi_theta;
   vector<float>   *mu_muid_trackcov_phi_qoverp;
   vector<float>   *mu_muid_trackcov_theta_qoverp;
   vector<float>   *mu_muid_trackfitchi2;
   vector<int>     *mu_muid_trackfitndof;
   vector<int>     *mu_muid_hastrack;
   vector<float>   *mu_muid_trackd0beam;
   vector<float>   *mu_muid_trackz0beam;
   vector<float>   *mu_muid_tracksigd0beam;
   vector<float>   *mu_muid_tracksigz0beam;
   vector<float>   *mu_muid_trackd0pv;
   vector<float>   *mu_muid_trackz0pv;
   vector<float>   *mu_muid_tracksigd0pv;
   vector<float>   *mu_muid_tracksigz0pv;
   vector<float>   *mu_muid_trackIPEstimate_d0_biasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_z0_biasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_sigd0_biasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_sigz0_biasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_d0_unbiasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_z0_unbiasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased;
   vector<float>   *mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased;
   vector<float>   *mu_muid_trackd0pvunbiased;
   vector<float>   *mu_muid_trackz0pvunbiased;
   vector<float>   *mu_muid_tracksigd0pvunbiased;
   vector<float>   *mu_muid_tracksigz0pvunbiased;
   vector<int>     *mu_muid_type;
   vector<int>     *mu_muid_origin;
   vector<float>   *mu_muid_truth_dr;
   vector<float>   *mu_muid_truth_E;
   vector<float>   *mu_muid_truth_pt;
   vector<float>   *mu_muid_truth_eta;
   vector<float>   *mu_muid_truth_phi;
   vector<int>     *mu_muid_truth_type;
   vector<int>     *mu_muid_truth_status;
   vector<int>     *mu_muid_truth_barcode;
   vector<int>     *mu_muid_truth_mothertype;
   vector<int>     *mu_muid_truth_motherbarcode;
   vector<int>     *mu_muid_truth_matched;
   vector<float>   *mu_muid_EFCB_dr;
   vector<int>     *mu_muid_EFCB_n;
   vector<vector<int> > *mu_muid_EFCB_MuonType;
   vector<vector<float> > *mu_muid_EFCB_pt;
   vector<vector<float> > *mu_muid_EFCB_eta;
   vector<vector<float> > *mu_muid_EFCB_phi;
   vector<vector<int> > *mu_muid_EFCB_hasCB;
   vector<int>     *mu_muid_EFCB_matched;
   vector<float>   *mu_muid_EFMG_dr;
   vector<int>     *mu_muid_EFMG_n;
   vector<vector<int> > *mu_muid_EFMG_MuonType;
   vector<vector<float> > *mu_muid_EFMG_pt;
   vector<vector<float> > *mu_muid_EFMG_eta;
   vector<vector<float> > *mu_muid_EFMG_phi;
   vector<vector<int> > *mu_muid_EFMG_hasMG;
   vector<int>     *mu_muid_EFMG_matched;
   vector<float>   *mu_muid_EFME_dr;
   vector<int>     *mu_muid_EFME_n;
   vector<vector<int> > *mu_muid_EFME_MuonType;
   vector<vector<float> > *mu_muid_EFME_pt;
   vector<vector<float> > *mu_muid_EFME_eta;
   vector<vector<float> > *mu_muid_EFME_phi;
   vector<vector<int> > *mu_muid_EFME_hasME;
   vector<int>     *mu_muid_EFME_matched;
   vector<float>   *mu_muid_L2CB_dr;
   vector<float>   *mu_muid_L2CB_pt;
   vector<float>   *mu_muid_L2CB_eta;
   vector<float>   *mu_muid_L2CB_phi;
   vector<float>   *mu_muid_L2CB_id_pt;
   vector<float>   *mu_muid_L2CB_ms_pt;
   vector<int>     *mu_muid_L2CB_nPixHits;
   vector<int>     *mu_muid_L2CB_nSCTHits;
   vector<int>     *mu_muid_L2CB_nTRTHits;
   vector<int>     *mu_muid_L2CB_nTRTHighTHits;
   vector<int>     *mu_muid_L2CB_matched;
   vector<float>   *mu_muid_L1_dr;
   vector<float>   *mu_muid_L1_pt;
   vector<float>   *mu_muid_L1_eta;
   vector<float>   *mu_muid_L1_phi;
   vector<short>   *mu_muid_L1_thrNumber;
   vector<short>   *mu_muid_L1_RoINumber;
   vector<short>   *mu_muid_L1_sectorAddress;
   vector<int>     *mu_muid_L1_firstCandidate;
   vector<int>     *mu_muid_L1_moreCandInRoI;
   vector<int>     *mu_muid_L1_moreCandInSector;
   vector<short>   *mu_muid_L1_source;
   vector<short>   *mu_muid_L1_hemisphere;
   vector<short>   *mu_muid_L1_charge;
   vector<int>     *mu_muid_L1_vetoed;
   vector<int>     *mu_muid_L1_matched;
   Int_t           mu_staco_n;
   vector<float>   *mu_staco_E;
   vector<float>   *mu_staco_pt;
   vector<float>   *mu_staco_m;
   vector<float>   *mu_staco_eta;
   vector<float>   *mu_staco_phi;
   vector<float>   *mu_staco_px;
   vector<float>   *mu_staco_py;
   vector<float>   *mu_staco_pz;
   vector<float>   *mu_staco_charge;
   vector<unsigned short> *mu_staco_allauthor;
   vector<int>     *mu_staco_author;
   vector<float>   *mu_staco_beta;
   vector<float>   *mu_staco_isMuonLikelihood;
   vector<float>   *mu_staco_matchchi2;
   vector<int>     *mu_staco_matchndof;
   vector<float>   *mu_staco_etcone20;
   vector<float>   *mu_staco_etcone30;
   vector<float>   *mu_staco_etcone40;
   vector<float>   *mu_staco_nucone20;
   vector<float>   *mu_staco_nucone30;
   vector<float>   *mu_staco_nucone40;
   vector<float>   *mu_staco_ptcone20;
   vector<float>   *mu_staco_ptcone30;
   vector<float>   *mu_staco_ptcone40;
   vector<float>   *mu_staco_etconeNoEm10;
   vector<float>   *mu_staco_etconeNoEm20;
   vector<float>   *mu_staco_etconeNoEm30;
   vector<float>   *mu_staco_etconeNoEm40;
   vector<float>   *mu_staco_scatteringCurvatureSignificance;
   vector<float>   *mu_staco_scatteringNeighbourSignificance;
   vector<float>   *mu_staco_momentumBalanceSignificance;
   vector<float>   *mu_staco_energyLossPar;
   vector<float>   *mu_staco_energyLossErr;
   vector<float>   *mu_staco_etCore;
   vector<float>   *mu_staco_energyLossType;
   vector<unsigned short> *mu_staco_caloMuonIdTag;
   vector<double>  *mu_staco_caloLRLikelihood;
   vector<int>     *mu_staco_bestMatch;
   vector<int>     *mu_staco_isStandAloneMuon;
   vector<int>     *mu_staco_isCombinedMuon;
   vector<int>     *mu_staco_isLowPtReconstructedMuon;
   vector<int>     *mu_staco_isSegmentTaggedMuon;
   vector<int>     *mu_staco_isCaloMuonId;
   vector<int>     *mu_staco_alsoFoundByLowPt;
   vector<int>     *mu_staco_alsoFoundByCaloMuonId;
   vector<int>     *mu_staco_isSiliconAssociatedForwardMuon;
   vector<int>     *mu_staco_loose;
   vector<int>     *mu_staco_medium;
   vector<int>     *mu_staco_tight;
   vector<float>   *mu_staco_d0_exPV;
   vector<float>   *mu_staco_z0_exPV;
   vector<float>   *mu_staco_phi_exPV;
   vector<float>   *mu_staco_theta_exPV;
   vector<float>   *mu_staco_qoverp_exPV;
   vector<float>   *mu_staco_cb_d0_exPV;
   vector<float>   *mu_staco_cb_z0_exPV;
   vector<float>   *mu_staco_cb_phi_exPV;
   vector<float>   *mu_staco_cb_theta_exPV;
   vector<float>   *mu_staco_cb_qoverp_exPV;
   vector<float>   *mu_staco_id_d0_exPV;
   vector<float>   *mu_staco_id_z0_exPV;
   vector<float>   *mu_staco_id_phi_exPV;
   vector<float>   *mu_staco_id_theta_exPV;
   vector<float>   *mu_staco_id_qoverp_exPV;
   vector<float>   *mu_staco_me_d0_exPV;
   vector<float>   *mu_staco_me_z0_exPV;
   vector<float>   *mu_staco_me_phi_exPV;
   vector<float>   *mu_staco_me_theta_exPV;
   vector<float>   *mu_staco_me_qoverp_exPV;
   vector<float>   *mu_staco_ie_d0_exPV;
   vector<float>   *mu_staco_ie_z0_exPV;
   vector<float>   *mu_staco_ie_phi_exPV;
   vector<float>   *mu_staco_ie_theta_exPV;
   vector<float>   *mu_staco_ie_qoverp_exPV;
   vector<vector<int> > *mu_staco_SpaceTime_detID;
   vector<vector<float> > *mu_staco_SpaceTime_t;
   vector<vector<float> > *mu_staco_SpaceTime_tError;
   vector<vector<float> > *mu_staco_SpaceTime_weight;
   vector<vector<float> > *mu_staco_SpaceTime_x;
   vector<vector<float> > *mu_staco_SpaceTime_y;
   vector<vector<float> > *mu_staco_SpaceTime_z;
   vector<float>   *mu_staco_cov_d0_exPV;
   vector<float>   *mu_staco_cov_z0_exPV;
   vector<float>   *mu_staco_cov_phi_exPV;
   vector<float>   *mu_staco_cov_theta_exPV;
   vector<float>   *mu_staco_cov_qoverp_exPV;
   vector<float>   *mu_staco_cov_d0_z0_exPV;
   vector<float>   *mu_staco_cov_d0_phi_exPV;
   vector<float>   *mu_staco_cov_d0_theta_exPV;
   vector<float>   *mu_staco_cov_d0_qoverp_exPV;
   vector<float>   *mu_staco_cov_z0_phi_exPV;
   vector<float>   *mu_staco_cov_z0_theta_exPV;
   vector<float>   *mu_staco_cov_z0_qoverp_exPV;
   vector<float>   *mu_staco_cov_phi_theta_exPV;
   vector<float>   *mu_staco_cov_phi_qoverp_exPV;
   vector<float>   *mu_staco_cov_theta_qoverp_exPV;
   vector<float>   *mu_staco_id_cov_d0_exPV;
   vector<float>   *mu_staco_id_cov_z0_exPV;
   vector<float>   *mu_staco_id_cov_phi_exPV;
   vector<float>   *mu_staco_id_cov_theta_exPV;
   vector<float>   *mu_staco_id_cov_qoverp_exPV;
   vector<float>   *mu_staco_id_cov_d0_z0_exPV;
   vector<float>   *mu_staco_id_cov_d0_phi_exPV;
   vector<float>   *mu_staco_id_cov_d0_theta_exPV;
   vector<float>   *mu_staco_id_cov_d0_qoverp_exPV;
   vector<float>   *mu_staco_id_cov_z0_phi_exPV;
   vector<float>   *mu_staco_id_cov_z0_theta_exPV;
   vector<float>   *mu_staco_id_cov_z0_qoverp_exPV;
   vector<float>   *mu_staco_id_cov_phi_theta_exPV;
   vector<float>   *mu_staco_id_cov_phi_qoverp_exPV;
   vector<float>   *mu_staco_id_cov_theta_qoverp_exPV;
   vector<float>   *mu_staco_me_cov_d0_exPV;
   vector<float>   *mu_staco_me_cov_z0_exPV;
   vector<float>   *mu_staco_me_cov_phi_exPV;
   vector<float>   *mu_staco_me_cov_theta_exPV;
   vector<float>   *mu_staco_me_cov_qoverp_exPV;
   vector<float>   *mu_staco_me_cov_d0_z0_exPV;
   vector<float>   *mu_staco_me_cov_d0_phi_exPV;
   vector<float>   *mu_staco_me_cov_d0_theta_exPV;
   vector<float>   *mu_staco_me_cov_d0_qoverp_exPV;
   vector<float>   *mu_staco_me_cov_z0_phi_exPV;
   vector<float>   *mu_staco_me_cov_z0_theta_exPV;
   vector<float>   *mu_staco_me_cov_z0_qoverp_exPV;
   vector<float>   *mu_staco_me_cov_phi_theta_exPV;
   vector<float>   *mu_staco_me_cov_phi_qoverp_exPV;
   vector<float>   *mu_staco_me_cov_theta_qoverp_exPV;
   vector<float>   *mu_staco_ms_d0;
   vector<float>   *mu_staco_ms_z0;
   vector<float>   *mu_staco_ms_phi;
   vector<float>   *mu_staco_ms_theta;
   vector<float>   *mu_staco_ms_qoverp;
   vector<float>   *mu_staco_id_d0;
   vector<float>   *mu_staco_id_z0;
   vector<float>   *mu_staco_id_phi;
   vector<float>   *mu_staco_id_theta;
   vector<float>   *mu_staco_id_qoverp;
   vector<float>   *mu_staco_me_d0;
   vector<float>   *mu_staco_me_z0;
   vector<float>   *mu_staco_me_phi;
   vector<float>   *mu_staco_me_theta;
   vector<float>   *mu_staco_me_qoverp;
   vector<float>   *mu_staco_ie_d0;
   vector<float>   *mu_staco_ie_z0;
   vector<float>   *mu_staco_ie_phi;
   vector<float>   *mu_staco_ie_theta;
   vector<float>   *mu_staco_ie_qoverp;
   vector<int>     *mu_staco_nOutliersOnTrack;
   vector<int>     *mu_staco_nBLHits;
   vector<int>     *mu_staco_nPixHits;
   vector<int>     *mu_staco_nSCTHits;
   vector<int>     *mu_staco_nTRTHits;
   vector<int>     *mu_staco_nTRTHighTHits;
   vector<int>     *mu_staco_nBLSharedHits;
   vector<int>     *mu_staco_nPixSharedHits;
   vector<int>     *mu_staco_nPixHoles;
   vector<int>     *mu_staco_nSCTSharedHits;
   vector<int>     *mu_staco_nSCTHoles;
   vector<int>     *mu_staco_nTRTOutliers;
   vector<int>     *mu_staco_nTRTHighTOutliers;
   vector<int>     *mu_staco_nGangedPixels;
   vector<int>     *mu_staco_nPixelDeadSensors;
   vector<int>     *mu_staco_nSCTDeadSensors;
   vector<int>     *mu_staco_nTRTDeadStraws;
   vector<int>     *mu_staco_expectBLayerHit;
   vector<int>     *mu_staco_nMDTHits;
   vector<int>     *mu_staco_nMDTHoles;
   vector<int>     *mu_staco_nCSCEtaHits;
   vector<int>     *mu_staco_nCSCEtaHoles;
   vector<int>     *mu_staco_nCSCUnspoiledEtaHits;
   vector<int>     *mu_staco_nCSCPhiHits;
   vector<int>     *mu_staco_nCSCPhiHoles;
   vector<int>     *mu_staco_nRPCEtaHits;
   vector<int>     *mu_staco_nRPCEtaHoles;
   vector<int>     *mu_staco_nRPCPhiHits;
   vector<int>     *mu_staco_nRPCPhiHoles;
   vector<int>     *mu_staco_nTGCEtaHits;
   vector<int>     *mu_staco_nTGCEtaHoles;
   vector<int>     *mu_staco_nTGCPhiHits;
   vector<int>     *mu_staco_nTGCPhiHoles;
   vector<int>     *mu_staco_nprecisionLayers;
   vector<int>     *mu_staco_nprecisionHoleLayers;
   vector<int>     *mu_staco_nphiLayers;
   vector<int>     *mu_staco_ntrigEtaLayers;
   vector<int>     *mu_staco_nphiHoleLayers;
   vector<int>     *mu_staco_ntrigEtaHoleLayers;
   vector<int>     *mu_staco_nMDTBIHits;
   vector<int>     *mu_staco_nMDTBMHits;
   vector<int>     *mu_staco_nMDTBOHits;
   vector<int>     *mu_staco_nMDTBEEHits;
   vector<int>     *mu_staco_nMDTBIS78Hits;
   vector<int>     *mu_staco_nMDTEIHits;
   vector<int>     *mu_staco_nMDTEMHits;
   vector<int>     *mu_staco_nMDTEOHits;
   vector<int>     *mu_staco_nMDTEEHits;
   vector<int>     *mu_staco_nRPCLayer1EtaHits;
   vector<int>     *mu_staco_nRPCLayer2EtaHits;
   vector<int>     *mu_staco_nRPCLayer3EtaHits;
   vector<int>     *mu_staco_nRPCLayer1PhiHits;
   vector<int>     *mu_staco_nRPCLayer2PhiHits;
   vector<int>     *mu_staco_nRPCLayer3PhiHits;
   vector<int>     *mu_staco_nTGCLayer1EtaHits;
   vector<int>     *mu_staco_nTGCLayer2EtaHits;
   vector<int>     *mu_staco_nTGCLayer3EtaHits;
   vector<int>     *mu_staco_nTGCLayer4EtaHits;
   vector<int>     *mu_staco_nTGCLayer1PhiHits;
   vector<int>     *mu_staco_nTGCLayer2PhiHits;
   vector<int>     *mu_staco_nTGCLayer3PhiHits;
   vector<int>     *mu_staco_nTGCLayer4PhiHits;
   vector<int>     *mu_staco_barrelSectors;
   vector<int>     *mu_staco_endcapSectors;
   vector<float>   *mu_staco_spec_surf_px;
   vector<float>   *mu_staco_spec_surf_py;
   vector<float>   *mu_staco_spec_surf_pz;
   vector<float>   *mu_staco_spec_surf_x;
   vector<float>   *mu_staco_spec_surf_y;
   vector<float>   *mu_staco_spec_surf_z;
   vector<float>   *mu_staco_trackd0;
   vector<float>   *mu_staco_trackz0;
   vector<float>   *mu_staco_trackphi;
   vector<float>   *mu_staco_tracktheta;
   vector<float>   *mu_staco_trackqoverp;
   vector<float>   *mu_staco_trackcov_d0;
   vector<float>   *mu_staco_trackcov_z0;
   vector<float>   *mu_staco_trackcov_phi;
   vector<float>   *mu_staco_trackcov_theta;
   vector<float>   *mu_staco_trackcov_qoverp;
   vector<float>   *mu_staco_trackcov_d0_z0;
   vector<float>   *mu_staco_trackcov_d0_phi;
   vector<float>   *mu_staco_trackcov_d0_theta;
   vector<float>   *mu_staco_trackcov_d0_qoverp;
   vector<float>   *mu_staco_trackcov_z0_phi;
   vector<float>   *mu_staco_trackcov_z0_theta;
   vector<float>   *mu_staco_trackcov_z0_qoverp;
   vector<float>   *mu_staco_trackcov_phi_theta;
   vector<float>   *mu_staco_trackcov_phi_qoverp;
   vector<float>   *mu_staco_trackcov_theta_qoverp;
   vector<float>   *mu_staco_trackfitchi2;
   vector<int>     *mu_staco_trackfitndof;
   vector<int>     *mu_staco_hastrack;
   vector<float>   *mu_staco_trackd0beam;
   vector<float>   *mu_staco_trackz0beam;
   vector<float>   *mu_staco_tracksigd0beam;
   vector<float>   *mu_staco_tracksigz0beam;
   vector<float>   *mu_staco_trackd0pv;
   vector<float>   *mu_staco_trackz0pv;
   vector<float>   *mu_staco_tracksigd0pv;
   vector<float>   *mu_staco_tracksigz0pv;
   vector<float>   *mu_staco_trackIPEstimate_d0_biasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_z0_biasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_sigd0_biasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_sigz0_biasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_d0_unbiasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_z0_unbiasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased;
   vector<float>   *mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased;
   vector<float>   *mu_staco_trackd0pvunbiased;
   vector<float>   *mu_staco_trackz0pvunbiased;
   vector<float>   *mu_staco_tracksigd0pvunbiased;
   vector<float>   *mu_staco_tracksigz0pvunbiased;
   vector<int>     *mu_staco_type;
   vector<int>     *mu_staco_origin;
   vector<float>   *mu_staco_truth_dr;
   vector<float>   *mu_staco_truth_E;
   vector<float>   *mu_staco_truth_pt;
   vector<float>   *mu_staco_truth_eta;
   vector<float>   *mu_staco_truth_phi;
   vector<int>     *mu_staco_truth_type;
   vector<int>     *mu_staco_truth_status;
   vector<int>     *mu_staco_truth_barcode;
   vector<int>     *mu_staco_truth_mothertype;
   vector<int>     *mu_staco_truth_motherbarcode;
   vector<int>     *mu_staco_truth_matched;
   vector<float>   *mu_staco_EFCB_dr;
   vector<int>     *mu_staco_EFCB_n;
   vector<vector<int> > *mu_staco_EFCB_MuonType;
   vector<vector<float> > *mu_staco_EFCB_pt;
   vector<vector<float> > *mu_staco_EFCB_eta;
   vector<vector<float> > *mu_staco_EFCB_phi;
   vector<vector<int> > *mu_staco_EFCB_hasCB;
   vector<int>     *mu_staco_EFCB_matched;
   vector<float>   *mu_staco_EFMG_dr;
   vector<int>     *mu_staco_EFMG_n;
   vector<vector<int> > *mu_staco_EFMG_MuonType;
   vector<vector<float> > *mu_staco_EFMG_pt;
   vector<vector<float> > *mu_staco_EFMG_eta;
   vector<vector<float> > *mu_staco_EFMG_phi;
   vector<vector<int> > *mu_staco_EFMG_hasMG;
   vector<int>     *mu_staco_EFMG_matched;
   vector<float>   *mu_staco_EFME_dr;
   vector<int>     *mu_staco_EFME_n;
   vector<vector<int> > *mu_staco_EFME_MuonType;
   vector<vector<float> > *mu_staco_EFME_pt;
   vector<vector<float> > *mu_staco_EFME_eta;
   vector<vector<float> > *mu_staco_EFME_phi;
   vector<vector<int> > *mu_staco_EFME_hasME;
   vector<int>     *mu_staco_EFME_matched;
   vector<float>   *mu_staco_L2CB_dr;
   vector<float>   *mu_staco_L2CB_pt;
   vector<float>   *mu_staco_L2CB_eta;
   vector<float>   *mu_staco_L2CB_phi;
   vector<float>   *mu_staco_L2CB_id_pt;
   vector<float>   *mu_staco_L2CB_ms_pt;
   vector<int>     *mu_staco_L2CB_nPixHits;
   vector<int>     *mu_staco_L2CB_nSCTHits;
   vector<int>     *mu_staco_L2CB_nTRTHits;
   vector<int>     *mu_staco_L2CB_nTRTHighTHits;
   vector<int>     *mu_staco_L2CB_matched;
   vector<float>   *mu_staco_L1_dr;
   vector<float>   *mu_staco_L1_pt;
   vector<float>   *mu_staco_L1_eta;
   vector<float>   *mu_staco_L1_phi;
   vector<short>   *mu_staco_L1_thrNumber;
   vector<short>   *mu_staco_L1_RoINumber;
   vector<short>   *mu_staco_L1_sectorAddress;
   vector<int>     *mu_staco_L1_firstCandidate;
   vector<int>     *mu_staco_L1_moreCandInRoI;
   vector<int>     *mu_staco_L1_moreCandInSector;
   vector<short>   *mu_staco_L1_source;
   vector<short>   *mu_staco_L1_hemisphere;
   vector<short>   *mu_staco_L1_charge;
   vector<int>     *mu_staco_L1_vetoed;
   vector<int>     *mu_staco_L1_matched;
   Int_t           mu_calo_n;
   vector<float>   *mu_calo_E;
   vector<float>   *mu_calo_pt;
   vector<float>   *mu_calo_m;
   vector<float>   *mu_calo_eta;
   vector<float>   *mu_calo_phi;
   vector<float>   *mu_calo_px;
   vector<float>   *mu_calo_py;
   vector<float>   *mu_calo_pz;
   vector<float>   *mu_calo_charge;
   vector<unsigned short> *mu_calo_allauthor;
   vector<int>     *mu_calo_author;
   vector<float>   *mu_calo_beta;
   vector<float>   *mu_calo_isMuonLikelihood;
   vector<float>   *mu_calo_matchchi2;
   vector<int>     *mu_calo_matchndof;
   vector<float>   *mu_calo_etcone20;
   vector<float>   *mu_calo_etcone30;
   vector<float>   *mu_calo_etcone40;
   vector<float>   *mu_calo_nucone20;
   vector<float>   *mu_calo_nucone30;
   vector<float>   *mu_calo_nucone40;
   vector<float>   *mu_calo_ptcone20;
   vector<float>   *mu_calo_ptcone30;
   vector<float>   *mu_calo_ptcone40;
   vector<float>   *mu_calo_etconeNoEm10;
   vector<float>   *mu_calo_etconeNoEm20;
   vector<float>   *mu_calo_etconeNoEm30;
   vector<float>   *mu_calo_etconeNoEm40;
   vector<float>   *mu_calo_scatteringCurvatureSignificance;
   vector<float>   *mu_calo_scatteringNeighbourSignificance;
   vector<float>   *mu_calo_momentumBalanceSignificance;
   vector<float>   *mu_calo_energyLossPar;
   vector<float>   *mu_calo_energyLossErr;
   vector<float>   *mu_calo_etCore;
   vector<float>   *mu_calo_energyLossType;
   vector<unsigned short> *mu_calo_caloMuonIdTag;
   vector<double>  *mu_calo_caloLRLikelihood;
   vector<int>     *mu_calo_bestMatch;
   vector<int>     *mu_calo_isStandAloneMuon;
   vector<int>     *mu_calo_isCombinedMuon;
   vector<int>     *mu_calo_isLowPtReconstructedMuon;
   vector<int>     *mu_calo_isSegmentTaggedMuon;
   vector<int>     *mu_calo_isCaloMuonId;
   vector<int>     *mu_calo_alsoFoundByLowPt;
   vector<int>     *mu_calo_alsoFoundByCaloMuonId;
   vector<int>     *mu_calo_isSiliconAssociatedForwardMuon;
   vector<int>     *mu_calo_loose;
   vector<int>     *mu_calo_medium;
   vector<int>     *mu_calo_tight;
   vector<float>   *mu_calo_d0_exPV;
   vector<float>   *mu_calo_z0_exPV;
   vector<float>   *mu_calo_phi_exPV;
   vector<float>   *mu_calo_theta_exPV;
   vector<float>   *mu_calo_qoverp_exPV;
   vector<float>   *mu_calo_cb_d0_exPV;
   vector<float>   *mu_calo_cb_z0_exPV;
   vector<float>   *mu_calo_cb_phi_exPV;
   vector<float>   *mu_calo_cb_theta_exPV;
   vector<float>   *mu_calo_cb_qoverp_exPV;
   vector<float>   *mu_calo_id_d0_exPV;
   vector<float>   *mu_calo_id_z0_exPV;
   vector<float>   *mu_calo_id_phi_exPV;
   vector<float>   *mu_calo_id_theta_exPV;
   vector<float>   *mu_calo_id_qoverp_exPV;
   vector<float>   *mu_calo_me_d0_exPV;
   vector<float>   *mu_calo_me_z0_exPV;
   vector<float>   *mu_calo_me_phi_exPV;
   vector<float>   *mu_calo_me_theta_exPV;
   vector<float>   *mu_calo_me_qoverp_exPV;
   vector<float>   *mu_calo_ie_d0_exPV;
   vector<float>   *mu_calo_ie_z0_exPV;
   vector<float>   *mu_calo_ie_phi_exPV;
   vector<float>   *mu_calo_ie_theta_exPV;
   vector<float>   *mu_calo_ie_qoverp_exPV;
   vector<vector<int> > *mu_calo_SpaceTime_detID;
   vector<vector<float> > *mu_calo_SpaceTime_t;
   vector<vector<float> > *mu_calo_SpaceTime_tError;
   vector<vector<float> > *mu_calo_SpaceTime_weight;
   vector<vector<float> > *mu_calo_SpaceTime_x;
   vector<vector<float> > *mu_calo_SpaceTime_y;
   vector<vector<float> > *mu_calo_SpaceTime_z;
   vector<float>   *mu_calo_cov_d0_exPV;
   vector<float>   *mu_calo_cov_z0_exPV;
   vector<float>   *mu_calo_cov_phi_exPV;
   vector<float>   *mu_calo_cov_theta_exPV;
   vector<float>   *mu_calo_cov_qoverp_exPV;
   vector<float>   *mu_calo_cov_d0_z0_exPV;
   vector<float>   *mu_calo_cov_d0_phi_exPV;
   vector<float>   *mu_calo_cov_d0_theta_exPV;
   vector<float>   *mu_calo_cov_d0_qoverp_exPV;
   vector<float>   *mu_calo_cov_z0_phi_exPV;
   vector<float>   *mu_calo_cov_z0_theta_exPV;
   vector<float>   *mu_calo_cov_z0_qoverp_exPV;
   vector<float>   *mu_calo_cov_phi_theta_exPV;
   vector<float>   *mu_calo_cov_phi_qoverp_exPV;
   vector<float>   *mu_calo_cov_theta_qoverp_exPV;
   vector<float>   *mu_calo_id_cov_d0_exPV;
   vector<float>   *mu_calo_id_cov_z0_exPV;
   vector<float>   *mu_calo_id_cov_phi_exPV;
   vector<float>   *mu_calo_id_cov_theta_exPV;
   vector<float>   *mu_calo_id_cov_qoverp_exPV;
   vector<float>   *mu_calo_id_cov_d0_z0_exPV;
   vector<float>   *mu_calo_id_cov_d0_phi_exPV;
   vector<float>   *mu_calo_id_cov_d0_theta_exPV;
   vector<float>   *mu_calo_id_cov_d0_qoverp_exPV;
   vector<float>   *mu_calo_id_cov_z0_phi_exPV;
   vector<float>   *mu_calo_id_cov_z0_theta_exPV;
   vector<float>   *mu_calo_id_cov_z0_qoverp_exPV;
   vector<float>   *mu_calo_id_cov_phi_theta_exPV;
   vector<float>   *mu_calo_id_cov_phi_qoverp_exPV;
   vector<float>   *mu_calo_id_cov_theta_qoverp_exPV;
   vector<float>   *mu_calo_me_cov_d0_exPV;
   vector<float>   *mu_calo_me_cov_z0_exPV;
   vector<float>   *mu_calo_me_cov_phi_exPV;
   vector<float>   *mu_calo_me_cov_theta_exPV;
   vector<float>   *mu_calo_me_cov_qoverp_exPV;
   vector<float>   *mu_calo_me_cov_d0_z0_exPV;
   vector<float>   *mu_calo_me_cov_d0_phi_exPV;
   vector<float>   *mu_calo_me_cov_d0_theta_exPV;
   vector<float>   *mu_calo_me_cov_d0_qoverp_exPV;
   vector<float>   *mu_calo_me_cov_z0_phi_exPV;
   vector<float>   *mu_calo_me_cov_z0_theta_exPV;
   vector<float>   *mu_calo_me_cov_z0_qoverp_exPV;
   vector<float>   *mu_calo_me_cov_phi_theta_exPV;
   vector<float>   *mu_calo_me_cov_phi_qoverp_exPV;
   vector<float>   *mu_calo_me_cov_theta_qoverp_exPV;
   vector<float>   *mu_calo_ms_d0;
   vector<float>   *mu_calo_ms_z0;
   vector<float>   *mu_calo_ms_phi;
   vector<float>   *mu_calo_ms_theta;
   vector<float>   *mu_calo_ms_qoverp;
   vector<float>   *mu_calo_id_d0;
   vector<float>   *mu_calo_id_z0;
   vector<float>   *mu_calo_id_phi;
   vector<float>   *mu_calo_id_theta;
   vector<float>   *mu_calo_id_qoverp;
   vector<float>   *mu_calo_me_d0;
   vector<float>   *mu_calo_me_z0;
   vector<float>   *mu_calo_me_phi;
   vector<float>   *mu_calo_me_theta;
   vector<float>   *mu_calo_me_qoverp;
   vector<float>   *mu_calo_ie_d0;
   vector<float>   *mu_calo_ie_z0;
   vector<float>   *mu_calo_ie_phi;
   vector<float>   *mu_calo_ie_theta;
   vector<float>   *mu_calo_ie_qoverp;
   vector<int>     *mu_calo_nOutliersOnTrack;
   vector<int>     *mu_calo_nBLHits;
   vector<int>     *mu_calo_nPixHits;
   vector<int>     *mu_calo_nSCTHits;
   vector<int>     *mu_calo_nTRTHits;
   vector<int>     *mu_calo_nTRTHighTHits;
   vector<int>     *mu_calo_nBLSharedHits;
   vector<int>     *mu_calo_nPixSharedHits;
   vector<int>     *mu_calo_nPixHoles;
   vector<int>     *mu_calo_nSCTSharedHits;
   vector<int>     *mu_calo_nSCTHoles;
   vector<int>     *mu_calo_nTRTOutliers;
   vector<int>     *mu_calo_nTRTHighTOutliers;
   vector<int>     *mu_calo_nGangedPixels;
   vector<int>     *mu_calo_nPixelDeadSensors;
   vector<int>     *mu_calo_nSCTDeadSensors;
   vector<int>     *mu_calo_nTRTDeadStraws;
   vector<int>     *mu_calo_expectBLayerHit;
   vector<int>     *mu_calo_nMDTHits;
   vector<int>     *mu_calo_nMDTHoles;
   vector<int>     *mu_calo_nCSCEtaHits;
   vector<int>     *mu_calo_nCSCEtaHoles;
   vector<int>     *mu_calo_nCSCUnspoiledEtaHits;
   vector<int>     *mu_calo_nCSCPhiHits;
   vector<int>     *mu_calo_nCSCPhiHoles;
   vector<int>     *mu_calo_nRPCEtaHits;
   vector<int>     *mu_calo_nRPCEtaHoles;
   vector<int>     *mu_calo_nRPCPhiHits;
   vector<int>     *mu_calo_nRPCPhiHoles;
   vector<int>     *mu_calo_nTGCEtaHits;
   vector<int>     *mu_calo_nTGCEtaHoles;
   vector<int>     *mu_calo_nTGCPhiHits;
   vector<int>     *mu_calo_nTGCPhiHoles;
   vector<int>     *mu_calo_nprecisionLayers;
   vector<int>     *mu_calo_nprecisionHoleLayers;
   vector<int>     *mu_calo_nphiLayers;
   vector<int>     *mu_calo_ntrigEtaLayers;
   vector<int>     *mu_calo_nphiHoleLayers;
   vector<int>     *mu_calo_ntrigEtaHoleLayers;
   vector<int>     *mu_calo_nMDTBIHits;
   vector<int>     *mu_calo_nMDTBMHits;
   vector<int>     *mu_calo_nMDTBOHits;
   vector<int>     *mu_calo_nMDTBEEHits;
   vector<int>     *mu_calo_nMDTBIS78Hits;
   vector<int>     *mu_calo_nMDTEIHits;
   vector<int>     *mu_calo_nMDTEMHits;
   vector<int>     *mu_calo_nMDTEOHits;
   vector<int>     *mu_calo_nMDTEEHits;
   vector<int>     *mu_calo_nRPCLayer1EtaHits;
   vector<int>     *mu_calo_nRPCLayer2EtaHits;
   vector<int>     *mu_calo_nRPCLayer3EtaHits;
   vector<int>     *mu_calo_nRPCLayer1PhiHits;
   vector<int>     *mu_calo_nRPCLayer2PhiHits;
   vector<int>     *mu_calo_nRPCLayer3PhiHits;
   vector<int>     *mu_calo_nTGCLayer1EtaHits;
   vector<int>     *mu_calo_nTGCLayer2EtaHits;
   vector<int>     *mu_calo_nTGCLayer3EtaHits;
   vector<int>     *mu_calo_nTGCLayer4EtaHits;
   vector<int>     *mu_calo_nTGCLayer1PhiHits;
   vector<int>     *mu_calo_nTGCLayer2PhiHits;
   vector<int>     *mu_calo_nTGCLayer3PhiHits;
   vector<int>     *mu_calo_nTGCLayer4PhiHits;
   vector<int>     *mu_calo_barrelSectors;
   vector<int>     *mu_calo_endcapSectors;
   vector<float>   *mu_calo_spec_surf_px;
   vector<float>   *mu_calo_spec_surf_py;
   vector<float>   *mu_calo_spec_surf_pz;
   vector<float>   *mu_calo_spec_surf_x;
   vector<float>   *mu_calo_spec_surf_y;
   vector<float>   *mu_calo_spec_surf_z;
   vector<float>   *mu_calo_trackd0;
   vector<float>   *mu_calo_trackz0;
   vector<float>   *mu_calo_trackphi;
   vector<float>   *mu_calo_tracktheta;
   vector<float>   *mu_calo_trackqoverp;
   vector<float>   *mu_calo_trackcov_d0;
   vector<float>   *mu_calo_trackcov_z0;
   vector<float>   *mu_calo_trackcov_phi;
   vector<float>   *mu_calo_trackcov_theta;
   vector<float>   *mu_calo_trackcov_qoverp;
   vector<float>   *mu_calo_trackcov_d0_z0;
   vector<float>   *mu_calo_trackcov_d0_phi;
   vector<float>   *mu_calo_trackcov_d0_theta;
   vector<float>   *mu_calo_trackcov_d0_qoverp;
   vector<float>   *mu_calo_trackcov_z0_phi;
   vector<float>   *mu_calo_trackcov_z0_theta;
   vector<float>   *mu_calo_trackcov_z0_qoverp;
   vector<float>   *mu_calo_trackcov_phi_theta;
   vector<float>   *mu_calo_trackcov_phi_qoverp;
   vector<float>   *mu_calo_trackcov_theta_qoverp;
   vector<float>   *mu_calo_trackfitchi2;
   vector<int>     *mu_calo_trackfitndof;
   vector<int>     *mu_calo_hastrack;
   vector<float>   *mu_calo_trackd0beam;
   vector<float>   *mu_calo_trackz0beam;
   vector<float>   *mu_calo_tracksigd0beam;
   vector<float>   *mu_calo_tracksigz0beam;
   vector<float>   *mu_calo_trackd0pv;
   vector<float>   *mu_calo_trackz0pv;
   vector<float>   *mu_calo_tracksigd0pv;
   vector<float>   *mu_calo_tracksigz0pv;
   vector<float>   *mu_calo_trackIPEstimate_d0_biasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_z0_biasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_sigd0_biasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_sigz0_biasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_d0_unbiasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_z0_unbiasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased;
   vector<float>   *mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased;
   vector<float>   *mu_calo_trackd0pvunbiased;
   vector<float>   *mu_calo_trackz0pvunbiased;
   vector<float>   *mu_calo_tracksigd0pvunbiased;
   vector<float>   *mu_calo_tracksigz0pvunbiased;
   vector<int>     *mu_calo_type;
   vector<int>     *mu_calo_origin;
   vector<float>   *mu_calo_truth_dr;
   vector<float>   *mu_calo_truth_E;
   vector<float>   *mu_calo_truth_pt;
   vector<float>   *mu_calo_truth_eta;
   vector<float>   *mu_calo_truth_phi;
   vector<int>     *mu_calo_truth_type;
   vector<int>     *mu_calo_truth_status;
   vector<int>     *mu_calo_truth_barcode;
   vector<int>     *mu_calo_truth_mothertype;
   vector<int>     *mu_calo_truth_motherbarcode;
   vector<int>     *mu_calo_truth_matched;
   vector<float>   *mu_calo_EFCB_dr;
   vector<int>     *mu_calo_EFCB_n;
   vector<vector<int> > *mu_calo_EFCB_MuonType;
   vector<vector<float> > *mu_calo_EFCB_pt;
   vector<vector<float> > *mu_calo_EFCB_eta;
   vector<vector<float> > *mu_calo_EFCB_phi;
   vector<vector<int> > *mu_calo_EFCB_hasCB;
   vector<int>     *mu_calo_EFCB_matched;
   vector<float>   *mu_calo_EFMG_dr;
   vector<int>     *mu_calo_EFMG_n;
   vector<vector<int> > *mu_calo_EFMG_MuonType;
   vector<vector<float> > *mu_calo_EFMG_pt;
   vector<vector<float> > *mu_calo_EFMG_eta;
   vector<vector<float> > *mu_calo_EFMG_phi;
   vector<vector<int> > *mu_calo_EFMG_hasMG;
   vector<int>     *mu_calo_EFMG_matched;
   vector<float>   *mu_calo_EFME_dr;
   vector<int>     *mu_calo_EFME_n;
   vector<vector<int> > *mu_calo_EFME_MuonType;
   vector<vector<float> > *mu_calo_EFME_pt;
   vector<vector<float> > *mu_calo_EFME_eta;
   vector<vector<float> > *mu_calo_EFME_phi;
   vector<vector<int> > *mu_calo_EFME_hasME;
   vector<int>     *mu_calo_EFME_matched;
   vector<float>   *mu_calo_L2CB_dr;
   vector<float>   *mu_calo_L2CB_pt;
   vector<float>   *mu_calo_L2CB_eta;
   vector<float>   *mu_calo_L2CB_phi;
   vector<float>   *mu_calo_L2CB_id_pt;
   vector<float>   *mu_calo_L2CB_ms_pt;
   vector<int>     *mu_calo_L2CB_nPixHits;
   vector<int>     *mu_calo_L2CB_nSCTHits;
   vector<int>     *mu_calo_L2CB_nTRTHits;
   vector<int>     *mu_calo_L2CB_nTRTHighTHits;
   vector<int>     *mu_calo_L2CB_matched;
   vector<float>   *mu_calo_L1_dr;
   vector<float>   *mu_calo_L1_pt;
   vector<float>   *mu_calo_L1_eta;
   vector<float>   *mu_calo_L1_phi;
   vector<short>   *mu_calo_L1_thrNumber;
   vector<short>   *mu_calo_L1_RoINumber;
   vector<short>   *mu_calo_L1_sectorAddress;
   vector<int>     *mu_calo_L1_firstCandidate;
   vector<int>     *mu_calo_L1_moreCandInRoI;
   vector<int>     *mu_calo_L1_moreCandInSector;
   vector<short>   *mu_calo_L1_source;
   vector<short>   *mu_calo_L1_hemisphere;
   vector<short>   *mu_calo_L1_charge;
   vector<int>     *mu_calo_L1_vetoed;
   vector<int>     *mu_calo_L1_matched;
   Int_t           mooreseg_n;
   vector<float>   *mooreseg_x;
   vector<float>   *mooreseg_y;
   vector<float>   *mooreseg_z;
   vector<float>   *mooreseg_phi;
   vector<float>   *mooreseg_theta;
   vector<float>   *mooreseg_locX;
   vector<float>   *mooreseg_locY;
   vector<float>   *mooreseg_locAngleXZ;
   vector<float>   *mooreseg_locAngleYZ;
   vector<int>     *mooreseg_sector;
   vector<int>     *mooreseg_stationEta;
   vector<int>     *mooreseg_isEndcap;
   vector<int>     *mooreseg_stationName;
   vector<int>     *mooreseg_author;
   vector<float>   *mooreseg_chi2;
   vector<int>     *mooreseg_ndof;
   vector<float>   *mooreseg_t0;
   vector<float>   *mooreseg_t0err;
   Int_t           mboyseg_n;
   vector<float>   *mboyseg_x;
   vector<float>   *mboyseg_y;
   vector<float>   *mboyseg_z;
   vector<float>   *mboyseg_phi;
   vector<float>   *mboyseg_theta;
   vector<float>   *mboyseg_locX;
   vector<float>   *mboyseg_locY;
   vector<float>   *mboyseg_locAngleXZ;
   vector<float>   *mboyseg_locAngleYZ;
   vector<int>     *mboyseg_sector;
   vector<int>     *mboyseg_stationEta;
   vector<int>     *mboyseg_isEndcap;
   vector<int>     *mboyseg_stationName;
   vector<int>     *mboyseg_author;
   vector<float>   *mboyseg_chi2;
   vector<int>     *mboyseg_ndof;
   vector<float>   *mboyseg_t0;
   vector<float>   *mboyseg_t0err;
   Int_t           mgseg_n;
   vector<float>   *mgseg_x;
   vector<float>   *mgseg_y;
   vector<float>   *mgseg_z;
   vector<float>   *mgseg_phi;
   vector<float>   *mgseg_theta;
   vector<float>   *mgseg_locX;
   vector<float>   *mgseg_locY;
   vector<float>   *mgseg_locAngleXZ;
   vector<float>   *mgseg_locAngleYZ;
   vector<int>     *mgseg_sector;
   vector<int>     *mgseg_stationEta;
   vector<int>     *mgseg_isEndcap;
   vector<int>     *mgseg_stationName;
   vector<int>     *mgseg_author;
   vector<float>   *mgseg_chi2;
   vector<int>     *mgseg_ndof;
   vector<float>   *mgseg_t0;
   vector<float>   *mgseg_t0err;
   Int_t           muonTruth_n;
   vector<float>   *muonTruth_pt;
   vector<float>   *muonTruth_m;
   vector<float>   *muonTruth_eta;
   vector<float>   *muonTruth_phi;
   vector<float>   *muonTruth_charge;
   vector<int>     *muonTruth_PDGID;
   vector<int>     *muonTruth_barcode;
   vector<int>     *muonTruth_type;
   vector<int>     *muonTruth_origin;
   Int_t           mcevt_n;
   vector<int>     *mcevt_signal_process_id;
   vector<int>     *mcevt_event_number;
   vector<double>  *mcevt_event_scale;
   vector<double>  *mcevt_alphaQCD;
   vector<double>  *mcevt_alphaQED;
   vector<int>     *mcevt_pdf_id1;
   vector<int>     *mcevt_pdf_id2;
   vector<double>  *mcevt_pdf_x1;
   vector<double>  *mcevt_pdf_x2;
   vector<double>  *mcevt_pdf_scale;
   vector<double>  *mcevt_pdf1;
   vector<double>  *mcevt_pdf2;
   vector<vector<double> > *mcevt_weight;
   vector<int>     *mcevt_nparticle;
   vector<short>   *mcevt_pileUpType;
   Int_t           mc_n;
   vector<float>   *mc_pt;
   vector<float>   *mc_m;
   vector<float>   *mc_eta;
   vector<float>   *mc_phi;
   vector<int>     *mc_status;
   vector<int>     *mc_barcode;
   vector<int>     *mc_pdgId;
   vector<float>   *mc_charge;
   vector<vector<int> > *mc_parents;
   vector<vector<int> > *mc_children;
   vector<float>   *mc_vx_x;
   vector<float>   *mc_vx_y;
   vector<float>   *mc_vx_z;
   vector<int>     *mc_vx_barcode;
   vector<vector<int> > *mc_child_index;
   vector<vector<int> > *mc_parent_index;
   Char_t          trig_bgCode;
   Int_t           trig_L1_mu_n;
   vector<float>   *trig_L1_mu_pt;
   vector<float>   *trig_L1_mu_eta;
   vector<float>   *trig_L1_mu_phi;
   vector<string>  *trig_L1_mu_thrName;
   vector<short>   *trig_L1_mu_thrNumber;
   vector<short>   *trig_L1_mu_RoINumber;
   vector<short>   *trig_L1_mu_sectorAddress;
   vector<int>     *trig_L1_mu_firstCandidate;
   vector<int>     *trig_L1_mu_moreCandInRoI;
   vector<int>     *trig_L1_mu_moreCandInSector;
   vector<short>   *trig_L1_mu_source;
   vector<short>   *trig_L1_mu_hemisphere;
   vector<short>   *trig_L1_mu_charge;
   vector<int>     *trig_L1_mu_vetoed;
   vector<unsigned int> *trig_L1_mu_RoIWord;
   vector<unsigned int> *muctpi_candMultiplicity;
   Int_t           muctpi_nDataWords;
   vector<unsigned int> *muctpi_dataWords;
   vector<float>   *muctpi_dw_eta;
   vector<float>   *muctpi_dw_phi;
   vector<short>   *muctpi_dw_source;
   vector<short>   *muctpi_dw_hemisphere;
   vector<short>   *muctpi_dw_bcid;
   vector<short>   *muctpi_dw_sectorID;
   vector<short>   *muctpi_dw_thrNumber;
   vector<short>   *muctpi_dw_RoINumber;
   vector<short>   *muctpi_dw_overlapFlags;
   vector<short>   *muctpi_dw_firstCandidate;
   vector<short>   *muctpi_dw_moreCandInRoI;
   vector<short>   *muctpi_dw_moreCandInSector;
   vector<short>   *muctpi_dw_charge;
   vector<short>   *muctpi_dw_vetoed;
   vector<float>   *TGC_prd_x;
   vector<float>   *TGC_prd_y;
   vector<float>   *TGC_prd_z;
   vector<float>   *TGC_prd_shortWidth;
   vector<float>   *TGC_prd_longWidth;
   vector<float>   *TGC_prd_length;
   vector<int>     *TGC_prd_isStrip;
   vector<int>     *TGC_prd_gasGap;
   vector<int>     *TGC_prd_channel;
   vector<int>     *TGC_prd_eta;
   vector<int>     *TGC_prd_phi;
   vector<int>     *TGC_prd_station;
   vector<int>     *TGC_prd_bunch;
   vector<float>   *TGC_coin_x_In;
   vector<float>   *TGC_coin_y_In;
   vector<float>   *TGC_coin_z_In;
   vector<float>   *TGC_coin_x_Out;
   vector<float>   *TGC_coin_y_Out;
   vector<float>   *TGC_coin_z_Out;
   vector<float>   *TGC_coin_width_In;
   vector<float>   *TGC_coin_width_Out;
   vector<float>   *TGC_coin_width_R;
   vector<float>   *TGC_coin_width_Phi;
   vector<int>     *TGC_coin_isAside;
   vector<int>     *TGC_coin_isForward;
   vector<int>     *TGC_coin_isStrip;
   vector<int>     *TGC_coin_isPositiveDeltaR;
   vector<int>     *TGC_coin_type;
   vector<int>     *TGC_coin_trackletId;
   vector<int>     *TGC_coin_trackletIdStrip;
   vector<int>     *TGC_coin_phi;
   vector<int>     *TGC_coin_roi;
   vector<int>     *TGC_coin_pt;
   vector<int>     *TGC_coin_delta;
   vector<int>     *TGC_coin_sub;
   vector<int>     *TGC_coin_bunch;
   vector<int>     *TGC_hierarchy_index;
   vector<int>     *TGC_hierarchy_dR_hiPt;
   vector<int>     *TGC_hierarchy_dPhi_hiPt;
   vector<int>     *TGC_hierarchy_dR_tracklet;
   vector<int>     *TGC_hierarchy_dPhi_tracklet;
   vector<int>     *TGC_hierarchy_isChamberBoundary;
   vector<float>   *RPC_prd_x;
   vector<float>   *RPC_prd_y;
   vector<float>   *RPC_prd_z;
   vector<float>   *RPC_prd_time;
   vector<int>     *RPC_prd_triggerInfo;
   vector<int>     *RPC_prd_ambiguityFlag;
   vector<int>     *RPC_prd_measuresPhi;
   vector<int>     *RPC_prd_inRibs;
   vector<int>     *RPC_prd_station;
   vector<int>     *RPC_prd_stationEta;
   vector<int>     *RPC_prd_stationPhi;
   vector<int>     *RPC_prd_doubletR;
   vector<int>     *RPC_prd_doubletZ;
   vector<float>   *RPC_prd_stripWidth;
   vector<float>   *RPC_prd_stripLength;
   vector<float>   *RPC_prd_stripPitch;
   vector<float>   *MDT_prd_x;
   vector<float>   *MDT_prd_y;
   vector<float>   *MDT_prd_z;
   vector<int>     *MDT_prd_adc;
   vector<int>     *MDT_prd_tdc;
   vector<int>     *MDT_prd_status;
   vector<float>   *MDT_prd_drift_radius;
   vector<float>   *MDT_prd_drift_radius_error;
   vector<int>     *ext_staco_ubias_type;
   vector<int>     *ext_staco_ubias_index;
   vector<int>     *ext_staco_ubias_size;
   vector<vector<int> > *ext_staco_ubias_targetVec;
   vector<vector<float> > *ext_staco_ubias_targetDistanceVec;
   vector<vector<float> > *ext_staco_ubias_targetEtaVec;
   vector<vector<float> > *ext_staco_ubias_targetPhiVec;
   vector<vector<float> > *ext_staco_ubias_targetDeltaEtaVec;
   vector<vector<float> > *ext_staco_ubias_targetDeltaPhiVec;
   vector<vector<float> > *ext_staco_ubias_targetPxVec;
   vector<vector<float> > *ext_staco_ubias_targetPyVec;
   vector<vector<float> > *ext_staco_ubias_targetPzVec;
   vector<int>     *ext_staco_bias_type;
   vector<int>     *ext_staco_bias_index;
   vector<int>     *ext_staco_bias_size;
   vector<vector<int> > *ext_staco_bias_targetVec;
   vector<vector<float> > *ext_staco_bias_targetDistanceVec;
   vector<vector<float> > *ext_staco_bias_targetEtaVec;
   vector<vector<float> > *ext_staco_bias_targetPhiVec;
   vector<vector<float> > *ext_staco_bias_targetDeltaEtaVec;
   vector<vector<float> > *ext_staco_bias_targetDeltaPhiVec;
   vector<vector<float> > *ext_staco_bias_targetPxVec;
   vector<vector<float> > *ext_staco_bias_targetPyVec;
   vector<vector<float> > *ext_staco_bias_targetPzVec;
   vector<int>     *ext_muid_ubias_type;
   vector<int>     *ext_muid_ubias_index;
   vector<int>     *ext_muid_ubias_size;
   vector<vector<int> > *ext_muid_ubias_targetVec;
   vector<vector<float> > *ext_muid_ubias_targetDistanceVec;
   vector<vector<float> > *ext_muid_ubias_targetEtaVec;
   vector<vector<float> > *ext_muid_ubias_targetPhiVec;
   vector<vector<float> > *ext_muid_ubias_targetDeltaEtaVec;
   vector<vector<float> > *ext_muid_ubias_targetDeltaPhiVec;
   vector<vector<float> > *ext_muid_ubias_targetPxVec;
   vector<vector<float> > *ext_muid_ubias_targetPyVec;
   vector<vector<float> > *ext_muid_ubias_targetPzVec;
   vector<int>     *ext_muid_bias_type;
   vector<int>     *ext_muid_bias_index;
   vector<int>     *ext_muid_bias_size;
   vector<vector<int> > *ext_muid_bias_targetVec;
   vector<vector<float> > *ext_muid_bias_targetDistanceVec;
   vector<vector<float> > *ext_muid_bias_targetEtaVec;
   vector<vector<float> > *ext_muid_bias_targetPhiVec;
   vector<vector<float> > *ext_muid_bias_targetDeltaEtaVec;
   vector<vector<float> > *ext_muid_bias_targetDeltaPhiVec;
   vector<vector<float> > *ext_muid_bias_targetPxVec;
   vector<vector<float> > *ext_muid_bias_targetPyVec;
   vector<vector<float> > *ext_muid_bias_targetPzVec;
   vector<int>     *ext_calo_ubias_type;
   vector<int>     *ext_calo_ubias_index;
   vector<int>     *ext_calo_ubias_size;
   vector<vector<int> > *ext_calo_ubias_targetVec;
   vector<vector<float> > *ext_calo_ubias_targetDistanceVec;
   vector<vector<float> > *ext_calo_ubias_targetEtaVec;
   vector<vector<float> > *ext_calo_ubias_targetPhiVec;
   vector<vector<float> > *ext_calo_ubias_targetDeltaEtaVec;
   vector<vector<float> > *ext_calo_ubias_targetDeltaPhiVec;
   vector<vector<float> > *ext_calo_ubias_targetPxVec;
   vector<vector<float> > *ext_calo_ubias_targetPyVec;
   vector<vector<float> > *ext_calo_ubias_targetPzVec;
   vector<string>  *trigger_info_chain;
   vector<int>     *trigger_info_isPassed;
   vector<int>     *trigger_info_nTracks;
   vector<vector<int> > *trigger_info_typeVec;
   vector<vector<float> > *trigger_info_ptVec;
   vector<vector<float> > *trigger_info_etaVec;
   vector<vector<float> > *trigger_info_phiVec;
   vector<vector<int> > *trigger_info_chargeVec;
   vector<vector<int> > *trigger_info_l1RoiWordVec;
   vector<int>     *tandp_staco_tag_index;
   vector<int>     *tandp_staco_probe_index;
   vector<int>     *tandp_staco_vertex_ndof;
   vector<float>   *tandp_staco_vertex_chi2;
   vector<float>   *tandp_staco_vertex_posX;
   vector<float>   *tandp_staco_vertex_posY;
   vector<float>   *tandp_staco_vertex_posZ;
   vector<float>   *tandp_staco_vertex_probability;
   vector<float>   *tandp_staco_v0_mass;
   vector<float>   *tandp_staco_v0_mass_error;
   vector<float>   *tandp_staco_v0_mass_probability;
   vector<float>   *tandp_staco_v0_px;
   vector<float>   *tandp_staco_v0_py;
   vector<float>   *tandp_staco_v0_pz;
   vector<float>   *tandp_staco_v0_pt;
   vector<float>   *tandp_staco_v0_pt_error;
   vector<int>     *tandp_staco_num_verticies;
   vector<vector<int> > *tandp_staco_vertex_index;
   vector<vector<float> > *tandp_staco_lxyVec;
   vector<vector<float> > *tandp_staco_lxy_errorVec;
   vector<vector<float> > *tandp_staco_tauVec;
   vector<vector<float> > *tandp_staco_tau_errorVec;
   vector<int>     *tandp_muid_tag_index;
   vector<int>     *tandp_muid_probe_index;
   vector<int>     *tandp_muid_vertex_ndof;
   vector<float>   *tandp_muid_vertex_chi2;
   vector<float>   *tandp_muid_vertex_posX;
   vector<float>   *tandp_muid_vertex_posY;
   vector<float>   *tandp_muid_vertex_posZ;
   vector<float>   *tandp_muid_vertex_probability;
   vector<float>   *tandp_muid_v0_mass;
   vector<float>   *tandp_muid_v0_mass_error;
   vector<float>   *tandp_muid_v0_mass_probability;
   vector<float>   *tandp_muid_v0_px;
   vector<float>   *tandp_muid_v0_py;
   vector<float>   *tandp_muid_v0_pz;
   vector<float>   *tandp_muid_v0_pt;
   vector<float>   *tandp_muid_v0_pt_error;
   vector<int>     *tandp_muid_num_verticies;
   vector<vector<int> > *tandp_muid_vertex_index;
   vector<vector<float> > *tandp_muid_lxyVec;
   vector<vector<float> > *tandp_muid_lxy_errorVec;
   vector<vector<float> > *tandp_muid_tauVec;
   vector<vector<float> > *tandp_muid_tau_errorVec;

   // List of branches
   TBranch        *b_EF_2mu10;   //!
   TBranch        *b_EF_2mu10_MSonly_g10_loose;   //!
   TBranch        *b_EF_2mu10_MSonly_g10_loose_EMPTY;   //!
   TBranch        *b_EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO;   //!
   TBranch        *b_EF_2mu13;   //!
   TBranch        *b_EF_2mu13_Zmumu_IDTrkNoCut;   //!
   TBranch        *b_EF_2mu13_l2muonSA;   //!
   TBranch        *b_EF_2mu15;   //!
   TBranch        *b_EF_2mu4T;   //!
   TBranch        *b_EF_2mu4T_2e5_tight1;   //!
   TBranch        *b_EF_2mu4T_Bmumu;   //!
   TBranch        *b_EF_2mu4T_Bmumu_Barrel;   //!
   TBranch        *b_EF_2mu4T_Bmumu_BarrelOnly;   //!
   TBranch        *b_EF_2mu4T_Bmumux;   //!
   TBranch        *b_EF_2mu4T_Bmumux_Barrel;   //!
   TBranch        *b_EF_2mu4T_Bmumux_BarrelOnly;   //!
   TBranch        *b_EF_2mu4T_DiMu;   //!
   TBranch        *b_EF_2mu4T_DiMu_Barrel;   //!
   TBranch        *b_EF_2mu4T_DiMu_BarrelOnly;   //!
   TBranch        *b_EF_2mu4T_DiMu_L2StarB;   //!
   TBranch        *b_EF_2mu4T_DiMu_L2StarC;   //!
   TBranch        *b_EF_2mu4T_DiMu_e5_tight1;   //!
   TBranch        *b_EF_2mu4T_DiMu_l2muonSA;   //!
   TBranch        *b_EF_2mu4T_DiMu_noVtx_noOS;   //!
   TBranch        *b_EF_2mu4T_Jpsimumu;   //!
   TBranch        *b_EF_2mu4T_Jpsimumu_Barrel;   //!
   TBranch        *b_EF_2mu4T_Jpsimumu_BarrelOnly;   //!
   TBranch        *b_EF_2mu4T_Jpsimumu_IDTrkNoCut;   //!
   TBranch        *b_EF_2mu4T_Upsimumu;   //!
   TBranch        *b_EF_2mu4T_Upsimumu_Barrel;   //!
   TBranch        *b_EF_2mu4T_Upsimumu_BarrelOnly;   //!
   TBranch        *b_EF_2mu4T_xe50_tclcw;   //!
   TBranch        *b_EF_2mu4T_xe60;   //!
   TBranch        *b_EF_2mu4T_xe60_tclcw;   //!
   TBranch        *b_EF_2mu6;   //!
   TBranch        *b_EF_2mu6_Bmumu;   //!
   TBranch        *b_EF_2mu6_Bmumux;   //!
   TBranch        *b_EF_2mu6_DiMu;   //!
   TBranch        *b_EF_2mu6_DiMu_DY20;   //!
   TBranch        *b_EF_2mu6_DiMu_DY25;   //!
   TBranch        *b_EF_2mu6_DiMu_noVtx_noOS;   //!
   TBranch        *b_EF_2mu6_Jpsimumu;   //!
   TBranch        *b_EF_2mu6_Upsimumu;   //!
   TBranch        *b_EF_2mu6i_DiMu_DY;   //!
   TBranch        *b_EF_2mu6i_DiMu_DY_2j25_a4tchad;   //!
   TBranch        *b_EF_2mu6i_DiMu_DY_noVtx_noOS;   //!
   TBranch        *b_EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad;   //!
   TBranch        *b_EF_2mu8_EFxe30;   //!
   TBranch        *b_EF_2mu8_EFxe30_tclcw;   //!
   TBranch        *b_EF_mu10;   //!
   TBranch        *b_EF_mu10_Jpsimumu;   //!
   TBranch        *b_EF_mu10_MSonly;   //!
   TBranch        *b_EF_mu10_Upsimumu_tight_FS;   //!
   TBranch        *b_EF_mu10i_g10_loose;   //!
   TBranch        *b_EF_mu10i_g10_loose_TauMass;   //!
   TBranch        *b_EF_mu10i_g10_medium;   //!
   TBranch        *b_EF_mu10i_g10_medium_TauMass;   //!
   TBranch        *b_EF_mu10i_loose_g12Tvh_loose;   //!
   TBranch        *b_EF_mu10i_loose_g12Tvh_loose_TauMass;   //!
   TBranch        *b_EF_mu10i_loose_g12Tvh_medium;   //!
   TBranch        *b_EF_mu10i_loose_g12Tvh_medium_TauMass;   //!
   TBranch        *b_EF_mu11_empty_NoAlg;   //!
   TBranch        *b_EF_mu13;   //!
   TBranch        *b_EF_mu15;   //!
   TBranch        *b_EF_mu18;   //!
   TBranch        *b_EF_mu18_2g10_loose;   //!
   TBranch        *b_EF_mu18_2g10_medium;   //!
   TBranch        *b_EF_mu18_2g15_loose;   //!
   TBranch        *b_EF_mu18_IDTrkNoCut_tight;   //!
   TBranch        *b_EF_mu18_g20vh_loose;   //!
   TBranch        *b_EF_mu18_medium;   //!
   TBranch        *b_EF_mu18_tight;   //!
   TBranch        *b_EF_mu18_tight_2mu4_EFFS;   //!
   TBranch        *b_EF_mu18_tight_e7_medium1;   //!
   TBranch        *b_EF_mu18_tight_mu8_EFFS;   //!
   TBranch        *b_EF_mu18i4_tight;   //!
   TBranch        *b_EF_mu18it_tight;   //!
   TBranch        *b_EF_mu20i_tight_g5_loose;   //!
   TBranch        *b_EF_mu20i_tight_g5_loose_TauMass;   //!
   TBranch        *b_EF_mu20i_tight_g5_medium;   //!
   TBranch        *b_EF_mu20i_tight_g5_medium_TauMass;   //!
   TBranch        *b_EF_mu20it_tight;   //!
   TBranch        *b_EF_mu22_IDTrkNoCut_tight;   //!
   TBranch        *b_EF_mu24;   //!
   TBranch        *b_EF_mu24_g20vh_loose;   //!
   TBranch        *b_EF_mu24_g20vh_medium;   //!
   TBranch        *b_EF_mu24_j65_a4tchad;   //!
   TBranch        *b_EF_mu24_j65_a4tchad_EFxe40;   //!
   TBranch        *b_EF_mu24_j65_a4tchad_EFxe40_tclcw;   //!
   TBranch        *b_EF_mu24_j65_a4tchad_EFxe50_tclcw;   //!
   TBranch        *b_EF_mu24_j65_a4tchad_EFxe60_tclcw;   //!
   TBranch        *b_EF_mu24_medium;   //!
   TBranch        *b_EF_mu24_muCombTag_NoEF_tight;   //!
   TBranch        *b_EF_mu24_tight;   //!
   TBranch        *b_EF_mu24_tight_2j35_a4tchad;   //!
   TBranch        *b_EF_mu24_tight_3j35_a4tchad;   //!
   TBranch        *b_EF_mu24_tight_4j35_a4tchad;   //!
   TBranch        *b_EF_mu24_tight_EFxe40;   //!
   TBranch        *b_EF_mu24_tight_L2StarB;   //!
   TBranch        *b_EF_mu24_tight_L2StarC;   //!
   TBranch        *b_EF_mu24_tight_MG;   //!
   TBranch        *b_EF_mu24_tight_MuonEF;   //!
   TBranch        *b_EF_mu24_tight_b35_mediumEF_j35_a4tchad;   //!
   TBranch        *b_EF_mu24_tight_mu6_EFFS;   //!
   TBranch        *b_EF_mu24i_tight;   //!
   TBranch        *b_EF_mu24i_tight_MG;   //!
   TBranch        *b_EF_mu24i_tight_MuonEF;   //!
   TBranch        *b_EF_mu24i_tight_l2muonSA;   //!
   TBranch        *b_EF_mu36_tight;   //!
   TBranch        *b_EF_mu40_MSonly_barrel_tight;   //!
   TBranch        *b_EF_mu40_muCombTag_NoEF;   //!
   TBranch        *b_EF_mu40_slow_outOfTime_tight;   //!
   TBranch        *b_EF_mu40_slow_tight;   //!
   TBranch        *b_EF_mu40_tight;   //!
   TBranch        *b_EF_mu4T;   //!
   TBranch        *b_EF_mu4T_Trk_Jpsi;   //!
   TBranch        *b_EF_mu4T_cosmic;   //!
   TBranch        *b_EF_mu4T_j110_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j110_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j145_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j145_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j15_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j15_a4tchad_matchedZ;   //!
   TBranch        *b_EF_mu4T_j180_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j180_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j220_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j220_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j25_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j25_a4tchad_matchedZ;   //!
   TBranch        *b_EF_mu4T_j280_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j280_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j35_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j35_a4tchad_matchedZ;   //!
   TBranch        *b_EF_mu4T_j360_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j360_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j45_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j45_a4tchad_L2FS_matchedZ;   //!
   TBranch        *b_EF_mu4T_j45_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j45_a4tchad_matchedZ;   //!
   TBranch        *b_EF_mu4T_j55_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j55_a4tchad_L2FS_matchedZ;   //!
   TBranch        *b_EF_mu4T_j55_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j55_a4tchad_matchedZ;   //!
   TBranch        *b_EF_mu4T_j65_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j65_a4tchad_matched;   //!
   TBranch        *b_EF_mu4T_j65_a4tchad_xe60_tclcw_loose;   //!
   TBranch        *b_EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose;   //!
   TBranch        *b_EF_mu4T_j80_a4tchad_L2FS_matched;   //!
   TBranch        *b_EF_mu4T_j80_a4tchad_matched;   //!
   TBranch        *b_EF_mu4Ti_g20Tvh_loose;   //!
   TBranch        *b_EF_mu4Ti_g20Tvh_loose_TauMass;   //!
   TBranch        *b_EF_mu4Ti_g20Tvh_medium;   //!
   TBranch        *b_EF_mu4Ti_g20Tvh_medium_TauMass;   //!
   TBranch        *b_EF_mu4Tmu6_Bmumu;   //!
   TBranch        *b_EF_mu4Tmu6_Bmumu_Barrel;   //!
   TBranch        *b_EF_mu4Tmu6_Bmumux;   //!
   TBranch        *b_EF_mu4Tmu6_Bmumux_Barrel;   //!
   TBranch        *b_EF_mu4Tmu6_DiMu;   //!
   TBranch        *b_EF_mu4Tmu6_DiMu_Barrel;   //!
   TBranch        *b_EF_mu4Tmu6_DiMu_noVtx_noOS;   //!
   TBranch        *b_EF_mu4Tmu6_Jpsimumu;   //!
   TBranch        *b_EF_mu4Tmu6_Jpsimumu_Barrel;   //!
   TBranch        *b_EF_mu4Tmu6_Jpsimumu_IDTrkNoCut;   //!
   TBranch        *b_EF_mu4Tmu6_Upsimumu;   //!
   TBranch        *b_EF_mu4Tmu6_Upsimumu_Barrel;   //!
   TBranch        *b_EF_mu4_L1MU11_MSonly_cosmic;   //!
   TBranch        *b_EF_mu4_L1MU11_cosmic;   //!
   TBranch        *b_EF_mu4_empty_NoAlg;   //!
   TBranch        *b_EF_mu4_firstempty_NoAlg;   //!
   TBranch        *b_EF_mu4_unpaired_iso_NoAlg;   //!
   TBranch        *b_EF_mu50_MSonly_barrel_tight;   //!
   TBranch        *b_EF_mu6;   //!
   TBranch        *b_EF_mu60_slow_outOfTime_tight1;   //!
   TBranch        *b_EF_mu60_slow_tight1;   //!
   TBranch        *b_EF_mu6_Jpsimumu_tight;   //!
   TBranch        *b_EF_mu6_MSonly;   //!
   TBranch        *b_EF_mu6_Trk_Jpsi_loose;   //!
   TBranch        *b_EF_mu6i;   //!
   TBranch        *b_EF_mu8;   //!
   TBranch        *b_EF_mu8_4j45_a4tchad_L2FS;   //!
   TBranch        *b_L1_2MU10;   //!
   TBranch        *b_L1_2MU4;   //!
   TBranch        *b_L1_2MU4_2EM3;   //!
   TBranch        *b_L1_2MU4_BARREL;   //!
   TBranch        *b_L1_2MU4_BARRELONLY;   //!
   TBranch        *b_L1_2MU4_EM3;   //!
   TBranch        *b_L1_2MU4_EMPTY;   //!
   TBranch        *b_L1_2MU4_FIRSTEMPTY;   //!
   TBranch        *b_L1_2MU4_MU6;   //!
   TBranch        *b_L1_2MU4_MU6_BARREL;   //!
   TBranch        *b_L1_2MU4_XE30;   //!
   TBranch        *b_L1_2MU4_XE40;   //!
   TBranch        *b_L1_2MU6;   //!
   TBranch        *b_L1_2MU6_UNPAIRED_ISO;   //!
   TBranch        *b_L1_2MU6_UNPAIRED_NONISO;   //!
   TBranch        *b_L1_MU10;   //!
   TBranch        *b_L1_MU10_EMPTY;   //!
   TBranch        *b_L1_MU10_FIRSTEMPTY;   //!
   TBranch        *b_L1_MU10_J20;   //!
   TBranch        *b_L1_MU10_UNPAIRED_ISO;   //!
   TBranch        *b_L1_MU10_XE20;   //!
   TBranch        *b_L1_MU10_XE25;   //!
   TBranch        *b_L1_MU11;   //!
   TBranch        *b_L1_MU11_EMPTY;   //!
   TBranch        *b_L1_MU15;   //!
   TBranch        *b_L1_MU20;   //!
   TBranch        *b_L1_MU20_FIRSTEMPTY;   //!
   TBranch        *b_L1_MU4;   //!
   TBranch        *b_L1_MU4_EMPTY;   //!
   TBranch        *b_L1_MU4_FIRSTEMPTY;   //!
   TBranch        *b_L1_MU4_J10;   //!
   TBranch        *b_L1_MU4_J15;   //!
   TBranch        *b_L1_MU4_J15_EMPTY;   //!
   TBranch        *b_L1_MU4_J15_UNPAIRED_ISO;   //!
   TBranch        *b_L1_MU4_J20_XE20;   //!
   TBranch        *b_L1_MU4_J20_XE35;   //!
   TBranch        *b_L1_MU4_J30;   //!
   TBranch        *b_L1_MU4_J50;   //!
   TBranch        *b_L1_MU4_J75;   //!
   TBranch        *b_L1_MU4_UNPAIRED_ISO;   //!
   TBranch        *b_L1_MU4_UNPAIRED_NONISO;   //!
   TBranch        *b_L1_MU6;   //!
   TBranch        *b_L1_MU6_2J20;   //!
   TBranch        *b_L1_MU6_FIRSTEMPTY;   //!
   TBranch        *b_L1_MU6_J15;   //!
   TBranch        *b_L1_MUB;   //!
   TBranch        *b_L1_MUE;   //!
   TBranch        *b_L2_2mu10;   //!
   TBranch        *b_L2_2mu10_MSonly_g10_loose;   //!
   TBranch        *b_L2_2mu10_MSonly_g10_loose_EMPTY;   //!
   TBranch        *b_L2_2mu10_MSonly_g10_loose_UNPAIRED_ISO;   //!
   TBranch        *b_L2_2mu13;   //!
   TBranch        *b_L2_2mu13_Zmumu_IDTrkNoCut;   //!
   TBranch        *b_L2_2mu13_l2muonSA;   //!
   TBranch        *b_L2_2mu15;   //!
   TBranch        *b_L2_2mu4T;   //!
   TBranch        *b_L2_2mu4T_2e5_tight1;   //!
   TBranch        *b_L2_2mu4T_Bmumu;   //!
   TBranch        *b_L2_2mu4T_Bmumu_Barrel;   //!
   TBranch        *b_L2_2mu4T_Bmumu_BarrelOnly;   //!
   TBranch        *b_L2_2mu4T_Bmumux;   //!
   TBranch        *b_L2_2mu4T_Bmumux_Barrel;   //!
   TBranch        *b_L2_2mu4T_Bmumux_BarrelOnly;   //!
   TBranch        *b_L2_2mu4T_DiMu;   //!
   TBranch        *b_L2_2mu4T_DiMu_Barrel;   //!
   TBranch        *b_L2_2mu4T_DiMu_BarrelOnly;   //!
   TBranch        *b_L2_2mu4T_DiMu_L2StarB;   //!
   TBranch        *b_L2_2mu4T_DiMu_L2StarC;   //!
   TBranch        *b_L2_2mu4T_DiMu_e5_tight1;   //!
   TBranch        *b_L2_2mu4T_DiMu_l2muonSA;   //!
   TBranch        *b_L2_2mu4T_DiMu_noVtx_noOS;   //!
   TBranch        *b_L2_2mu4T_Jpsimumu;   //!
   TBranch        *b_L2_2mu4T_Jpsimumu_Barrel;   //!
   TBranch        *b_L2_2mu4T_Jpsimumu_BarrelOnly;   //!
   TBranch        *b_L2_2mu4T_Jpsimumu_IDTrkNoCut;   //!
   TBranch        *b_L2_2mu4T_Upsimumu;   //!
   TBranch        *b_L2_2mu4T_Upsimumu_Barrel;   //!
   TBranch        *b_L2_2mu4T_Upsimumu_BarrelOnly;   //!
   TBranch        *b_L2_2mu4T_xe35;   //!
   TBranch        *b_L2_2mu4T_xe45;   //!
   TBranch        *b_L2_2mu4T_xe60;   //!
   TBranch        *b_L2_2mu6;   //!
   TBranch        *b_L2_2mu6_Bmumu;   //!
   TBranch        *b_L2_2mu6_Bmumux;   //!
   TBranch        *b_L2_2mu6_DiMu;   //!
   TBranch        *b_L2_2mu6_DiMu_DY20;   //!
   TBranch        *b_L2_2mu6_DiMu_DY25;   //!
   TBranch        *b_L2_2mu6_DiMu_noVtx_noOS;   //!
   TBranch        *b_L2_2mu6_Jpsimumu;   //!
   TBranch        *b_L2_2mu6_Upsimumu;   //!
   TBranch        *b_L2_2mu6i_DiMu_DY;   //!
   TBranch        *b_L2_2mu6i_DiMu_DY_2j25_a4tchad;   //!
   TBranch        *b_L2_2mu6i_DiMu_DY_noVtx_noOS;   //!
   TBranch        *b_L2_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad;   //!
   TBranch        *b_L2_2mu8_EFxe30;   //!
   TBranch        *b_L2_mu10;   //!
   TBranch        *b_L2_mu10_Jpsimumu;   //!
   TBranch        *b_L2_mu10_MSonly;   //!
   TBranch        *b_L2_mu10_Upsimumu_tight_FS;   //!
   TBranch        *b_L2_mu10i_g10_loose;   //!
   TBranch        *b_L2_mu10i_g10_loose_TauMass;   //!
   TBranch        *b_L2_mu10i_g10_medium;   //!
   TBranch        *b_L2_mu10i_g10_medium_TauMass;   //!
   TBranch        *b_L2_mu10i_loose_g12Tvh_loose;   //!
   TBranch        *b_L2_mu10i_loose_g12Tvh_loose_TauMass;   //!
   TBranch        *b_L2_mu10i_loose_g12Tvh_medium;   //!
   TBranch        *b_L2_mu10i_loose_g12Tvh_medium_TauMass;   //!
   TBranch        *b_L2_mu11_empty_NoAlg;   //!
   TBranch        *b_L2_mu13;   //!
   TBranch        *b_L2_mu15;   //!
   TBranch        *b_L2_mu15_l2cal;   //!
   TBranch        *b_L2_mu18;   //!
   TBranch        *b_L2_mu18_2g10_loose;   //!
   TBranch        *b_L2_mu18_2g10_medium;   //!
   TBranch        *b_L2_mu18_2g15_loose;   //!
   TBranch        *b_L2_mu18_IDTrkNoCut_tight;   //!
   TBranch        *b_L2_mu18_g20vh_loose;   //!
   TBranch        *b_L2_mu18_medium;   //!
   TBranch        *b_L2_mu18_tight;   //!
   TBranch        *b_L2_mu18_tight_e7_medium1;   //!
   TBranch        *b_L2_mu18i4_tight;   //!
   TBranch        *b_L2_mu18it_tight;   //!
   TBranch        *b_L2_mu20i_tight_g5_loose;   //!
   TBranch        *b_L2_mu20i_tight_g5_loose_TauMass;   //!
   TBranch        *b_L2_mu20i_tight_g5_medium;   //!
   TBranch        *b_L2_mu20i_tight_g5_medium_TauMass;   //!
   TBranch        *b_L2_mu20it_tight;   //!
   TBranch        *b_L2_mu22_IDTrkNoCut_tight;   //!
   TBranch        *b_L2_mu24;   //!
   TBranch        *b_L2_mu24_g20vh_loose;   //!
   TBranch        *b_L2_mu24_g20vh_medium;   //!
   TBranch        *b_L2_mu24_j60_c4cchad_EFxe40;   //!
   TBranch        *b_L2_mu24_j60_c4cchad_EFxe50;   //!
   TBranch        *b_L2_mu24_j60_c4cchad_EFxe60;   //!
   TBranch        *b_L2_mu24_j60_c4cchad_xe35;   //!
   TBranch        *b_L2_mu24_j65_c4cchad;   //!
   TBranch        *b_L2_mu24_medium;   //!
   TBranch        *b_L2_mu24_muCombTag_NoEF_tight;   //!
   TBranch        *b_L2_mu24_tight;   //!
   TBranch        *b_L2_mu24_tight_2j35_a4tchad;   //!
   TBranch        *b_L2_mu24_tight_3j35_a4tchad;   //!
   TBranch        *b_L2_mu24_tight_4j35_a4tchad;   //!
   TBranch        *b_L2_mu24_tight_EFxe40;   //!
   TBranch        *b_L2_mu24_tight_L2StarB;   //!
   TBranch        *b_L2_mu24_tight_L2StarC;   //!
   TBranch        *b_L2_mu24_tight_l2muonSA;   //!
   TBranch        *b_L2_mu36_tight;   //!
   TBranch        *b_L2_mu40_MSonly_barrel_tight;   //!
   TBranch        *b_L2_mu40_muCombTag_NoEF;   //!
   TBranch        *b_L2_mu40_slow_outOfTime_tight;   //!
   TBranch        *b_L2_mu40_slow_tight;   //!
   TBranch        *b_L2_mu40_tight;   //!
   TBranch        *b_L2_mu4T;   //!
   TBranch        *b_L2_mu4T_Trk_Jpsi;   //!
   TBranch        *b_L2_mu4T_cosmic;   //!
   TBranch        *b_L2_mu4T_j105_c4cchad;   //!
   TBranch        *b_L2_mu4T_j10_a4TTem;   //!
   TBranch        *b_L2_mu4T_j140_c4cchad;   //!
   TBranch        *b_L2_mu4T_j15_a4TTem;   //!
   TBranch        *b_L2_mu4T_j165_c4cchad;   //!
   TBranch        *b_L2_mu4T_j30_a4TTem;   //!
   TBranch        *b_L2_mu4T_j40_c4cchad;   //!
   TBranch        *b_L2_mu4T_j50_a4TTem;   //!
   TBranch        *b_L2_mu4T_j50_c4cchad;   //!
   TBranch        *b_L2_mu4T_j60_c4cchad;   //!
   TBranch        *b_L2_mu4T_j60_c4cchad_xe40;   //!
   TBranch        *b_L2_mu4T_j75_a4TTem;   //!
   TBranch        *b_L2_mu4T_j75_c4cchad;   //!
   TBranch        *b_L2_mu4Ti_g20Tvh_loose;   //!
   TBranch        *b_L2_mu4Ti_g20Tvh_loose_TauMass;   //!
   TBranch        *b_L2_mu4Ti_g20Tvh_medium;   //!
   TBranch        *b_L2_mu4Ti_g20Tvh_medium_TauMass;   //!
   TBranch        *b_L2_mu4Tmu6_Bmumu;   //!
   TBranch        *b_L2_mu4Tmu6_Bmumu_Barrel;   //!
   TBranch        *b_L2_mu4Tmu6_Bmumux;   //!
   TBranch        *b_L2_mu4Tmu6_Bmumux_Barrel;   //!
   TBranch        *b_L2_mu4Tmu6_DiMu;   //!
   TBranch        *b_L2_mu4Tmu6_DiMu_Barrel;   //!
   TBranch        *b_L2_mu4Tmu6_DiMu_noVtx_noOS;   //!
   TBranch        *b_L2_mu4Tmu6_Jpsimumu;   //!
   TBranch        *b_L2_mu4Tmu6_Jpsimumu_Barrel;   //!
   TBranch        *b_L2_mu4Tmu6_Jpsimumu_IDTrkNoCut;   //!
   TBranch        *b_L2_mu4Tmu6_Upsimumu;   //!
   TBranch        *b_L2_mu4Tmu6_Upsimumu_Barrel;   //!
   TBranch        *b_L2_mu4_L1MU11_MSonly_cosmic;   //!
   TBranch        *b_L2_mu4_L1MU11_cosmic;   //!
   TBranch        *b_L2_mu4_empty_NoAlg;   //!
   TBranch        *b_L2_mu4_firstempty_NoAlg;   //!
   TBranch        *b_L2_mu4_l2cal_empty;   //!
   TBranch        *b_L2_mu4_unpaired_iso_NoAlg;   //!
   TBranch        *b_L2_mu50_MSonly_barrel_tight;   //!
   TBranch        *b_L2_mu6;   //!
   TBranch        *b_L2_mu60_slow_outOfTime_tight1;   //!
   TBranch        *b_L2_mu60_slow_tight1;   //!
   TBranch        *b_L2_mu6_Jpsimumu_tight;   //!
   TBranch        *b_L2_mu6_MSonly;   //!
   TBranch        *b_L2_mu6_Trk_Jpsi_loose;   //!
   TBranch        *b_L2_mu8;   //!
   TBranch        *b_L2_mu8_4j15_a4TTem;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_timestamp;   //!
   TBranch        *b_timestamp_ns;   //!
   TBranch        *b_lbn;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_detmask0;   //!
   TBranch        *b_detmask1;   //!
   TBranch        *b_actualIntPerXing;   //!
   TBranch        *b_averageIntPerXing;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_mc_event_number;   //!
   TBranch        *b_mc_event_weight;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_fwdFlags;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_pixelError;   //!
   TBranch        *b_sctError;   //!
   TBranch        *b_trtError;   //!
   TBranch        *b_larError;   //!
   TBranch        *b_tileError;   //!
   TBranch        *b_muonError;   //!
   TBranch        *b_fwdError;   //!
   TBranch        *b_coreError;   //!
   TBranch        *b_isSimulation;   //!
   TBranch        *b_isCalibration;   //!
   TBranch        *b_isTestBeam;   //!
   TBranch        *b_vxp_n;   //!
   TBranch        *b_vxp_x;   //!
   TBranch        *b_vxp_y;   //!
   TBranch        *b_vxp_z;   //!
   TBranch        *b_vxp_cov_x;   //!
   TBranch        *b_vxp_cov_y;   //!
   TBranch        *b_vxp_cov_z;   //!
   TBranch        *b_vxp_cov_xy;   //!
   TBranch        *b_vxp_cov_xz;   //!
   TBranch        *b_vxp_cov_yz;   //!
   TBranch        *b_vxp_type;   //!
   TBranch        *b_vxp_chi2;   //!
   TBranch        *b_vxp_ndof;   //!
   TBranch        *b_vxp_px;   //!
   TBranch        *b_vxp_py;   //!
   TBranch        *b_vxp_pz;   //!
   TBranch        *b_vxp_E;   //!
   TBranch        *b_vxp_m;   //!
   TBranch        *b_vxp_nTracks;   //!
   TBranch        *b_vxp_sumPt;   //!
   TBranch        *b_vxp_trk_weight;   //!
   TBranch        *b_vxp_trk_n;   //!
   TBranch        *b_vxp_trk_index;   //!
   TBranch        *b_trk_n;   //!
   TBranch        *b_trk_pt;   //!
   TBranch        *b_trk_eta;   //!
   TBranch        *b_trk_d0_wrtPV;   //!
   TBranch        *b_trk_z0_wrtPV;   //!
   TBranch        *b_trk_phi_wrtPV;   //!
   TBranch        *b_trk_theta_wrtPV;   //!
   TBranch        *b_trk_qoverp_wrtPV;   //!
   TBranch        *b_trk_cov_d0_wrtPV;   //!
   TBranch        *b_trk_cov_z0_wrtPV;   //!
   TBranch        *b_trk_cov_phi_wrtPV;   //!
   TBranch        *b_trk_cov_theta_wrtPV;   //!
   TBranch        *b_trk_cov_qoverp_wrtPV;   //!
   TBranch        *b_trk_d0_wrtBS;   //!
   TBranch        *b_trk_z0_wrtBS;   //!
   TBranch        *b_trk_phi_wrtBS;   //!
   TBranch        *b_trk_cov_d0_wrtBS;   //!
   TBranch        *b_trk_cov_z0_wrtBS;   //!
   TBranch        *b_trk_cov_phi_wrtBS;   //!
   TBranch        *b_trk_cov_theta_wrtBS;   //!
   TBranch        *b_trk_cov_qoverp_wrtBS;   //!
   TBranch        *b_trk_cov_d0_z0_wrtBS;   //!
   TBranch        *b_trk_cov_d0_phi_wrtBS;   //!
   TBranch        *b_trk_cov_d0_theta_wrtBS;   //!
   TBranch        *b_trk_cov_d0_qoverp_wrtBS;   //!
   TBranch        *b_trk_cov_z0_phi_wrtBS;   //!
   TBranch        *b_trk_cov_z0_theta_wrtBS;   //!
   TBranch        *b_trk_cov_z0_qoverp_wrtBS;   //!
   TBranch        *b_trk_cov_phi_theta_wrtBS;   //!
   TBranch        *b_trk_cov_phi_qoverp_wrtBS;   //!
   TBranch        *b_trk_cov_theta_qoverp_wrtBS;   //!
   TBranch        *b_trk_d0_wrtBL;   //!
   TBranch        *b_trk_z0_wrtBL;   //!
   TBranch        *b_trk_phi_wrtBL;   //!
   TBranch        *b_trk_d0_err_wrtBL;   //!
   TBranch        *b_trk_z0_err_wrtBL;   //!
   TBranch        *b_trk_phi_err_wrtBL;   //!
   TBranch        *b_trk_theta_err_wrtBL;   //!
   TBranch        *b_trk_qoverp_err_wrtBL;   //!
   TBranch        *b_trk_d0_z0_err_wrtBL;   //!
   TBranch        *b_trk_d0_phi_err_wrtBL;   //!
   TBranch        *b_trk_d0_theta_err_wrtBL;   //!
   TBranch        *b_trk_d0_qoverp_err_wrtBL;   //!
   TBranch        *b_trk_z0_phi_err_wrtBL;   //!
   TBranch        *b_trk_z0_theta_err_wrtBL;   //!
   TBranch        *b_trk_z0_qoverp_err_wrtBL;   //!
   TBranch        *b_trk_phi_theta_err_wrtBL;   //!
   TBranch        *b_trk_phi_qoverp_err_wrtBL;   //!
   TBranch        *b_trk_theta_qoverp_err_wrtBL;   //!
   TBranch        *b_trk_chi2;   //!
   TBranch        *b_trk_ndof;   //!
   TBranch        *b_trk_nBLHits;   //!
   TBranch        *b_trk_nPixHits;   //!
   TBranch        *b_trk_nSCTHits;   //!
   TBranch        *b_trk_nTRTHits;   //!
   TBranch        *b_trk_nTRTHighTHits;   //!
   TBranch        *b_trk_nTRTXenonHits;   //!
   TBranch        *b_trk_nPixHoles;   //!
   TBranch        *b_trk_nSCTHoles;   //!
   TBranch        *b_trk_nTRTHoles;   //!
   TBranch        *b_trk_nPixelDeadSensors;   //!
   TBranch        *b_trk_nSCTDeadSensors;   //!
   TBranch        *b_trk_nBLSharedHits;   //!
   TBranch        *b_trk_nPixSharedHits;   //!
   TBranch        *b_trk_nSCTSharedHits;   //!
   TBranch        *b_trk_nBLayerSplitHits;   //!
   TBranch        *b_trk_nPixSplitHits;   //!
   TBranch        *b_trk_expectBLayerHit;   //!
   TBranch        *b_trk_nMDTHits;   //!
   TBranch        *b_trk_nCSCEtaHits;   //!
   TBranch        *b_trk_nCSCPhiHits;   //!
   TBranch        *b_trk_nRPCEtaHits;   //!
   TBranch        *b_trk_nRPCPhiHits;   //!
   TBranch        *b_trk_nTGCEtaHits;   //!
   TBranch        *b_trk_nTGCPhiHits;   //!
   TBranch        *b_trk_nHits;   //!
   TBranch        *b_trk_nHoles;   //!
   TBranch        *b_trk_hitPattern;   //!
   TBranch        *b_trk_TRTHighTHitsRatio;   //!
   TBranch        *b_trk_TRTHighTOutliersRatio;   //!
   TBranch        *b_trk_fitter;   //!
   TBranch        *b_trk_patternReco1;   //!
   TBranch        *b_trk_patternReco2;   //!
   TBranch        *b_trk_trackProperties;   //!
   TBranch        *b_trk_particleHypothesis;   //!
   TBranch        *b_trk_blayerPrediction_x;   //!
   TBranch        *b_trk_blayerPrediction_y;   //!
   TBranch        *b_trk_blayerPrediction_z;   //!
   TBranch        *b_trk_blayerPrediction_locX;   //!
   TBranch        *b_trk_blayerPrediction_locY;   //!
   TBranch        *b_trk_blayerPrediction_err_locX;   //!
   TBranch        *b_trk_blayerPrediction_err_locY;   //!
   TBranch        *b_trk_blayerPrediction_etaDistToEdge;   //!
   TBranch        *b_trk_blayerPrediction_phiDistToEdge;   //!
   TBranch        *b_trk_blayerPrediction_detElementId;   //!
   TBranch        *b_trk_blayerPrediction_row;   //!
   TBranch        *b_trk_blayerPrediction_col;   //!
   TBranch        *b_trk_blayerPrediction_type;   //!
   TBranch        *b_trk_mc_probability;   //!
   TBranch        *b_trk_mc_barcode;   //!
   TBranch        *b_mu_muid_n;   //!
   TBranch        *b_mu_muid_E;   //!
   TBranch        *b_mu_muid_pt;   //!
   TBranch        *b_mu_muid_m;   //!
   TBranch        *b_mu_muid_eta;   //!
   TBranch        *b_mu_muid_phi;   //!
   TBranch        *b_mu_muid_px;   //!
   TBranch        *b_mu_muid_py;   //!
   TBranch        *b_mu_muid_pz;   //!
   TBranch        *b_mu_muid_charge;   //!
   TBranch        *b_mu_muid_allauthor;   //!
   TBranch        *b_mu_muid_author;   //!
   TBranch        *b_mu_muid_beta;   //!
   TBranch        *b_mu_muid_isMuonLikelihood;   //!
   TBranch        *b_mu_muid_matchchi2;   //!
   TBranch        *b_mu_muid_matchndof;   //!
   TBranch        *b_mu_muid_etcone20;   //!
   TBranch        *b_mu_muid_etcone30;   //!
   TBranch        *b_mu_muid_etcone40;   //!
   TBranch        *b_mu_muid_nucone20;   //!
   TBranch        *b_mu_muid_nucone30;   //!
   TBranch        *b_mu_muid_nucone40;   //!
   TBranch        *b_mu_muid_ptcone20;   //!
   TBranch        *b_mu_muid_ptcone30;   //!
   TBranch        *b_mu_muid_ptcone40;   //!
   TBranch        *b_mu_muid_etconeNoEm10;   //!
   TBranch        *b_mu_muid_etconeNoEm20;   //!
   TBranch        *b_mu_muid_etconeNoEm30;   //!
   TBranch        *b_mu_muid_etconeNoEm40;   //!
   TBranch        *b_mu_muid_scatteringCurvatureSignificance;   //!
   TBranch        *b_mu_muid_scatteringNeighbourSignificance;   //!
   TBranch        *b_mu_muid_momentumBalanceSignificance;   //!
   TBranch        *b_mu_muid_energyLossPar;   //!
   TBranch        *b_mu_muid_energyLossErr;   //!
   TBranch        *b_mu_muid_etCore;   //!
   TBranch        *b_mu_muid_energyLossType;   //!
   TBranch        *b_mu_muid_caloMuonIdTag;   //!
   TBranch        *b_mu_muid_caloLRLikelihood;   //!
   TBranch        *b_mu_muid_bestMatch;   //!
   TBranch        *b_mu_muid_isStandAloneMuon;   //!
   TBranch        *b_mu_muid_isCombinedMuon;   //!
   TBranch        *b_mu_muid_isLowPtReconstructedMuon;   //!
   TBranch        *b_mu_muid_isSegmentTaggedMuon;   //!
   TBranch        *b_mu_muid_isCaloMuonId;   //!
   TBranch        *b_mu_muid_alsoFoundByLowPt;   //!
   TBranch        *b_mu_muid_alsoFoundByCaloMuonId;   //!
   TBranch        *b_mu_muid_isSiliconAssociatedForwardMuon;   //!
   TBranch        *b_mu_muid_loose;   //!
   TBranch        *b_mu_muid_medium;   //!
   TBranch        *b_mu_muid_tight;   //!
   TBranch        *b_mu_muid_d0_exPV;   //!
   TBranch        *b_mu_muid_z0_exPV;   //!
   TBranch        *b_mu_muid_phi_exPV;   //!
   TBranch        *b_mu_muid_theta_exPV;   //!
   TBranch        *b_mu_muid_qoverp_exPV;   //!
   TBranch        *b_mu_muid_cb_d0_exPV;   //!
   TBranch        *b_mu_muid_cb_z0_exPV;   //!
   TBranch        *b_mu_muid_cb_phi_exPV;   //!
   TBranch        *b_mu_muid_cb_theta_exPV;   //!
   TBranch        *b_mu_muid_cb_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_d0_exPV;   //!
   TBranch        *b_mu_muid_id_z0_exPV;   //!
   TBranch        *b_mu_muid_id_phi_exPV;   //!
   TBranch        *b_mu_muid_id_theta_exPV;   //!
   TBranch        *b_mu_muid_id_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_d0_exPV;   //!
   TBranch        *b_mu_muid_me_z0_exPV;   //!
   TBranch        *b_mu_muid_me_phi_exPV;   //!
   TBranch        *b_mu_muid_me_theta_exPV;   //!
   TBranch        *b_mu_muid_me_qoverp_exPV;   //!
   TBranch        *b_mu_muid_ie_d0_exPV;   //!
   TBranch        *b_mu_muid_ie_z0_exPV;   //!
   TBranch        *b_mu_muid_ie_phi_exPV;   //!
   TBranch        *b_mu_muid_ie_theta_exPV;   //!
   TBranch        *b_mu_muid_ie_qoverp_exPV;   //!
   TBranch        *b_mu_muid_SpaceTime_detID;   //!
   TBranch        *b_mu_muid_SpaceTime_t;   //!
   TBranch        *b_mu_muid_SpaceTime_tError;   //!
   TBranch        *b_mu_muid_SpaceTime_weight;   //!
   TBranch        *b_mu_muid_SpaceTime_x;   //!
   TBranch        *b_mu_muid_SpaceTime_y;   //!
   TBranch        *b_mu_muid_SpaceTime_z;   //!
   TBranch        *b_mu_muid_cov_d0_exPV;   //!
   TBranch        *b_mu_muid_cov_z0_exPV;   //!
   TBranch        *b_mu_muid_cov_phi_exPV;   //!
   TBranch        *b_mu_muid_cov_theta_exPV;   //!
   TBranch        *b_mu_muid_cov_qoverp_exPV;   //!
   TBranch        *b_mu_muid_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_muid_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_muid_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_muid_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_muid_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_muid_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_muid_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_muid_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_cov_d0_exPV;   //!
   TBranch        *b_mu_muid_id_cov_z0_exPV;   //!
   TBranch        *b_mu_muid_id_cov_phi_exPV;   //!
   TBranch        *b_mu_muid_id_cov_theta_exPV;   //!
   TBranch        *b_mu_muid_id_cov_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_muid_id_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_muid_id_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_muid_id_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_muid_id_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_muid_id_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_muid_id_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_muid_id_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_cov_d0_exPV;   //!
   TBranch        *b_mu_muid_me_cov_z0_exPV;   //!
   TBranch        *b_mu_muid_me_cov_phi_exPV;   //!
   TBranch        *b_mu_muid_me_cov_theta_exPV;   //!
   TBranch        *b_mu_muid_me_cov_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_muid_me_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_muid_me_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_muid_me_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_muid_me_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_muid_me_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_muid_me_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_muid_me_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_muid_ms_d0;   //!
   TBranch        *b_mu_muid_ms_z0;   //!
   TBranch        *b_mu_muid_ms_phi;   //!
   TBranch        *b_mu_muid_ms_theta;   //!
   TBranch        *b_mu_muid_ms_qoverp;   //!
   TBranch        *b_mu_muid_id_d0;   //!
   TBranch        *b_mu_muid_id_z0;   //!
   TBranch        *b_mu_muid_id_phi;   //!
   TBranch        *b_mu_muid_id_theta;   //!
   TBranch        *b_mu_muid_id_qoverp;   //!
   TBranch        *b_mu_muid_me_d0;   //!
   TBranch        *b_mu_muid_me_z0;   //!
   TBranch        *b_mu_muid_me_phi;   //!
   TBranch        *b_mu_muid_me_theta;   //!
   TBranch        *b_mu_muid_me_qoverp;   //!
   TBranch        *b_mu_muid_ie_d0;   //!
   TBranch        *b_mu_muid_ie_z0;   //!
   TBranch        *b_mu_muid_ie_phi;   //!
   TBranch        *b_mu_muid_ie_theta;   //!
   TBranch        *b_mu_muid_ie_qoverp;   //!
   TBranch        *b_mu_muid_nOutliersOnTrack;   //!
   TBranch        *b_mu_muid_nBLHits;   //!
   TBranch        *b_mu_muid_nPixHits;   //!
   TBranch        *b_mu_muid_nSCTHits;   //!
   TBranch        *b_mu_muid_nTRTHits;   //!
   TBranch        *b_mu_muid_nTRTHighTHits;   //!
   TBranch        *b_mu_muid_nBLSharedHits;   //!
   TBranch        *b_mu_muid_nPixSharedHits;   //!
   TBranch        *b_mu_muid_nPixHoles;   //!
   TBranch        *b_mu_muid_nSCTSharedHits;   //!
   TBranch        *b_mu_muid_nSCTHoles;   //!
   TBranch        *b_mu_muid_nTRTOutliers;   //!
   TBranch        *b_mu_muid_nTRTHighTOutliers;   //!
   TBranch        *b_mu_muid_nGangedPixels;   //!
   TBranch        *b_mu_muid_nPixelDeadSensors;   //!
   TBranch        *b_mu_muid_nSCTDeadSensors;   //!
   TBranch        *b_mu_muid_nTRTDeadStraws;   //!
   TBranch        *b_mu_muid_expectBLayerHit;   //!
   TBranch        *b_mu_muid_nMDTHits;   //!
   TBranch        *b_mu_muid_nMDTHoles;   //!
   TBranch        *b_mu_muid_nCSCEtaHits;   //!
   TBranch        *b_mu_muid_nCSCEtaHoles;   //!
   TBranch        *b_mu_muid_nCSCUnspoiledEtaHits;   //!
   TBranch        *b_mu_muid_nCSCPhiHits;   //!
   TBranch        *b_mu_muid_nCSCPhiHoles;   //!
   TBranch        *b_mu_muid_nRPCEtaHits;   //!
   TBranch        *b_mu_muid_nRPCEtaHoles;   //!
   TBranch        *b_mu_muid_nRPCPhiHits;   //!
   TBranch        *b_mu_muid_nRPCPhiHoles;   //!
   TBranch        *b_mu_muid_nTGCEtaHits;   //!
   TBranch        *b_mu_muid_nTGCEtaHoles;   //!
   TBranch        *b_mu_muid_nTGCPhiHits;   //!
   TBranch        *b_mu_muid_nTGCPhiHoles;   //!
   TBranch        *b_mu_muid_nprecisionLayers;   //!
   TBranch        *b_mu_muid_nprecisionHoleLayers;   //!
   TBranch        *b_mu_muid_nphiLayers;   //!
   TBranch        *b_mu_muid_ntrigEtaLayers;   //!
   TBranch        *b_mu_muid_nphiHoleLayers;   //!
   TBranch        *b_mu_muid_ntrigEtaHoleLayers;   //!
   TBranch        *b_mu_muid_nMDTBIHits;   //!
   TBranch        *b_mu_muid_nMDTBMHits;   //!
   TBranch        *b_mu_muid_nMDTBOHits;   //!
   TBranch        *b_mu_muid_nMDTBEEHits;   //!
   TBranch        *b_mu_muid_nMDTBIS78Hits;   //!
   TBranch        *b_mu_muid_nMDTEIHits;   //!
   TBranch        *b_mu_muid_nMDTEMHits;   //!
   TBranch        *b_mu_muid_nMDTEOHits;   //!
   TBranch        *b_mu_muid_nMDTEEHits;   //!
   TBranch        *b_mu_muid_nRPCLayer1EtaHits;   //!
   TBranch        *b_mu_muid_nRPCLayer2EtaHits;   //!
   TBranch        *b_mu_muid_nRPCLayer3EtaHits;   //!
   TBranch        *b_mu_muid_nRPCLayer1PhiHits;   //!
   TBranch        *b_mu_muid_nRPCLayer2PhiHits;   //!
   TBranch        *b_mu_muid_nRPCLayer3PhiHits;   //!
   TBranch        *b_mu_muid_nTGCLayer1EtaHits;   //!
   TBranch        *b_mu_muid_nTGCLayer2EtaHits;   //!
   TBranch        *b_mu_muid_nTGCLayer3EtaHits;   //!
   TBranch        *b_mu_muid_nTGCLayer4EtaHits;   //!
   TBranch        *b_mu_muid_nTGCLayer1PhiHits;   //!
   TBranch        *b_mu_muid_nTGCLayer2PhiHits;   //!
   TBranch        *b_mu_muid_nTGCLayer3PhiHits;   //!
   TBranch        *b_mu_muid_nTGCLayer4PhiHits;   //!
   TBranch        *b_mu_muid_barrelSectors;   //!
   TBranch        *b_mu_muid_endcapSectors;   //!
   TBranch        *b_mu_muid_spec_surf_px;   //!
   TBranch        *b_mu_muid_spec_surf_py;   //!
   TBranch        *b_mu_muid_spec_surf_pz;   //!
   TBranch        *b_mu_muid_spec_surf_x;   //!
   TBranch        *b_mu_muid_spec_surf_y;   //!
   TBranch        *b_mu_muid_spec_surf_z;   //!
   TBranch        *b_mu_muid_trackd0;   //!
   TBranch        *b_mu_muid_trackz0;   //!
   TBranch        *b_mu_muid_trackphi;   //!
   TBranch        *b_mu_muid_tracktheta;   //!
   TBranch        *b_mu_muid_trackqoverp;   //!
   TBranch        *b_mu_muid_trackcov_d0;   //!
   TBranch        *b_mu_muid_trackcov_z0;   //!
   TBranch        *b_mu_muid_trackcov_phi;   //!
   TBranch        *b_mu_muid_trackcov_theta;   //!
   TBranch        *b_mu_muid_trackcov_qoverp;   //!
   TBranch        *b_mu_muid_trackcov_d0_z0;   //!
   TBranch        *b_mu_muid_trackcov_d0_phi;   //!
   TBranch        *b_mu_muid_trackcov_d0_theta;   //!
   TBranch        *b_mu_muid_trackcov_d0_qoverp;   //!
   TBranch        *b_mu_muid_trackcov_z0_phi;   //!
   TBranch        *b_mu_muid_trackcov_z0_theta;   //!
   TBranch        *b_mu_muid_trackcov_z0_qoverp;   //!
   TBranch        *b_mu_muid_trackcov_phi_theta;   //!
   TBranch        *b_mu_muid_trackcov_phi_qoverp;   //!
   TBranch        *b_mu_muid_trackcov_theta_qoverp;   //!
   TBranch        *b_mu_muid_trackfitchi2;   //!
   TBranch        *b_mu_muid_trackfitndof;   //!
   TBranch        *b_mu_muid_hastrack;   //!
   TBranch        *b_mu_muid_trackd0beam;   //!
   TBranch        *b_mu_muid_trackz0beam;   //!
   TBranch        *b_mu_muid_tracksigd0beam;   //!
   TBranch        *b_mu_muid_tracksigz0beam;   //!
   TBranch        *b_mu_muid_trackd0pv;   //!
   TBranch        *b_mu_muid_trackz0pv;   //!
   TBranch        *b_mu_muid_tracksigd0pv;   //!
   TBranch        *b_mu_muid_tracksigz0pv;   //!
   TBranch        *b_mu_muid_trackIPEstimate_d0_biasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_z0_biasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_sigd0_biasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_sigz0_biasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_d0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_z0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_muid_trackd0pvunbiased;   //!
   TBranch        *b_mu_muid_trackz0pvunbiased;   //!
   TBranch        *b_mu_muid_tracksigd0pvunbiased;   //!
   TBranch        *b_mu_muid_tracksigz0pvunbiased;   //!
   TBranch        *b_mu_muid_type;   //!
   TBranch        *b_mu_muid_origin;   //!
   TBranch        *b_mu_muid_truth_dr;   //!
   TBranch        *b_mu_muid_truth_E;   //!
   TBranch        *b_mu_muid_truth_pt;   //!
   TBranch        *b_mu_muid_truth_eta;   //!
   TBranch        *b_mu_muid_truth_phi;   //!
   TBranch        *b_mu_muid_truth_type;   //!
   TBranch        *b_mu_muid_truth_status;   //!
   TBranch        *b_mu_muid_truth_barcode;   //!
   TBranch        *b_mu_muid_truth_mothertype;   //!
   TBranch        *b_mu_muid_truth_motherbarcode;   //!
   TBranch        *b_mu_muid_truth_matched;   //!
   TBranch        *b_mu_muid_EFCB_dr;   //!
   TBranch        *b_mu_muid_EFCB_n;   //!
   TBranch        *b_mu_muid_EFCB_MuonType;   //!
   TBranch        *b_mu_muid_EFCB_pt;   //!
   TBranch        *b_mu_muid_EFCB_eta;   //!
   TBranch        *b_mu_muid_EFCB_phi;   //!
   TBranch        *b_mu_muid_EFCB_hasCB;   //!
   TBranch        *b_mu_muid_EFCB_matched;   //!
   TBranch        *b_mu_muid_EFMG_dr;   //!
   TBranch        *b_mu_muid_EFMG_n;   //!
   TBranch        *b_mu_muid_EFMG_MuonType;   //!
   TBranch        *b_mu_muid_EFMG_pt;   //!
   TBranch        *b_mu_muid_EFMG_eta;   //!
   TBranch        *b_mu_muid_EFMG_phi;   //!
   TBranch        *b_mu_muid_EFMG_hasMG;   //!
   TBranch        *b_mu_muid_EFMG_matched;   //!
   TBranch        *b_mu_muid_EFME_dr;   //!
   TBranch        *b_mu_muid_EFME_n;   //!
   TBranch        *b_mu_muid_EFME_MuonType;   //!
   TBranch        *b_mu_muid_EFME_pt;   //!
   TBranch        *b_mu_muid_EFME_eta;   //!
   TBranch        *b_mu_muid_EFME_phi;   //!
   TBranch        *b_mu_muid_EFME_hasME;   //!
   TBranch        *b_mu_muid_EFME_matched;   //!
   TBranch        *b_mu_muid_L2CB_dr;   //!
   TBranch        *b_mu_muid_L2CB_pt;   //!
   TBranch        *b_mu_muid_L2CB_eta;   //!
   TBranch        *b_mu_muid_L2CB_phi;   //!
   TBranch        *b_mu_muid_L2CB_id_pt;   //!
   TBranch        *b_mu_muid_L2CB_ms_pt;   //!
   TBranch        *b_mu_muid_L2CB_nPixHits;   //!
   TBranch        *b_mu_muid_L2CB_nSCTHits;   //!
   TBranch        *b_mu_muid_L2CB_nTRTHits;   //!
   TBranch        *b_mu_muid_L2CB_nTRTHighTHits;   //!
   TBranch        *b_mu_muid_L2CB_matched;   //!
   TBranch        *b_mu_muid_L1_dr;   //!
   TBranch        *b_mu_muid_L1_pt;   //!
   TBranch        *b_mu_muid_L1_eta;   //!
   TBranch        *b_mu_muid_L1_phi;   //!
   TBranch        *b_mu_muid_L1_thrNumber;   //!
   TBranch        *b_mu_muid_L1_RoINumber;   //!
   TBranch        *b_mu_muid_L1_sectorAddress;   //!
   TBranch        *b_mu_muid_L1_firstCandidate;   //!
   TBranch        *b_mu_muid_L1_moreCandInRoI;   //!
   TBranch        *b_mu_muid_L1_moreCandInSector;   //!
   TBranch        *b_mu_muid_L1_source;   //!
   TBranch        *b_mu_muid_L1_hemisphere;   //!
   TBranch        *b_mu_muid_L1_charge;   //!
   TBranch        *b_mu_muid_L1_vetoed;   //!
   TBranch        *b_mu_muid_L1_matched;   //!
   TBranch        *b_mu_staco_n;   //!
   TBranch        *b_mu_staco_E;   //!
   TBranch        *b_mu_staco_pt;   //!
   TBranch        *b_mu_staco_m;   //!
   TBranch        *b_mu_staco_eta;   //!
   TBranch        *b_mu_staco_phi;   //!
   TBranch        *b_mu_staco_px;   //!
   TBranch        *b_mu_staco_py;   //!
   TBranch        *b_mu_staco_pz;   //!
   TBranch        *b_mu_staco_charge;   //!
   TBranch        *b_mu_staco_allauthor;   //!
   TBranch        *b_mu_staco_author;   //!
   TBranch        *b_mu_staco_beta;   //!
   TBranch        *b_mu_staco_isMuonLikelihood;   //!
   TBranch        *b_mu_staco_matchchi2;   //!
   TBranch        *b_mu_staco_matchndof;   //!
   TBranch        *b_mu_staco_etcone20;   //!
   TBranch        *b_mu_staco_etcone30;   //!
   TBranch        *b_mu_staco_etcone40;   //!
   TBranch        *b_mu_staco_nucone20;   //!
   TBranch        *b_mu_staco_nucone30;   //!
   TBranch        *b_mu_staco_nucone40;   //!
   TBranch        *b_mu_staco_ptcone20;   //!
   TBranch        *b_mu_staco_ptcone30;   //!
   TBranch        *b_mu_staco_ptcone40;   //!
   TBranch        *b_mu_staco_etconeNoEm10;   //!
   TBranch        *b_mu_staco_etconeNoEm20;   //!
   TBranch        *b_mu_staco_etconeNoEm30;   //!
   TBranch        *b_mu_staco_etconeNoEm40;   //!
   TBranch        *b_mu_staco_scatteringCurvatureSignificance;   //!
   TBranch        *b_mu_staco_scatteringNeighbourSignificance;   //!
   TBranch        *b_mu_staco_momentumBalanceSignificance;   //!
   TBranch        *b_mu_staco_energyLossPar;   //!
   TBranch        *b_mu_staco_energyLossErr;   //!
   TBranch        *b_mu_staco_etCore;   //!
   TBranch        *b_mu_staco_energyLossType;   //!
   TBranch        *b_mu_staco_caloMuonIdTag;   //!
   TBranch        *b_mu_staco_caloLRLikelihood;   //!
   TBranch        *b_mu_staco_bestMatch;   //!
   TBranch        *b_mu_staco_isStandAloneMuon;   //!
   TBranch        *b_mu_staco_isCombinedMuon;   //!
   TBranch        *b_mu_staco_isLowPtReconstructedMuon;   //!
   TBranch        *b_mu_staco_isSegmentTaggedMuon;   //!
   TBranch        *b_mu_staco_isCaloMuonId;   //!
   TBranch        *b_mu_staco_alsoFoundByLowPt;   //!
   TBranch        *b_mu_staco_alsoFoundByCaloMuonId;   //!
   TBranch        *b_mu_staco_isSiliconAssociatedForwardMuon;   //!
   TBranch        *b_mu_staco_loose;   //!
   TBranch        *b_mu_staco_medium;   //!
   TBranch        *b_mu_staco_tight;   //!
   TBranch        *b_mu_staco_d0_exPV;   //!
   TBranch        *b_mu_staco_z0_exPV;   //!
   TBranch        *b_mu_staco_phi_exPV;   //!
   TBranch        *b_mu_staco_theta_exPV;   //!
   TBranch        *b_mu_staco_qoverp_exPV;   //!
   TBranch        *b_mu_staco_cb_d0_exPV;   //!
   TBranch        *b_mu_staco_cb_z0_exPV;   //!
   TBranch        *b_mu_staco_cb_phi_exPV;   //!
   TBranch        *b_mu_staco_cb_theta_exPV;   //!
   TBranch        *b_mu_staco_cb_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_d0_exPV;   //!
   TBranch        *b_mu_staco_id_z0_exPV;   //!
   TBranch        *b_mu_staco_id_phi_exPV;   //!
   TBranch        *b_mu_staco_id_theta_exPV;   //!
   TBranch        *b_mu_staco_id_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_d0_exPV;   //!
   TBranch        *b_mu_staco_me_z0_exPV;   //!
   TBranch        *b_mu_staco_me_phi_exPV;   //!
   TBranch        *b_mu_staco_me_theta_exPV;   //!
   TBranch        *b_mu_staco_me_qoverp_exPV;   //!
   TBranch        *b_mu_staco_ie_d0_exPV;   //!
   TBranch        *b_mu_staco_ie_z0_exPV;   //!
   TBranch        *b_mu_staco_ie_phi_exPV;   //!
   TBranch        *b_mu_staco_ie_theta_exPV;   //!
   TBranch        *b_mu_staco_ie_qoverp_exPV;   //!
   TBranch        *b_mu_staco_SpaceTime_detID;   //!
   TBranch        *b_mu_staco_SpaceTime_t;   //!
   TBranch        *b_mu_staco_SpaceTime_tError;   //!
   TBranch        *b_mu_staco_SpaceTime_weight;   //!
   TBranch        *b_mu_staco_SpaceTime_x;   //!
   TBranch        *b_mu_staco_SpaceTime_y;   //!
   TBranch        *b_mu_staco_SpaceTime_z;   //!
   TBranch        *b_mu_staco_cov_d0_exPV;   //!
   TBranch        *b_mu_staco_cov_z0_exPV;   //!
   TBranch        *b_mu_staco_cov_phi_exPV;   //!
   TBranch        *b_mu_staco_cov_theta_exPV;   //!
   TBranch        *b_mu_staco_cov_qoverp_exPV;   //!
   TBranch        *b_mu_staco_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_staco_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_staco_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_staco_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_staco_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_staco_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_staco_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_staco_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_cov_d0_exPV;   //!
   TBranch        *b_mu_staco_id_cov_z0_exPV;   //!
   TBranch        *b_mu_staco_id_cov_phi_exPV;   //!
   TBranch        *b_mu_staco_id_cov_theta_exPV;   //!
   TBranch        *b_mu_staco_id_cov_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_staco_id_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_staco_id_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_staco_id_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_staco_id_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_staco_id_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_staco_id_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_staco_id_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_cov_d0_exPV;   //!
   TBranch        *b_mu_staco_me_cov_z0_exPV;   //!
   TBranch        *b_mu_staco_me_cov_phi_exPV;   //!
   TBranch        *b_mu_staco_me_cov_theta_exPV;   //!
   TBranch        *b_mu_staco_me_cov_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_staco_me_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_staco_me_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_staco_me_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_staco_me_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_staco_me_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_staco_me_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_staco_me_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_staco_ms_d0;   //!
   TBranch        *b_mu_staco_ms_z0;   //!
   TBranch        *b_mu_staco_ms_phi;   //!
   TBranch        *b_mu_staco_ms_theta;   //!
   TBranch        *b_mu_staco_ms_qoverp;   //!
   TBranch        *b_mu_staco_id_d0;   //!
   TBranch        *b_mu_staco_id_z0;   //!
   TBranch        *b_mu_staco_id_phi;   //!
   TBranch        *b_mu_staco_id_theta;   //!
   TBranch        *b_mu_staco_id_qoverp;   //!
   TBranch        *b_mu_staco_me_d0;   //!
   TBranch        *b_mu_staco_me_z0;   //!
   TBranch        *b_mu_staco_me_phi;   //!
   TBranch        *b_mu_staco_me_theta;   //!
   TBranch        *b_mu_staco_me_qoverp;   //!
   TBranch        *b_mu_staco_ie_d0;   //!
   TBranch        *b_mu_staco_ie_z0;   //!
   TBranch        *b_mu_staco_ie_phi;   //!
   TBranch        *b_mu_staco_ie_theta;   //!
   TBranch        *b_mu_staco_ie_qoverp;   //!
   TBranch        *b_mu_staco_nOutliersOnTrack;   //!
   TBranch        *b_mu_staco_nBLHits;   //!
   TBranch        *b_mu_staco_nPixHits;   //!
   TBranch        *b_mu_staco_nSCTHits;   //!
   TBranch        *b_mu_staco_nTRTHits;   //!
   TBranch        *b_mu_staco_nTRTHighTHits;   //!
   TBranch        *b_mu_staco_nBLSharedHits;   //!
   TBranch        *b_mu_staco_nPixSharedHits;   //!
   TBranch        *b_mu_staco_nPixHoles;   //!
   TBranch        *b_mu_staco_nSCTSharedHits;   //!
   TBranch        *b_mu_staco_nSCTHoles;   //!
   TBranch        *b_mu_staco_nTRTOutliers;   //!
   TBranch        *b_mu_staco_nTRTHighTOutliers;   //!
   TBranch        *b_mu_staco_nGangedPixels;   //!
   TBranch        *b_mu_staco_nPixelDeadSensors;   //!
   TBranch        *b_mu_staco_nSCTDeadSensors;   //!
   TBranch        *b_mu_staco_nTRTDeadStraws;   //!
   TBranch        *b_mu_staco_expectBLayerHit;   //!
   TBranch        *b_mu_staco_nMDTHits;   //!
   TBranch        *b_mu_staco_nMDTHoles;   //!
   TBranch        *b_mu_staco_nCSCEtaHits;   //!
   TBranch        *b_mu_staco_nCSCEtaHoles;   //!
   TBranch        *b_mu_staco_nCSCUnspoiledEtaHits;   //!
   TBranch        *b_mu_staco_nCSCPhiHits;   //!
   TBranch        *b_mu_staco_nCSCPhiHoles;   //!
   TBranch        *b_mu_staco_nRPCEtaHits;   //!
   TBranch        *b_mu_staco_nRPCEtaHoles;   //!
   TBranch        *b_mu_staco_nRPCPhiHits;   //!
   TBranch        *b_mu_staco_nRPCPhiHoles;   //!
   TBranch        *b_mu_staco_nTGCEtaHits;   //!
   TBranch        *b_mu_staco_nTGCEtaHoles;   //!
   TBranch        *b_mu_staco_nTGCPhiHits;   //!
   TBranch        *b_mu_staco_nTGCPhiHoles;   //!
   TBranch        *b_mu_staco_nprecisionLayers;   //!
   TBranch        *b_mu_staco_nprecisionHoleLayers;   //!
   TBranch        *b_mu_staco_nphiLayers;   //!
   TBranch        *b_mu_staco_ntrigEtaLayers;   //!
   TBranch        *b_mu_staco_nphiHoleLayers;   //!
   TBranch        *b_mu_staco_ntrigEtaHoleLayers;   //!
   TBranch        *b_mu_staco_nMDTBIHits;   //!
   TBranch        *b_mu_staco_nMDTBMHits;   //!
   TBranch        *b_mu_staco_nMDTBOHits;   //!
   TBranch        *b_mu_staco_nMDTBEEHits;   //!
   TBranch        *b_mu_staco_nMDTBIS78Hits;   //!
   TBranch        *b_mu_staco_nMDTEIHits;   //!
   TBranch        *b_mu_staco_nMDTEMHits;   //!
   TBranch        *b_mu_staco_nMDTEOHits;   //!
   TBranch        *b_mu_staco_nMDTEEHits;   //!
   TBranch        *b_mu_staco_nRPCLayer1EtaHits;   //!
   TBranch        *b_mu_staco_nRPCLayer2EtaHits;   //!
   TBranch        *b_mu_staco_nRPCLayer3EtaHits;   //!
   TBranch        *b_mu_staco_nRPCLayer1PhiHits;   //!
   TBranch        *b_mu_staco_nRPCLayer2PhiHits;   //!
   TBranch        *b_mu_staco_nRPCLayer3PhiHits;   //!
   TBranch        *b_mu_staco_nTGCLayer1EtaHits;   //!
   TBranch        *b_mu_staco_nTGCLayer2EtaHits;   //!
   TBranch        *b_mu_staco_nTGCLayer3EtaHits;   //!
   TBranch        *b_mu_staco_nTGCLayer4EtaHits;   //!
   TBranch        *b_mu_staco_nTGCLayer1PhiHits;   //!
   TBranch        *b_mu_staco_nTGCLayer2PhiHits;   //!
   TBranch        *b_mu_staco_nTGCLayer3PhiHits;   //!
   TBranch        *b_mu_staco_nTGCLayer4PhiHits;   //!
   TBranch        *b_mu_staco_barrelSectors;   //!
   TBranch        *b_mu_staco_endcapSectors;   //!
   TBranch        *b_mu_staco_spec_surf_px;   //!
   TBranch        *b_mu_staco_spec_surf_py;   //!
   TBranch        *b_mu_staco_spec_surf_pz;   //!
   TBranch        *b_mu_staco_spec_surf_x;   //!
   TBranch        *b_mu_staco_spec_surf_y;   //!
   TBranch        *b_mu_staco_spec_surf_z;   //!
   TBranch        *b_mu_staco_trackd0;   //!
   TBranch        *b_mu_staco_trackz0;   //!
   TBranch        *b_mu_staco_trackphi;   //!
   TBranch        *b_mu_staco_tracktheta;   //!
   TBranch        *b_mu_staco_trackqoverp;   //!
   TBranch        *b_mu_staco_trackcov_d0;   //!
   TBranch        *b_mu_staco_trackcov_z0;   //!
   TBranch        *b_mu_staco_trackcov_phi;   //!
   TBranch        *b_mu_staco_trackcov_theta;   //!
   TBranch        *b_mu_staco_trackcov_qoverp;   //!
   TBranch        *b_mu_staco_trackcov_d0_z0;   //!
   TBranch        *b_mu_staco_trackcov_d0_phi;   //!
   TBranch        *b_mu_staco_trackcov_d0_theta;   //!
   TBranch        *b_mu_staco_trackcov_d0_qoverp;   //!
   TBranch        *b_mu_staco_trackcov_z0_phi;   //!
   TBranch        *b_mu_staco_trackcov_z0_theta;   //!
   TBranch        *b_mu_staco_trackcov_z0_qoverp;   //!
   TBranch        *b_mu_staco_trackcov_phi_theta;   //!
   TBranch        *b_mu_staco_trackcov_phi_qoverp;   //!
   TBranch        *b_mu_staco_trackcov_theta_qoverp;   //!
   TBranch        *b_mu_staco_trackfitchi2;   //!
   TBranch        *b_mu_staco_trackfitndof;   //!
   TBranch        *b_mu_staco_hastrack;   //!
   TBranch        *b_mu_staco_trackd0beam;   //!
   TBranch        *b_mu_staco_trackz0beam;   //!
   TBranch        *b_mu_staco_tracksigd0beam;   //!
   TBranch        *b_mu_staco_tracksigz0beam;   //!
   TBranch        *b_mu_staco_trackd0pv;   //!
   TBranch        *b_mu_staco_trackz0pv;   //!
   TBranch        *b_mu_staco_tracksigd0pv;   //!
   TBranch        *b_mu_staco_tracksigz0pv;   //!
   TBranch        *b_mu_staco_trackIPEstimate_d0_biasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_z0_biasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_sigd0_biasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_sigz0_biasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_d0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_z0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_staco_trackd0pvunbiased;   //!
   TBranch        *b_mu_staco_trackz0pvunbiased;   //!
   TBranch        *b_mu_staco_tracksigd0pvunbiased;   //!
   TBranch        *b_mu_staco_tracksigz0pvunbiased;   //!
   TBranch        *b_mu_staco_type;   //!
   TBranch        *b_mu_staco_origin;   //!
   TBranch        *b_mu_staco_truth_dr;   //!
   TBranch        *b_mu_staco_truth_E;   //!
   TBranch        *b_mu_staco_truth_pt;   //!
   TBranch        *b_mu_staco_truth_eta;   //!
   TBranch        *b_mu_staco_truth_phi;   //!
   TBranch        *b_mu_staco_truth_type;   //!
   TBranch        *b_mu_staco_truth_status;   //!
   TBranch        *b_mu_staco_truth_barcode;   //!
   TBranch        *b_mu_staco_truth_mothertype;   //!
   TBranch        *b_mu_staco_truth_motherbarcode;   //!
   TBranch        *b_mu_staco_truth_matched;   //!
   TBranch        *b_mu_staco_EFCB_dr;   //!
   TBranch        *b_mu_staco_EFCB_n;   //!
   TBranch        *b_mu_staco_EFCB_MuonType;   //!
   TBranch        *b_mu_staco_EFCB_pt;   //!
   TBranch        *b_mu_staco_EFCB_eta;   //!
   TBranch        *b_mu_staco_EFCB_phi;   //!
   TBranch        *b_mu_staco_EFCB_hasCB;   //!
   TBranch        *b_mu_staco_EFCB_matched;   //!
   TBranch        *b_mu_staco_EFMG_dr;   //!
   TBranch        *b_mu_staco_EFMG_n;   //!
   TBranch        *b_mu_staco_EFMG_MuonType;   //!
   TBranch        *b_mu_staco_EFMG_pt;   //!
   TBranch        *b_mu_staco_EFMG_eta;   //!
   TBranch        *b_mu_staco_EFMG_phi;   //!
   TBranch        *b_mu_staco_EFMG_hasMG;   //!
   TBranch        *b_mu_staco_EFMG_matched;   //!
   TBranch        *b_mu_staco_EFME_dr;   //!
   TBranch        *b_mu_staco_EFME_n;   //!
   TBranch        *b_mu_staco_EFME_MuonType;   //!
   TBranch        *b_mu_staco_EFME_pt;   //!
   TBranch        *b_mu_staco_EFME_eta;   //!
   TBranch        *b_mu_staco_EFME_phi;   //!
   TBranch        *b_mu_staco_EFME_hasME;   //!
   TBranch        *b_mu_staco_EFME_matched;   //!
   TBranch        *b_mu_staco_L2CB_dr;   //!
   TBranch        *b_mu_staco_L2CB_pt;   //!
   TBranch        *b_mu_staco_L2CB_eta;   //!
   TBranch        *b_mu_staco_L2CB_phi;   //!
   TBranch        *b_mu_staco_L2CB_id_pt;   //!
   TBranch        *b_mu_staco_L2CB_ms_pt;   //!
   TBranch        *b_mu_staco_L2CB_nPixHits;   //!
   TBranch        *b_mu_staco_L2CB_nSCTHits;   //!
   TBranch        *b_mu_staco_L2CB_nTRTHits;   //!
   TBranch        *b_mu_staco_L2CB_nTRTHighTHits;   //!
   TBranch        *b_mu_staco_L2CB_matched;   //!
   TBranch        *b_mu_staco_L1_dr;   //!
   TBranch        *b_mu_staco_L1_pt;   //!
   TBranch        *b_mu_staco_L1_eta;   //!
   TBranch        *b_mu_staco_L1_phi;   //!
   TBranch        *b_mu_staco_L1_thrNumber;   //!
   TBranch        *b_mu_staco_L1_RoINumber;   //!
   TBranch        *b_mu_staco_L1_sectorAddress;   //!
   TBranch        *b_mu_staco_L1_firstCandidate;   //!
   TBranch        *b_mu_staco_L1_moreCandInRoI;   //!
   TBranch        *b_mu_staco_L1_moreCandInSector;   //!
   TBranch        *b_mu_staco_L1_source;   //!
   TBranch        *b_mu_staco_L1_hemisphere;   //!
   TBranch        *b_mu_staco_L1_charge;   //!
   TBranch        *b_mu_staco_L1_vetoed;   //!
   TBranch        *b_mu_staco_L1_matched;   //!
   TBranch        *b_mu_calo_n;   //!
   TBranch        *b_mu_calo_E;   //!
   TBranch        *b_mu_calo_pt;   //!
   TBranch        *b_mu_calo_m;   //!
   TBranch        *b_mu_calo_eta;   //!
   TBranch        *b_mu_calo_phi;   //!
   TBranch        *b_mu_calo_px;   //!
   TBranch        *b_mu_calo_py;   //!
   TBranch        *b_mu_calo_pz;   //!
   TBranch        *b_mu_calo_charge;   //!
   TBranch        *b_mu_calo_allauthor;   //!
   TBranch        *b_mu_calo_author;   //!
   TBranch        *b_mu_calo_beta;   //!
   TBranch        *b_mu_calo_isMuonLikelihood;   //!
   TBranch        *b_mu_calo_matchchi2;   //!
   TBranch        *b_mu_calo_matchndof;   //!
   TBranch        *b_mu_calo_etcone20;   //!
   TBranch        *b_mu_calo_etcone30;   //!
   TBranch        *b_mu_calo_etcone40;   //!
   TBranch        *b_mu_calo_nucone20;   //!
   TBranch        *b_mu_calo_nucone30;   //!
   TBranch        *b_mu_calo_nucone40;   //!
   TBranch        *b_mu_calo_ptcone20;   //!
   TBranch        *b_mu_calo_ptcone30;   //!
   TBranch        *b_mu_calo_ptcone40;   //!
   TBranch        *b_mu_calo_etconeNoEm10;   //!
   TBranch        *b_mu_calo_etconeNoEm20;   //!
   TBranch        *b_mu_calo_etconeNoEm30;   //!
   TBranch        *b_mu_calo_etconeNoEm40;   //!
   TBranch        *b_mu_calo_scatteringCurvatureSignificance;   //!
   TBranch        *b_mu_calo_scatteringNeighbourSignificance;   //!
   TBranch        *b_mu_calo_momentumBalanceSignificance;   //!
   TBranch        *b_mu_calo_energyLossPar;   //!
   TBranch        *b_mu_calo_energyLossErr;   //!
   TBranch        *b_mu_calo_etCore;   //!
   TBranch        *b_mu_calo_energyLossType;   //!
   TBranch        *b_mu_calo_caloMuonIdTag;   //!
   TBranch        *b_mu_calo_caloLRLikelihood;   //!
   TBranch        *b_mu_calo_bestMatch;   //!
   TBranch        *b_mu_calo_isStandAloneMuon;   //!
   TBranch        *b_mu_calo_isCombinedMuon;   //!
   TBranch        *b_mu_calo_isLowPtReconstructedMuon;   //!
   TBranch        *b_mu_calo_isSegmentTaggedMuon;   //!
   TBranch        *b_mu_calo_isCaloMuonId;   //!
   TBranch        *b_mu_calo_alsoFoundByLowPt;   //!
   TBranch        *b_mu_calo_alsoFoundByCaloMuonId;   //!
   TBranch        *b_mu_calo_isSiliconAssociatedForwardMuon;   //!
   TBranch        *b_mu_calo_loose;   //!
   TBranch        *b_mu_calo_medium;   //!
   TBranch        *b_mu_calo_tight;   //!
   TBranch        *b_mu_calo_d0_exPV;   //!
   TBranch        *b_mu_calo_z0_exPV;   //!
   TBranch        *b_mu_calo_phi_exPV;   //!
   TBranch        *b_mu_calo_theta_exPV;   //!
   TBranch        *b_mu_calo_qoverp_exPV;   //!
   TBranch        *b_mu_calo_cb_d0_exPV;   //!
   TBranch        *b_mu_calo_cb_z0_exPV;   //!
   TBranch        *b_mu_calo_cb_phi_exPV;   //!
   TBranch        *b_mu_calo_cb_theta_exPV;   //!
   TBranch        *b_mu_calo_cb_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_d0_exPV;   //!
   TBranch        *b_mu_calo_id_z0_exPV;   //!
   TBranch        *b_mu_calo_id_phi_exPV;   //!
   TBranch        *b_mu_calo_id_theta_exPV;   //!
   TBranch        *b_mu_calo_id_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_d0_exPV;   //!
   TBranch        *b_mu_calo_me_z0_exPV;   //!
   TBranch        *b_mu_calo_me_phi_exPV;   //!
   TBranch        *b_mu_calo_me_theta_exPV;   //!
   TBranch        *b_mu_calo_me_qoverp_exPV;   //!
   TBranch        *b_mu_calo_ie_d0_exPV;   //!
   TBranch        *b_mu_calo_ie_z0_exPV;   //!
   TBranch        *b_mu_calo_ie_phi_exPV;   //!
   TBranch        *b_mu_calo_ie_theta_exPV;   //!
   TBranch        *b_mu_calo_ie_qoverp_exPV;   //!
   TBranch        *b_mu_calo_SpaceTime_detID;   //!
   TBranch        *b_mu_calo_SpaceTime_t;   //!
   TBranch        *b_mu_calo_SpaceTime_tError;   //!
   TBranch        *b_mu_calo_SpaceTime_weight;   //!
   TBranch        *b_mu_calo_SpaceTime_x;   //!
   TBranch        *b_mu_calo_SpaceTime_y;   //!
   TBranch        *b_mu_calo_SpaceTime_z;   //!
   TBranch        *b_mu_calo_cov_d0_exPV;   //!
   TBranch        *b_mu_calo_cov_z0_exPV;   //!
   TBranch        *b_mu_calo_cov_phi_exPV;   //!
   TBranch        *b_mu_calo_cov_theta_exPV;   //!
   TBranch        *b_mu_calo_cov_qoverp_exPV;   //!
   TBranch        *b_mu_calo_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_calo_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_calo_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_calo_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_calo_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_calo_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_calo_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_calo_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_cov_d0_exPV;   //!
   TBranch        *b_mu_calo_id_cov_z0_exPV;   //!
   TBranch        *b_mu_calo_id_cov_phi_exPV;   //!
   TBranch        *b_mu_calo_id_cov_theta_exPV;   //!
   TBranch        *b_mu_calo_id_cov_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_calo_id_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_calo_id_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_calo_id_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_calo_id_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_calo_id_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_calo_id_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_calo_id_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_cov_d0_exPV;   //!
   TBranch        *b_mu_calo_me_cov_z0_exPV;   //!
   TBranch        *b_mu_calo_me_cov_phi_exPV;   //!
   TBranch        *b_mu_calo_me_cov_theta_exPV;   //!
   TBranch        *b_mu_calo_me_cov_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_cov_d0_z0_exPV;   //!
   TBranch        *b_mu_calo_me_cov_d0_phi_exPV;   //!
   TBranch        *b_mu_calo_me_cov_d0_theta_exPV;   //!
   TBranch        *b_mu_calo_me_cov_d0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_cov_z0_phi_exPV;   //!
   TBranch        *b_mu_calo_me_cov_z0_theta_exPV;   //!
   TBranch        *b_mu_calo_me_cov_z0_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_cov_phi_theta_exPV;   //!
   TBranch        *b_mu_calo_me_cov_phi_qoverp_exPV;   //!
   TBranch        *b_mu_calo_me_cov_theta_qoverp_exPV;   //!
   TBranch        *b_mu_calo_ms_d0;   //!
   TBranch        *b_mu_calo_ms_z0;   //!
   TBranch        *b_mu_calo_ms_phi;   //!
   TBranch        *b_mu_calo_ms_theta;   //!
   TBranch        *b_mu_calo_ms_qoverp;   //!
   TBranch        *b_mu_calo_id_d0;   //!
   TBranch        *b_mu_calo_id_z0;   //!
   TBranch        *b_mu_calo_id_phi;   //!
   TBranch        *b_mu_calo_id_theta;   //!
   TBranch        *b_mu_calo_id_qoverp;   //!
   TBranch        *b_mu_calo_me_d0;   //!
   TBranch        *b_mu_calo_me_z0;   //!
   TBranch        *b_mu_calo_me_phi;   //!
   TBranch        *b_mu_calo_me_theta;   //!
   TBranch        *b_mu_calo_me_qoverp;   //!
   TBranch        *b_mu_calo_ie_d0;   //!
   TBranch        *b_mu_calo_ie_z0;   //!
   TBranch        *b_mu_calo_ie_phi;   //!
   TBranch        *b_mu_calo_ie_theta;   //!
   TBranch        *b_mu_calo_ie_qoverp;   //!
   TBranch        *b_mu_calo_nOutliersOnTrack;   //!
   TBranch        *b_mu_calo_nBLHits;   //!
   TBranch        *b_mu_calo_nPixHits;   //!
   TBranch        *b_mu_calo_nSCTHits;   //!
   TBranch        *b_mu_calo_nTRTHits;   //!
   TBranch        *b_mu_calo_nTRTHighTHits;   //!
   TBranch        *b_mu_calo_nBLSharedHits;   //!
   TBranch        *b_mu_calo_nPixSharedHits;   //!
   TBranch        *b_mu_calo_nPixHoles;   //!
   TBranch        *b_mu_calo_nSCTSharedHits;   //!
   TBranch        *b_mu_calo_nSCTHoles;   //!
   TBranch        *b_mu_calo_nTRTOutliers;   //!
   TBranch        *b_mu_calo_nTRTHighTOutliers;   //!
   TBranch        *b_mu_calo_nGangedPixels;   //!
   TBranch        *b_mu_calo_nPixelDeadSensors;   //!
   TBranch        *b_mu_calo_nSCTDeadSensors;   //!
   TBranch        *b_mu_calo_nTRTDeadStraws;   //!
   TBranch        *b_mu_calo_expectBLayerHit;   //!
   TBranch        *b_mu_calo_nMDTHits;   //!
   TBranch        *b_mu_calo_nMDTHoles;   //!
   TBranch        *b_mu_calo_nCSCEtaHits;   //!
   TBranch        *b_mu_calo_nCSCEtaHoles;   //!
   TBranch        *b_mu_calo_nCSCUnspoiledEtaHits;   //!
   TBranch        *b_mu_calo_nCSCPhiHits;   //!
   TBranch        *b_mu_calo_nCSCPhiHoles;   //!
   TBranch        *b_mu_calo_nRPCEtaHits;   //!
   TBranch        *b_mu_calo_nRPCEtaHoles;   //!
   TBranch        *b_mu_calo_nRPCPhiHits;   //!
   TBranch        *b_mu_calo_nRPCPhiHoles;   //!
   TBranch        *b_mu_calo_nTGCEtaHits;   //!
   TBranch        *b_mu_calo_nTGCEtaHoles;   //!
   TBranch        *b_mu_calo_nTGCPhiHits;   //!
   TBranch        *b_mu_calo_nTGCPhiHoles;   //!
   TBranch        *b_mu_calo_nprecisionLayers;   //!
   TBranch        *b_mu_calo_nprecisionHoleLayers;   //!
   TBranch        *b_mu_calo_nphiLayers;   //!
   TBranch        *b_mu_calo_ntrigEtaLayers;   //!
   TBranch        *b_mu_calo_nphiHoleLayers;   //!
   TBranch        *b_mu_calo_ntrigEtaHoleLayers;   //!
   TBranch        *b_mu_calo_nMDTBIHits;   //!
   TBranch        *b_mu_calo_nMDTBMHits;   //!
   TBranch        *b_mu_calo_nMDTBOHits;   //!
   TBranch        *b_mu_calo_nMDTBEEHits;   //!
   TBranch        *b_mu_calo_nMDTBIS78Hits;   //!
   TBranch        *b_mu_calo_nMDTEIHits;   //!
   TBranch        *b_mu_calo_nMDTEMHits;   //!
   TBranch        *b_mu_calo_nMDTEOHits;   //!
   TBranch        *b_mu_calo_nMDTEEHits;   //!
   TBranch        *b_mu_calo_nRPCLayer1EtaHits;   //!
   TBranch        *b_mu_calo_nRPCLayer2EtaHits;   //!
   TBranch        *b_mu_calo_nRPCLayer3EtaHits;   //!
   TBranch        *b_mu_calo_nRPCLayer1PhiHits;   //!
   TBranch        *b_mu_calo_nRPCLayer2PhiHits;   //!
   TBranch        *b_mu_calo_nRPCLayer3PhiHits;   //!
   TBranch        *b_mu_calo_nTGCLayer1EtaHits;   //!
   TBranch        *b_mu_calo_nTGCLayer2EtaHits;   //!
   TBranch        *b_mu_calo_nTGCLayer3EtaHits;   //!
   TBranch        *b_mu_calo_nTGCLayer4EtaHits;   //!
   TBranch        *b_mu_calo_nTGCLayer1PhiHits;   //!
   TBranch        *b_mu_calo_nTGCLayer2PhiHits;   //!
   TBranch        *b_mu_calo_nTGCLayer3PhiHits;   //!
   TBranch        *b_mu_calo_nTGCLayer4PhiHits;   //!
   TBranch        *b_mu_calo_barrelSectors;   //!
   TBranch        *b_mu_calo_endcapSectors;   //!
   TBranch        *b_mu_calo_spec_surf_px;   //!
   TBranch        *b_mu_calo_spec_surf_py;   //!
   TBranch        *b_mu_calo_spec_surf_pz;   //!
   TBranch        *b_mu_calo_spec_surf_x;   //!
   TBranch        *b_mu_calo_spec_surf_y;   //!
   TBranch        *b_mu_calo_spec_surf_z;   //!
   TBranch        *b_mu_calo_trackd0;   //!
   TBranch        *b_mu_calo_trackz0;   //!
   TBranch        *b_mu_calo_trackphi;   //!
   TBranch        *b_mu_calo_tracktheta;   //!
   TBranch        *b_mu_calo_trackqoverp;   //!
   TBranch        *b_mu_calo_trackcov_d0;   //!
   TBranch        *b_mu_calo_trackcov_z0;   //!
   TBranch        *b_mu_calo_trackcov_phi;   //!
   TBranch        *b_mu_calo_trackcov_theta;   //!
   TBranch        *b_mu_calo_trackcov_qoverp;   //!
   TBranch        *b_mu_calo_trackcov_d0_z0;   //!
   TBranch        *b_mu_calo_trackcov_d0_phi;   //!
   TBranch        *b_mu_calo_trackcov_d0_theta;   //!
   TBranch        *b_mu_calo_trackcov_d0_qoverp;   //!
   TBranch        *b_mu_calo_trackcov_z0_phi;   //!
   TBranch        *b_mu_calo_trackcov_z0_theta;   //!
   TBranch        *b_mu_calo_trackcov_z0_qoverp;   //!
   TBranch        *b_mu_calo_trackcov_phi_theta;   //!
   TBranch        *b_mu_calo_trackcov_phi_qoverp;   //!
   TBranch        *b_mu_calo_trackcov_theta_qoverp;   //!
   TBranch        *b_mu_calo_trackfitchi2;   //!
   TBranch        *b_mu_calo_trackfitndof;   //!
   TBranch        *b_mu_calo_hastrack;   //!
   TBranch        *b_mu_calo_trackd0beam;   //!
   TBranch        *b_mu_calo_trackz0beam;   //!
   TBranch        *b_mu_calo_tracksigd0beam;   //!
   TBranch        *b_mu_calo_tracksigz0beam;   //!
   TBranch        *b_mu_calo_trackd0pv;   //!
   TBranch        *b_mu_calo_trackz0pv;   //!
   TBranch        *b_mu_calo_tracksigd0pv;   //!
   TBranch        *b_mu_calo_tracksigz0pv;   //!
   TBranch        *b_mu_calo_trackIPEstimate_d0_biasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_z0_biasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_sigd0_biasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_sigz0_biasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_d0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_z0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased;   //!
   TBranch        *b_mu_calo_trackd0pvunbiased;   //!
   TBranch        *b_mu_calo_trackz0pvunbiased;   //!
   TBranch        *b_mu_calo_tracksigd0pvunbiased;   //!
   TBranch        *b_mu_calo_tracksigz0pvunbiased;   //!
   TBranch        *b_mu_calo_type;   //!
   TBranch        *b_mu_calo_origin;   //!
   TBranch        *b_mu_calo_truth_dr;   //!
   TBranch        *b_mu_calo_truth_E;   //!
   TBranch        *b_mu_calo_truth_pt;   //!
   TBranch        *b_mu_calo_truth_eta;   //!
   TBranch        *b_mu_calo_truth_phi;   //!
   TBranch        *b_mu_calo_truth_type;   //!
   TBranch        *b_mu_calo_truth_status;   //!
   TBranch        *b_mu_calo_truth_barcode;   //!
   TBranch        *b_mu_calo_truth_mothertype;   //!
   TBranch        *b_mu_calo_truth_motherbarcode;   //!
   TBranch        *b_mu_calo_truth_matched;   //!
   TBranch        *b_mu_calo_EFCB_dr;   //!
   TBranch        *b_mu_calo_EFCB_n;   //!
   TBranch        *b_mu_calo_EFCB_MuonType;   //!
   TBranch        *b_mu_calo_EFCB_pt;   //!
   TBranch        *b_mu_calo_EFCB_eta;   //!
   TBranch        *b_mu_calo_EFCB_phi;   //!
   TBranch        *b_mu_calo_EFCB_hasCB;   //!
   TBranch        *b_mu_calo_EFCB_matched;   //!
   TBranch        *b_mu_calo_EFMG_dr;   //!
   TBranch        *b_mu_calo_EFMG_n;   //!
   TBranch        *b_mu_calo_EFMG_MuonType;   //!
   TBranch        *b_mu_calo_EFMG_pt;   //!
   TBranch        *b_mu_calo_EFMG_eta;   //!
   TBranch        *b_mu_calo_EFMG_phi;   //!
   TBranch        *b_mu_calo_EFMG_hasMG;   //!
   TBranch        *b_mu_calo_EFMG_matched;   //!
   TBranch        *b_mu_calo_EFME_dr;   //!
   TBranch        *b_mu_calo_EFME_n;   //!
   TBranch        *b_mu_calo_EFME_MuonType;   //!
   TBranch        *b_mu_calo_EFME_pt;   //!
   TBranch        *b_mu_calo_EFME_eta;   //!
   TBranch        *b_mu_calo_EFME_phi;   //!
   TBranch        *b_mu_calo_EFME_hasME;   //!
   TBranch        *b_mu_calo_EFME_matched;   //!
   TBranch        *b_mu_calo_L2CB_dr;   //!
   TBranch        *b_mu_calo_L2CB_pt;   //!
   TBranch        *b_mu_calo_L2CB_eta;   //!
   TBranch        *b_mu_calo_L2CB_phi;   //!
   TBranch        *b_mu_calo_L2CB_id_pt;   //!
   TBranch        *b_mu_calo_L2CB_ms_pt;   //!
   TBranch        *b_mu_calo_L2CB_nPixHits;   //!
   TBranch        *b_mu_calo_L2CB_nSCTHits;   //!
   TBranch        *b_mu_calo_L2CB_nTRTHits;   //!
   TBranch        *b_mu_calo_L2CB_nTRTHighTHits;   //!
   TBranch        *b_mu_calo_L2CB_matched;   //!
   TBranch        *b_mu_calo_L1_dr;   //!
   TBranch        *b_mu_calo_L1_pt;   //!
   TBranch        *b_mu_calo_L1_eta;   //!
   TBranch        *b_mu_calo_L1_phi;   //!
   TBranch        *b_mu_calo_L1_thrNumber;   //!
   TBranch        *b_mu_calo_L1_RoINumber;   //!
   TBranch        *b_mu_calo_L1_sectorAddress;   //!
   TBranch        *b_mu_calo_L1_firstCandidate;   //!
   TBranch        *b_mu_calo_L1_moreCandInRoI;   //!
   TBranch        *b_mu_calo_L1_moreCandInSector;   //!
   TBranch        *b_mu_calo_L1_source;   //!
   TBranch        *b_mu_calo_L1_hemisphere;   //!
   TBranch        *b_mu_calo_L1_charge;   //!
   TBranch        *b_mu_calo_L1_vetoed;   //!
   TBranch        *b_mu_calo_L1_matched;   //!
   TBranch        *b_mooreseg_n;   //!
   TBranch        *b_mooreseg_x;   //!
   TBranch        *b_mooreseg_y;   //!
   TBranch        *b_mooreseg_z;   //!
   TBranch        *b_mooreseg_phi;   //!
   TBranch        *b_mooreseg_theta;   //!
   TBranch        *b_mooreseg_locX;   //!
   TBranch        *b_mooreseg_locY;   //!
   TBranch        *b_mooreseg_locAngleXZ;   //!
   TBranch        *b_mooreseg_locAngleYZ;   //!
   TBranch        *b_mooreseg_sector;   //!
   TBranch        *b_mooreseg_stationEta;   //!
   TBranch        *b_mooreseg_isEndcap;   //!
   TBranch        *b_mooreseg_stationName;   //!
   TBranch        *b_mooreseg_author;   //!
   TBranch        *b_mooreseg_chi2;   //!
   TBranch        *b_mooreseg_ndof;   //!
   TBranch        *b_mooreseg_t0;   //!
   TBranch        *b_mooreseg_t0err;   //!
   TBranch        *b_mboyseg_n;   //!
   TBranch        *b_mboyseg_x;   //!
   TBranch        *b_mboyseg_y;   //!
   TBranch        *b_mboyseg_z;   //!
   TBranch        *b_mboyseg_phi;   //!
   TBranch        *b_mboyseg_theta;   //!
   TBranch        *b_mboyseg_locX;   //!
   TBranch        *b_mboyseg_locY;   //!
   TBranch        *b_mboyseg_locAngleXZ;   //!
   TBranch        *b_mboyseg_locAngleYZ;   //!
   TBranch        *b_mboyseg_sector;   //!
   TBranch        *b_mboyseg_stationEta;   //!
   TBranch        *b_mboyseg_isEndcap;   //!
   TBranch        *b_mboyseg_stationName;   //!
   TBranch        *b_mboyseg_author;   //!
   TBranch        *b_mboyseg_chi2;   //!
   TBranch        *b_mboyseg_ndof;   //!
   TBranch        *b_mboyseg_t0;   //!
   TBranch        *b_mboyseg_t0err;   //!
   TBranch        *b_mgseg_n;   //!
   TBranch        *b_mgseg_x;   //!
   TBranch        *b_mgseg_y;   //!
   TBranch        *b_mgseg_z;   //!
   TBranch        *b_mgseg_phi;   //!
   TBranch        *b_mgseg_theta;   //!
   TBranch        *b_mgseg_locX;   //!
   TBranch        *b_mgseg_locY;   //!
   TBranch        *b_mgseg_locAngleXZ;   //!
   TBranch        *b_mgseg_locAngleYZ;   //!
   TBranch        *b_mgseg_sector;   //!
   TBranch        *b_mgseg_stationEta;   //!
   TBranch        *b_mgseg_isEndcap;   //!
   TBranch        *b_mgseg_stationName;   //!
   TBranch        *b_mgseg_author;   //!
   TBranch        *b_mgseg_chi2;   //!
   TBranch        *b_mgseg_ndof;   //!
   TBranch        *b_mgseg_t0;   //!
   TBranch        *b_mgseg_t0err;   //!
   TBranch        *b_muonTruth_n;   //!
   TBranch        *b_muonTruth_pt;   //!
   TBranch        *b_muonTruth_m;   //!
   TBranch        *b_muonTruth_eta;   //!
   TBranch        *b_muonTruth_phi;   //!
   TBranch        *b_muonTruth_charge;   //!
   TBranch        *b_muonTruth_PDGID;   //!
   TBranch        *b_muonTruth_barcode;   //!
   TBranch        *b_muonTruth_type;   //!
   TBranch        *b_muonTruth_origin;   //!
   TBranch        *b_mcevt_n;   //!
   TBranch        *b_mcevt_signal_process_id;   //!
   TBranch        *b_mcevt_event_number;   //!
   TBranch        *b_mcevt_event_scale;   //!
   TBranch        *b_mcevt_alphaQCD;   //!
   TBranch        *b_mcevt_alphaQED;   //!
   TBranch        *b_mcevt_pdf_id1;   //!
   TBranch        *b_mcevt_pdf_id2;   //!
   TBranch        *b_mcevt_pdf_x1;   //!
   TBranch        *b_mcevt_pdf_x2;   //!
   TBranch        *b_mcevt_pdf_scale;   //!
   TBranch        *b_mcevt_pdf1;   //!
   TBranch        *b_mcevt_pdf2;   //!
   TBranch        *b_mcevt_weight;   //!
   TBranch        *b_mcevt_nparticle;   //!
   TBranch        *b_mcevt_pileUpType;   //!
   TBranch        *b_mc_n;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_m;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_status;   //!
   TBranch        *b_mc_barcode;   //!
   TBranch        *b_mc_pdgId;   //!
   TBranch        *b_mc_charge;   //!
   TBranch        *b_mc_parents;   //!
   TBranch        *b_mc_children;   //!
   TBranch        *b_mc_vx_x;   //!
   TBranch        *b_mc_vx_y;   //!
   TBranch        *b_mc_vx_z;   //!
   TBranch        *b_mc_vx_barcode;   //!
   TBranch        *b_mc_child_index;   //!
   TBranch        *b_mc_parent_index;   //!
   TBranch        *b_trig_bgCode;   //!
   TBranch        *b_trig_L1_mu_n;   //!
   TBranch        *b_trig_L1_mu_pt;   //!
   TBranch        *b_trig_L1_mu_eta;   //!
   TBranch        *b_trig_L1_mu_phi;   //!
   TBranch        *b_trig_L1_mu_thrName;   //!
   TBranch        *b_trig_L1_mu_thrNumber;   //!
   TBranch        *b_trig_L1_mu_RoINumber;   //!
   TBranch        *b_trig_L1_mu_sectorAddress;   //!
   TBranch        *b_trig_L1_mu_firstCandidate;   //!
   TBranch        *b_trig_L1_mu_moreCandInRoI;   //!
   TBranch        *b_trig_L1_mu_moreCandInSector;   //!
   TBranch        *b_trig_L1_mu_source;   //!
   TBranch        *b_trig_L1_mu_hemisphere;   //!
   TBranch        *b_trig_L1_mu_charge;   //!
   TBranch        *b_trig_L1_mu_vetoed;   //!
   TBranch        *b_trig_L1_mu_RoIWord;   //!
   TBranch        *b_muctpi_candMultiplicity;   //!
   TBranch        *b_muctpi_nDataWords;   //!
   TBranch        *b_muctpi_dataWords;   //!
   TBranch        *b_muctpi_dw_eta;   //!
   TBranch        *b_muctpi_dw_phi;   //!
   TBranch        *b_muctpi_dw_source;   //!
   TBranch        *b_muctpi_dw_hemisphere;   //!
   TBranch        *b_muctpi_dw_bcid;   //!
   TBranch        *b_muctpi_dw_sectorID;   //!
   TBranch        *b_muctpi_dw_thrNumber;   //!
   TBranch        *b_muctpi_dw_RoINumber;   //!
   TBranch        *b_muctpi_dw_overlapFlags;   //!
   TBranch        *b_muctpi_dw_firstCandidate;   //!
   TBranch        *b_muctpi_dw_moreCandInRoI;   //!
   TBranch        *b_muctpi_dw_moreCandInSector;   //!
   TBranch        *b_muctpi_dw_charge;   //!
   TBranch        *b_muctpi_dw_vetoed;   //!
   TBranch        *b_TGC_prd_x;   //!
   TBranch        *b_TGC_prd_y;   //!
   TBranch        *b_TGC_prd_z;   //!
   TBranch        *b_TGC_prd_shortWidth;   //!
   TBranch        *b_TGC_prd_longWidth;   //!
   TBranch        *b_TGC_prd_length;   //!
   TBranch        *b_TGC_prd_isStrip;   //!
   TBranch        *b_TGC_prd_gasGap;   //!
   TBranch        *b_TGC_prd_channel;   //!
   TBranch        *b_TGC_prd_eta;   //!
   TBranch        *b_TGC_prd_phi;   //!
   TBranch        *b_TGC_prd_station;   //!
   TBranch        *b_TGC_prd_bunch;   //!
   TBranch        *b_TGC_coin_x_In;   //!
   TBranch        *b_TGC_coin_y_In;   //!
   TBranch        *b_TGC_coin_z_In;   //!
   TBranch        *b_TGC_coin_x_Out;   //!
   TBranch        *b_TGC_coin_y_Out;   //!
   TBranch        *b_TGC_coin_z_Out;   //!
   TBranch        *b_TGC_coin_width_In;   //!
   TBranch        *b_TGC_coin_width_Out;   //!
   TBranch        *b_TGC_coin_width_R;   //!
   TBranch        *b_TGC_coin_width_Phi;   //!
   TBranch        *b_TGC_coin_isAside;   //!
   TBranch        *b_TGC_coin_isForward;   //!
   TBranch        *b_TGC_coin_isStrip;   //!
   TBranch        *b_TGC_coin_isPositiveDeltaR;   //!
   TBranch        *b_TGC_coin_type;   //!
   TBranch        *b_TGC_coin_trackletId;   //!
   TBranch        *b_TGC_coin_trackletIdStrip;   //!
   TBranch        *b_TGC_coin_phi;   //!
   TBranch        *b_TGC_coin_roi;   //!
   TBranch        *b_TGC_coin_pt;   //!
   TBranch        *b_TGC_coin_delta;   //!
   TBranch        *b_TGC_coin_sub;   //!
   TBranch        *b_TGC_coin_bunch;   //!
   TBranch        *b_TGC_hierarchy_index;   //!
   TBranch        *b_TGC_hierarchy_dR_hiPt;   //!
   TBranch        *b_TGC_hierarchy_dPhi_hiPt;   //!
   TBranch        *b_TGC_hierarchy_dR_tracklet;   //!
   TBranch        *b_TGC_hierarchy_dPhi_tracklet;   //!
   TBranch        *b_TGC_hierarchy_isChamberBoundary;   //!
   TBranch        *b_RPC_prd_x;   //!
   TBranch        *b_RPC_prd_y;   //!
   TBranch        *b_RPC_prd_z;   //!
   TBranch        *b_RPC_prd_time;   //!
   TBranch        *b_RPC_prd_triggerInfo;   //!
   TBranch        *b_RPC_prd_ambiguityFlag;   //!
   TBranch        *b_RPC_prd_measuresPhi;   //!
   TBranch        *b_RPC_prd_inRibs;   //!
   TBranch        *b_RPC_prd_station;   //!
   TBranch        *b_RPC_prd_stationEta;   //!
   TBranch        *b_RPC_prd_stationPhi;   //!
   TBranch        *b_RPC_prd_doubletR;   //!
   TBranch        *b_RPC_prd_doubletZ;   //!
   TBranch        *b_RPC_prd_stripWidth;   //!
   TBranch        *b_RPC_prd_stripLength;   //!
   TBranch        *b_RPC_prd_stripPitch;   //!
   TBranch        *b_MDT_prd_x;   //!
   TBranch        *b_MDT_prd_y;   //!
   TBranch        *b_MDT_prd_z;   //!
   TBranch        *b_MDT_prd_adc;   //!
   TBranch        *b_MDT_prd_tdc;   //!
   TBranch        *b_MDT_prd_status;   //!
   TBranch        *b_MDT_prd_drift_radius;   //!
   TBranch        *b_MDT_prd_drift_radius_error;   //!
   TBranch        *b_ext_staco_ubias_type;   //!
   TBranch        *b_ext_staco_ubias_index;   //!
   TBranch        *b_ext_staco_ubias_size;   //!
   TBranch        *b_ext_staco_ubias_targetVec;   //!
   TBranch        *b_ext_staco_ubias_targetDistanceVec;   //!
   TBranch        *b_ext_staco_ubias_targetEtaVec;   //!
   TBranch        *b_ext_staco_ubias_targetPhiVec;   //!
   TBranch        *b_ext_staco_ubias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_staco_ubias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_staco_ubias_targetPxVec;   //!
   TBranch        *b_ext_staco_ubias_targetPyVec;   //!
   TBranch        *b_ext_staco_ubias_targetPzVec;   //!
   TBranch        *b_ext_staco_bias_type;   //!
   TBranch        *b_ext_staco_bias_index;   //!
   TBranch        *b_ext_staco_bias_size;   //!
   TBranch        *b_ext_staco_bias_targetVec;   //!
   TBranch        *b_ext_staco_bias_targetDistanceVec;   //!
   TBranch        *b_ext_staco_bias_targetEtaVec;   //!
   TBranch        *b_ext_staco_bias_targetPhiVec;   //!
   TBranch        *b_ext_staco_bias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_staco_bias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_staco_bias_targetPxVec;   //!
   TBranch        *b_ext_staco_bias_targetPyVec;   //!
   TBranch        *b_ext_staco_bias_targetPzVec;   //!
   TBranch        *b_ext_muid_ubias_type;   //!
   TBranch        *b_ext_muid_ubias_index;   //!
   TBranch        *b_ext_muid_ubias_size;   //!
   TBranch        *b_ext_muid_ubias_targetVec;   //!
   TBranch        *b_ext_muid_ubias_targetDistanceVec;   //!
   TBranch        *b_ext_muid_ubias_targetEtaVec;   //!
   TBranch        *b_ext_muid_ubias_targetPhiVec;   //!
   TBranch        *b_ext_muid_ubias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_muid_ubias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_muid_ubias_targetPxVec;   //!
   TBranch        *b_ext_muid_ubias_targetPyVec;   //!
   TBranch        *b_ext_muid_ubias_targetPzVec;   //!
   TBranch        *b_ext_muid_bias_type;   //!
   TBranch        *b_ext_muid_bias_index;   //!
   TBranch        *b_ext_muid_bias_size;   //!
   TBranch        *b_ext_muid_bias_targetVec;   //!
   TBranch        *b_ext_muid_bias_targetDistanceVec;   //!
   TBranch        *b_ext_muid_bias_targetEtaVec;   //!
   TBranch        *b_ext_muid_bias_targetPhiVec;   //!
   TBranch        *b_ext_muid_bias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_muid_bias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_muid_bias_targetPxVec;   //!
   TBranch        *b_ext_muid_bias_targetPyVec;   //!
   TBranch        *b_ext_muid_bias_targetPzVec;   //!
   TBranch        *b_ext_calo_ubias_type;   //!
   TBranch        *b_ext_calo_ubias_index;   //!
   TBranch        *b_ext_calo_ubias_size;   //!
   TBranch        *b_ext_calo_ubias_targetVec;   //!
   TBranch        *b_ext_calo_ubias_targetDistanceVec;   //!
   TBranch        *b_ext_calo_ubias_targetEtaVec;   //!
   TBranch        *b_ext_calo_ubias_targetPhiVec;   //!
   TBranch        *b_ext_calo_ubias_targetDeltaEtaVec;   //!
   TBranch        *b_ext_calo_ubias_targetDeltaPhiVec;   //!
   TBranch        *b_ext_calo_ubias_targetPxVec;   //!
   TBranch        *b_ext_calo_ubias_targetPyVec;   //!
   TBranch        *b_ext_calo_ubias_targetPzVec;   //!
   TBranch        *b_trigger_info_chain;   //!
   TBranch        *b_trigger_info_isPassed;   //!
   TBranch        *b_trigger_info_nTracks;   //!
   TBranch        *b_trigger_info_typeVec;   //!
   TBranch        *b_trigger_info_ptVec;   //!
   TBranch        *b_trigger_info_etaVec;   //!
   TBranch        *b_trigger_info_phiVec;   //!
   TBranch        *b_trigger_info_chargeVec;   //!
   TBranch        *b_trigger_info_l1RoiWordVec;   //!
   TBranch        *b_tandp_staco_tag_index;   //!
   TBranch        *b_tandp_staco_probe_index;   //!
   TBranch        *b_tandp_staco_vertex_ndof;   //!
   TBranch        *b_tandp_staco_vertex_chi2;   //!
   TBranch        *b_tandp_staco_vertex_posX;   //!
   TBranch        *b_tandp_staco_vertex_posY;   //!
   TBranch        *b_tandp_staco_vertex_posZ;   //!
   TBranch        *b_tandp_staco_vertex_probability;   //!
   TBranch        *b_tandp_staco_v0_mass;   //!
   TBranch        *b_tandp_staco_v0_mass_error;   //!
   TBranch        *b_tandp_staco_v0_mass_probability;   //!
   TBranch        *b_tandp_staco_v0_px;   //!
   TBranch        *b_tandp_staco_v0_py;   //!
   TBranch        *b_tandp_staco_v0_pz;   //!
   TBranch        *b_tandp_staco_v0_pt;   //!
   TBranch        *b_tandp_staco_v0_pt_error;   //!
   TBranch        *b_tandp_staco_num_verticies;   //!
   TBranch        *b_tandp_staco_vertex_index;   //!
   TBranch        *b_tandp_staco_lxyVec;   //!
   TBranch        *b_tandp_staco_lxy_errorVec;   //!
   TBranch        *b_tandp_staco_tauVec;   //!
   TBranch        *b_tandp_staco_tau_errorVec;   //!
   TBranch        *b_tandp_muid_tag_index;   //!
   TBranch        *b_tandp_muid_probe_index;   //!
   TBranch        *b_tandp_muid_vertex_ndof;   //!
   TBranch        *b_tandp_muid_vertex_chi2;   //!
   TBranch        *b_tandp_muid_vertex_posX;   //!
   TBranch        *b_tandp_muid_vertex_posY;   //!
   TBranch        *b_tandp_muid_vertex_posZ;   //!
   TBranch        *b_tandp_muid_vertex_probability;   //!
   TBranch        *b_tandp_muid_v0_mass;   //!
   TBranch        *b_tandp_muid_v0_mass_error;   //!
   TBranch        *b_tandp_muid_v0_mass_probability;   //!
   TBranch        *b_tandp_muid_v0_px;   //!
   TBranch        *b_tandp_muid_v0_py;   //!
   TBranch        *b_tandp_muid_v0_pz;   //!
   TBranch        *b_tandp_muid_v0_pt;   //!
   TBranch        *b_tandp_muid_v0_pt_error;   //!
   TBranch        *b_tandp_muid_num_verticies;   //!
   TBranch        *b_tandp_muid_vertex_index;   //!
   TBranch        *b_tandp_muid_lxyVec;   //!
   TBranch        *b_tandp_muid_lxy_errorVec;   //!
   TBranch        *b_tandp_muid_tauVec;   //!
   TBranch        *b_tandp_muid_tau_errorVec;   //!

   physics(TTree *tree=0);
   virtual ~physics();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef physics_cxx
physics::physics(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/chai/sgt1/atlas/L1TGCD3PD/singleMuonMC/user.yhorii.SGLMU.NTUP_L1TGC_mc12_single_mu_1000gev_NominalMDTPosition.l_tgc0_EXT0.7059222/user.yhorii.4224653.EXT0._000001.L1TGC.pool.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/chai/sgt1/atlas/L1TGCD3PD/singleMuonMC/user.yhorii.SGLMU.NTUP_L1TGC_mc12_single_mu_1000gev_NominalMDTPosition.l_tgc0_EXT0.7059222/user.yhorii.4224653.EXT0._000001.L1TGC.pool.root");
      }
      f->GetObject("physics",tree);

   }
   Init(tree);
}

physics::~physics()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t physics::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t physics::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void physics::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   vxp_x = 0;
   vxp_y = 0;
   vxp_z = 0;
   vxp_cov_x = 0;
   vxp_cov_y = 0;
   vxp_cov_z = 0;
   vxp_cov_xy = 0;
   vxp_cov_xz = 0;
   vxp_cov_yz = 0;
   vxp_type = 0;
   vxp_chi2 = 0;
   vxp_ndof = 0;
   vxp_px = 0;
   vxp_py = 0;
   vxp_pz = 0;
   vxp_E = 0;
   vxp_m = 0;
   vxp_nTracks = 0;
   vxp_sumPt = 0;
   vxp_trk_weight = 0;
   vxp_trk_n = 0;
   vxp_trk_index = 0;
   trk_pt = 0;
   trk_eta = 0;
   trk_d0_wrtPV = 0;
   trk_z0_wrtPV = 0;
   trk_phi_wrtPV = 0;
   trk_theta_wrtPV = 0;
   trk_qoverp_wrtPV = 0;
   trk_cov_d0_wrtPV = 0;
   trk_cov_z0_wrtPV = 0;
   trk_cov_phi_wrtPV = 0;
   trk_cov_theta_wrtPV = 0;
   trk_cov_qoverp_wrtPV = 0;
   trk_d0_wrtBS = 0;
   trk_z0_wrtBS = 0;
   trk_phi_wrtBS = 0;
   trk_cov_d0_wrtBS = 0;
   trk_cov_z0_wrtBS = 0;
   trk_cov_phi_wrtBS = 0;
   trk_cov_theta_wrtBS = 0;
   trk_cov_qoverp_wrtBS = 0;
   trk_cov_d0_z0_wrtBS = 0;
   trk_cov_d0_phi_wrtBS = 0;
   trk_cov_d0_theta_wrtBS = 0;
   trk_cov_d0_qoverp_wrtBS = 0;
   trk_cov_z0_phi_wrtBS = 0;
   trk_cov_z0_theta_wrtBS = 0;
   trk_cov_z0_qoverp_wrtBS = 0;
   trk_cov_phi_theta_wrtBS = 0;
   trk_cov_phi_qoverp_wrtBS = 0;
   trk_cov_theta_qoverp_wrtBS = 0;
   trk_d0_wrtBL = 0;
   trk_z0_wrtBL = 0;
   trk_phi_wrtBL = 0;
   trk_d0_err_wrtBL = 0;
   trk_z0_err_wrtBL = 0;
   trk_phi_err_wrtBL = 0;
   trk_theta_err_wrtBL = 0;
   trk_qoverp_err_wrtBL = 0;
   trk_d0_z0_err_wrtBL = 0;
   trk_d0_phi_err_wrtBL = 0;
   trk_d0_theta_err_wrtBL = 0;
   trk_d0_qoverp_err_wrtBL = 0;
   trk_z0_phi_err_wrtBL = 0;
   trk_z0_theta_err_wrtBL = 0;
   trk_z0_qoverp_err_wrtBL = 0;
   trk_phi_theta_err_wrtBL = 0;
   trk_phi_qoverp_err_wrtBL = 0;
   trk_theta_qoverp_err_wrtBL = 0;
   trk_chi2 = 0;
   trk_ndof = 0;
   trk_nBLHits = 0;
   trk_nPixHits = 0;
   trk_nSCTHits = 0;
   trk_nTRTHits = 0;
   trk_nTRTHighTHits = 0;
   trk_nTRTXenonHits = 0;
   trk_nPixHoles = 0;
   trk_nSCTHoles = 0;
   trk_nTRTHoles = 0;
   trk_nPixelDeadSensors = 0;
   trk_nSCTDeadSensors = 0;
   trk_nBLSharedHits = 0;
   trk_nPixSharedHits = 0;
   trk_nSCTSharedHits = 0;
   trk_nBLayerSplitHits = 0;
   trk_nPixSplitHits = 0;
   trk_expectBLayerHit = 0;
   trk_nMDTHits = 0;
   trk_nCSCEtaHits = 0;
   trk_nCSCPhiHits = 0;
   trk_nRPCEtaHits = 0;
   trk_nRPCPhiHits = 0;
   trk_nTGCEtaHits = 0;
   trk_nTGCPhiHits = 0;
   trk_nHits = 0;
   trk_nHoles = 0;
   trk_hitPattern = 0;
   trk_TRTHighTHitsRatio = 0;
   trk_TRTHighTOutliersRatio = 0;
   trk_fitter = 0;
   trk_patternReco1 = 0;
   trk_patternReco2 = 0;
   trk_trackProperties = 0;
   trk_particleHypothesis = 0;
   trk_blayerPrediction_x = 0;
   trk_blayerPrediction_y = 0;
   trk_blayerPrediction_z = 0;
   trk_blayerPrediction_locX = 0;
   trk_blayerPrediction_locY = 0;
   trk_blayerPrediction_err_locX = 0;
   trk_blayerPrediction_err_locY = 0;
   trk_blayerPrediction_etaDistToEdge = 0;
   trk_blayerPrediction_phiDistToEdge = 0;
   trk_blayerPrediction_detElementId = 0;
   trk_blayerPrediction_row = 0;
   trk_blayerPrediction_col = 0;
   trk_blayerPrediction_type = 0;
   trk_mc_probability = 0;
   trk_mc_barcode = 0;
   mu_muid_E = 0;
   mu_muid_pt = 0;
   mu_muid_m = 0;
   mu_muid_eta = 0;
   mu_muid_phi = 0;
   mu_muid_px = 0;
   mu_muid_py = 0;
   mu_muid_pz = 0;
   mu_muid_charge = 0;
   mu_muid_allauthor = 0;
   mu_muid_author = 0;
   mu_muid_beta = 0;
   mu_muid_isMuonLikelihood = 0;
   mu_muid_matchchi2 = 0;
   mu_muid_matchndof = 0;
   mu_muid_etcone20 = 0;
   mu_muid_etcone30 = 0;
   mu_muid_etcone40 = 0;
   mu_muid_nucone20 = 0;
   mu_muid_nucone30 = 0;
   mu_muid_nucone40 = 0;
   mu_muid_ptcone20 = 0;
   mu_muid_ptcone30 = 0;
   mu_muid_ptcone40 = 0;
   mu_muid_etconeNoEm10 = 0;
   mu_muid_etconeNoEm20 = 0;
   mu_muid_etconeNoEm30 = 0;
   mu_muid_etconeNoEm40 = 0;
   mu_muid_scatteringCurvatureSignificance = 0;
   mu_muid_scatteringNeighbourSignificance = 0;
   mu_muid_momentumBalanceSignificance = 0;
   mu_muid_energyLossPar = 0;
   mu_muid_energyLossErr = 0;
   mu_muid_etCore = 0;
   mu_muid_energyLossType = 0;
   mu_muid_caloMuonIdTag = 0;
   mu_muid_caloLRLikelihood = 0;
   mu_muid_bestMatch = 0;
   mu_muid_isStandAloneMuon = 0;
   mu_muid_isCombinedMuon = 0;
   mu_muid_isLowPtReconstructedMuon = 0;
   mu_muid_isSegmentTaggedMuon = 0;
   mu_muid_isCaloMuonId = 0;
   mu_muid_alsoFoundByLowPt = 0;
   mu_muid_alsoFoundByCaloMuonId = 0;
   mu_muid_isSiliconAssociatedForwardMuon = 0;
   mu_muid_loose = 0;
   mu_muid_medium = 0;
   mu_muid_tight = 0;
   mu_muid_d0_exPV = 0;
   mu_muid_z0_exPV = 0;
   mu_muid_phi_exPV = 0;
   mu_muid_theta_exPV = 0;
   mu_muid_qoverp_exPV = 0;
   mu_muid_cb_d0_exPV = 0;
   mu_muid_cb_z0_exPV = 0;
   mu_muid_cb_phi_exPV = 0;
   mu_muid_cb_theta_exPV = 0;
   mu_muid_cb_qoverp_exPV = 0;
   mu_muid_id_d0_exPV = 0;
   mu_muid_id_z0_exPV = 0;
   mu_muid_id_phi_exPV = 0;
   mu_muid_id_theta_exPV = 0;
   mu_muid_id_qoverp_exPV = 0;
   mu_muid_me_d0_exPV = 0;
   mu_muid_me_z0_exPV = 0;
   mu_muid_me_phi_exPV = 0;
   mu_muid_me_theta_exPV = 0;
   mu_muid_me_qoverp_exPV = 0;
   mu_muid_ie_d0_exPV = 0;
   mu_muid_ie_z0_exPV = 0;
   mu_muid_ie_phi_exPV = 0;
   mu_muid_ie_theta_exPV = 0;
   mu_muid_ie_qoverp_exPV = 0;
   mu_muid_SpaceTime_detID = 0;
   mu_muid_SpaceTime_t = 0;
   mu_muid_SpaceTime_tError = 0;
   mu_muid_SpaceTime_weight = 0;
   mu_muid_SpaceTime_x = 0;
   mu_muid_SpaceTime_y = 0;
   mu_muid_SpaceTime_z = 0;
   mu_muid_cov_d0_exPV = 0;
   mu_muid_cov_z0_exPV = 0;
   mu_muid_cov_phi_exPV = 0;
   mu_muid_cov_theta_exPV = 0;
   mu_muid_cov_qoverp_exPV = 0;
   mu_muid_cov_d0_z0_exPV = 0;
   mu_muid_cov_d0_phi_exPV = 0;
   mu_muid_cov_d0_theta_exPV = 0;
   mu_muid_cov_d0_qoverp_exPV = 0;
   mu_muid_cov_z0_phi_exPV = 0;
   mu_muid_cov_z0_theta_exPV = 0;
   mu_muid_cov_z0_qoverp_exPV = 0;
   mu_muid_cov_phi_theta_exPV = 0;
   mu_muid_cov_phi_qoverp_exPV = 0;
   mu_muid_cov_theta_qoverp_exPV = 0;
   mu_muid_id_cov_d0_exPV = 0;
   mu_muid_id_cov_z0_exPV = 0;
   mu_muid_id_cov_phi_exPV = 0;
   mu_muid_id_cov_theta_exPV = 0;
   mu_muid_id_cov_qoverp_exPV = 0;
   mu_muid_id_cov_d0_z0_exPV = 0;
   mu_muid_id_cov_d0_phi_exPV = 0;
   mu_muid_id_cov_d0_theta_exPV = 0;
   mu_muid_id_cov_d0_qoverp_exPV = 0;
   mu_muid_id_cov_z0_phi_exPV = 0;
   mu_muid_id_cov_z0_theta_exPV = 0;
   mu_muid_id_cov_z0_qoverp_exPV = 0;
   mu_muid_id_cov_phi_theta_exPV = 0;
   mu_muid_id_cov_phi_qoverp_exPV = 0;
   mu_muid_id_cov_theta_qoverp_exPV = 0;
   mu_muid_me_cov_d0_exPV = 0;
   mu_muid_me_cov_z0_exPV = 0;
   mu_muid_me_cov_phi_exPV = 0;
   mu_muid_me_cov_theta_exPV = 0;
   mu_muid_me_cov_qoverp_exPV = 0;
   mu_muid_me_cov_d0_z0_exPV = 0;
   mu_muid_me_cov_d0_phi_exPV = 0;
   mu_muid_me_cov_d0_theta_exPV = 0;
   mu_muid_me_cov_d0_qoverp_exPV = 0;
   mu_muid_me_cov_z0_phi_exPV = 0;
   mu_muid_me_cov_z0_theta_exPV = 0;
   mu_muid_me_cov_z0_qoverp_exPV = 0;
   mu_muid_me_cov_phi_theta_exPV = 0;
   mu_muid_me_cov_phi_qoverp_exPV = 0;
   mu_muid_me_cov_theta_qoverp_exPV = 0;
   mu_muid_ms_d0 = 0;
   mu_muid_ms_z0 = 0;
   mu_muid_ms_phi = 0;
   mu_muid_ms_theta = 0;
   mu_muid_ms_qoverp = 0;
   mu_muid_id_d0 = 0;
   mu_muid_id_z0 = 0;
   mu_muid_id_phi = 0;
   mu_muid_id_theta = 0;
   mu_muid_id_qoverp = 0;
   mu_muid_me_d0 = 0;
   mu_muid_me_z0 = 0;
   mu_muid_me_phi = 0;
   mu_muid_me_theta = 0;
   mu_muid_me_qoverp = 0;
   mu_muid_ie_d0 = 0;
   mu_muid_ie_z0 = 0;
   mu_muid_ie_phi = 0;
   mu_muid_ie_theta = 0;
   mu_muid_ie_qoverp = 0;
   mu_muid_nOutliersOnTrack = 0;
   mu_muid_nBLHits = 0;
   mu_muid_nPixHits = 0;
   mu_muid_nSCTHits = 0;
   mu_muid_nTRTHits = 0;
   mu_muid_nTRTHighTHits = 0;
   mu_muid_nBLSharedHits = 0;
   mu_muid_nPixSharedHits = 0;
   mu_muid_nPixHoles = 0;
   mu_muid_nSCTSharedHits = 0;
   mu_muid_nSCTHoles = 0;
   mu_muid_nTRTOutliers = 0;
   mu_muid_nTRTHighTOutliers = 0;
   mu_muid_nGangedPixels = 0;
   mu_muid_nPixelDeadSensors = 0;
   mu_muid_nSCTDeadSensors = 0;
   mu_muid_nTRTDeadStraws = 0;
   mu_muid_expectBLayerHit = 0;
   mu_muid_nMDTHits = 0;
   mu_muid_nMDTHoles = 0;
   mu_muid_nCSCEtaHits = 0;
   mu_muid_nCSCEtaHoles = 0;
   mu_muid_nCSCUnspoiledEtaHits = 0;
   mu_muid_nCSCPhiHits = 0;
   mu_muid_nCSCPhiHoles = 0;
   mu_muid_nRPCEtaHits = 0;
   mu_muid_nRPCEtaHoles = 0;
   mu_muid_nRPCPhiHits = 0;
   mu_muid_nRPCPhiHoles = 0;
   mu_muid_nTGCEtaHits = 0;
   mu_muid_nTGCEtaHoles = 0;
   mu_muid_nTGCPhiHits = 0;
   mu_muid_nTGCPhiHoles = 0;
   mu_muid_nprecisionLayers = 0;
   mu_muid_nprecisionHoleLayers = 0;
   mu_muid_nphiLayers = 0;
   mu_muid_ntrigEtaLayers = 0;
   mu_muid_nphiHoleLayers = 0;
   mu_muid_ntrigEtaHoleLayers = 0;
   mu_muid_nMDTBIHits = 0;
   mu_muid_nMDTBMHits = 0;
   mu_muid_nMDTBOHits = 0;
   mu_muid_nMDTBEEHits = 0;
   mu_muid_nMDTBIS78Hits = 0;
   mu_muid_nMDTEIHits = 0;
   mu_muid_nMDTEMHits = 0;
   mu_muid_nMDTEOHits = 0;
   mu_muid_nMDTEEHits = 0;
   mu_muid_nRPCLayer1EtaHits = 0;
   mu_muid_nRPCLayer2EtaHits = 0;
   mu_muid_nRPCLayer3EtaHits = 0;
   mu_muid_nRPCLayer1PhiHits = 0;
   mu_muid_nRPCLayer2PhiHits = 0;
   mu_muid_nRPCLayer3PhiHits = 0;
   mu_muid_nTGCLayer1EtaHits = 0;
   mu_muid_nTGCLayer2EtaHits = 0;
   mu_muid_nTGCLayer3EtaHits = 0;
   mu_muid_nTGCLayer4EtaHits = 0;
   mu_muid_nTGCLayer1PhiHits = 0;
   mu_muid_nTGCLayer2PhiHits = 0;
   mu_muid_nTGCLayer3PhiHits = 0;
   mu_muid_nTGCLayer4PhiHits = 0;
   mu_muid_barrelSectors = 0;
   mu_muid_endcapSectors = 0;
   mu_muid_spec_surf_px = 0;
   mu_muid_spec_surf_py = 0;
   mu_muid_spec_surf_pz = 0;
   mu_muid_spec_surf_x = 0;
   mu_muid_spec_surf_y = 0;
   mu_muid_spec_surf_z = 0;
   mu_muid_trackd0 = 0;
   mu_muid_trackz0 = 0;
   mu_muid_trackphi = 0;
   mu_muid_tracktheta = 0;
   mu_muid_trackqoverp = 0;
   mu_muid_trackcov_d0 = 0;
   mu_muid_trackcov_z0 = 0;
   mu_muid_trackcov_phi = 0;
   mu_muid_trackcov_theta = 0;
   mu_muid_trackcov_qoverp = 0;
   mu_muid_trackcov_d0_z0 = 0;
   mu_muid_trackcov_d0_phi = 0;
   mu_muid_trackcov_d0_theta = 0;
   mu_muid_trackcov_d0_qoverp = 0;
   mu_muid_trackcov_z0_phi = 0;
   mu_muid_trackcov_z0_theta = 0;
   mu_muid_trackcov_z0_qoverp = 0;
   mu_muid_trackcov_phi_theta = 0;
   mu_muid_trackcov_phi_qoverp = 0;
   mu_muid_trackcov_theta_qoverp = 0;
   mu_muid_trackfitchi2 = 0;
   mu_muid_trackfitndof = 0;
   mu_muid_hastrack = 0;
   mu_muid_trackd0beam = 0;
   mu_muid_trackz0beam = 0;
   mu_muid_tracksigd0beam = 0;
   mu_muid_tracksigz0beam = 0;
   mu_muid_trackd0pv = 0;
   mu_muid_trackz0pv = 0;
   mu_muid_tracksigd0pv = 0;
   mu_muid_tracksigz0pv = 0;
   mu_muid_trackIPEstimate_d0_biasedpvunbiased = 0;
   mu_muid_trackIPEstimate_z0_biasedpvunbiased = 0;
   mu_muid_trackIPEstimate_sigd0_biasedpvunbiased = 0;
   mu_muid_trackIPEstimate_sigz0_biasedpvunbiased = 0;
   mu_muid_trackIPEstimate_d0_unbiasedpvunbiased = 0;
   mu_muid_trackIPEstimate_z0_unbiasedpvunbiased = 0;
   mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased = 0;
   mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased = 0;
   mu_muid_trackd0pvunbiased = 0;
   mu_muid_trackz0pvunbiased = 0;
   mu_muid_tracksigd0pvunbiased = 0;
   mu_muid_tracksigz0pvunbiased = 0;
   mu_muid_type = 0;
   mu_muid_origin = 0;
   mu_muid_truth_dr = 0;
   mu_muid_truth_E = 0;
   mu_muid_truth_pt = 0;
   mu_muid_truth_eta = 0;
   mu_muid_truth_phi = 0;
   mu_muid_truth_type = 0;
   mu_muid_truth_status = 0;
   mu_muid_truth_barcode = 0;
   mu_muid_truth_mothertype = 0;
   mu_muid_truth_motherbarcode = 0;
   mu_muid_truth_matched = 0;
   mu_muid_EFCB_dr = 0;
   mu_muid_EFCB_n = 0;
   mu_muid_EFCB_MuonType = 0;
   mu_muid_EFCB_pt = 0;
   mu_muid_EFCB_eta = 0;
   mu_muid_EFCB_phi = 0;
   mu_muid_EFCB_hasCB = 0;
   mu_muid_EFCB_matched = 0;
   mu_muid_EFMG_dr = 0;
   mu_muid_EFMG_n = 0;
   mu_muid_EFMG_MuonType = 0;
   mu_muid_EFMG_pt = 0;
   mu_muid_EFMG_eta = 0;
   mu_muid_EFMG_phi = 0;
   mu_muid_EFMG_hasMG = 0;
   mu_muid_EFMG_matched = 0;
   mu_muid_EFME_dr = 0;
   mu_muid_EFME_n = 0;
   mu_muid_EFME_MuonType = 0;
   mu_muid_EFME_pt = 0;
   mu_muid_EFME_eta = 0;
   mu_muid_EFME_phi = 0;
   mu_muid_EFME_hasME = 0;
   mu_muid_EFME_matched = 0;
   mu_muid_L2CB_dr = 0;
   mu_muid_L2CB_pt = 0;
   mu_muid_L2CB_eta = 0;
   mu_muid_L2CB_phi = 0;
   mu_muid_L2CB_id_pt = 0;
   mu_muid_L2CB_ms_pt = 0;
   mu_muid_L2CB_nPixHits = 0;
   mu_muid_L2CB_nSCTHits = 0;
   mu_muid_L2CB_nTRTHits = 0;
   mu_muid_L2CB_nTRTHighTHits = 0;
   mu_muid_L2CB_matched = 0;
   mu_muid_L1_dr = 0;
   mu_muid_L1_pt = 0;
   mu_muid_L1_eta = 0;
   mu_muid_L1_phi = 0;
   mu_muid_L1_thrNumber = 0;
   mu_muid_L1_RoINumber = 0;
   mu_muid_L1_sectorAddress = 0;
   mu_muid_L1_firstCandidate = 0;
   mu_muid_L1_moreCandInRoI = 0;
   mu_muid_L1_moreCandInSector = 0;
   mu_muid_L1_source = 0;
   mu_muid_L1_hemisphere = 0;
   mu_muid_L1_charge = 0;
   mu_muid_L1_vetoed = 0;
   mu_muid_L1_matched = 0;
   mu_staco_E = 0;
   mu_staco_pt = 0;
   mu_staco_m = 0;
   mu_staco_eta = 0;
   mu_staco_phi = 0;
   mu_staco_px = 0;
   mu_staco_py = 0;
   mu_staco_pz = 0;
   mu_staco_charge = 0;
   mu_staco_allauthor = 0;
   mu_staco_author = 0;
   mu_staco_beta = 0;
   mu_staco_isMuonLikelihood = 0;
   mu_staco_matchchi2 = 0;
   mu_staco_matchndof = 0;
   mu_staco_etcone20 = 0;
   mu_staco_etcone30 = 0;
   mu_staco_etcone40 = 0;
   mu_staco_nucone20 = 0;
   mu_staco_nucone30 = 0;
   mu_staco_nucone40 = 0;
   mu_staco_ptcone20 = 0;
   mu_staco_ptcone30 = 0;
   mu_staco_ptcone40 = 0;
   mu_staco_etconeNoEm10 = 0;
   mu_staco_etconeNoEm20 = 0;
   mu_staco_etconeNoEm30 = 0;
   mu_staco_etconeNoEm40 = 0;
   mu_staco_scatteringCurvatureSignificance = 0;
   mu_staco_scatteringNeighbourSignificance = 0;
   mu_staco_momentumBalanceSignificance = 0;
   mu_staco_energyLossPar = 0;
   mu_staco_energyLossErr = 0;
   mu_staco_etCore = 0;
   mu_staco_energyLossType = 0;
   mu_staco_caloMuonIdTag = 0;
   mu_staco_caloLRLikelihood = 0;
   mu_staco_bestMatch = 0;
   mu_staco_isStandAloneMuon = 0;
   mu_staco_isCombinedMuon = 0;
   mu_staco_isLowPtReconstructedMuon = 0;
   mu_staco_isSegmentTaggedMuon = 0;
   mu_staco_isCaloMuonId = 0;
   mu_staco_alsoFoundByLowPt = 0;
   mu_staco_alsoFoundByCaloMuonId = 0;
   mu_staco_isSiliconAssociatedForwardMuon = 0;
   mu_staco_loose = 0;
   mu_staco_medium = 0;
   mu_staco_tight = 0;
   mu_staco_d0_exPV = 0;
   mu_staco_z0_exPV = 0;
   mu_staco_phi_exPV = 0;
   mu_staco_theta_exPV = 0;
   mu_staco_qoverp_exPV = 0;
   mu_staco_cb_d0_exPV = 0;
   mu_staco_cb_z0_exPV = 0;
   mu_staco_cb_phi_exPV = 0;
   mu_staco_cb_theta_exPV = 0;
   mu_staco_cb_qoverp_exPV = 0;
   mu_staco_id_d0_exPV = 0;
   mu_staco_id_z0_exPV = 0;
   mu_staco_id_phi_exPV = 0;
   mu_staco_id_theta_exPV = 0;
   mu_staco_id_qoverp_exPV = 0;
   mu_staco_me_d0_exPV = 0;
   mu_staco_me_z0_exPV = 0;
   mu_staco_me_phi_exPV = 0;
   mu_staco_me_theta_exPV = 0;
   mu_staco_me_qoverp_exPV = 0;
   mu_staco_ie_d0_exPV = 0;
   mu_staco_ie_z0_exPV = 0;
   mu_staco_ie_phi_exPV = 0;
   mu_staco_ie_theta_exPV = 0;
   mu_staco_ie_qoverp_exPV = 0;
   mu_staco_SpaceTime_detID = 0;
   mu_staco_SpaceTime_t = 0;
   mu_staco_SpaceTime_tError = 0;
   mu_staco_SpaceTime_weight = 0;
   mu_staco_SpaceTime_x = 0;
   mu_staco_SpaceTime_y = 0;
   mu_staco_SpaceTime_z = 0;
   mu_staco_cov_d0_exPV = 0;
   mu_staco_cov_z0_exPV = 0;
   mu_staco_cov_phi_exPV = 0;
   mu_staco_cov_theta_exPV = 0;
   mu_staco_cov_qoverp_exPV = 0;
   mu_staco_cov_d0_z0_exPV = 0;
   mu_staco_cov_d0_phi_exPV = 0;
   mu_staco_cov_d0_theta_exPV = 0;
   mu_staco_cov_d0_qoverp_exPV = 0;
   mu_staco_cov_z0_phi_exPV = 0;
   mu_staco_cov_z0_theta_exPV = 0;
   mu_staco_cov_z0_qoverp_exPV = 0;
   mu_staco_cov_phi_theta_exPV = 0;
   mu_staco_cov_phi_qoverp_exPV = 0;
   mu_staco_cov_theta_qoverp_exPV = 0;
   mu_staco_id_cov_d0_exPV = 0;
   mu_staco_id_cov_z0_exPV = 0;
   mu_staco_id_cov_phi_exPV = 0;
   mu_staco_id_cov_theta_exPV = 0;
   mu_staco_id_cov_qoverp_exPV = 0;
   mu_staco_id_cov_d0_z0_exPV = 0;
   mu_staco_id_cov_d0_phi_exPV = 0;
   mu_staco_id_cov_d0_theta_exPV = 0;
   mu_staco_id_cov_d0_qoverp_exPV = 0;
   mu_staco_id_cov_z0_phi_exPV = 0;
   mu_staco_id_cov_z0_theta_exPV = 0;
   mu_staco_id_cov_z0_qoverp_exPV = 0;
   mu_staco_id_cov_phi_theta_exPV = 0;
   mu_staco_id_cov_phi_qoverp_exPV = 0;
   mu_staco_id_cov_theta_qoverp_exPV = 0;
   mu_staco_me_cov_d0_exPV = 0;
   mu_staco_me_cov_z0_exPV = 0;
   mu_staco_me_cov_phi_exPV = 0;
   mu_staco_me_cov_theta_exPV = 0;
   mu_staco_me_cov_qoverp_exPV = 0;
   mu_staco_me_cov_d0_z0_exPV = 0;
   mu_staco_me_cov_d0_phi_exPV = 0;
   mu_staco_me_cov_d0_theta_exPV = 0;
   mu_staco_me_cov_d0_qoverp_exPV = 0;
   mu_staco_me_cov_z0_phi_exPV = 0;
   mu_staco_me_cov_z0_theta_exPV = 0;
   mu_staco_me_cov_z0_qoverp_exPV = 0;
   mu_staco_me_cov_phi_theta_exPV = 0;
   mu_staco_me_cov_phi_qoverp_exPV = 0;
   mu_staco_me_cov_theta_qoverp_exPV = 0;
   mu_staco_ms_d0 = 0;
   mu_staco_ms_z0 = 0;
   mu_staco_ms_phi = 0;
   mu_staco_ms_theta = 0;
   mu_staco_ms_qoverp = 0;
   mu_staco_id_d0 = 0;
   mu_staco_id_z0 = 0;
   mu_staco_id_phi = 0;
   mu_staco_id_theta = 0;
   mu_staco_id_qoverp = 0;
   mu_staco_me_d0 = 0;
   mu_staco_me_z0 = 0;
   mu_staco_me_phi = 0;
   mu_staco_me_theta = 0;
   mu_staco_me_qoverp = 0;
   mu_staco_ie_d0 = 0;
   mu_staco_ie_z0 = 0;
   mu_staco_ie_phi = 0;
   mu_staco_ie_theta = 0;
   mu_staco_ie_qoverp = 0;
   mu_staco_nOutliersOnTrack = 0;
   mu_staco_nBLHits = 0;
   mu_staco_nPixHits = 0;
   mu_staco_nSCTHits = 0;
   mu_staco_nTRTHits = 0;
   mu_staco_nTRTHighTHits = 0;
   mu_staco_nBLSharedHits = 0;
   mu_staco_nPixSharedHits = 0;
   mu_staco_nPixHoles = 0;
   mu_staco_nSCTSharedHits = 0;
   mu_staco_nSCTHoles = 0;
   mu_staco_nTRTOutliers = 0;
   mu_staco_nTRTHighTOutliers = 0;
   mu_staco_nGangedPixels = 0;
   mu_staco_nPixelDeadSensors = 0;
   mu_staco_nSCTDeadSensors = 0;
   mu_staco_nTRTDeadStraws = 0;
   mu_staco_expectBLayerHit = 0;
   mu_staco_nMDTHits = 0;
   mu_staco_nMDTHoles = 0;
   mu_staco_nCSCEtaHits = 0;
   mu_staco_nCSCEtaHoles = 0;
   mu_staco_nCSCUnspoiledEtaHits = 0;
   mu_staco_nCSCPhiHits = 0;
   mu_staco_nCSCPhiHoles = 0;
   mu_staco_nRPCEtaHits = 0;
   mu_staco_nRPCEtaHoles = 0;
   mu_staco_nRPCPhiHits = 0;
   mu_staco_nRPCPhiHoles = 0;
   mu_staco_nTGCEtaHits = 0;
   mu_staco_nTGCEtaHoles = 0;
   mu_staco_nTGCPhiHits = 0;
   mu_staco_nTGCPhiHoles = 0;
   mu_staco_nprecisionLayers = 0;
   mu_staco_nprecisionHoleLayers = 0;
   mu_staco_nphiLayers = 0;
   mu_staco_ntrigEtaLayers = 0;
   mu_staco_nphiHoleLayers = 0;
   mu_staco_ntrigEtaHoleLayers = 0;
   mu_staco_nMDTBIHits = 0;
   mu_staco_nMDTBMHits = 0;
   mu_staco_nMDTBOHits = 0;
   mu_staco_nMDTBEEHits = 0;
   mu_staco_nMDTBIS78Hits = 0;
   mu_staco_nMDTEIHits = 0;
   mu_staco_nMDTEMHits = 0;
   mu_staco_nMDTEOHits = 0;
   mu_staco_nMDTEEHits = 0;
   mu_staco_nRPCLayer1EtaHits = 0;
   mu_staco_nRPCLayer2EtaHits = 0;
   mu_staco_nRPCLayer3EtaHits = 0;
   mu_staco_nRPCLayer1PhiHits = 0;
   mu_staco_nRPCLayer2PhiHits = 0;
   mu_staco_nRPCLayer3PhiHits = 0;
   mu_staco_nTGCLayer1EtaHits = 0;
   mu_staco_nTGCLayer2EtaHits = 0;
   mu_staco_nTGCLayer3EtaHits = 0;
   mu_staco_nTGCLayer4EtaHits = 0;
   mu_staco_nTGCLayer1PhiHits = 0;
   mu_staco_nTGCLayer2PhiHits = 0;
   mu_staco_nTGCLayer3PhiHits = 0;
   mu_staco_nTGCLayer4PhiHits = 0;
   mu_staco_barrelSectors = 0;
   mu_staco_endcapSectors = 0;
   mu_staco_spec_surf_px = 0;
   mu_staco_spec_surf_py = 0;
   mu_staco_spec_surf_pz = 0;
   mu_staco_spec_surf_x = 0;
   mu_staco_spec_surf_y = 0;
   mu_staco_spec_surf_z = 0;
   mu_staco_trackd0 = 0;
   mu_staco_trackz0 = 0;
   mu_staco_trackphi = 0;
   mu_staco_tracktheta = 0;
   mu_staco_trackqoverp = 0;
   mu_staco_trackcov_d0 = 0;
   mu_staco_trackcov_z0 = 0;
   mu_staco_trackcov_phi = 0;
   mu_staco_trackcov_theta = 0;
   mu_staco_trackcov_qoverp = 0;
   mu_staco_trackcov_d0_z0 = 0;
   mu_staco_trackcov_d0_phi = 0;
   mu_staco_trackcov_d0_theta = 0;
   mu_staco_trackcov_d0_qoverp = 0;
   mu_staco_trackcov_z0_phi = 0;
   mu_staco_trackcov_z0_theta = 0;
   mu_staco_trackcov_z0_qoverp = 0;
   mu_staco_trackcov_phi_theta = 0;
   mu_staco_trackcov_phi_qoverp = 0;
   mu_staco_trackcov_theta_qoverp = 0;
   mu_staco_trackfitchi2 = 0;
   mu_staco_trackfitndof = 0;
   mu_staco_hastrack = 0;
   mu_staco_trackd0beam = 0;
   mu_staco_trackz0beam = 0;
   mu_staco_tracksigd0beam = 0;
   mu_staco_tracksigz0beam = 0;
   mu_staco_trackd0pv = 0;
   mu_staco_trackz0pv = 0;
   mu_staco_tracksigd0pv = 0;
   mu_staco_tracksigz0pv = 0;
   mu_staco_trackIPEstimate_d0_biasedpvunbiased = 0;
   mu_staco_trackIPEstimate_z0_biasedpvunbiased = 0;
   mu_staco_trackIPEstimate_sigd0_biasedpvunbiased = 0;
   mu_staco_trackIPEstimate_sigz0_biasedpvunbiased = 0;
   mu_staco_trackIPEstimate_d0_unbiasedpvunbiased = 0;
   mu_staco_trackIPEstimate_z0_unbiasedpvunbiased = 0;
   mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased = 0;
   mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased = 0;
   mu_staco_trackd0pvunbiased = 0;
   mu_staco_trackz0pvunbiased = 0;
   mu_staco_tracksigd0pvunbiased = 0;
   mu_staco_tracksigz0pvunbiased = 0;
   mu_staco_type = 0;
   mu_staco_origin = 0;
   mu_staco_truth_dr = 0;
   mu_staco_truth_E = 0;
   mu_staco_truth_pt = 0;
   mu_staco_truth_eta = 0;
   mu_staco_truth_phi = 0;
   mu_staco_truth_type = 0;
   mu_staco_truth_status = 0;
   mu_staco_truth_barcode = 0;
   mu_staco_truth_mothertype = 0;
   mu_staco_truth_motherbarcode = 0;
   mu_staco_truth_matched = 0;
   mu_staco_EFCB_dr = 0;
   mu_staco_EFCB_n = 0;
   mu_staco_EFCB_MuonType = 0;
   mu_staco_EFCB_pt = 0;
   mu_staco_EFCB_eta = 0;
   mu_staco_EFCB_phi = 0;
   mu_staco_EFCB_hasCB = 0;
   mu_staco_EFCB_matched = 0;
   mu_staco_EFMG_dr = 0;
   mu_staco_EFMG_n = 0;
   mu_staco_EFMG_MuonType = 0;
   mu_staco_EFMG_pt = 0;
   mu_staco_EFMG_eta = 0;
   mu_staco_EFMG_phi = 0;
   mu_staco_EFMG_hasMG = 0;
   mu_staco_EFMG_matched = 0;
   mu_staco_EFME_dr = 0;
   mu_staco_EFME_n = 0;
   mu_staco_EFME_MuonType = 0;
   mu_staco_EFME_pt = 0;
   mu_staco_EFME_eta = 0;
   mu_staco_EFME_phi = 0;
   mu_staco_EFME_hasME = 0;
   mu_staco_EFME_matched = 0;
   mu_staco_L2CB_dr = 0;
   mu_staco_L2CB_pt = 0;
   mu_staco_L2CB_eta = 0;
   mu_staco_L2CB_phi = 0;
   mu_staco_L2CB_id_pt = 0;
   mu_staco_L2CB_ms_pt = 0;
   mu_staco_L2CB_nPixHits = 0;
   mu_staco_L2CB_nSCTHits = 0;
   mu_staco_L2CB_nTRTHits = 0;
   mu_staco_L2CB_nTRTHighTHits = 0;
   mu_staco_L2CB_matched = 0;
   mu_staco_L1_dr = 0;
   mu_staco_L1_pt = 0;
   mu_staco_L1_eta = 0;
   mu_staco_L1_phi = 0;
   mu_staco_L1_thrNumber = 0;
   mu_staco_L1_RoINumber = 0;
   mu_staco_L1_sectorAddress = 0;
   mu_staco_L1_firstCandidate = 0;
   mu_staco_L1_moreCandInRoI = 0;
   mu_staco_L1_moreCandInSector = 0;
   mu_staco_L1_source = 0;
   mu_staco_L1_hemisphere = 0;
   mu_staco_L1_charge = 0;
   mu_staco_L1_vetoed = 0;
   mu_staco_L1_matched = 0;
   mu_calo_E = 0;
   mu_calo_pt = 0;
   mu_calo_m = 0;
   mu_calo_eta = 0;
   mu_calo_phi = 0;
   mu_calo_px = 0;
   mu_calo_py = 0;
   mu_calo_pz = 0;
   mu_calo_charge = 0;
   mu_calo_allauthor = 0;
   mu_calo_author = 0;
   mu_calo_beta = 0;
   mu_calo_isMuonLikelihood = 0;
   mu_calo_matchchi2 = 0;
   mu_calo_matchndof = 0;
   mu_calo_etcone20 = 0;
   mu_calo_etcone30 = 0;
   mu_calo_etcone40 = 0;
   mu_calo_nucone20 = 0;
   mu_calo_nucone30 = 0;
   mu_calo_nucone40 = 0;
   mu_calo_ptcone20 = 0;
   mu_calo_ptcone30 = 0;
   mu_calo_ptcone40 = 0;
   mu_calo_etconeNoEm10 = 0;
   mu_calo_etconeNoEm20 = 0;
   mu_calo_etconeNoEm30 = 0;
   mu_calo_etconeNoEm40 = 0;
   mu_calo_scatteringCurvatureSignificance = 0;
   mu_calo_scatteringNeighbourSignificance = 0;
   mu_calo_momentumBalanceSignificance = 0;
   mu_calo_energyLossPar = 0;
   mu_calo_energyLossErr = 0;
   mu_calo_etCore = 0;
   mu_calo_energyLossType = 0;
   mu_calo_caloMuonIdTag = 0;
   mu_calo_caloLRLikelihood = 0;
   mu_calo_bestMatch = 0;
   mu_calo_isStandAloneMuon = 0;
   mu_calo_isCombinedMuon = 0;
   mu_calo_isLowPtReconstructedMuon = 0;
   mu_calo_isSegmentTaggedMuon = 0;
   mu_calo_isCaloMuonId = 0;
   mu_calo_alsoFoundByLowPt = 0;
   mu_calo_alsoFoundByCaloMuonId = 0;
   mu_calo_isSiliconAssociatedForwardMuon = 0;
   mu_calo_loose = 0;
   mu_calo_medium = 0;
   mu_calo_tight = 0;
   mu_calo_d0_exPV = 0;
   mu_calo_z0_exPV = 0;
   mu_calo_phi_exPV = 0;
   mu_calo_theta_exPV = 0;
   mu_calo_qoverp_exPV = 0;
   mu_calo_cb_d0_exPV = 0;
   mu_calo_cb_z0_exPV = 0;
   mu_calo_cb_phi_exPV = 0;
   mu_calo_cb_theta_exPV = 0;
   mu_calo_cb_qoverp_exPV = 0;
   mu_calo_id_d0_exPV = 0;
   mu_calo_id_z0_exPV = 0;
   mu_calo_id_phi_exPV = 0;
   mu_calo_id_theta_exPV = 0;
   mu_calo_id_qoverp_exPV = 0;
   mu_calo_me_d0_exPV = 0;
   mu_calo_me_z0_exPV = 0;
   mu_calo_me_phi_exPV = 0;
   mu_calo_me_theta_exPV = 0;
   mu_calo_me_qoverp_exPV = 0;
   mu_calo_ie_d0_exPV = 0;
   mu_calo_ie_z0_exPV = 0;
   mu_calo_ie_phi_exPV = 0;
   mu_calo_ie_theta_exPV = 0;
   mu_calo_ie_qoverp_exPV = 0;
   mu_calo_SpaceTime_detID = 0;
   mu_calo_SpaceTime_t = 0;
   mu_calo_SpaceTime_tError = 0;
   mu_calo_SpaceTime_weight = 0;
   mu_calo_SpaceTime_x = 0;
   mu_calo_SpaceTime_y = 0;
   mu_calo_SpaceTime_z = 0;
   mu_calo_cov_d0_exPV = 0;
   mu_calo_cov_z0_exPV = 0;
   mu_calo_cov_phi_exPV = 0;
   mu_calo_cov_theta_exPV = 0;
   mu_calo_cov_qoverp_exPV = 0;
   mu_calo_cov_d0_z0_exPV = 0;
   mu_calo_cov_d0_phi_exPV = 0;
   mu_calo_cov_d0_theta_exPV = 0;
   mu_calo_cov_d0_qoverp_exPV = 0;
   mu_calo_cov_z0_phi_exPV = 0;
   mu_calo_cov_z0_theta_exPV = 0;
   mu_calo_cov_z0_qoverp_exPV = 0;
   mu_calo_cov_phi_theta_exPV = 0;
   mu_calo_cov_phi_qoverp_exPV = 0;
   mu_calo_cov_theta_qoverp_exPV = 0;
   mu_calo_id_cov_d0_exPV = 0;
   mu_calo_id_cov_z0_exPV = 0;
   mu_calo_id_cov_phi_exPV = 0;
   mu_calo_id_cov_theta_exPV = 0;
   mu_calo_id_cov_qoverp_exPV = 0;
   mu_calo_id_cov_d0_z0_exPV = 0;
   mu_calo_id_cov_d0_phi_exPV = 0;
   mu_calo_id_cov_d0_theta_exPV = 0;
   mu_calo_id_cov_d0_qoverp_exPV = 0;
   mu_calo_id_cov_z0_phi_exPV = 0;
   mu_calo_id_cov_z0_theta_exPV = 0;
   mu_calo_id_cov_z0_qoverp_exPV = 0;
   mu_calo_id_cov_phi_theta_exPV = 0;
   mu_calo_id_cov_phi_qoverp_exPV = 0;
   mu_calo_id_cov_theta_qoverp_exPV = 0;
   mu_calo_me_cov_d0_exPV = 0;
   mu_calo_me_cov_z0_exPV = 0;
   mu_calo_me_cov_phi_exPV = 0;
   mu_calo_me_cov_theta_exPV = 0;
   mu_calo_me_cov_qoverp_exPV = 0;
   mu_calo_me_cov_d0_z0_exPV = 0;
   mu_calo_me_cov_d0_phi_exPV = 0;
   mu_calo_me_cov_d0_theta_exPV = 0;
   mu_calo_me_cov_d0_qoverp_exPV = 0;
   mu_calo_me_cov_z0_phi_exPV = 0;
   mu_calo_me_cov_z0_theta_exPV = 0;
   mu_calo_me_cov_z0_qoverp_exPV = 0;
   mu_calo_me_cov_phi_theta_exPV = 0;
   mu_calo_me_cov_phi_qoverp_exPV = 0;
   mu_calo_me_cov_theta_qoverp_exPV = 0;
   mu_calo_ms_d0 = 0;
   mu_calo_ms_z0 = 0;
   mu_calo_ms_phi = 0;
   mu_calo_ms_theta = 0;
   mu_calo_ms_qoverp = 0;
   mu_calo_id_d0 = 0;
   mu_calo_id_z0 = 0;
   mu_calo_id_phi = 0;
   mu_calo_id_theta = 0;
   mu_calo_id_qoverp = 0;
   mu_calo_me_d0 = 0;
   mu_calo_me_z0 = 0;
   mu_calo_me_phi = 0;
   mu_calo_me_theta = 0;
   mu_calo_me_qoverp = 0;
   mu_calo_ie_d0 = 0;
   mu_calo_ie_z0 = 0;
   mu_calo_ie_phi = 0;
   mu_calo_ie_theta = 0;
   mu_calo_ie_qoverp = 0;
   mu_calo_nOutliersOnTrack = 0;
   mu_calo_nBLHits = 0;
   mu_calo_nPixHits = 0;
   mu_calo_nSCTHits = 0;
   mu_calo_nTRTHits = 0;
   mu_calo_nTRTHighTHits = 0;
   mu_calo_nBLSharedHits = 0;
   mu_calo_nPixSharedHits = 0;
   mu_calo_nPixHoles = 0;
   mu_calo_nSCTSharedHits = 0;
   mu_calo_nSCTHoles = 0;
   mu_calo_nTRTOutliers = 0;
   mu_calo_nTRTHighTOutliers = 0;
   mu_calo_nGangedPixels = 0;
   mu_calo_nPixelDeadSensors = 0;
   mu_calo_nSCTDeadSensors = 0;
   mu_calo_nTRTDeadStraws = 0;
   mu_calo_expectBLayerHit = 0;
   mu_calo_nMDTHits = 0;
   mu_calo_nMDTHoles = 0;
   mu_calo_nCSCEtaHits = 0;
   mu_calo_nCSCEtaHoles = 0;
   mu_calo_nCSCUnspoiledEtaHits = 0;
   mu_calo_nCSCPhiHits = 0;
   mu_calo_nCSCPhiHoles = 0;
   mu_calo_nRPCEtaHits = 0;
   mu_calo_nRPCEtaHoles = 0;
   mu_calo_nRPCPhiHits = 0;
   mu_calo_nRPCPhiHoles = 0;
   mu_calo_nTGCEtaHits = 0;
   mu_calo_nTGCEtaHoles = 0;
   mu_calo_nTGCPhiHits = 0;
   mu_calo_nTGCPhiHoles = 0;
   mu_calo_nprecisionLayers = 0;
   mu_calo_nprecisionHoleLayers = 0;
   mu_calo_nphiLayers = 0;
   mu_calo_ntrigEtaLayers = 0;
   mu_calo_nphiHoleLayers = 0;
   mu_calo_ntrigEtaHoleLayers = 0;
   mu_calo_nMDTBIHits = 0;
   mu_calo_nMDTBMHits = 0;
   mu_calo_nMDTBOHits = 0;
   mu_calo_nMDTBEEHits = 0;
   mu_calo_nMDTBIS78Hits = 0;
   mu_calo_nMDTEIHits = 0;
   mu_calo_nMDTEMHits = 0;
   mu_calo_nMDTEOHits = 0;
   mu_calo_nMDTEEHits = 0;
   mu_calo_nRPCLayer1EtaHits = 0;
   mu_calo_nRPCLayer2EtaHits = 0;
   mu_calo_nRPCLayer3EtaHits = 0;
   mu_calo_nRPCLayer1PhiHits = 0;
   mu_calo_nRPCLayer2PhiHits = 0;
   mu_calo_nRPCLayer3PhiHits = 0;
   mu_calo_nTGCLayer1EtaHits = 0;
   mu_calo_nTGCLayer2EtaHits = 0;
   mu_calo_nTGCLayer3EtaHits = 0;
   mu_calo_nTGCLayer4EtaHits = 0;
   mu_calo_nTGCLayer1PhiHits = 0;
   mu_calo_nTGCLayer2PhiHits = 0;
   mu_calo_nTGCLayer3PhiHits = 0;
   mu_calo_nTGCLayer4PhiHits = 0;
   mu_calo_barrelSectors = 0;
   mu_calo_endcapSectors = 0;
   mu_calo_spec_surf_px = 0;
   mu_calo_spec_surf_py = 0;
   mu_calo_spec_surf_pz = 0;
   mu_calo_spec_surf_x = 0;
   mu_calo_spec_surf_y = 0;
   mu_calo_spec_surf_z = 0;
   mu_calo_trackd0 = 0;
   mu_calo_trackz0 = 0;
   mu_calo_trackphi = 0;
   mu_calo_tracktheta = 0;
   mu_calo_trackqoverp = 0;
   mu_calo_trackcov_d0 = 0;
   mu_calo_trackcov_z0 = 0;
   mu_calo_trackcov_phi = 0;
   mu_calo_trackcov_theta = 0;
   mu_calo_trackcov_qoverp = 0;
   mu_calo_trackcov_d0_z0 = 0;
   mu_calo_trackcov_d0_phi = 0;
   mu_calo_trackcov_d0_theta = 0;
   mu_calo_trackcov_d0_qoverp = 0;
   mu_calo_trackcov_z0_phi = 0;
   mu_calo_trackcov_z0_theta = 0;
   mu_calo_trackcov_z0_qoverp = 0;
   mu_calo_trackcov_phi_theta = 0;
   mu_calo_trackcov_phi_qoverp = 0;
   mu_calo_trackcov_theta_qoverp = 0;
   mu_calo_trackfitchi2 = 0;
   mu_calo_trackfitndof = 0;
   mu_calo_hastrack = 0;
   mu_calo_trackd0beam = 0;
   mu_calo_trackz0beam = 0;
   mu_calo_tracksigd0beam = 0;
   mu_calo_tracksigz0beam = 0;
   mu_calo_trackd0pv = 0;
   mu_calo_trackz0pv = 0;
   mu_calo_tracksigd0pv = 0;
   mu_calo_tracksigz0pv = 0;
   mu_calo_trackIPEstimate_d0_biasedpvunbiased = 0;
   mu_calo_trackIPEstimate_z0_biasedpvunbiased = 0;
   mu_calo_trackIPEstimate_sigd0_biasedpvunbiased = 0;
   mu_calo_trackIPEstimate_sigz0_biasedpvunbiased = 0;
   mu_calo_trackIPEstimate_d0_unbiasedpvunbiased = 0;
   mu_calo_trackIPEstimate_z0_unbiasedpvunbiased = 0;
   mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased = 0;
   mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased = 0;
   mu_calo_trackd0pvunbiased = 0;
   mu_calo_trackz0pvunbiased = 0;
   mu_calo_tracksigd0pvunbiased = 0;
   mu_calo_tracksigz0pvunbiased = 0;
   mu_calo_type = 0;
   mu_calo_origin = 0;
   mu_calo_truth_dr = 0;
   mu_calo_truth_E = 0;
   mu_calo_truth_pt = 0;
   mu_calo_truth_eta = 0;
   mu_calo_truth_phi = 0;
   mu_calo_truth_type = 0;
   mu_calo_truth_status = 0;
   mu_calo_truth_barcode = 0;
   mu_calo_truth_mothertype = 0;
   mu_calo_truth_motherbarcode = 0;
   mu_calo_truth_matched = 0;
   mu_calo_EFCB_dr = 0;
   mu_calo_EFCB_n = 0;
   mu_calo_EFCB_MuonType = 0;
   mu_calo_EFCB_pt = 0;
   mu_calo_EFCB_eta = 0;
   mu_calo_EFCB_phi = 0;
   mu_calo_EFCB_hasCB = 0;
   mu_calo_EFCB_matched = 0;
   mu_calo_EFMG_dr = 0;
   mu_calo_EFMG_n = 0;
   mu_calo_EFMG_MuonType = 0;
   mu_calo_EFMG_pt = 0;
   mu_calo_EFMG_eta = 0;
   mu_calo_EFMG_phi = 0;
   mu_calo_EFMG_hasMG = 0;
   mu_calo_EFMG_matched = 0;
   mu_calo_EFME_dr = 0;
   mu_calo_EFME_n = 0;
   mu_calo_EFME_MuonType = 0;
   mu_calo_EFME_pt = 0;
   mu_calo_EFME_eta = 0;
   mu_calo_EFME_phi = 0;
   mu_calo_EFME_hasME = 0;
   mu_calo_EFME_matched = 0;
   mu_calo_L2CB_dr = 0;
   mu_calo_L2CB_pt = 0;
   mu_calo_L2CB_eta = 0;
   mu_calo_L2CB_phi = 0;
   mu_calo_L2CB_id_pt = 0;
   mu_calo_L2CB_ms_pt = 0;
   mu_calo_L2CB_nPixHits = 0;
   mu_calo_L2CB_nSCTHits = 0;
   mu_calo_L2CB_nTRTHits = 0;
   mu_calo_L2CB_nTRTHighTHits = 0;
   mu_calo_L2CB_matched = 0;
   mu_calo_L1_dr = 0;
   mu_calo_L1_pt = 0;
   mu_calo_L1_eta = 0;
   mu_calo_L1_phi = 0;
   mu_calo_L1_thrNumber = 0;
   mu_calo_L1_RoINumber = 0;
   mu_calo_L1_sectorAddress = 0;
   mu_calo_L1_firstCandidate = 0;
   mu_calo_L1_moreCandInRoI = 0;
   mu_calo_L1_moreCandInSector = 0;
   mu_calo_L1_source = 0;
   mu_calo_L1_hemisphere = 0;
   mu_calo_L1_charge = 0;
   mu_calo_L1_vetoed = 0;
   mu_calo_L1_matched = 0;
   mooreseg_x = 0;
   mooreseg_y = 0;
   mooreseg_z = 0;
   mooreseg_phi = 0;
   mooreseg_theta = 0;
   mooreseg_locX = 0;
   mooreseg_locY = 0;
   mooreseg_locAngleXZ = 0;
   mooreseg_locAngleYZ = 0;
   mooreseg_sector = 0;
   mooreseg_stationEta = 0;
   mooreseg_isEndcap = 0;
   mooreseg_stationName = 0;
   mooreseg_author = 0;
   mooreseg_chi2 = 0;
   mooreseg_ndof = 0;
   mooreseg_t0 = 0;
   mooreseg_t0err = 0;
   mboyseg_x = 0;
   mboyseg_y = 0;
   mboyseg_z = 0;
   mboyseg_phi = 0;
   mboyseg_theta = 0;
   mboyseg_locX = 0;
   mboyseg_locY = 0;
   mboyseg_locAngleXZ = 0;
   mboyseg_locAngleYZ = 0;
   mboyseg_sector = 0;
   mboyseg_stationEta = 0;
   mboyseg_isEndcap = 0;
   mboyseg_stationName = 0;
   mboyseg_author = 0;
   mboyseg_chi2 = 0;
   mboyseg_ndof = 0;
   mboyseg_t0 = 0;
   mboyseg_t0err = 0;
   mgseg_x = 0;
   mgseg_y = 0;
   mgseg_z = 0;
   mgseg_phi = 0;
   mgseg_theta = 0;
   mgseg_locX = 0;
   mgseg_locY = 0;
   mgseg_locAngleXZ = 0;
   mgseg_locAngleYZ = 0;
   mgseg_sector = 0;
   mgseg_stationEta = 0;
   mgseg_isEndcap = 0;
   mgseg_stationName = 0;
   mgseg_author = 0;
   mgseg_chi2 = 0;
   mgseg_ndof = 0;
   mgseg_t0 = 0;
   mgseg_t0err = 0;
   muonTruth_pt = 0;
   muonTruth_m = 0;
   muonTruth_eta = 0;
   muonTruth_phi = 0;
   muonTruth_charge = 0;
   muonTruth_PDGID = 0;
   muonTruth_barcode = 0;
   muonTruth_type = 0;
   muonTruth_origin = 0;
   mcevt_signal_process_id = 0;
   mcevt_event_number = 0;
   mcevt_event_scale = 0;
   mcevt_alphaQCD = 0;
   mcevt_alphaQED = 0;
   mcevt_pdf_id1 = 0;
   mcevt_pdf_id2 = 0;
   mcevt_pdf_x1 = 0;
   mcevt_pdf_x2 = 0;
   mcevt_pdf_scale = 0;
   mcevt_pdf1 = 0;
   mcevt_pdf2 = 0;
   mcevt_weight = 0;
   mcevt_nparticle = 0;
   mcevt_pileUpType = 0;
   mc_pt = 0;
   mc_m = 0;
   mc_eta = 0;
   mc_phi = 0;
   mc_status = 0;
   mc_barcode = 0;
   mc_pdgId = 0;
   mc_charge = 0;
   mc_parents = 0;
   mc_children = 0;
   mc_vx_x = 0;
   mc_vx_y = 0;
   mc_vx_z = 0;
   mc_vx_barcode = 0;
   mc_child_index = 0;
   mc_parent_index = 0;
   trig_L1_mu_pt = 0;
   trig_L1_mu_eta = 0;
   trig_L1_mu_phi = 0;
   trig_L1_mu_thrName = 0;
   trig_L1_mu_thrNumber = 0;
   trig_L1_mu_RoINumber = 0;
   trig_L1_mu_sectorAddress = 0;
   trig_L1_mu_firstCandidate = 0;
   trig_L1_mu_moreCandInRoI = 0;
   trig_L1_mu_moreCandInSector = 0;
   trig_L1_mu_source = 0;
   trig_L1_mu_hemisphere = 0;
   trig_L1_mu_charge = 0;
   trig_L1_mu_vetoed = 0;
   trig_L1_mu_RoIWord = 0;
   muctpi_candMultiplicity = 0;
   muctpi_dataWords = 0;
   muctpi_dw_eta = 0;
   muctpi_dw_phi = 0;
   muctpi_dw_source = 0;
   muctpi_dw_hemisphere = 0;
   muctpi_dw_bcid = 0;
   muctpi_dw_sectorID = 0;
   muctpi_dw_thrNumber = 0;
   muctpi_dw_RoINumber = 0;
   muctpi_dw_overlapFlags = 0;
   muctpi_dw_firstCandidate = 0;
   muctpi_dw_moreCandInRoI = 0;
   muctpi_dw_moreCandInSector = 0;
   muctpi_dw_charge = 0;
   muctpi_dw_vetoed = 0;
   TGC_prd_x = 0;
   TGC_prd_y = 0;
   TGC_prd_z = 0;
   TGC_prd_shortWidth = 0;
   TGC_prd_longWidth = 0;
   TGC_prd_length = 0;
   TGC_prd_isStrip = 0;
   TGC_prd_gasGap = 0;
   TGC_prd_channel = 0;
   TGC_prd_eta = 0;
   TGC_prd_phi = 0;
   TGC_prd_station = 0;
   TGC_prd_bunch = 0;
   TGC_coin_x_In = 0;
   TGC_coin_y_In = 0;
   TGC_coin_z_In = 0;
   TGC_coin_x_Out = 0;
   TGC_coin_y_Out = 0;
   TGC_coin_z_Out = 0;
   TGC_coin_width_In = 0;
   TGC_coin_width_Out = 0;
   TGC_coin_width_R = 0;
   TGC_coin_width_Phi = 0;
   TGC_coin_isAside = 0;
   TGC_coin_isForward = 0;
   TGC_coin_isStrip = 0;
   TGC_coin_isPositiveDeltaR = 0;
   TGC_coin_type = 0;
   TGC_coin_trackletId = 0;
   TGC_coin_trackletIdStrip = 0;
   TGC_coin_phi = 0;
   TGC_coin_roi = 0;
   TGC_coin_pt = 0;
   TGC_coin_delta = 0;
   TGC_coin_sub = 0;
   TGC_coin_bunch = 0;
   TGC_hierarchy_index = 0;
   TGC_hierarchy_dR_hiPt = 0;
   TGC_hierarchy_dPhi_hiPt = 0;
   TGC_hierarchy_dR_tracklet = 0;
   TGC_hierarchy_dPhi_tracklet = 0;
   TGC_hierarchy_isChamberBoundary = 0;
   RPC_prd_x = 0;
   RPC_prd_y = 0;
   RPC_prd_z = 0;
   RPC_prd_time = 0;
   RPC_prd_triggerInfo = 0;
   RPC_prd_ambiguityFlag = 0;
   RPC_prd_measuresPhi = 0;
   RPC_prd_inRibs = 0;
   RPC_prd_station = 0;
   RPC_prd_stationEta = 0;
   RPC_prd_stationPhi = 0;
   RPC_prd_doubletR = 0;
   RPC_prd_doubletZ = 0;
   RPC_prd_stripWidth = 0;
   RPC_prd_stripLength = 0;
   RPC_prd_stripPitch = 0;
   MDT_prd_x = 0;
   MDT_prd_y = 0;
   MDT_prd_z = 0;
   MDT_prd_adc = 0;
   MDT_prd_tdc = 0;
   MDT_prd_status = 0;
   MDT_prd_drift_radius = 0;
   MDT_prd_drift_radius_error = 0;
   ext_staco_ubias_type = 0;
   ext_staco_ubias_index = 0;
   ext_staco_ubias_size = 0;
   ext_staco_ubias_targetVec = 0;
   ext_staco_ubias_targetDistanceVec = 0;
   ext_staco_ubias_targetEtaVec = 0;
   ext_staco_ubias_targetPhiVec = 0;
   ext_staco_ubias_targetDeltaEtaVec = 0;
   ext_staco_ubias_targetDeltaPhiVec = 0;
   ext_staco_ubias_targetPxVec = 0;
   ext_staco_ubias_targetPyVec = 0;
   ext_staco_ubias_targetPzVec = 0;
   ext_staco_bias_type = 0;
   ext_staco_bias_index = 0;
   ext_staco_bias_size = 0;
   ext_staco_bias_targetVec = 0;
   ext_staco_bias_targetDistanceVec = 0;
   ext_staco_bias_targetEtaVec = 0;
   ext_staco_bias_targetPhiVec = 0;
   ext_staco_bias_targetDeltaEtaVec = 0;
   ext_staco_bias_targetDeltaPhiVec = 0;
   ext_staco_bias_targetPxVec = 0;
   ext_staco_bias_targetPyVec = 0;
   ext_staco_bias_targetPzVec = 0;
   ext_muid_ubias_type = 0;
   ext_muid_ubias_index = 0;
   ext_muid_ubias_size = 0;
   ext_muid_ubias_targetVec = 0;
   ext_muid_ubias_targetDistanceVec = 0;
   ext_muid_ubias_targetEtaVec = 0;
   ext_muid_ubias_targetPhiVec = 0;
   ext_muid_ubias_targetDeltaEtaVec = 0;
   ext_muid_ubias_targetDeltaPhiVec = 0;
   ext_muid_ubias_targetPxVec = 0;
   ext_muid_ubias_targetPyVec = 0;
   ext_muid_ubias_targetPzVec = 0;
   ext_muid_bias_type = 0;
   ext_muid_bias_index = 0;
   ext_muid_bias_size = 0;
   ext_muid_bias_targetVec = 0;
   ext_muid_bias_targetDistanceVec = 0;
   ext_muid_bias_targetEtaVec = 0;
   ext_muid_bias_targetPhiVec = 0;
   ext_muid_bias_targetDeltaEtaVec = 0;
   ext_muid_bias_targetDeltaPhiVec = 0;
   ext_muid_bias_targetPxVec = 0;
   ext_muid_bias_targetPyVec = 0;
   ext_muid_bias_targetPzVec = 0;
   ext_calo_ubias_type = 0;
   ext_calo_ubias_index = 0;
   ext_calo_ubias_size = 0;
   ext_calo_ubias_targetVec = 0;
   ext_calo_ubias_targetDistanceVec = 0;
   ext_calo_ubias_targetEtaVec = 0;
   ext_calo_ubias_targetPhiVec = 0;
   ext_calo_ubias_targetDeltaEtaVec = 0;
   ext_calo_ubias_targetDeltaPhiVec = 0;
   ext_calo_ubias_targetPxVec = 0;
   ext_calo_ubias_targetPyVec = 0;
   ext_calo_ubias_targetPzVec = 0;
   trigger_info_chain = 0;
   trigger_info_isPassed = 0;
   trigger_info_nTracks = 0;
   trigger_info_typeVec = 0;
   trigger_info_ptVec = 0;
   trigger_info_etaVec = 0;
   trigger_info_phiVec = 0;
   trigger_info_chargeVec = 0;
   trigger_info_l1RoiWordVec = 0;
   tandp_staco_tag_index = 0;
   tandp_staco_probe_index = 0;
   tandp_staco_vertex_ndof = 0;
   tandp_staco_vertex_chi2 = 0;
   tandp_staco_vertex_posX = 0;
   tandp_staco_vertex_posY = 0;
   tandp_staco_vertex_posZ = 0;
   tandp_staco_vertex_probability = 0;
   tandp_staco_v0_mass = 0;
   tandp_staco_v0_mass_error = 0;
   tandp_staco_v0_mass_probability = 0;
   tandp_staco_v0_px = 0;
   tandp_staco_v0_py = 0;
   tandp_staco_v0_pz = 0;
   tandp_staco_v0_pt = 0;
   tandp_staco_v0_pt_error = 0;
   tandp_staco_num_verticies = 0;
   tandp_staco_vertex_index = 0;
   tandp_staco_lxyVec = 0;
   tandp_staco_lxy_errorVec = 0;
   tandp_staco_tauVec = 0;
   tandp_staco_tau_errorVec = 0;
   tandp_muid_tag_index = 0;
   tandp_muid_probe_index = 0;
   tandp_muid_vertex_ndof = 0;
   tandp_muid_vertex_chi2 = 0;
   tandp_muid_vertex_posX = 0;
   tandp_muid_vertex_posY = 0;
   tandp_muid_vertex_posZ = 0;
   tandp_muid_vertex_probability = 0;
   tandp_muid_v0_mass = 0;
   tandp_muid_v0_mass_error = 0;
   tandp_muid_v0_mass_probability = 0;
   tandp_muid_v0_px = 0;
   tandp_muid_v0_py = 0;
   tandp_muid_v0_pz = 0;
   tandp_muid_v0_pt = 0;
   tandp_muid_v0_pt_error = 0;
   tandp_muid_num_verticies = 0;
   tandp_muid_vertex_index = 0;
   tandp_muid_lxyVec = 0;
   tandp_muid_lxy_errorVec = 0;
   tandp_muid_tauVec = 0;
   tandp_muid_tau_errorVec = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EF_2mu10", &EF_2mu10, &b_EF_2mu10);
   fChain->SetBranchAddress("EF_2mu10_MSonly_g10_loose", &EF_2mu10_MSonly_g10_loose, &b_EF_2mu10_MSonly_g10_loose);
   fChain->SetBranchAddress("EF_2mu10_MSonly_g10_loose_EMPTY", &EF_2mu10_MSonly_g10_loose_EMPTY, &b_EF_2mu10_MSonly_g10_loose_EMPTY);
   fChain->SetBranchAddress("EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO", &EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO, &b_EF_2mu10_MSonly_g10_loose_UNPAIRED_ISO);
   fChain->SetBranchAddress("EF_2mu13", &EF_2mu13, &b_EF_2mu13);
   fChain->SetBranchAddress("EF_2mu13_Zmumu_IDTrkNoCut", &EF_2mu13_Zmumu_IDTrkNoCut, &b_EF_2mu13_Zmumu_IDTrkNoCut);
   fChain->SetBranchAddress("EF_2mu13_l2muonSA", &EF_2mu13_l2muonSA, &b_EF_2mu13_l2muonSA);
   fChain->SetBranchAddress("EF_2mu15", &EF_2mu15, &b_EF_2mu15);
   fChain->SetBranchAddress("EF_2mu4T", &EF_2mu4T, &b_EF_2mu4T);
   fChain->SetBranchAddress("EF_2mu4T_2e5_tight1", &EF_2mu4T_2e5_tight1, &b_EF_2mu4T_2e5_tight1);
   fChain->SetBranchAddress("EF_2mu4T_Bmumu", &EF_2mu4T_Bmumu, &b_EF_2mu4T_Bmumu);
   fChain->SetBranchAddress("EF_2mu4T_Bmumu_Barrel", &EF_2mu4T_Bmumu_Barrel, &b_EF_2mu4T_Bmumu_Barrel);
   fChain->SetBranchAddress("EF_2mu4T_Bmumu_BarrelOnly", &EF_2mu4T_Bmumu_BarrelOnly, &b_EF_2mu4T_Bmumu_BarrelOnly);
   fChain->SetBranchAddress("EF_2mu4T_Bmumux", &EF_2mu4T_Bmumux, &b_EF_2mu4T_Bmumux);
   fChain->SetBranchAddress("EF_2mu4T_Bmumux_Barrel", &EF_2mu4T_Bmumux_Barrel, &b_EF_2mu4T_Bmumux_Barrel);
   fChain->SetBranchAddress("EF_2mu4T_Bmumux_BarrelOnly", &EF_2mu4T_Bmumux_BarrelOnly, &b_EF_2mu4T_Bmumux_BarrelOnly);
   fChain->SetBranchAddress("EF_2mu4T_DiMu", &EF_2mu4T_DiMu, &b_EF_2mu4T_DiMu);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_Barrel", &EF_2mu4T_DiMu_Barrel, &b_EF_2mu4T_DiMu_Barrel);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_BarrelOnly", &EF_2mu4T_DiMu_BarrelOnly, &b_EF_2mu4T_DiMu_BarrelOnly);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_L2StarB", &EF_2mu4T_DiMu_L2StarB, &b_EF_2mu4T_DiMu_L2StarB);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_L2StarC", &EF_2mu4T_DiMu_L2StarC, &b_EF_2mu4T_DiMu_L2StarC);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_e5_tight1", &EF_2mu4T_DiMu_e5_tight1, &b_EF_2mu4T_DiMu_e5_tight1);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_l2muonSA", &EF_2mu4T_DiMu_l2muonSA, &b_EF_2mu4T_DiMu_l2muonSA);
   fChain->SetBranchAddress("EF_2mu4T_DiMu_noVtx_noOS", &EF_2mu4T_DiMu_noVtx_noOS, &b_EF_2mu4T_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("EF_2mu4T_Jpsimumu", &EF_2mu4T_Jpsimumu, &b_EF_2mu4T_Jpsimumu);
   fChain->SetBranchAddress("EF_2mu4T_Jpsimumu_Barrel", &EF_2mu4T_Jpsimumu_Barrel, &b_EF_2mu4T_Jpsimumu_Barrel);
   fChain->SetBranchAddress("EF_2mu4T_Jpsimumu_BarrelOnly", &EF_2mu4T_Jpsimumu_BarrelOnly, &b_EF_2mu4T_Jpsimumu_BarrelOnly);
   fChain->SetBranchAddress("EF_2mu4T_Jpsimumu_IDTrkNoCut", &EF_2mu4T_Jpsimumu_IDTrkNoCut, &b_EF_2mu4T_Jpsimumu_IDTrkNoCut);
   fChain->SetBranchAddress("EF_2mu4T_Upsimumu", &EF_2mu4T_Upsimumu, &b_EF_2mu4T_Upsimumu);
   fChain->SetBranchAddress("EF_2mu4T_Upsimumu_Barrel", &EF_2mu4T_Upsimumu_Barrel, &b_EF_2mu4T_Upsimumu_Barrel);
   fChain->SetBranchAddress("EF_2mu4T_Upsimumu_BarrelOnly", &EF_2mu4T_Upsimumu_BarrelOnly, &b_EF_2mu4T_Upsimumu_BarrelOnly);
   fChain->SetBranchAddress("EF_2mu4T_xe50_tclcw", &EF_2mu4T_xe50_tclcw, &b_EF_2mu4T_xe50_tclcw);
   fChain->SetBranchAddress("EF_2mu4T_xe60", &EF_2mu4T_xe60, &b_EF_2mu4T_xe60);
   fChain->SetBranchAddress("EF_2mu4T_xe60_tclcw", &EF_2mu4T_xe60_tclcw, &b_EF_2mu4T_xe60_tclcw);
   fChain->SetBranchAddress("EF_2mu6", &EF_2mu6, &b_EF_2mu6);
   fChain->SetBranchAddress("EF_2mu6_Bmumu", &EF_2mu6_Bmumu, &b_EF_2mu6_Bmumu);
   fChain->SetBranchAddress("EF_2mu6_Bmumux", &EF_2mu6_Bmumux, &b_EF_2mu6_Bmumux);
   fChain->SetBranchAddress("EF_2mu6_DiMu", &EF_2mu6_DiMu, &b_EF_2mu6_DiMu);
   fChain->SetBranchAddress("EF_2mu6_DiMu_DY20", &EF_2mu6_DiMu_DY20, &b_EF_2mu6_DiMu_DY20);
   fChain->SetBranchAddress("EF_2mu6_DiMu_DY25", &EF_2mu6_DiMu_DY25, &b_EF_2mu6_DiMu_DY25);
   fChain->SetBranchAddress("EF_2mu6_DiMu_noVtx_noOS", &EF_2mu6_DiMu_noVtx_noOS, &b_EF_2mu6_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("EF_2mu6_Jpsimumu", &EF_2mu6_Jpsimumu, &b_EF_2mu6_Jpsimumu);
   fChain->SetBranchAddress("EF_2mu6_Upsimumu", &EF_2mu6_Upsimumu, &b_EF_2mu6_Upsimumu);
   fChain->SetBranchAddress("EF_2mu6i_DiMu_DY", &EF_2mu6i_DiMu_DY, &b_EF_2mu6i_DiMu_DY);
   fChain->SetBranchAddress("EF_2mu6i_DiMu_DY_2j25_a4tchad", &EF_2mu6i_DiMu_DY_2j25_a4tchad, &b_EF_2mu6i_DiMu_DY_2j25_a4tchad);
   fChain->SetBranchAddress("EF_2mu6i_DiMu_DY_noVtx_noOS", &EF_2mu6i_DiMu_DY_noVtx_noOS, &b_EF_2mu6i_DiMu_DY_noVtx_noOS);
   fChain->SetBranchAddress("EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad", &EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad, &b_EF_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad);
   fChain->SetBranchAddress("EF_2mu8_EFxe30", &EF_2mu8_EFxe30, &b_EF_2mu8_EFxe30);
   fChain->SetBranchAddress("EF_2mu8_EFxe30_tclcw", &EF_2mu8_EFxe30_tclcw, &b_EF_2mu8_EFxe30_tclcw);
   fChain->SetBranchAddress("EF_mu10", &EF_mu10, &b_EF_mu10);
   fChain->SetBranchAddress("EF_mu10_Jpsimumu", &EF_mu10_Jpsimumu, &b_EF_mu10_Jpsimumu);
   fChain->SetBranchAddress("EF_mu10_MSonly", &EF_mu10_MSonly, &b_EF_mu10_MSonly);
   fChain->SetBranchAddress("EF_mu10_Upsimumu_tight_FS", &EF_mu10_Upsimumu_tight_FS, &b_EF_mu10_Upsimumu_tight_FS);
   fChain->SetBranchAddress("EF_mu10i_g10_loose", &EF_mu10i_g10_loose, &b_EF_mu10i_g10_loose);
   fChain->SetBranchAddress("EF_mu10i_g10_loose_TauMass", &EF_mu10i_g10_loose_TauMass, &b_EF_mu10i_g10_loose_TauMass);
   fChain->SetBranchAddress("EF_mu10i_g10_medium", &EF_mu10i_g10_medium, &b_EF_mu10i_g10_medium);
   fChain->SetBranchAddress("EF_mu10i_g10_medium_TauMass", &EF_mu10i_g10_medium_TauMass, &b_EF_mu10i_g10_medium_TauMass);
   fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_loose", &EF_mu10i_loose_g12Tvh_loose, &b_EF_mu10i_loose_g12Tvh_loose);
   fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_loose_TauMass", &EF_mu10i_loose_g12Tvh_loose_TauMass, &b_EF_mu10i_loose_g12Tvh_loose_TauMass);
   fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_medium", &EF_mu10i_loose_g12Tvh_medium, &b_EF_mu10i_loose_g12Tvh_medium);
   fChain->SetBranchAddress("EF_mu10i_loose_g12Tvh_medium_TauMass", &EF_mu10i_loose_g12Tvh_medium_TauMass, &b_EF_mu10i_loose_g12Tvh_medium_TauMass);
   fChain->SetBranchAddress("EF_mu11_empty_NoAlg", &EF_mu11_empty_NoAlg, &b_EF_mu11_empty_NoAlg);
   fChain->SetBranchAddress("EF_mu13", &EF_mu13, &b_EF_mu13);
   fChain->SetBranchAddress("EF_mu15", &EF_mu15, &b_EF_mu15);
   fChain->SetBranchAddress("EF_mu18", &EF_mu18, &b_EF_mu18);
   fChain->SetBranchAddress("EF_mu18_2g10_loose", &EF_mu18_2g10_loose, &b_EF_mu18_2g10_loose);
   fChain->SetBranchAddress("EF_mu18_2g10_medium", &EF_mu18_2g10_medium, &b_EF_mu18_2g10_medium);
   fChain->SetBranchAddress("EF_mu18_2g15_loose", &EF_mu18_2g15_loose, &b_EF_mu18_2g15_loose);
   fChain->SetBranchAddress("EF_mu18_IDTrkNoCut_tight", &EF_mu18_IDTrkNoCut_tight, &b_EF_mu18_IDTrkNoCut_tight);
   fChain->SetBranchAddress("EF_mu18_g20vh_loose", &EF_mu18_g20vh_loose, &b_EF_mu18_g20vh_loose);
   fChain->SetBranchAddress("EF_mu18_medium", &EF_mu18_medium, &b_EF_mu18_medium);
   fChain->SetBranchAddress("EF_mu18_tight", &EF_mu18_tight, &b_EF_mu18_tight);
   fChain->SetBranchAddress("EF_mu18_tight_2mu4_EFFS", &EF_mu18_tight_2mu4_EFFS, &b_EF_mu18_tight_2mu4_EFFS);
   fChain->SetBranchAddress("EF_mu18_tight_e7_medium1", &EF_mu18_tight_e7_medium1, &b_EF_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("EF_mu18_tight_mu8_EFFS", &EF_mu18_tight_mu8_EFFS, &b_EF_mu18_tight_mu8_EFFS);
   fChain->SetBranchAddress("EF_mu18i4_tight", &EF_mu18i4_tight, &b_EF_mu18i4_tight);
   fChain->SetBranchAddress("EF_mu18it_tight", &EF_mu18it_tight, &b_EF_mu18it_tight);
   fChain->SetBranchAddress("EF_mu20i_tight_g5_loose", &EF_mu20i_tight_g5_loose, &b_EF_mu20i_tight_g5_loose);
   fChain->SetBranchAddress("EF_mu20i_tight_g5_loose_TauMass", &EF_mu20i_tight_g5_loose_TauMass, &b_EF_mu20i_tight_g5_loose_TauMass);
   fChain->SetBranchAddress("EF_mu20i_tight_g5_medium", &EF_mu20i_tight_g5_medium, &b_EF_mu20i_tight_g5_medium);
   fChain->SetBranchAddress("EF_mu20i_tight_g5_medium_TauMass", &EF_mu20i_tight_g5_medium_TauMass, &b_EF_mu20i_tight_g5_medium_TauMass);
   fChain->SetBranchAddress("EF_mu20it_tight", &EF_mu20it_tight, &b_EF_mu20it_tight);
   fChain->SetBranchAddress("EF_mu22_IDTrkNoCut_tight", &EF_mu22_IDTrkNoCut_tight, &b_EF_mu22_IDTrkNoCut_tight);
   fChain->SetBranchAddress("EF_mu24", &EF_mu24, &b_EF_mu24);
   fChain->SetBranchAddress("EF_mu24_g20vh_loose", &EF_mu24_g20vh_loose, &b_EF_mu24_g20vh_loose);
   fChain->SetBranchAddress("EF_mu24_g20vh_medium", &EF_mu24_g20vh_medium, &b_EF_mu24_g20vh_medium);
   fChain->SetBranchAddress("EF_mu24_j65_a4tchad", &EF_mu24_j65_a4tchad, &b_EF_mu24_j65_a4tchad);
   fChain->SetBranchAddress("EF_mu24_j65_a4tchad_EFxe40", &EF_mu24_j65_a4tchad_EFxe40, &b_EF_mu24_j65_a4tchad_EFxe40);
   fChain->SetBranchAddress("EF_mu24_j65_a4tchad_EFxe40_tclcw", &EF_mu24_j65_a4tchad_EFxe40_tclcw, &b_EF_mu24_j65_a4tchad_EFxe40_tclcw);
   fChain->SetBranchAddress("EF_mu24_j65_a4tchad_EFxe50_tclcw", &EF_mu24_j65_a4tchad_EFxe50_tclcw, &b_EF_mu24_j65_a4tchad_EFxe50_tclcw);
   fChain->SetBranchAddress("EF_mu24_j65_a4tchad_EFxe60_tclcw", &EF_mu24_j65_a4tchad_EFxe60_tclcw, &b_EF_mu24_j65_a4tchad_EFxe60_tclcw);
   fChain->SetBranchAddress("EF_mu24_medium", &EF_mu24_medium, &b_EF_mu24_medium);
   fChain->SetBranchAddress("EF_mu24_muCombTag_NoEF_tight", &EF_mu24_muCombTag_NoEF_tight, &b_EF_mu24_muCombTag_NoEF_tight);
   fChain->SetBranchAddress("EF_mu24_tight", &EF_mu24_tight, &b_EF_mu24_tight);
   fChain->SetBranchAddress("EF_mu24_tight_2j35_a4tchad", &EF_mu24_tight_2j35_a4tchad, &b_EF_mu24_tight_2j35_a4tchad);
   fChain->SetBranchAddress("EF_mu24_tight_3j35_a4tchad", &EF_mu24_tight_3j35_a4tchad, &b_EF_mu24_tight_3j35_a4tchad);
   fChain->SetBranchAddress("EF_mu24_tight_4j35_a4tchad", &EF_mu24_tight_4j35_a4tchad, &b_EF_mu24_tight_4j35_a4tchad);
   fChain->SetBranchAddress("EF_mu24_tight_EFxe40", &EF_mu24_tight_EFxe40, &b_EF_mu24_tight_EFxe40);
   fChain->SetBranchAddress("EF_mu24_tight_L2StarB", &EF_mu24_tight_L2StarB, &b_EF_mu24_tight_L2StarB);
   fChain->SetBranchAddress("EF_mu24_tight_L2StarC", &EF_mu24_tight_L2StarC, &b_EF_mu24_tight_L2StarC);
   fChain->SetBranchAddress("EF_mu24_tight_MG", &EF_mu24_tight_MG, &b_EF_mu24_tight_MG);
   fChain->SetBranchAddress("EF_mu24_tight_MuonEF", &EF_mu24_tight_MuonEF, &b_EF_mu24_tight_MuonEF);
   fChain->SetBranchAddress("EF_mu24_tight_b35_mediumEF_j35_a4tchad", &EF_mu24_tight_b35_mediumEF_j35_a4tchad, &b_EF_mu24_tight_b35_mediumEF_j35_a4tchad);
   fChain->SetBranchAddress("EF_mu24_tight_mu6_EFFS", &EF_mu24_tight_mu6_EFFS, &b_EF_mu24_tight_mu6_EFFS);
   fChain->SetBranchAddress("EF_mu24i_tight", &EF_mu24i_tight, &b_EF_mu24i_tight);
   fChain->SetBranchAddress("EF_mu24i_tight_MG", &EF_mu24i_tight_MG, &b_EF_mu24i_tight_MG);
   fChain->SetBranchAddress("EF_mu24i_tight_MuonEF", &EF_mu24i_tight_MuonEF, &b_EF_mu24i_tight_MuonEF);
   fChain->SetBranchAddress("EF_mu24i_tight_l2muonSA", &EF_mu24i_tight_l2muonSA, &b_EF_mu24i_tight_l2muonSA);
   fChain->SetBranchAddress("EF_mu36_tight", &EF_mu36_tight, &b_EF_mu36_tight);
   fChain->SetBranchAddress("EF_mu40_MSonly_barrel_tight", &EF_mu40_MSonly_barrel_tight, &b_EF_mu40_MSonly_barrel_tight);
   fChain->SetBranchAddress("EF_mu40_muCombTag_NoEF", &EF_mu40_muCombTag_NoEF, &b_EF_mu40_muCombTag_NoEF);
   fChain->SetBranchAddress("EF_mu40_slow_outOfTime_tight", &EF_mu40_slow_outOfTime_tight, &b_EF_mu40_slow_outOfTime_tight);
   fChain->SetBranchAddress("EF_mu40_slow_tight", &EF_mu40_slow_tight, &b_EF_mu40_slow_tight);
   fChain->SetBranchAddress("EF_mu40_tight", &EF_mu40_tight, &b_EF_mu40_tight);
   fChain->SetBranchAddress("EF_mu4T", &EF_mu4T, &b_EF_mu4T);
   fChain->SetBranchAddress("EF_mu4T_Trk_Jpsi", &EF_mu4T_Trk_Jpsi, &b_EF_mu4T_Trk_Jpsi);
   fChain->SetBranchAddress("EF_mu4T_cosmic", &EF_mu4T_cosmic, &b_EF_mu4T_cosmic);
   fChain->SetBranchAddress("EF_mu4T_j110_a4tchad_L2FS_matched", &EF_mu4T_j110_a4tchad_L2FS_matched, &b_EF_mu4T_j110_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j110_a4tchad_matched", &EF_mu4T_j110_a4tchad_matched, &b_EF_mu4T_j110_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j145_a4tchad_L2FS_matched", &EF_mu4T_j145_a4tchad_L2FS_matched, &b_EF_mu4T_j145_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j145_a4tchad_matched", &EF_mu4T_j145_a4tchad_matched, &b_EF_mu4T_j145_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j15_a4tchad_matched", &EF_mu4T_j15_a4tchad_matched, &b_EF_mu4T_j15_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j15_a4tchad_matchedZ", &EF_mu4T_j15_a4tchad_matchedZ, &b_EF_mu4T_j15_a4tchad_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j180_a4tchad_L2FS_matched", &EF_mu4T_j180_a4tchad_L2FS_matched, &b_EF_mu4T_j180_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j180_a4tchad_matched", &EF_mu4T_j180_a4tchad_matched, &b_EF_mu4T_j180_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j220_a4tchad_L2FS_matched", &EF_mu4T_j220_a4tchad_L2FS_matched, &b_EF_mu4T_j220_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j220_a4tchad_matched", &EF_mu4T_j220_a4tchad_matched, &b_EF_mu4T_j220_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j25_a4tchad_matched", &EF_mu4T_j25_a4tchad_matched, &b_EF_mu4T_j25_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j25_a4tchad_matchedZ", &EF_mu4T_j25_a4tchad_matchedZ, &b_EF_mu4T_j25_a4tchad_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j280_a4tchad_L2FS_matched", &EF_mu4T_j280_a4tchad_L2FS_matched, &b_EF_mu4T_j280_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j280_a4tchad_matched", &EF_mu4T_j280_a4tchad_matched, &b_EF_mu4T_j280_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j35_a4tchad_matched", &EF_mu4T_j35_a4tchad_matched, &b_EF_mu4T_j35_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j35_a4tchad_matchedZ", &EF_mu4T_j35_a4tchad_matchedZ, &b_EF_mu4T_j35_a4tchad_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j360_a4tchad_L2FS_matched", &EF_mu4T_j360_a4tchad_L2FS_matched, &b_EF_mu4T_j360_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j360_a4tchad_matched", &EF_mu4T_j360_a4tchad_matched, &b_EF_mu4T_j360_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j45_a4tchad_L2FS_matched", &EF_mu4T_j45_a4tchad_L2FS_matched, &b_EF_mu4T_j45_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j45_a4tchad_L2FS_matchedZ", &EF_mu4T_j45_a4tchad_L2FS_matchedZ, &b_EF_mu4T_j45_a4tchad_L2FS_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j45_a4tchad_matched", &EF_mu4T_j45_a4tchad_matched, &b_EF_mu4T_j45_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j45_a4tchad_matchedZ", &EF_mu4T_j45_a4tchad_matchedZ, &b_EF_mu4T_j45_a4tchad_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j55_a4tchad_L2FS_matched", &EF_mu4T_j55_a4tchad_L2FS_matched, &b_EF_mu4T_j55_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j55_a4tchad_L2FS_matchedZ", &EF_mu4T_j55_a4tchad_L2FS_matchedZ, &b_EF_mu4T_j55_a4tchad_L2FS_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j55_a4tchad_matched", &EF_mu4T_j55_a4tchad_matched, &b_EF_mu4T_j55_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j55_a4tchad_matchedZ", &EF_mu4T_j55_a4tchad_matchedZ, &b_EF_mu4T_j55_a4tchad_matchedZ);
   fChain->SetBranchAddress("EF_mu4T_j65_a4tchad_L2FS_matched", &EF_mu4T_j65_a4tchad_L2FS_matched, &b_EF_mu4T_j65_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j65_a4tchad_matched", &EF_mu4T_j65_a4tchad_matched, &b_EF_mu4T_j65_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4T_j65_a4tchad_xe60_tclcw_loose", &EF_mu4T_j65_a4tchad_xe60_tclcw_loose, &b_EF_mu4T_j65_a4tchad_xe60_tclcw_loose);
   fChain->SetBranchAddress("EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose", &EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose, &b_EF_mu4T_j65_a4tchad_xe70_tclcw_veryloose);
   fChain->SetBranchAddress("EF_mu4T_j80_a4tchad_L2FS_matched", &EF_mu4T_j80_a4tchad_L2FS_matched, &b_EF_mu4T_j80_a4tchad_L2FS_matched);
   fChain->SetBranchAddress("EF_mu4T_j80_a4tchad_matched", &EF_mu4T_j80_a4tchad_matched, &b_EF_mu4T_j80_a4tchad_matched);
   fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_loose", &EF_mu4Ti_g20Tvh_loose, &b_EF_mu4Ti_g20Tvh_loose);
   fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_loose_TauMass", &EF_mu4Ti_g20Tvh_loose_TauMass, &b_EF_mu4Ti_g20Tvh_loose_TauMass);
   fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_medium", &EF_mu4Ti_g20Tvh_medium, &b_EF_mu4Ti_g20Tvh_medium);
   fChain->SetBranchAddress("EF_mu4Ti_g20Tvh_medium_TauMass", &EF_mu4Ti_g20Tvh_medium_TauMass, &b_EF_mu4Ti_g20Tvh_medium_TauMass);
   fChain->SetBranchAddress("EF_mu4Tmu6_Bmumu", &EF_mu4Tmu6_Bmumu, &b_EF_mu4Tmu6_Bmumu);
   fChain->SetBranchAddress("EF_mu4Tmu6_Bmumu_Barrel", &EF_mu4Tmu6_Bmumu_Barrel, &b_EF_mu4Tmu6_Bmumu_Barrel);
   fChain->SetBranchAddress("EF_mu4Tmu6_Bmumux", &EF_mu4Tmu6_Bmumux, &b_EF_mu4Tmu6_Bmumux);
   fChain->SetBranchAddress("EF_mu4Tmu6_Bmumux_Barrel", &EF_mu4Tmu6_Bmumux_Barrel, &b_EF_mu4Tmu6_Bmumux_Barrel);
   fChain->SetBranchAddress("EF_mu4Tmu6_DiMu", &EF_mu4Tmu6_DiMu, &b_EF_mu4Tmu6_DiMu);
   fChain->SetBranchAddress("EF_mu4Tmu6_DiMu_Barrel", &EF_mu4Tmu6_DiMu_Barrel, &b_EF_mu4Tmu6_DiMu_Barrel);
   fChain->SetBranchAddress("EF_mu4Tmu6_DiMu_noVtx_noOS", &EF_mu4Tmu6_DiMu_noVtx_noOS, &b_EF_mu4Tmu6_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("EF_mu4Tmu6_Jpsimumu", &EF_mu4Tmu6_Jpsimumu, &b_EF_mu4Tmu6_Jpsimumu);
   fChain->SetBranchAddress("EF_mu4Tmu6_Jpsimumu_Barrel", &EF_mu4Tmu6_Jpsimumu_Barrel, &b_EF_mu4Tmu6_Jpsimumu_Barrel);
   fChain->SetBranchAddress("EF_mu4Tmu6_Jpsimumu_IDTrkNoCut", &EF_mu4Tmu6_Jpsimumu_IDTrkNoCut, &b_EF_mu4Tmu6_Jpsimumu_IDTrkNoCut);
   fChain->SetBranchAddress("EF_mu4Tmu6_Upsimumu", &EF_mu4Tmu6_Upsimumu, &b_EF_mu4Tmu6_Upsimumu);
   fChain->SetBranchAddress("EF_mu4Tmu6_Upsimumu_Barrel", &EF_mu4Tmu6_Upsimumu_Barrel, &b_EF_mu4Tmu6_Upsimumu_Barrel);
   fChain->SetBranchAddress("EF_mu4_L1MU11_MSonly_cosmic", &EF_mu4_L1MU11_MSonly_cosmic, &b_EF_mu4_L1MU11_MSonly_cosmic);
   fChain->SetBranchAddress("EF_mu4_L1MU11_cosmic", &EF_mu4_L1MU11_cosmic, &b_EF_mu4_L1MU11_cosmic);
   fChain->SetBranchAddress("EF_mu4_empty_NoAlg", &EF_mu4_empty_NoAlg, &b_EF_mu4_empty_NoAlg);
   fChain->SetBranchAddress("EF_mu4_firstempty_NoAlg", &EF_mu4_firstempty_NoAlg, &b_EF_mu4_firstempty_NoAlg);
   fChain->SetBranchAddress("EF_mu4_unpaired_iso_NoAlg", &EF_mu4_unpaired_iso_NoAlg, &b_EF_mu4_unpaired_iso_NoAlg);
   fChain->SetBranchAddress("EF_mu50_MSonly_barrel_tight", &EF_mu50_MSonly_barrel_tight, &b_EF_mu50_MSonly_barrel_tight);
   fChain->SetBranchAddress("EF_mu6", &EF_mu6, &b_EF_mu6);
   fChain->SetBranchAddress("EF_mu60_slow_outOfTime_tight1", &EF_mu60_slow_outOfTime_tight1, &b_EF_mu60_slow_outOfTime_tight1);
   fChain->SetBranchAddress("EF_mu60_slow_tight1", &EF_mu60_slow_tight1, &b_EF_mu60_slow_tight1);
   fChain->SetBranchAddress("EF_mu6_Jpsimumu_tight", &EF_mu6_Jpsimumu_tight, &b_EF_mu6_Jpsimumu_tight);
   fChain->SetBranchAddress("EF_mu6_MSonly", &EF_mu6_MSonly, &b_EF_mu6_MSonly);
   fChain->SetBranchAddress("EF_mu6_Trk_Jpsi_loose", &EF_mu6_Trk_Jpsi_loose, &b_EF_mu6_Trk_Jpsi_loose);
   fChain->SetBranchAddress("EF_mu6i", &EF_mu6i, &b_EF_mu6i);
   fChain->SetBranchAddress("EF_mu8", &EF_mu8, &b_EF_mu8);
   fChain->SetBranchAddress("EF_mu8_4j45_a4tchad_L2FS", &EF_mu8_4j45_a4tchad_L2FS, &b_EF_mu8_4j45_a4tchad_L2FS);
   fChain->SetBranchAddress("L1_2MU10", &L1_2MU10, &b_L1_2MU10);
   fChain->SetBranchAddress("L1_2MU4", &L1_2MU4, &b_L1_2MU4);
   fChain->SetBranchAddress("L1_2MU4_2EM3", &L1_2MU4_2EM3, &b_L1_2MU4_2EM3);
   fChain->SetBranchAddress("L1_2MU4_BARREL", &L1_2MU4_BARREL, &b_L1_2MU4_BARREL);
   fChain->SetBranchAddress("L1_2MU4_BARRELONLY", &L1_2MU4_BARRELONLY, &b_L1_2MU4_BARRELONLY);
   fChain->SetBranchAddress("L1_2MU4_EM3", &L1_2MU4_EM3, &b_L1_2MU4_EM3);
   fChain->SetBranchAddress("L1_2MU4_EMPTY", &L1_2MU4_EMPTY, &b_L1_2MU4_EMPTY);
   fChain->SetBranchAddress("L1_2MU4_FIRSTEMPTY", &L1_2MU4_FIRSTEMPTY, &b_L1_2MU4_FIRSTEMPTY);
   fChain->SetBranchAddress("L1_2MU4_MU6", &L1_2MU4_MU6, &b_L1_2MU4_MU6);
   fChain->SetBranchAddress("L1_2MU4_MU6_BARREL", &L1_2MU4_MU6_BARREL, &b_L1_2MU4_MU6_BARREL);
   fChain->SetBranchAddress("L1_2MU4_XE30", &L1_2MU4_XE30, &b_L1_2MU4_XE30);
   fChain->SetBranchAddress("L1_2MU4_XE40", &L1_2MU4_XE40, &b_L1_2MU4_XE40);
   fChain->SetBranchAddress("L1_2MU6", &L1_2MU6, &b_L1_2MU6);
   fChain->SetBranchAddress("L1_2MU6_UNPAIRED_ISO", &L1_2MU6_UNPAIRED_ISO, &b_L1_2MU6_UNPAIRED_ISO);
   fChain->SetBranchAddress("L1_2MU6_UNPAIRED_NONISO", &L1_2MU6_UNPAIRED_NONISO, &b_L1_2MU6_UNPAIRED_NONISO);
   fChain->SetBranchAddress("L1_MU10", &L1_MU10, &b_L1_MU10);
   fChain->SetBranchAddress("L1_MU10_EMPTY", &L1_MU10_EMPTY, &b_L1_MU10_EMPTY);
   fChain->SetBranchAddress("L1_MU10_FIRSTEMPTY", &L1_MU10_FIRSTEMPTY, &b_L1_MU10_FIRSTEMPTY);
   fChain->SetBranchAddress("L1_MU10_J20", &L1_MU10_J20, &b_L1_MU10_J20);
   fChain->SetBranchAddress("L1_MU10_UNPAIRED_ISO", &L1_MU10_UNPAIRED_ISO, &b_L1_MU10_UNPAIRED_ISO);
   fChain->SetBranchAddress("L1_MU10_XE20", &L1_MU10_XE20, &b_L1_MU10_XE20);
   fChain->SetBranchAddress("L1_MU10_XE25", &L1_MU10_XE25, &b_L1_MU10_XE25);
   fChain->SetBranchAddress("L1_MU11", &L1_MU11, &b_L1_MU11);
   fChain->SetBranchAddress("L1_MU11_EMPTY", &L1_MU11_EMPTY, &b_L1_MU11_EMPTY);
   fChain->SetBranchAddress("L1_MU15", &L1_MU15, &b_L1_MU15);
   fChain->SetBranchAddress("L1_MU20", &L1_MU20, &b_L1_MU20);
   fChain->SetBranchAddress("L1_MU20_FIRSTEMPTY", &L1_MU20_FIRSTEMPTY, &b_L1_MU20_FIRSTEMPTY);
   fChain->SetBranchAddress("L1_MU4", &L1_MU4, &b_L1_MU4);
   fChain->SetBranchAddress("L1_MU4_EMPTY", &L1_MU4_EMPTY, &b_L1_MU4_EMPTY);
   fChain->SetBranchAddress("L1_MU4_FIRSTEMPTY", &L1_MU4_FIRSTEMPTY, &b_L1_MU4_FIRSTEMPTY);
   fChain->SetBranchAddress("L1_MU4_J10", &L1_MU4_J10, &b_L1_MU4_J10);
   fChain->SetBranchAddress("L1_MU4_J15", &L1_MU4_J15, &b_L1_MU4_J15);
   fChain->SetBranchAddress("L1_MU4_J15_EMPTY", &L1_MU4_J15_EMPTY, &b_L1_MU4_J15_EMPTY);
   fChain->SetBranchAddress("L1_MU4_J15_UNPAIRED_ISO", &L1_MU4_J15_UNPAIRED_ISO, &b_L1_MU4_J15_UNPAIRED_ISO);
   fChain->SetBranchAddress("L1_MU4_J20_XE20", &L1_MU4_J20_XE20, &b_L1_MU4_J20_XE20);
   fChain->SetBranchAddress("L1_MU4_J20_XE35", &L1_MU4_J20_XE35, &b_L1_MU4_J20_XE35);
   fChain->SetBranchAddress("L1_MU4_J30", &L1_MU4_J30, &b_L1_MU4_J30);
   fChain->SetBranchAddress("L1_MU4_J50", &L1_MU4_J50, &b_L1_MU4_J50);
   fChain->SetBranchAddress("L1_MU4_J75", &L1_MU4_J75, &b_L1_MU4_J75);
   fChain->SetBranchAddress("L1_MU4_UNPAIRED_ISO", &L1_MU4_UNPAIRED_ISO, &b_L1_MU4_UNPAIRED_ISO);
   fChain->SetBranchAddress("L1_MU4_UNPAIRED_NONISO", &L1_MU4_UNPAIRED_NONISO, &b_L1_MU4_UNPAIRED_NONISO);
   fChain->SetBranchAddress("L1_MU6", &L1_MU6, &b_L1_MU6);
   fChain->SetBranchAddress("L1_MU6_2J20", &L1_MU6_2J20, &b_L1_MU6_2J20);
   fChain->SetBranchAddress("L1_MU6_FIRSTEMPTY", &L1_MU6_FIRSTEMPTY, &b_L1_MU6_FIRSTEMPTY);
   fChain->SetBranchAddress("L1_MU6_J15", &L1_MU6_J15, &b_L1_MU6_J15);
   fChain->SetBranchAddress("L1_MUB", &L1_MUB, &b_L1_MUB);
   fChain->SetBranchAddress("L1_MUE", &L1_MUE, &b_L1_MUE);
   fChain->SetBranchAddress("L2_2mu10", &L2_2mu10, &b_L2_2mu10);
   fChain->SetBranchAddress("L2_2mu10_MSonly_g10_loose", &L2_2mu10_MSonly_g10_loose, &b_L2_2mu10_MSonly_g10_loose);
   fChain->SetBranchAddress("L2_2mu10_MSonly_g10_loose_EMPTY", &L2_2mu10_MSonly_g10_loose_EMPTY, &b_L2_2mu10_MSonly_g10_loose_EMPTY);
   fChain->SetBranchAddress("L2_2mu10_MSonly_g10_loose_UNPAIRED_ISO", &L2_2mu10_MSonly_g10_loose_UNPAIRED_ISO, &b_L2_2mu10_MSonly_g10_loose_UNPAIRED_ISO);
   fChain->SetBranchAddress("L2_2mu13", &L2_2mu13, &b_L2_2mu13);
   fChain->SetBranchAddress("L2_2mu13_Zmumu_IDTrkNoCut", &L2_2mu13_Zmumu_IDTrkNoCut, &b_L2_2mu13_Zmumu_IDTrkNoCut);
   fChain->SetBranchAddress("L2_2mu13_l2muonSA", &L2_2mu13_l2muonSA, &b_L2_2mu13_l2muonSA);
   fChain->SetBranchAddress("L2_2mu15", &L2_2mu15, &b_L2_2mu15);
   fChain->SetBranchAddress("L2_2mu4T", &L2_2mu4T, &b_L2_2mu4T);
   fChain->SetBranchAddress("L2_2mu4T_2e5_tight1", &L2_2mu4T_2e5_tight1, &b_L2_2mu4T_2e5_tight1);
   fChain->SetBranchAddress("L2_2mu4T_Bmumu", &L2_2mu4T_Bmumu, &b_L2_2mu4T_Bmumu);
   fChain->SetBranchAddress("L2_2mu4T_Bmumu_Barrel", &L2_2mu4T_Bmumu_Barrel, &b_L2_2mu4T_Bmumu_Barrel);
   fChain->SetBranchAddress("L2_2mu4T_Bmumu_BarrelOnly", &L2_2mu4T_Bmumu_BarrelOnly, &b_L2_2mu4T_Bmumu_BarrelOnly);
   fChain->SetBranchAddress("L2_2mu4T_Bmumux", &L2_2mu4T_Bmumux, &b_L2_2mu4T_Bmumux);
   fChain->SetBranchAddress("L2_2mu4T_Bmumux_Barrel", &L2_2mu4T_Bmumux_Barrel, &b_L2_2mu4T_Bmumux_Barrel);
   fChain->SetBranchAddress("L2_2mu4T_Bmumux_BarrelOnly", &L2_2mu4T_Bmumux_BarrelOnly, &b_L2_2mu4T_Bmumux_BarrelOnly);
   fChain->SetBranchAddress("L2_2mu4T_DiMu", &L2_2mu4T_DiMu, &b_L2_2mu4T_DiMu);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_Barrel", &L2_2mu4T_DiMu_Barrel, &b_L2_2mu4T_DiMu_Barrel);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_BarrelOnly", &L2_2mu4T_DiMu_BarrelOnly, &b_L2_2mu4T_DiMu_BarrelOnly);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_L2StarB", &L2_2mu4T_DiMu_L2StarB, &b_L2_2mu4T_DiMu_L2StarB);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_L2StarC", &L2_2mu4T_DiMu_L2StarC, &b_L2_2mu4T_DiMu_L2StarC);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_e5_tight1", &L2_2mu4T_DiMu_e5_tight1, &b_L2_2mu4T_DiMu_e5_tight1);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_l2muonSA", &L2_2mu4T_DiMu_l2muonSA, &b_L2_2mu4T_DiMu_l2muonSA);
   fChain->SetBranchAddress("L2_2mu4T_DiMu_noVtx_noOS", &L2_2mu4T_DiMu_noVtx_noOS, &b_L2_2mu4T_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("L2_2mu4T_Jpsimumu", &L2_2mu4T_Jpsimumu, &b_L2_2mu4T_Jpsimumu);
   fChain->SetBranchAddress("L2_2mu4T_Jpsimumu_Barrel", &L2_2mu4T_Jpsimumu_Barrel, &b_L2_2mu4T_Jpsimumu_Barrel);
   fChain->SetBranchAddress("L2_2mu4T_Jpsimumu_BarrelOnly", &L2_2mu4T_Jpsimumu_BarrelOnly, &b_L2_2mu4T_Jpsimumu_BarrelOnly);
   fChain->SetBranchAddress("L2_2mu4T_Jpsimumu_IDTrkNoCut", &L2_2mu4T_Jpsimumu_IDTrkNoCut, &b_L2_2mu4T_Jpsimumu_IDTrkNoCut);
   fChain->SetBranchAddress("L2_2mu4T_Upsimumu", &L2_2mu4T_Upsimumu, &b_L2_2mu4T_Upsimumu);
   fChain->SetBranchAddress("L2_2mu4T_Upsimumu_Barrel", &L2_2mu4T_Upsimumu_Barrel, &b_L2_2mu4T_Upsimumu_Barrel);
   fChain->SetBranchAddress("L2_2mu4T_Upsimumu_BarrelOnly", &L2_2mu4T_Upsimumu_BarrelOnly, &b_L2_2mu4T_Upsimumu_BarrelOnly);
   fChain->SetBranchAddress("L2_2mu4T_xe35", &L2_2mu4T_xe35, &b_L2_2mu4T_xe35);
   fChain->SetBranchAddress("L2_2mu4T_xe45", &L2_2mu4T_xe45, &b_L2_2mu4T_xe45);
   fChain->SetBranchAddress("L2_2mu4T_xe60", &L2_2mu4T_xe60, &b_L2_2mu4T_xe60);
   fChain->SetBranchAddress("L2_2mu6", &L2_2mu6, &b_L2_2mu6);
   fChain->SetBranchAddress("L2_2mu6_Bmumu", &L2_2mu6_Bmumu, &b_L2_2mu6_Bmumu);
   fChain->SetBranchAddress("L2_2mu6_Bmumux", &L2_2mu6_Bmumux, &b_L2_2mu6_Bmumux);
   fChain->SetBranchAddress("L2_2mu6_DiMu", &L2_2mu6_DiMu, &b_L2_2mu6_DiMu);
   fChain->SetBranchAddress("L2_2mu6_DiMu_DY20", &L2_2mu6_DiMu_DY20, &b_L2_2mu6_DiMu_DY20);
   fChain->SetBranchAddress("L2_2mu6_DiMu_DY25", &L2_2mu6_DiMu_DY25, &b_L2_2mu6_DiMu_DY25);
   fChain->SetBranchAddress("L2_2mu6_DiMu_noVtx_noOS", &L2_2mu6_DiMu_noVtx_noOS, &b_L2_2mu6_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("L2_2mu6_Jpsimumu", &L2_2mu6_Jpsimumu, &b_L2_2mu6_Jpsimumu);
   fChain->SetBranchAddress("L2_2mu6_Upsimumu", &L2_2mu6_Upsimumu, &b_L2_2mu6_Upsimumu);
   fChain->SetBranchAddress("L2_2mu6i_DiMu_DY", &L2_2mu6i_DiMu_DY, &b_L2_2mu6i_DiMu_DY);
   fChain->SetBranchAddress("L2_2mu6i_DiMu_DY_2j25_a4tchad", &L2_2mu6i_DiMu_DY_2j25_a4tchad, &b_L2_2mu6i_DiMu_DY_2j25_a4tchad);
   fChain->SetBranchAddress("L2_2mu6i_DiMu_DY_noVtx_noOS", &L2_2mu6i_DiMu_DY_noVtx_noOS, &b_L2_2mu6i_DiMu_DY_noVtx_noOS);
   fChain->SetBranchAddress("L2_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad", &L2_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad, &b_L2_2mu6i_DiMu_DY_noVtx_noOS_2j25_a4tchad);
   fChain->SetBranchAddress("L2_2mu8_EFxe30", &L2_2mu8_EFxe30, &b_L2_2mu8_EFxe30);
   fChain->SetBranchAddress("L2_mu10", &L2_mu10, &b_L2_mu10);
   fChain->SetBranchAddress("L2_mu10_Jpsimumu", &L2_mu10_Jpsimumu, &b_L2_mu10_Jpsimumu);
   fChain->SetBranchAddress("L2_mu10_MSonly", &L2_mu10_MSonly, &b_L2_mu10_MSonly);
   fChain->SetBranchAddress("L2_mu10_Upsimumu_tight_FS", &L2_mu10_Upsimumu_tight_FS, &b_L2_mu10_Upsimumu_tight_FS);
   fChain->SetBranchAddress("L2_mu10i_g10_loose", &L2_mu10i_g10_loose, &b_L2_mu10i_g10_loose);
   fChain->SetBranchAddress("L2_mu10i_g10_loose_TauMass", &L2_mu10i_g10_loose_TauMass, &b_L2_mu10i_g10_loose_TauMass);
   fChain->SetBranchAddress("L2_mu10i_g10_medium", &L2_mu10i_g10_medium, &b_L2_mu10i_g10_medium);
   fChain->SetBranchAddress("L2_mu10i_g10_medium_TauMass", &L2_mu10i_g10_medium_TauMass, &b_L2_mu10i_g10_medium_TauMass);
   fChain->SetBranchAddress("L2_mu10i_loose_g12Tvh_loose", &L2_mu10i_loose_g12Tvh_loose, &b_L2_mu10i_loose_g12Tvh_loose);
   fChain->SetBranchAddress("L2_mu10i_loose_g12Tvh_loose_TauMass", &L2_mu10i_loose_g12Tvh_loose_TauMass, &b_L2_mu10i_loose_g12Tvh_loose_TauMass);
   fChain->SetBranchAddress("L2_mu10i_loose_g12Tvh_medium", &L2_mu10i_loose_g12Tvh_medium, &b_L2_mu10i_loose_g12Tvh_medium);
   fChain->SetBranchAddress("L2_mu10i_loose_g12Tvh_medium_TauMass", &L2_mu10i_loose_g12Tvh_medium_TauMass, &b_L2_mu10i_loose_g12Tvh_medium_TauMass);
   fChain->SetBranchAddress("L2_mu11_empty_NoAlg", &L2_mu11_empty_NoAlg, &b_L2_mu11_empty_NoAlg);
   fChain->SetBranchAddress("L2_mu13", &L2_mu13, &b_L2_mu13);
   fChain->SetBranchAddress("L2_mu15", &L2_mu15, &b_L2_mu15);
   fChain->SetBranchAddress("L2_mu15_l2cal", &L2_mu15_l2cal, &b_L2_mu15_l2cal);
   fChain->SetBranchAddress("L2_mu18", &L2_mu18, &b_L2_mu18);
   fChain->SetBranchAddress("L2_mu18_2g10_loose", &L2_mu18_2g10_loose, &b_L2_mu18_2g10_loose);
   fChain->SetBranchAddress("L2_mu18_2g10_medium", &L2_mu18_2g10_medium, &b_L2_mu18_2g10_medium);
   fChain->SetBranchAddress("L2_mu18_2g15_loose", &L2_mu18_2g15_loose, &b_L2_mu18_2g15_loose);
   fChain->SetBranchAddress("L2_mu18_IDTrkNoCut_tight", &L2_mu18_IDTrkNoCut_tight, &b_L2_mu18_IDTrkNoCut_tight);
   fChain->SetBranchAddress("L2_mu18_g20vh_loose", &L2_mu18_g20vh_loose, &b_L2_mu18_g20vh_loose);
   fChain->SetBranchAddress("L2_mu18_medium", &L2_mu18_medium, &b_L2_mu18_medium);
   fChain->SetBranchAddress("L2_mu18_tight", &L2_mu18_tight, &b_L2_mu18_tight);
   fChain->SetBranchAddress("L2_mu18_tight_e7_medium1", &L2_mu18_tight_e7_medium1, &b_L2_mu18_tight_e7_medium1);
   fChain->SetBranchAddress("L2_mu18i4_tight", &L2_mu18i4_tight, &b_L2_mu18i4_tight);
   fChain->SetBranchAddress("L2_mu18it_tight", &L2_mu18it_tight, &b_L2_mu18it_tight);
   fChain->SetBranchAddress("L2_mu20i_tight_g5_loose", &L2_mu20i_tight_g5_loose, &b_L2_mu20i_tight_g5_loose);
   fChain->SetBranchAddress("L2_mu20i_tight_g5_loose_TauMass", &L2_mu20i_tight_g5_loose_TauMass, &b_L2_mu20i_tight_g5_loose_TauMass);
   fChain->SetBranchAddress("L2_mu20i_tight_g5_medium", &L2_mu20i_tight_g5_medium, &b_L2_mu20i_tight_g5_medium);
   fChain->SetBranchAddress("L2_mu20i_tight_g5_medium_TauMass", &L2_mu20i_tight_g5_medium_TauMass, &b_L2_mu20i_tight_g5_medium_TauMass);
   fChain->SetBranchAddress("L2_mu20it_tight", &L2_mu20it_tight, &b_L2_mu20it_tight);
   fChain->SetBranchAddress("L2_mu22_IDTrkNoCut_tight", &L2_mu22_IDTrkNoCut_tight, &b_L2_mu22_IDTrkNoCut_tight);
   fChain->SetBranchAddress("L2_mu24", &L2_mu24, &b_L2_mu24);
   fChain->SetBranchAddress("L2_mu24_g20vh_loose", &L2_mu24_g20vh_loose, &b_L2_mu24_g20vh_loose);
   fChain->SetBranchAddress("L2_mu24_g20vh_medium", &L2_mu24_g20vh_medium, &b_L2_mu24_g20vh_medium);
   fChain->SetBranchAddress("L2_mu24_j60_c4cchad_EFxe40", &L2_mu24_j60_c4cchad_EFxe40, &b_L2_mu24_j60_c4cchad_EFxe40);
   fChain->SetBranchAddress("L2_mu24_j60_c4cchad_EFxe50", &L2_mu24_j60_c4cchad_EFxe50, &b_L2_mu24_j60_c4cchad_EFxe50);
   fChain->SetBranchAddress("L2_mu24_j60_c4cchad_EFxe60", &L2_mu24_j60_c4cchad_EFxe60, &b_L2_mu24_j60_c4cchad_EFxe60);
   fChain->SetBranchAddress("L2_mu24_j60_c4cchad_xe35", &L2_mu24_j60_c4cchad_xe35, &b_L2_mu24_j60_c4cchad_xe35);
   fChain->SetBranchAddress("L2_mu24_j65_c4cchad", &L2_mu24_j65_c4cchad, &b_L2_mu24_j65_c4cchad);
   fChain->SetBranchAddress("L2_mu24_medium", &L2_mu24_medium, &b_L2_mu24_medium);
   fChain->SetBranchAddress("L2_mu24_muCombTag_NoEF_tight", &L2_mu24_muCombTag_NoEF_tight, &b_L2_mu24_muCombTag_NoEF_tight);
   fChain->SetBranchAddress("L2_mu24_tight", &L2_mu24_tight, &b_L2_mu24_tight);
   fChain->SetBranchAddress("L2_mu24_tight_2j35_a4tchad", &L2_mu24_tight_2j35_a4tchad, &b_L2_mu24_tight_2j35_a4tchad);
   fChain->SetBranchAddress("L2_mu24_tight_3j35_a4tchad", &L2_mu24_tight_3j35_a4tchad, &b_L2_mu24_tight_3j35_a4tchad);
   fChain->SetBranchAddress("L2_mu24_tight_4j35_a4tchad", &L2_mu24_tight_4j35_a4tchad, &b_L2_mu24_tight_4j35_a4tchad);
   fChain->SetBranchAddress("L2_mu24_tight_EFxe40", &L2_mu24_tight_EFxe40, &b_L2_mu24_tight_EFxe40);
   fChain->SetBranchAddress("L2_mu24_tight_L2StarB", &L2_mu24_tight_L2StarB, &b_L2_mu24_tight_L2StarB);
   fChain->SetBranchAddress("L2_mu24_tight_L2StarC", &L2_mu24_tight_L2StarC, &b_L2_mu24_tight_L2StarC);
   fChain->SetBranchAddress("L2_mu24_tight_l2muonSA", &L2_mu24_tight_l2muonSA, &b_L2_mu24_tight_l2muonSA);
   fChain->SetBranchAddress("L2_mu36_tight", &L2_mu36_tight, &b_L2_mu36_tight);
   fChain->SetBranchAddress("L2_mu40_MSonly_barrel_tight", &L2_mu40_MSonly_barrel_tight, &b_L2_mu40_MSonly_barrel_tight);
   fChain->SetBranchAddress("L2_mu40_muCombTag_NoEF", &L2_mu40_muCombTag_NoEF, &b_L2_mu40_muCombTag_NoEF);
   fChain->SetBranchAddress("L2_mu40_slow_outOfTime_tight", &L2_mu40_slow_outOfTime_tight, &b_L2_mu40_slow_outOfTime_tight);
   fChain->SetBranchAddress("L2_mu40_slow_tight", &L2_mu40_slow_tight, &b_L2_mu40_slow_tight);
   fChain->SetBranchAddress("L2_mu40_tight", &L2_mu40_tight, &b_L2_mu40_tight);
   fChain->SetBranchAddress("L2_mu4T", &L2_mu4T, &b_L2_mu4T);
   fChain->SetBranchAddress("L2_mu4T_Trk_Jpsi", &L2_mu4T_Trk_Jpsi, &b_L2_mu4T_Trk_Jpsi);
   fChain->SetBranchAddress("L2_mu4T_cosmic", &L2_mu4T_cosmic, &b_L2_mu4T_cosmic);
   fChain->SetBranchAddress("L2_mu4T_j105_c4cchad", &L2_mu4T_j105_c4cchad, &b_L2_mu4T_j105_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j10_a4TTem", &L2_mu4T_j10_a4TTem, &b_L2_mu4T_j10_a4TTem);
   fChain->SetBranchAddress("L2_mu4T_j140_c4cchad", &L2_mu4T_j140_c4cchad, &b_L2_mu4T_j140_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j15_a4TTem", &L2_mu4T_j15_a4TTem, &b_L2_mu4T_j15_a4TTem);
   fChain->SetBranchAddress("L2_mu4T_j165_c4cchad", &L2_mu4T_j165_c4cchad, &b_L2_mu4T_j165_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j30_a4TTem", &L2_mu4T_j30_a4TTem, &b_L2_mu4T_j30_a4TTem);
   fChain->SetBranchAddress("L2_mu4T_j40_c4cchad", &L2_mu4T_j40_c4cchad, &b_L2_mu4T_j40_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j50_a4TTem", &L2_mu4T_j50_a4TTem, &b_L2_mu4T_j50_a4TTem);
   fChain->SetBranchAddress("L2_mu4T_j50_c4cchad", &L2_mu4T_j50_c4cchad, &b_L2_mu4T_j50_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j60_c4cchad", &L2_mu4T_j60_c4cchad, &b_L2_mu4T_j60_c4cchad);
   fChain->SetBranchAddress("L2_mu4T_j60_c4cchad_xe40", &L2_mu4T_j60_c4cchad_xe40, &b_L2_mu4T_j60_c4cchad_xe40);
   fChain->SetBranchAddress("L2_mu4T_j75_a4TTem", &L2_mu4T_j75_a4TTem, &b_L2_mu4T_j75_a4TTem);
   fChain->SetBranchAddress("L2_mu4T_j75_c4cchad", &L2_mu4T_j75_c4cchad, &b_L2_mu4T_j75_c4cchad);
   fChain->SetBranchAddress("L2_mu4Ti_g20Tvh_loose", &L2_mu4Ti_g20Tvh_loose, &b_L2_mu4Ti_g20Tvh_loose);
   fChain->SetBranchAddress("L2_mu4Ti_g20Tvh_loose_TauMass", &L2_mu4Ti_g20Tvh_loose_TauMass, &b_L2_mu4Ti_g20Tvh_loose_TauMass);
   fChain->SetBranchAddress("L2_mu4Ti_g20Tvh_medium", &L2_mu4Ti_g20Tvh_medium, &b_L2_mu4Ti_g20Tvh_medium);
   fChain->SetBranchAddress("L2_mu4Ti_g20Tvh_medium_TauMass", &L2_mu4Ti_g20Tvh_medium_TauMass, &b_L2_mu4Ti_g20Tvh_medium_TauMass);
   fChain->SetBranchAddress("L2_mu4Tmu6_Bmumu", &L2_mu4Tmu6_Bmumu, &b_L2_mu4Tmu6_Bmumu);
   fChain->SetBranchAddress("L2_mu4Tmu6_Bmumu_Barrel", &L2_mu4Tmu6_Bmumu_Barrel, &b_L2_mu4Tmu6_Bmumu_Barrel);
   fChain->SetBranchAddress("L2_mu4Tmu6_Bmumux", &L2_mu4Tmu6_Bmumux, &b_L2_mu4Tmu6_Bmumux);
   fChain->SetBranchAddress("L2_mu4Tmu6_Bmumux_Barrel", &L2_mu4Tmu6_Bmumux_Barrel, &b_L2_mu4Tmu6_Bmumux_Barrel);
   fChain->SetBranchAddress("L2_mu4Tmu6_DiMu", &L2_mu4Tmu6_DiMu, &b_L2_mu4Tmu6_DiMu);
   fChain->SetBranchAddress("L2_mu4Tmu6_DiMu_Barrel", &L2_mu4Tmu6_DiMu_Barrel, &b_L2_mu4Tmu6_DiMu_Barrel);
   fChain->SetBranchAddress("L2_mu4Tmu6_DiMu_noVtx_noOS", &L2_mu4Tmu6_DiMu_noVtx_noOS, &b_L2_mu4Tmu6_DiMu_noVtx_noOS);
   fChain->SetBranchAddress("L2_mu4Tmu6_Jpsimumu", &L2_mu4Tmu6_Jpsimumu, &b_L2_mu4Tmu6_Jpsimumu);
   fChain->SetBranchAddress("L2_mu4Tmu6_Jpsimumu_Barrel", &L2_mu4Tmu6_Jpsimumu_Barrel, &b_L2_mu4Tmu6_Jpsimumu_Barrel);
   fChain->SetBranchAddress("L2_mu4Tmu6_Jpsimumu_IDTrkNoCut", &L2_mu4Tmu6_Jpsimumu_IDTrkNoCut, &b_L2_mu4Tmu6_Jpsimumu_IDTrkNoCut);
   fChain->SetBranchAddress("L2_mu4Tmu6_Upsimumu", &L2_mu4Tmu6_Upsimumu, &b_L2_mu4Tmu6_Upsimumu);
   fChain->SetBranchAddress("L2_mu4Tmu6_Upsimumu_Barrel", &L2_mu4Tmu6_Upsimumu_Barrel, &b_L2_mu4Tmu6_Upsimumu_Barrel);
   fChain->SetBranchAddress("L2_mu4_L1MU11_MSonly_cosmic", &L2_mu4_L1MU11_MSonly_cosmic, &b_L2_mu4_L1MU11_MSonly_cosmic);
   fChain->SetBranchAddress("L2_mu4_L1MU11_cosmic", &L2_mu4_L1MU11_cosmic, &b_L2_mu4_L1MU11_cosmic);
   fChain->SetBranchAddress("L2_mu4_empty_NoAlg", &L2_mu4_empty_NoAlg, &b_L2_mu4_empty_NoAlg);
   fChain->SetBranchAddress("L2_mu4_firstempty_NoAlg", &L2_mu4_firstempty_NoAlg, &b_L2_mu4_firstempty_NoAlg);
   fChain->SetBranchAddress("L2_mu4_l2cal_empty", &L2_mu4_l2cal_empty, &b_L2_mu4_l2cal_empty);
   fChain->SetBranchAddress("L2_mu4_unpaired_iso_NoAlg", &L2_mu4_unpaired_iso_NoAlg, &b_L2_mu4_unpaired_iso_NoAlg);
   fChain->SetBranchAddress("L2_mu50_MSonly_barrel_tight", &L2_mu50_MSonly_barrel_tight, &b_L2_mu50_MSonly_barrel_tight);
   fChain->SetBranchAddress("L2_mu6", &L2_mu6, &b_L2_mu6);
   fChain->SetBranchAddress("L2_mu60_slow_outOfTime_tight1", &L2_mu60_slow_outOfTime_tight1, &b_L2_mu60_slow_outOfTime_tight1);
   fChain->SetBranchAddress("L2_mu60_slow_tight1", &L2_mu60_slow_tight1, &b_L2_mu60_slow_tight1);
   fChain->SetBranchAddress("L2_mu6_Jpsimumu_tight", &L2_mu6_Jpsimumu_tight, &b_L2_mu6_Jpsimumu_tight);
   fChain->SetBranchAddress("L2_mu6_MSonly", &L2_mu6_MSonly, &b_L2_mu6_MSonly);
   fChain->SetBranchAddress("L2_mu6_Trk_Jpsi_loose", &L2_mu6_Trk_Jpsi_loose, &b_L2_mu6_Trk_Jpsi_loose);
   fChain->SetBranchAddress("L2_mu8", &L2_mu8, &b_L2_mu8);
   fChain->SetBranchAddress("L2_mu8_4j15_a4TTem", &L2_mu8_4j15_a4TTem, &b_L2_mu8_4j15_a4TTem);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("timestamp", &timestamp, &b_timestamp);
   fChain->SetBranchAddress("timestamp_ns", &timestamp_ns, &b_timestamp_ns);
   fChain->SetBranchAddress("lbn", &lbn, &b_lbn);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("detmask0", &detmask0, &b_detmask0);
   fChain->SetBranchAddress("detmask1", &detmask1, &b_detmask1);
   fChain->SetBranchAddress("actualIntPerXing", &actualIntPerXing, &b_actualIntPerXing);
   fChain->SetBranchAddress("averageIntPerXing", &averageIntPerXing, &b_averageIntPerXing);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("mc_event_number", &mc_event_number, &b_mc_event_number);
   fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("fwdFlags", &fwdFlags, &b_fwdFlags);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("pixelError", &pixelError, &b_pixelError);
   fChain->SetBranchAddress("sctError", &sctError, &b_sctError);
   fChain->SetBranchAddress("trtError", &trtError, &b_trtError);
   fChain->SetBranchAddress("larError", &larError, &b_larError);
   fChain->SetBranchAddress("tileError", &tileError, &b_tileError);
   fChain->SetBranchAddress("muonError", &muonError, &b_muonError);
   fChain->SetBranchAddress("fwdError", &fwdError, &b_fwdError);
   fChain->SetBranchAddress("coreError", &coreError, &b_coreError);
   fChain->SetBranchAddress("isSimulation", &isSimulation, &b_isSimulation);
   fChain->SetBranchAddress("isCalibration", &isCalibration, &b_isCalibration);
   fChain->SetBranchAddress("isTestBeam", &isTestBeam, &b_isTestBeam);
   fChain->SetBranchAddress("vxp_n", &vxp_n, &b_vxp_n);
   fChain->SetBranchAddress("vxp_x", &vxp_x, &b_vxp_x);
   fChain->SetBranchAddress("vxp_y", &vxp_y, &b_vxp_y);
   fChain->SetBranchAddress("vxp_z", &vxp_z, &b_vxp_z);
   fChain->SetBranchAddress("vxp_cov_x", &vxp_cov_x, &b_vxp_cov_x);
   fChain->SetBranchAddress("vxp_cov_y", &vxp_cov_y, &b_vxp_cov_y);
   fChain->SetBranchAddress("vxp_cov_z", &vxp_cov_z, &b_vxp_cov_z);
   fChain->SetBranchAddress("vxp_cov_xy", &vxp_cov_xy, &b_vxp_cov_xy);
   fChain->SetBranchAddress("vxp_cov_xz", &vxp_cov_xz, &b_vxp_cov_xz);
   fChain->SetBranchAddress("vxp_cov_yz", &vxp_cov_yz, &b_vxp_cov_yz);
   fChain->SetBranchAddress("vxp_type", &vxp_type, &b_vxp_type);
   fChain->SetBranchAddress("vxp_chi2", &vxp_chi2, &b_vxp_chi2);
   fChain->SetBranchAddress("vxp_ndof", &vxp_ndof, &b_vxp_ndof);
   fChain->SetBranchAddress("vxp_px", &vxp_px, &b_vxp_px);
   fChain->SetBranchAddress("vxp_py", &vxp_py, &b_vxp_py);
   fChain->SetBranchAddress("vxp_pz", &vxp_pz, &b_vxp_pz);
   fChain->SetBranchAddress("vxp_E", &vxp_E, &b_vxp_E);
   fChain->SetBranchAddress("vxp_m", &vxp_m, &b_vxp_m);
   fChain->SetBranchAddress("vxp_nTracks", &vxp_nTracks, &b_vxp_nTracks);
   fChain->SetBranchAddress("vxp_sumPt", &vxp_sumPt, &b_vxp_sumPt);
   fChain->SetBranchAddress("vxp_trk_weight", &vxp_trk_weight, &b_vxp_trk_weight);
   fChain->SetBranchAddress("vxp_trk_n", &vxp_trk_n, &b_vxp_trk_n);
   fChain->SetBranchAddress("vxp_trk_index", &vxp_trk_index, &b_vxp_trk_index);
   fChain->SetBranchAddress("trk_n", &trk_n, &b_trk_n);
   fChain->SetBranchAddress("trk_pt", &trk_pt, &b_trk_pt);
   fChain->SetBranchAddress("trk_eta", &trk_eta, &b_trk_eta);
   fChain->SetBranchAddress("trk_d0_wrtPV", &trk_d0_wrtPV, &b_trk_d0_wrtPV);
   fChain->SetBranchAddress("trk_z0_wrtPV", &trk_z0_wrtPV, &b_trk_z0_wrtPV);
   fChain->SetBranchAddress("trk_phi_wrtPV", &trk_phi_wrtPV, &b_trk_phi_wrtPV);
   fChain->SetBranchAddress("trk_theta_wrtPV", &trk_theta_wrtPV, &b_trk_theta_wrtPV);
   fChain->SetBranchAddress("trk_qoverp_wrtPV", &trk_qoverp_wrtPV, &b_trk_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_cov_d0_wrtPV", &trk_cov_d0_wrtPV, &b_trk_cov_d0_wrtPV);
   fChain->SetBranchAddress("trk_cov_z0_wrtPV", &trk_cov_z0_wrtPV, &b_trk_cov_z0_wrtPV);
   fChain->SetBranchAddress("trk_cov_phi_wrtPV", &trk_cov_phi_wrtPV, &b_trk_cov_phi_wrtPV);
   fChain->SetBranchAddress("trk_cov_theta_wrtPV", &trk_cov_theta_wrtPV, &b_trk_cov_theta_wrtPV);
   fChain->SetBranchAddress("trk_cov_qoverp_wrtPV", &trk_cov_qoverp_wrtPV, &b_trk_cov_qoverp_wrtPV);
   fChain->SetBranchAddress("trk_d0_wrtBS", &trk_d0_wrtBS, &b_trk_d0_wrtBS);
   fChain->SetBranchAddress("trk_z0_wrtBS", &trk_z0_wrtBS, &b_trk_z0_wrtBS);
   fChain->SetBranchAddress("trk_phi_wrtBS", &trk_phi_wrtBS, &b_trk_phi_wrtBS);
   fChain->SetBranchAddress("trk_cov_d0_wrtBS", &trk_cov_d0_wrtBS, &b_trk_cov_d0_wrtBS);
   fChain->SetBranchAddress("trk_cov_z0_wrtBS", &trk_cov_z0_wrtBS, &b_trk_cov_z0_wrtBS);
   fChain->SetBranchAddress("trk_cov_phi_wrtBS", &trk_cov_phi_wrtBS, &b_trk_cov_phi_wrtBS);
   fChain->SetBranchAddress("trk_cov_theta_wrtBS", &trk_cov_theta_wrtBS, &b_trk_cov_theta_wrtBS);
   fChain->SetBranchAddress("trk_cov_qoverp_wrtBS", &trk_cov_qoverp_wrtBS, &b_trk_cov_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_cov_d0_z0_wrtBS", &trk_cov_d0_z0_wrtBS, &b_trk_cov_d0_z0_wrtBS);
   fChain->SetBranchAddress("trk_cov_d0_phi_wrtBS", &trk_cov_d0_phi_wrtBS, &b_trk_cov_d0_phi_wrtBS);
   fChain->SetBranchAddress("trk_cov_d0_theta_wrtBS", &trk_cov_d0_theta_wrtBS, &b_trk_cov_d0_theta_wrtBS);
   fChain->SetBranchAddress("trk_cov_d0_qoverp_wrtBS", &trk_cov_d0_qoverp_wrtBS, &b_trk_cov_d0_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_cov_z0_phi_wrtBS", &trk_cov_z0_phi_wrtBS, &b_trk_cov_z0_phi_wrtBS);
   fChain->SetBranchAddress("trk_cov_z0_theta_wrtBS", &trk_cov_z0_theta_wrtBS, &b_trk_cov_z0_theta_wrtBS);
   fChain->SetBranchAddress("trk_cov_z0_qoverp_wrtBS", &trk_cov_z0_qoverp_wrtBS, &b_trk_cov_z0_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_cov_phi_theta_wrtBS", &trk_cov_phi_theta_wrtBS, &b_trk_cov_phi_theta_wrtBS);
   fChain->SetBranchAddress("trk_cov_phi_qoverp_wrtBS", &trk_cov_phi_qoverp_wrtBS, &b_trk_cov_phi_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_cov_theta_qoverp_wrtBS", &trk_cov_theta_qoverp_wrtBS, &b_trk_cov_theta_qoverp_wrtBS);
   fChain->SetBranchAddress("trk_d0_wrtBL", &trk_d0_wrtBL, &b_trk_d0_wrtBL);
   fChain->SetBranchAddress("trk_z0_wrtBL", &trk_z0_wrtBL, &b_trk_z0_wrtBL);
   fChain->SetBranchAddress("trk_phi_wrtBL", &trk_phi_wrtBL, &b_trk_phi_wrtBL);
   fChain->SetBranchAddress("trk_d0_err_wrtBL", &trk_d0_err_wrtBL, &b_trk_d0_err_wrtBL);
   fChain->SetBranchAddress("trk_z0_err_wrtBL", &trk_z0_err_wrtBL, &b_trk_z0_err_wrtBL);
   fChain->SetBranchAddress("trk_phi_err_wrtBL", &trk_phi_err_wrtBL, &b_trk_phi_err_wrtBL);
   fChain->SetBranchAddress("trk_theta_err_wrtBL", &trk_theta_err_wrtBL, &b_trk_theta_err_wrtBL);
   fChain->SetBranchAddress("trk_qoverp_err_wrtBL", &trk_qoverp_err_wrtBL, &b_trk_qoverp_err_wrtBL);
   fChain->SetBranchAddress("trk_d0_z0_err_wrtBL", &trk_d0_z0_err_wrtBL, &b_trk_d0_z0_err_wrtBL);
   fChain->SetBranchAddress("trk_d0_phi_err_wrtBL", &trk_d0_phi_err_wrtBL, &b_trk_d0_phi_err_wrtBL);
   fChain->SetBranchAddress("trk_d0_theta_err_wrtBL", &trk_d0_theta_err_wrtBL, &b_trk_d0_theta_err_wrtBL);
   fChain->SetBranchAddress("trk_d0_qoverp_err_wrtBL", &trk_d0_qoverp_err_wrtBL, &b_trk_d0_qoverp_err_wrtBL);
   fChain->SetBranchAddress("trk_z0_phi_err_wrtBL", &trk_z0_phi_err_wrtBL, &b_trk_z0_phi_err_wrtBL);
   fChain->SetBranchAddress("trk_z0_theta_err_wrtBL", &trk_z0_theta_err_wrtBL, &b_trk_z0_theta_err_wrtBL);
   fChain->SetBranchAddress("trk_z0_qoverp_err_wrtBL", &trk_z0_qoverp_err_wrtBL, &b_trk_z0_qoverp_err_wrtBL);
   fChain->SetBranchAddress("trk_phi_theta_err_wrtBL", &trk_phi_theta_err_wrtBL, &b_trk_phi_theta_err_wrtBL);
   fChain->SetBranchAddress("trk_phi_qoverp_err_wrtBL", &trk_phi_qoverp_err_wrtBL, &b_trk_phi_qoverp_err_wrtBL);
   fChain->SetBranchAddress("trk_theta_qoverp_err_wrtBL", &trk_theta_qoverp_err_wrtBL, &b_trk_theta_qoverp_err_wrtBL);
   fChain->SetBranchAddress("trk_chi2", &trk_chi2, &b_trk_chi2);
   fChain->SetBranchAddress("trk_ndof", &trk_ndof, &b_trk_ndof);
   fChain->SetBranchAddress("trk_nBLHits", &trk_nBLHits, &b_trk_nBLHits);
   fChain->SetBranchAddress("trk_nPixHits", &trk_nPixHits, &b_trk_nPixHits);
   fChain->SetBranchAddress("trk_nSCTHits", &trk_nSCTHits, &b_trk_nSCTHits);
   fChain->SetBranchAddress("trk_nTRTHits", &trk_nTRTHits, &b_trk_nTRTHits);
   fChain->SetBranchAddress("trk_nTRTHighTHits", &trk_nTRTHighTHits, &b_trk_nTRTHighTHits);
   fChain->SetBranchAddress("trk_nTRTXenonHits", &trk_nTRTXenonHits, &b_trk_nTRTXenonHits);
   fChain->SetBranchAddress("trk_nPixHoles", &trk_nPixHoles, &b_trk_nPixHoles);
   fChain->SetBranchAddress("trk_nSCTHoles", &trk_nSCTHoles, &b_trk_nSCTHoles);
   fChain->SetBranchAddress("trk_nTRTHoles", &trk_nTRTHoles, &b_trk_nTRTHoles);
   fChain->SetBranchAddress("trk_nPixelDeadSensors", &trk_nPixelDeadSensors, &b_trk_nPixelDeadSensors);
   fChain->SetBranchAddress("trk_nSCTDeadSensors", &trk_nSCTDeadSensors, &b_trk_nSCTDeadSensors);
   fChain->SetBranchAddress("trk_nBLSharedHits", &trk_nBLSharedHits, &b_trk_nBLSharedHits);
   fChain->SetBranchAddress("trk_nPixSharedHits", &trk_nPixSharedHits, &b_trk_nPixSharedHits);
   fChain->SetBranchAddress("trk_nSCTSharedHits", &trk_nSCTSharedHits, &b_trk_nSCTSharedHits);
   fChain->SetBranchAddress("trk_nBLayerSplitHits", &trk_nBLayerSplitHits, &b_trk_nBLayerSplitHits);
   fChain->SetBranchAddress("trk_nPixSplitHits", &trk_nPixSplitHits, &b_trk_nPixSplitHits);
   fChain->SetBranchAddress("trk_expectBLayerHit", &trk_expectBLayerHit, &b_trk_expectBLayerHit);
   fChain->SetBranchAddress("trk_nMDTHits", &trk_nMDTHits, &b_trk_nMDTHits);
   fChain->SetBranchAddress("trk_nCSCEtaHits", &trk_nCSCEtaHits, &b_trk_nCSCEtaHits);
   fChain->SetBranchAddress("trk_nCSCPhiHits", &trk_nCSCPhiHits, &b_trk_nCSCPhiHits);
   fChain->SetBranchAddress("trk_nRPCEtaHits", &trk_nRPCEtaHits, &b_trk_nRPCEtaHits);
   fChain->SetBranchAddress("trk_nRPCPhiHits", &trk_nRPCPhiHits, &b_trk_nRPCPhiHits);
   fChain->SetBranchAddress("trk_nTGCEtaHits", &trk_nTGCEtaHits, &b_trk_nTGCEtaHits);
   fChain->SetBranchAddress("trk_nTGCPhiHits", &trk_nTGCPhiHits, &b_trk_nTGCPhiHits);
   fChain->SetBranchAddress("trk_nHits", &trk_nHits, &b_trk_nHits);
   fChain->SetBranchAddress("trk_nHoles", &trk_nHoles, &b_trk_nHoles);
   fChain->SetBranchAddress("trk_hitPattern", &trk_hitPattern, &b_trk_hitPattern);
   fChain->SetBranchAddress("trk_TRTHighTHitsRatio", &trk_TRTHighTHitsRatio, &b_trk_TRTHighTHitsRatio);
   fChain->SetBranchAddress("trk_TRTHighTOutliersRatio", &trk_TRTHighTOutliersRatio, &b_trk_TRTHighTOutliersRatio);
   fChain->SetBranchAddress("trk_fitter", &trk_fitter, &b_trk_fitter);
   fChain->SetBranchAddress("trk_patternReco1", &trk_patternReco1, &b_trk_patternReco1);
   fChain->SetBranchAddress("trk_patternReco2", &trk_patternReco2, &b_trk_patternReco2);
   fChain->SetBranchAddress("trk_trackProperties", &trk_trackProperties, &b_trk_trackProperties);
   fChain->SetBranchAddress("trk_particleHypothesis", &trk_particleHypothesis, &b_trk_particleHypothesis);
   fChain->SetBranchAddress("trk_blayerPrediction_x", &trk_blayerPrediction_x, &b_trk_blayerPrediction_x);
   fChain->SetBranchAddress("trk_blayerPrediction_y", &trk_blayerPrediction_y, &b_trk_blayerPrediction_y);
   fChain->SetBranchAddress("trk_blayerPrediction_z", &trk_blayerPrediction_z, &b_trk_blayerPrediction_z);
   fChain->SetBranchAddress("trk_blayerPrediction_locX", &trk_blayerPrediction_locX, &b_trk_blayerPrediction_locX);
   fChain->SetBranchAddress("trk_blayerPrediction_locY", &trk_blayerPrediction_locY, &b_trk_blayerPrediction_locY);
   fChain->SetBranchAddress("trk_blayerPrediction_err_locX", &trk_blayerPrediction_err_locX, &b_trk_blayerPrediction_err_locX);
   fChain->SetBranchAddress("trk_blayerPrediction_err_locY", &trk_blayerPrediction_err_locY, &b_trk_blayerPrediction_err_locY);
   fChain->SetBranchAddress("trk_blayerPrediction_etaDistToEdge", &trk_blayerPrediction_etaDistToEdge, &b_trk_blayerPrediction_etaDistToEdge);
   fChain->SetBranchAddress("trk_blayerPrediction_phiDistToEdge", &trk_blayerPrediction_phiDistToEdge, &b_trk_blayerPrediction_phiDistToEdge);
   fChain->SetBranchAddress("trk_blayerPrediction_detElementId", &trk_blayerPrediction_detElementId, &b_trk_blayerPrediction_detElementId);
   fChain->SetBranchAddress("trk_blayerPrediction_row", &trk_blayerPrediction_row, &b_trk_blayerPrediction_row);
   fChain->SetBranchAddress("trk_blayerPrediction_col", &trk_blayerPrediction_col, &b_trk_blayerPrediction_col);
   fChain->SetBranchAddress("trk_blayerPrediction_type", &trk_blayerPrediction_type, &b_trk_blayerPrediction_type);
   fChain->SetBranchAddress("trk_mc_probability", &trk_mc_probability, &b_trk_mc_probability);
   fChain->SetBranchAddress("trk_mc_barcode", &trk_mc_barcode, &b_trk_mc_barcode);
   fChain->SetBranchAddress("mu_muid_n", &mu_muid_n, &b_mu_muid_n);
   fChain->SetBranchAddress("mu_muid_E", &mu_muid_E, &b_mu_muid_E);
   fChain->SetBranchAddress("mu_muid_pt", &mu_muid_pt, &b_mu_muid_pt);
   fChain->SetBranchAddress("mu_muid_m", &mu_muid_m, &b_mu_muid_m);
   fChain->SetBranchAddress("mu_muid_eta", &mu_muid_eta, &b_mu_muid_eta);
   fChain->SetBranchAddress("mu_muid_phi", &mu_muid_phi, &b_mu_muid_phi);
   fChain->SetBranchAddress("mu_muid_px", &mu_muid_px, &b_mu_muid_px);
   fChain->SetBranchAddress("mu_muid_py", &mu_muid_py, &b_mu_muid_py);
   fChain->SetBranchAddress("mu_muid_pz", &mu_muid_pz, &b_mu_muid_pz);
   fChain->SetBranchAddress("mu_muid_charge", &mu_muid_charge, &b_mu_muid_charge);
   fChain->SetBranchAddress("mu_muid_allauthor", &mu_muid_allauthor, &b_mu_muid_allauthor);
   fChain->SetBranchAddress("mu_muid_author", &mu_muid_author, &b_mu_muid_author);
   fChain->SetBranchAddress("mu_muid_beta", &mu_muid_beta, &b_mu_muid_beta);
   fChain->SetBranchAddress("mu_muid_isMuonLikelihood", &mu_muid_isMuonLikelihood, &b_mu_muid_isMuonLikelihood);
   fChain->SetBranchAddress("mu_muid_matchchi2", &mu_muid_matchchi2, &b_mu_muid_matchchi2);
   fChain->SetBranchAddress("mu_muid_matchndof", &mu_muid_matchndof, &b_mu_muid_matchndof);
   fChain->SetBranchAddress("mu_muid_etcone20", &mu_muid_etcone20, &b_mu_muid_etcone20);
   fChain->SetBranchAddress("mu_muid_etcone30", &mu_muid_etcone30, &b_mu_muid_etcone30);
   fChain->SetBranchAddress("mu_muid_etcone40", &mu_muid_etcone40, &b_mu_muid_etcone40);
   fChain->SetBranchAddress("mu_muid_nucone20", &mu_muid_nucone20, &b_mu_muid_nucone20);
   fChain->SetBranchAddress("mu_muid_nucone30", &mu_muid_nucone30, &b_mu_muid_nucone30);
   fChain->SetBranchAddress("mu_muid_nucone40", &mu_muid_nucone40, &b_mu_muid_nucone40);
   fChain->SetBranchAddress("mu_muid_ptcone20", &mu_muid_ptcone20, &b_mu_muid_ptcone20);
   fChain->SetBranchAddress("mu_muid_ptcone30", &mu_muid_ptcone30, &b_mu_muid_ptcone30);
   fChain->SetBranchAddress("mu_muid_ptcone40", &mu_muid_ptcone40, &b_mu_muid_ptcone40);
   fChain->SetBranchAddress("mu_muid_etconeNoEm10", &mu_muid_etconeNoEm10, &b_mu_muid_etconeNoEm10);
   fChain->SetBranchAddress("mu_muid_etconeNoEm20", &mu_muid_etconeNoEm20, &b_mu_muid_etconeNoEm20);
   fChain->SetBranchAddress("mu_muid_etconeNoEm30", &mu_muid_etconeNoEm30, &b_mu_muid_etconeNoEm30);
   fChain->SetBranchAddress("mu_muid_etconeNoEm40", &mu_muid_etconeNoEm40, &b_mu_muid_etconeNoEm40);
   fChain->SetBranchAddress("mu_muid_scatteringCurvatureSignificance", &mu_muid_scatteringCurvatureSignificance, &b_mu_muid_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("mu_muid_scatteringNeighbourSignificance", &mu_muid_scatteringNeighbourSignificance, &b_mu_muid_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("mu_muid_momentumBalanceSignificance", &mu_muid_momentumBalanceSignificance, &b_mu_muid_momentumBalanceSignificance);
   fChain->SetBranchAddress("mu_muid_energyLossPar", &mu_muid_energyLossPar, &b_mu_muid_energyLossPar);
   fChain->SetBranchAddress("mu_muid_energyLossErr", &mu_muid_energyLossErr, &b_mu_muid_energyLossErr);
   fChain->SetBranchAddress("mu_muid_etCore", &mu_muid_etCore, &b_mu_muid_etCore);
   fChain->SetBranchAddress("mu_muid_energyLossType", &mu_muid_energyLossType, &b_mu_muid_energyLossType);
   fChain->SetBranchAddress("mu_muid_caloMuonIdTag", &mu_muid_caloMuonIdTag, &b_mu_muid_caloMuonIdTag);
   fChain->SetBranchAddress("mu_muid_caloLRLikelihood", &mu_muid_caloLRLikelihood, &b_mu_muid_caloLRLikelihood);
   fChain->SetBranchAddress("mu_muid_bestMatch", &mu_muid_bestMatch, &b_mu_muid_bestMatch);
   fChain->SetBranchAddress("mu_muid_isStandAloneMuon", &mu_muid_isStandAloneMuon, &b_mu_muid_isStandAloneMuon);
   fChain->SetBranchAddress("mu_muid_isCombinedMuon", &mu_muid_isCombinedMuon, &b_mu_muid_isCombinedMuon);
   fChain->SetBranchAddress("mu_muid_isLowPtReconstructedMuon", &mu_muid_isLowPtReconstructedMuon, &b_mu_muid_isLowPtReconstructedMuon);
   fChain->SetBranchAddress("mu_muid_isSegmentTaggedMuon", &mu_muid_isSegmentTaggedMuon, &b_mu_muid_isSegmentTaggedMuon);
   fChain->SetBranchAddress("mu_muid_isCaloMuonId", &mu_muid_isCaloMuonId, &b_mu_muid_isCaloMuonId);
   fChain->SetBranchAddress("mu_muid_alsoFoundByLowPt", &mu_muid_alsoFoundByLowPt, &b_mu_muid_alsoFoundByLowPt);
   fChain->SetBranchAddress("mu_muid_alsoFoundByCaloMuonId", &mu_muid_alsoFoundByCaloMuonId, &b_mu_muid_alsoFoundByCaloMuonId);
   fChain->SetBranchAddress("mu_muid_isSiliconAssociatedForwardMuon", &mu_muid_isSiliconAssociatedForwardMuon, &b_mu_muid_isSiliconAssociatedForwardMuon);
   fChain->SetBranchAddress("mu_muid_loose", &mu_muid_loose, &b_mu_muid_loose);
   fChain->SetBranchAddress("mu_muid_medium", &mu_muid_medium, &b_mu_muid_medium);
   fChain->SetBranchAddress("mu_muid_tight", &mu_muid_tight, &b_mu_muid_tight);
   fChain->SetBranchAddress("mu_muid_d0_exPV", &mu_muid_d0_exPV, &b_mu_muid_d0_exPV);
   fChain->SetBranchAddress("mu_muid_z0_exPV", &mu_muid_z0_exPV, &b_mu_muid_z0_exPV);
   fChain->SetBranchAddress("mu_muid_phi_exPV", &mu_muid_phi_exPV, &b_mu_muid_phi_exPV);
   fChain->SetBranchAddress("mu_muid_theta_exPV", &mu_muid_theta_exPV, &b_mu_muid_theta_exPV);
   fChain->SetBranchAddress("mu_muid_qoverp_exPV", &mu_muid_qoverp_exPV, &b_mu_muid_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_cb_d0_exPV", &mu_muid_cb_d0_exPV, &b_mu_muid_cb_d0_exPV);
   fChain->SetBranchAddress("mu_muid_cb_z0_exPV", &mu_muid_cb_z0_exPV, &b_mu_muid_cb_z0_exPV);
   fChain->SetBranchAddress("mu_muid_cb_phi_exPV", &mu_muid_cb_phi_exPV, &b_mu_muid_cb_phi_exPV);
   fChain->SetBranchAddress("mu_muid_cb_theta_exPV", &mu_muid_cb_theta_exPV, &b_mu_muid_cb_theta_exPV);
   fChain->SetBranchAddress("mu_muid_cb_qoverp_exPV", &mu_muid_cb_qoverp_exPV, &b_mu_muid_cb_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_d0_exPV", &mu_muid_id_d0_exPV, &b_mu_muid_id_d0_exPV);
   fChain->SetBranchAddress("mu_muid_id_z0_exPV", &mu_muid_id_z0_exPV, &b_mu_muid_id_z0_exPV);
   fChain->SetBranchAddress("mu_muid_id_phi_exPV", &mu_muid_id_phi_exPV, &b_mu_muid_id_phi_exPV);
   fChain->SetBranchAddress("mu_muid_id_theta_exPV", &mu_muid_id_theta_exPV, &b_mu_muid_id_theta_exPV);
   fChain->SetBranchAddress("mu_muid_id_qoverp_exPV", &mu_muid_id_qoverp_exPV, &b_mu_muid_id_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_d0_exPV", &mu_muid_me_d0_exPV, &b_mu_muid_me_d0_exPV);
   fChain->SetBranchAddress("mu_muid_me_z0_exPV", &mu_muid_me_z0_exPV, &b_mu_muid_me_z0_exPV);
   fChain->SetBranchAddress("mu_muid_me_phi_exPV", &mu_muid_me_phi_exPV, &b_mu_muid_me_phi_exPV);
   fChain->SetBranchAddress("mu_muid_me_theta_exPV", &mu_muid_me_theta_exPV, &b_mu_muid_me_theta_exPV);
   fChain->SetBranchAddress("mu_muid_me_qoverp_exPV", &mu_muid_me_qoverp_exPV, &b_mu_muid_me_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_ie_d0_exPV", &mu_muid_ie_d0_exPV, &b_mu_muid_ie_d0_exPV);
   fChain->SetBranchAddress("mu_muid_ie_z0_exPV", &mu_muid_ie_z0_exPV, &b_mu_muid_ie_z0_exPV);
   fChain->SetBranchAddress("mu_muid_ie_phi_exPV", &mu_muid_ie_phi_exPV, &b_mu_muid_ie_phi_exPV);
   fChain->SetBranchAddress("mu_muid_ie_theta_exPV", &mu_muid_ie_theta_exPV, &b_mu_muid_ie_theta_exPV);
   fChain->SetBranchAddress("mu_muid_ie_qoverp_exPV", &mu_muid_ie_qoverp_exPV, &b_mu_muid_ie_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_SpaceTime_detID", &mu_muid_SpaceTime_detID, &b_mu_muid_SpaceTime_detID);
   fChain->SetBranchAddress("mu_muid_SpaceTime_t", &mu_muid_SpaceTime_t, &b_mu_muid_SpaceTime_t);
   fChain->SetBranchAddress("mu_muid_SpaceTime_tError", &mu_muid_SpaceTime_tError, &b_mu_muid_SpaceTime_tError);
   fChain->SetBranchAddress("mu_muid_SpaceTime_weight", &mu_muid_SpaceTime_weight, &b_mu_muid_SpaceTime_weight);
   fChain->SetBranchAddress("mu_muid_SpaceTime_x", &mu_muid_SpaceTime_x, &b_mu_muid_SpaceTime_x);
   fChain->SetBranchAddress("mu_muid_SpaceTime_y", &mu_muid_SpaceTime_y, &b_mu_muid_SpaceTime_y);
   fChain->SetBranchAddress("mu_muid_SpaceTime_z", &mu_muid_SpaceTime_z, &b_mu_muid_SpaceTime_z);
   fChain->SetBranchAddress("mu_muid_cov_d0_exPV", &mu_muid_cov_d0_exPV, &b_mu_muid_cov_d0_exPV);
   fChain->SetBranchAddress("mu_muid_cov_z0_exPV", &mu_muid_cov_z0_exPV, &b_mu_muid_cov_z0_exPV);
   fChain->SetBranchAddress("mu_muid_cov_phi_exPV", &mu_muid_cov_phi_exPV, &b_mu_muid_cov_phi_exPV);
   fChain->SetBranchAddress("mu_muid_cov_theta_exPV", &mu_muid_cov_theta_exPV, &b_mu_muid_cov_theta_exPV);
   fChain->SetBranchAddress("mu_muid_cov_qoverp_exPV", &mu_muid_cov_qoverp_exPV, &b_mu_muid_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_cov_d0_z0_exPV", &mu_muid_cov_d0_z0_exPV, &b_mu_muid_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_muid_cov_d0_phi_exPV", &mu_muid_cov_d0_phi_exPV, &b_mu_muid_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_cov_d0_theta_exPV", &mu_muid_cov_d0_theta_exPV, &b_mu_muid_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_cov_d0_qoverp_exPV", &mu_muid_cov_d0_qoverp_exPV, &b_mu_muid_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_cov_z0_phi_exPV", &mu_muid_cov_z0_phi_exPV, &b_mu_muid_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_cov_z0_theta_exPV", &mu_muid_cov_z0_theta_exPV, &b_mu_muid_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_cov_z0_qoverp_exPV", &mu_muid_cov_z0_qoverp_exPV, &b_mu_muid_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_cov_phi_theta_exPV", &mu_muid_cov_phi_theta_exPV, &b_mu_muid_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_muid_cov_phi_qoverp_exPV", &mu_muid_cov_phi_qoverp_exPV, &b_mu_muid_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_cov_theta_qoverp_exPV", &mu_muid_cov_theta_qoverp_exPV, &b_mu_muid_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_d0_exPV", &mu_muid_id_cov_d0_exPV, &b_mu_muid_id_cov_d0_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_z0_exPV", &mu_muid_id_cov_z0_exPV, &b_mu_muid_id_cov_z0_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_phi_exPV", &mu_muid_id_cov_phi_exPV, &b_mu_muid_id_cov_phi_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_theta_exPV", &mu_muid_id_cov_theta_exPV, &b_mu_muid_id_cov_theta_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_qoverp_exPV", &mu_muid_id_cov_qoverp_exPV, &b_mu_muid_id_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_d0_z0_exPV", &mu_muid_id_cov_d0_z0_exPV, &b_mu_muid_id_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_d0_phi_exPV", &mu_muid_id_cov_d0_phi_exPV, &b_mu_muid_id_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_d0_theta_exPV", &mu_muid_id_cov_d0_theta_exPV, &b_mu_muid_id_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_d0_qoverp_exPV", &mu_muid_id_cov_d0_qoverp_exPV, &b_mu_muid_id_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_z0_phi_exPV", &mu_muid_id_cov_z0_phi_exPV, &b_mu_muid_id_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_z0_theta_exPV", &mu_muid_id_cov_z0_theta_exPV, &b_mu_muid_id_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_z0_qoverp_exPV", &mu_muid_id_cov_z0_qoverp_exPV, &b_mu_muid_id_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_phi_theta_exPV", &mu_muid_id_cov_phi_theta_exPV, &b_mu_muid_id_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_phi_qoverp_exPV", &mu_muid_id_cov_phi_qoverp_exPV, &b_mu_muid_id_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_id_cov_theta_qoverp_exPV", &mu_muid_id_cov_theta_qoverp_exPV, &b_mu_muid_id_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_d0_exPV", &mu_muid_me_cov_d0_exPV, &b_mu_muid_me_cov_d0_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_z0_exPV", &mu_muid_me_cov_z0_exPV, &b_mu_muid_me_cov_z0_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_phi_exPV", &mu_muid_me_cov_phi_exPV, &b_mu_muid_me_cov_phi_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_theta_exPV", &mu_muid_me_cov_theta_exPV, &b_mu_muid_me_cov_theta_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_qoverp_exPV", &mu_muid_me_cov_qoverp_exPV, &b_mu_muid_me_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_d0_z0_exPV", &mu_muid_me_cov_d0_z0_exPV, &b_mu_muid_me_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_d0_phi_exPV", &mu_muid_me_cov_d0_phi_exPV, &b_mu_muid_me_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_d0_theta_exPV", &mu_muid_me_cov_d0_theta_exPV, &b_mu_muid_me_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_d0_qoverp_exPV", &mu_muid_me_cov_d0_qoverp_exPV, &b_mu_muid_me_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_z0_phi_exPV", &mu_muid_me_cov_z0_phi_exPV, &b_mu_muid_me_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_z0_theta_exPV", &mu_muid_me_cov_z0_theta_exPV, &b_mu_muid_me_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_z0_qoverp_exPV", &mu_muid_me_cov_z0_qoverp_exPV, &b_mu_muid_me_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_phi_theta_exPV", &mu_muid_me_cov_phi_theta_exPV, &b_mu_muid_me_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_phi_qoverp_exPV", &mu_muid_me_cov_phi_qoverp_exPV, &b_mu_muid_me_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_me_cov_theta_qoverp_exPV", &mu_muid_me_cov_theta_qoverp_exPV, &b_mu_muid_me_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_muid_ms_d0", &mu_muid_ms_d0, &b_mu_muid_ms_d0);
   fChain->SetBranchAddress("mu_muid_ms_z0", &mu_muid_ms_z0, &b_mu_muid_ms_z0);
   fChain->SetBranchAddress("mu_muid_ms_phi", &mu_muid_ms_phi, &b_mu_muid_ms_phi);
   fChain->SetBranchAddress("mu_muid_ms_theta", &mu_muid_ms_theta, &b_mu_muid_ms_theta);
   fChain->SetBranchAddress("mu_muid_ms_qoverp", &mu_muid_ms_qoverp, &b_mu_muid_ms_qoverp);
   fChain->SetBranchAddress("mu_muid_id_d0", &mu_muid_id_d0, &b_mu_muid_id_d0);
   fChain->SetBranchAddress("mu_muid_id_z0", &mu_muid_id_z0, &b_mu_muid_id_z0);
   fChain->SetBranchAddress("mu_muid_id_phi", &mu_muid_id_phi, &b_mu_muid_id_phi);
   fChain->SetBranchAddress("mu_muid_id_theta", &mu_muid_id_theta, &b_mu_muid_id_theta);
   fChain->SetBranchAddress("mu_muid_id_qoverp", &mu_muid_id_qoverp, &b_mu_muid_id_qoverp);
   fChain->SetBranchAddress("mu_muid_me_d0", &mu_muid_me_d0, &b_mu_muid_me_d0);
   fChain->SetBranchAddress("mu_muid_me_z0", &mu_muid_me_z0, &b_mu_muid_me_z0);
   fChain->SetBranchAddress("mu_muid_me_phi", &mu_muid_me_phi, &b_mu_muid_me_phi);
   fChain->SetBranchAddress("mu_muid_me_theta", &mu_muid_me_theta, &b_mu_muid_me_theta);
   fChain->SetBranchAddress("mu_muid_me_qoverp", &mu_muid_me_qoverp, &b_mu_muid_me_qoverp);
   fChain->SetBranchAddress("mu_muid_ie_d0", &mu_muid_ie_d0, &b_mu_muid_ie_d0);
   fChain->SetBranchAddress("mu_muid_ie_z0", &mu_muid_ie_z0, &b_mu_muid_ie_z0);
   fChain->SetBranchAddress("mu_muid_ie_phi", &mu_muid_ie_phi, &b_mu_muid_ie_phi);
   fChain->SetBranchAddress("mu_muid_ie_theta", &mu_muid_ie_theta, &b_mu_muid_ie_theta);
   fChain->SetBranchAddress("mu_muid_ie_qoverp", &mu_muid_ie_qoverp, &b_mu_muid_ie_qoverp);
   fChain->SetBranchAddress("mu_muid_nOutliersOnTrack", &mu_muid_nOutliersOnTrack, &b_mu_muid_nOutliersOnTrack);
   fChain->SetBranchAddress("mu_muid_nBLHits", &mu_muid_nBLHits, &b_mu_muid_nBLHits);
   fChain->SetBranchAddress("mu_muid_nPixHits", &mu_muid_nPixHits, &b_mu_muid_nPixHits);
   fChain->SetBranchAddress("mu_muid_nSCTHits", &mu_muid_nSCTHits, &b_mu_muid_nSCTHits);
   fChain->SetBranchAddress("mu_muid_nTRTHits", &mu_muid_nTRTHits, &b_mu_muid_nTRTHits);
   fChain->SetBranchAddress("mu_muid_nTRTHighTHits", &mu_muid_nTRTHighTHits, &b_mu_muid_nTRTHighTHits);
   fChain->SetBranchAddress("mu_muid_nBLSharedHits", &mu_muid_nBLSharedHits, &b_mu_muid_nBLSharedHits);
   fChain->SetBranchAddress("mu_muid_nPixSharedHits", &mu_muid_nPixSharedHits, &b_mu_muid_nPixSharedHits);
   fChain->SetBranchAddress("mu_muid_nPixHoles", &mu_muid_nPixHoles, &b_mu_muid_nPixHoles);
   fChain->SetBranchAddress("mu_muid_nSCTSharedHits", &mu_muid_nSCTSharedHits, &b_mu_muid_nSCTSharedHits);
   fChain->SetBranchAddress("mu_muid_nSCTHoles", &mu_muid_nSCTHoles, &b_mu_muid_nSCTHoles);
   fChain->SetBranchAddress("mu_muid_nTRTOutliers", &mu_muid_nTRTOutliers, &b_mu_muid_nTRTOutliers);
   fChain->SetBranchAddress("mu_muid_nTRTHighTOutliers", &mu_muid_nTRTHighTOutliers, &b_mu_muid_nTRTHighTOutliers);
   fChain->SetBranchAddress("mu_muid_nGangedPixels", &mu_muid_nGangedPixels, &b_mu_muid_nGangedPixels);
   fChain->SetBranchAddress("mu_muid_nPixelDeadSensors", &mu_muid_nPixelDeadSensors, &b_mu_muid_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_muid_nSCTDeadSensors", &mu_muid_nSCTDeadSensors, &b_mu_muid_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_muid_nTRTDeadStraws", &mu_muid_nTRTDeadStraws, &b_mu_muid_nTRTDeadStraws);
   fChain->SetBranchAddress("mu_muid_expectBLayerHit", &mu_muid_expectBLayerHit, &b_mu_muid_expectBLayerHit);
   fChain->SetBranchAddress("mu_muid_nMDTHits", &mu_muid_nMDTHits, &b_mu_muid_nMDTHits);
   fChain->SetBranchAddress("mu_muid_nMDTHoles", &mu_muid_nMDTHoles, &b_mu_muid_nMDTHoles);
   fChain->SetBranchAddress("mu_muid_nCSCEtaHits", &mu_muid_nCSCEtaHits, &b_mu_muid_nCSCEtaHits);
   fChain->SetBranchAddress("mu_muid_nCSCEtaHoles", &mu_muid_nCSCEtaHoles, &b_mu_muid_nCSCEtaHoles);
   fChain->SetBranchAddress("mu_muid_nCSCUnspoiledEtaHits", &mu_muid_nCSCUnspoiledEtaHits, &b_mu_muid_nCSCUnspoiledEtaHits);
   fChain->SetBranchAddress("mu_muid_nCSCPhiHits", &mu_muid_nCSCPhiHits, &b_mu_muid_nCSCPhiHits);
   fChain->SetBranchAddress("mu_muid_nCSCPhiHoles", &mu_muid_nCSCPhiHoles, &b_mu_muid_nCSCPhiHoles);
   fChain->SetBranchAddress("mu_muid_nRPCEtaHits", &mu_muid_nRPCEtaHits, &b_mu_muid_nRPCEtaHits);
   fChain->SetBranchAddress("mu_muid_nRPCEtaHoles", &mu_muid_nRPCEtaHoles, &b_mu_muid_nRPCEtaHoles);
   fChain->SetBranchAddress("mu_muid_nRPCPhiHits", &mu_muid_nRPCPhiHits, &b_mu_muid_nRPCPhiHits);
   fChain->SetBranchAddress("mu_muid_nRPCPhiHoles", &mu_muid_nRPCPhiHoles, &b_mu_muid_nRPCPhiHoles);
   fChain->SetBranchAddress("mu_muid_nTGCEtaHits", &mu_muid_nTGCEtaHits, &b_mu_muid_nTGCEtaHits);
   fChain->SetBranchAddress("mu_muid_nTGCEtaHoles", &mu_muid_nTGCEtaHoles, &b_mu_muid_nTGCEtaHoles);
   fChain->SetBranchAddress("mu_muid_nTGCPhiHits", &mu_muid_nTGCPhiHits, &b_mu_muid_nTGCPhiHits);
   fChain->SetBranchAddress("mu_muid_nTGCPhiHoles", &mu_muid_nTGCPhiHoles, &b_mu_muid_nTGCPhiHoles);
   fChain->SetBranchAddress("mu_muid_nprecisionLayers", &mu_muid_nprecisionLayers, &b_mu_muid_nprecisionLayers);
   fChain->SetBranchAddress("mu_muid_nprecisionHoleLayers", &mu_muid_nprecisionHoleLayers, &b_mu_muid_nprecisionHoleLayers);
   fChain->SetBranchAddress("mu_muid_nphiLayers", &mu_muid_nphiLayers, &b_mu_muid_nphiLayers);
   fChain->SetBranchAddress("mu_muid_ntrigEtaLayers", &mu_muid_ntrigEtaLayers, &b_mu_muid_ntrigEtaLayers);
   fChain->SetBranchAddress("mu_muid_nphiHoleLayers", &mu_muid_nphiHoleLayers, &b_mu_muid_nphiHoleLayers);
   fChain->SetBranchAddress("mu_muid_ntrigEtaHoleLayers", &mu_muid_ntrigEtaHoleLayers, &b_mu_muid_ntrigEtaHoleLayers);
   fChain->SetBranchAddress("mu_muid_nMDTBIHits", &mu_muid_nMDTBIHits, &b_mu_muid_nMDTBIHits);
   fChain->SetBranchAddress("mu_muid_nMDTBMHits", &mu_muid_nMDTBMHits, &b_mu_muid_nMDTBMHits);
   fChain->SetBranchAddress("mu_muid_nMDTBOHits", &mu_muid_nMDTBOHits, &b_mu_muid_nMDTBOHits);
   fChain->SetBranchAddress("mu_muid_nMDTBEEHits", &mu_muid_nMDTBEEHits, &b_mu_muid_nMDTBEEHits);
   fChain->SetBranchAddress("mu_muid_nMDTBIS78Hits", &mu_muid_nMDTBIS78Hits, &b_mu_muid_nMDTBIS78Hits);
   fChain->SetBranchAddress("mu_muid_nMDTEIHits", &mu_muid_nMDTEIHits, &b_mu_muid_nMDTEIHits);
   fChain->SetBranchAddress("mu_muid_nMDTEMHits", &mu_muid_nMDTEMHits, &b_mu_muid_nMDTEMHits);
   fChain->SetBranchAddress("mu_muid_nMDTEOHits", &mu_muid_nMDTEOHits, &b_mu_muid_nMDTEOHits);
   fChain->SetBranchAddress("mu_muid_nMDTEEHits", &mu_muid_nMDTEEHits, &b_mu_muid_nMDTEEHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer1EtaHits", &mu_muid_nRPCLayer1EtaHits, &b_mu_muid_nRPCLayer1EtaHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer2EtaHits", &mu_muid_nRPCLayer2EtaHits, &b_mu_muid_nRPCLayer2EtaHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer3EtaHits", &mu_muid_nRPCLayer3EtaHits, &b_mu_muid_nRPCLayer3EtaHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer1PhiHits", &mu_muid_nRPCLayer1PhiHits, &b_mu_muid_nRPCLayer1PhiHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer2PhiHits", &mu_muid_nRPCLayer2PhiHits, &b_mu_muid_nRPCLayer2PhiHits);
   fChain->SetBranchAddress("mu_muid_nRPCLayer3PhiHits", &mu_muid_nRPCLayer3PhiHits, &b_mu_muid_nRPCLayer3PhiHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer1EtaHits", &mu_muid_nTGCLayer1EtaHits, &b_mu_muid_nTGCLayer1EtaHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer2EtaHits", &mu_muid_nTGCLayer2EtaHits, &b_mu_muid_nTGCLayer2EtaHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer3EtaHits", &mu_muid_nTGCLayer3EtaHits, &b_mu_muid_nTGCLayer3EtaHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer4EtaHits", &mu_muid_nTGCLayer4EtaHits, &b_mu_muid_nTGCLayer4EtaHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer1PhiHits", &mu_muid_nTGCLayer1PhiHits, &b_mu_muid_nTGCLayer1PhiHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer2PhiHits", &mu_muid_nTGCLayer2PhiHits, &b_mu_muid_nTGCLayer2PhiHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer3PhiHits", &mu_muid_nTGCLayer3PhiHits, &b_mu_muid_nTGCLayer3PhiHits);
   fChain->SetBranchAddress("mu_muid_nTGCLayer4PhiHits", &mu_muid_nTGCLayer4PhiHits, &b_mu_muid_nTGCLayer4PhiHits);
   fChain->SetBranchAddress("mu_muid_barrelSectors", &mu_muid_barrelSectors, &b_mu_muid_barrelSectors);
   fChain->SetBranchAddress("mu_muid_endcapSectors", &mu_muid_endcapSectors, &b_mu_muid_endcapSectors);
   fChain->SetBranchAddress("mu_muid_spec_surf_px", &mu_muid_spec_surf_px, &b_mu_muid_spec_surf_px);
   fChain->SetBranchAddress("mu_muid_spec_surf_py", &mu_muid_spec_surf_py, &b_mu_muid_spec_surf_py);
   fChain->SetBranchAddress("mu_muid_spec_surf_pz", &mu_muid_spec_surf_pz, &b_mu_muid_spec_surf_pz);
   fChain->SetBranchAddress("mu_muid_spec_surf_x", &mu_muid_spec_surf_x, &b_mu_muid_spec_surf_x);
   fChain->SetBranchAddress("mu_muid_spec_surf_y", &mu_muid_spec_surf_y, &b_mu_muid_spec_surf_y);
   fChain->SetBranchAddress("mu_muid_spec_surf_z", &mu_muid_spec_surf_z, &b_mu_muid_spec_surf_z);
   fChain->SetBranchAddress("mu_muid_trackd0", &mu_muid_trackd0, &b_mu_muid_trackd0);
   fChain->SetBranchAddress("mu_muid_trackz0", &mu_muid_trackz0, &b_mu_muid_trackz0);
   fChain->SetBranchAddress("mu_muid_trackphi", &mu_muid_trackphi, &b_mu_muid_trackphi);
   fChain->SetBranchAddress("mu_muid_tracktheta", &mu_muid_tracktheta, &b_mu_muid_tracktheta);
   fChain->SetBranchAddress("mu_muid_trackqoverp", &mu_muid_trackqoverp, &b_mu_muid_trackqoverp);
   fChain->SetBranchAddress("mu_muid_trackcov_d0", &mu_muid_trackcov_d0, &b_mu_muid_trackcov_d0);
   fChain->SetBranchAddress("mu_muid_trackcov_z0", &mu_muid_trackcov_z0, &b_mu_muid_trackcov_z0);
   fChain->SetBranchAddress("mu_muid_trackcov_phi", &mu_muid_trackcov_phi, &b_mu_muid_trackcov_phi);
   fChain->SetBranchAddress("mu_muid_trackcov_theta", &mu_muid_trackcov_theta, &b_mu_muid_trackcov_theta);
   fChain->SetBranchAddress("mu_muid_trackcov_qoverp", &mu_muid_trackcov_qoverp, &b_mu_muid_trackcov_qoverp);
   fChain->SetBranchAddress("mu_muid_trackcov_d0_z0", &mu_muid_trackcov_d0_z0, &b_mu_muid_trackcov_d0_z0);
   fChain->SetBranchAddress("mu_muid_trackcov_d0_phi", &mu_muid_trackcov_d0_phi, &b_mu_muid_trackcov_d0_phi);
   fChain->SetBranchAddress("mu_muid_trackcov_d0_theta", &mu_muid_trackcov_d0_theta, &b_mu_muid_trackcov_d0_theta);
   fChain->SetBranchAddress("mu_muid_trackcov_d0_qoverp", &mu_muid_trackcov_d0_qoverp, &b_mu_muid_trackcov_d0_qoverp);
   fChain->SetBranchAddress("mu_muid_trackcov_z0_phi", &mu_muid_trackcov_z0_phi, &b_mu_muid_trackcov_z0_phi);
   fChain->SetBranchAddress("mu_muid_trackcov_z0_theta", &mu_muid_trackcov_z0_theta, &b_mu_muid_trackcov_z0_theta);
   fChain->SetBranchAddress("mu_muid_trackcov_z0_qoverp", &mu_muid_trackcov_z0_qoverp, &b_mu_muid_trackcov_z0_qoverp);
   fChain->SetBranchAddress("mu_muid_trackcov_phi_theta", &mu_muid_trackcov_phi_theta, &b_mu_muid_trackcov_phi_theta);
   fChain->SetBranchAddress("mu_muid_trackcov_phi_qoverp", &mu_muid_trackcov_phi_qoverp, &b_mu_muid_trackcov_phi_qoverp);
   fChain->SetBranchAddress("mu_muid_trackcov_theta_qoverp", &mu_muid_trackcov_theta_qoverp, &b_mu_muid_trackcov_theta_qoverp);
   fChain->SetBranchAddress("mu_muid_trackfitchi2", &mu_muid_trackfitchi2, &b_mu_muid_trackfitchi2);
   fChain->SetBranchAddress("mu_muid_trackfitndof", &mu_muid_trackfitndof, &b_mu_muid_trackfitndof);
   fChain->SetBranchAddress("mu_muid_hastrack", &mu_muid_hastrack, &b_mu_muid_hastrack);
   fChain->SetBranchAddress("mu_muid_trackd0beam", &mu_muid_trackd0beam, &b_mu_muid_trackd0beam);
   fChain->SetBranchAddress("mu_muid_trackz0beam", &mu_muid_trackz0beam, &b_mu_muid_trackz0beam);
   fChain->SetBranchAddress("mu_muid_tracksigd0beam", &mu_muid_tracksigd0beam, &b_mu_muid_tracksigd0beam);
   fChain->SetBranchAddress("mu_muid_tracksigz0beam", &mu_muid_tracksigz0beam, &b_mu_muid_tracksigz0beam);
   fChain->SetBranchAddress("mu_muid_trackd0pv", &mu_muid_trackd0pv, &b_mu_muid_trackd0pv);
   fChain->SetBranchAddress("mu_muid_trackz0pv", &mu_muid_trackz0pv, &b_mu_muid_trackz0pv);
   fChain->SetBranchAddress("mu_muid_tracksigd0pv", &mu_muid_tracksigd0pv, &b_mu_muid_tracksigd0pv);
   fChain->SetBranchAddress("mu_muid_tracksigz0pv", &mu_muid_tracksigz0pv, &b_mu_muid_tracksigz0pv);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_d0_biasedpvunbiased", &mu_muid_trackIPEstimate_d0_biasedpvunbiased, &b_mu_muid_trackIPEstimate_d0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_z0_biasedpvunbiased", &mu_muid_trackIPEstimate_z0_biasedpvunbiased, &b_mu_muid_trackIPEstimate_z0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_sigd0_biasedpvunbiased", &mu_muid_trackIPEstimate_sigd0_biasedpvunbiased, &b_mu_muid_trackIPEstimate_sigd0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_sigz0_biasedpvunbiased", &mu_muid_trackIPEstimate_sigz0_biasedpvunbiased, &b_mu_muid_trackIPEstimate_sigz0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_d0_unbiasedpvunbiased", &mu_muid_trackIPEstimate_d0_unbiasedpvunbiased, &b_mu_muid_trackIPEstimate_d0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_z0_unbiasedpvunbiased", &mu_muid_trackIPEstimate_z0_unbiasedpvunbiased, &b_mu_muid_trackIPEstimate_z0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased", &mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased, &b_mu_muid_trackIPEstimate_sigd0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased", &mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased, &b_mu_muid_trackIPEstimate_sigz0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_muid_trackd0pvunbiased", &mu_muid_trackd0pvunbiased, &b_mu_muid_trackd0pvunbiased);
   fChain->SetBranchAddress("mu_muid_trackz0pvunbiased", &mu_muid_trackz0pvunbiased, &b_mu_muid_trackz0pvunbiased);
   fChain->SetBranchAddress("mu_muid_tracksigd0pvunbiased", &mu_muid_tracksigd0pvunbiased, &b_mu_muid_tracksigd0pvunbiased);
   fChain->SetBranchAddress("mu_muid_tracksigz0pvunbiased", &mu_muid_tracksigz0pvunbiased, &b_mu_muid_tracksigz0pvunbiased);
   fChain->SetBranchAddress("mu_muid_type", &mu_muid_type, &b_mu_muid_type);
   fChain->SetBranchAddress("mu_muid_origin", &mu_muid_origin, &b_mu_muid_origin);
   fChain->SetBranchAddress("mu_muid_truth_dr", &mu_muid_truth_dr, &b_mu_muid_truth_dr);
   fChain->SetBranchAddress("mu_muid_truth_E", &mu_muid_truth_E, &b_mu_muid_truth_E);
   fChain->SetBranchAddress("mu_muid_truth_pt", &mu_muid_truth_pt, &b_mu_muid_truth_pt);
   fChain->SetBranchAddress("mu_muid_truth_eta", &mu_muid_truth_eta, &b_mu_muid_truth_eta);
   fChain->SetBranchAddress("mu_muid_truth_phi", &mu_muid_truth_phi, &b_mu_muid_truth_phi);
   fChain->SetBranchAddress("mu_muid_truth_type", &mu_muid_truth_type, &b_mu_muid_truth_type);
   fChain->SetBranchAddress("mu_muid_truth_status", &mu_muid_truth_status, &b_mu_muid_truth_status);
   fChain->SetBranchAddress("mu_muid_truth_barcode", &mu_muid_truth_barcode, &b_mu_muid_truth_barcode);
   fChain->SetBranchAddress("mu_muid_truth_mothertype", &mu_muid_truth_mothertype, &b_mu_muid_truth_mothertype);
   fChain->SetBranchAddress("mu_muid_truth_motherbarcode", &mu_muid_truth_motherbarcode, &b_mu_muid_truth_motherbarcode);
   fChain->SetBranchAddress("mu_muid_truth_matched", &mu_muid_truth_matched, &b_mu_muid_truth_matched);
   fChain->SetBranchAddress("mu_muid_EFCB_dr", &mu_muid_EFCB_dr, &b_mu_muid_EFCB_dr);
   fChain->SetBranchAddress("mu_muid_EFCB_n", &mu_muid_EFCB_n, &b_mu_muid_EFCB_n);
   fChain->SetBranchAddress("mu_muid_EFCB_MuonType", &mu_muid_EFCB_MuonType, &b_mu_muid_EFCB_MuonType);
   fChain->SetBranchAddress("mu_muid_EFCB_pt", &mu_muid_EFCB_pt, &b_mu_muid_EFCB_pt);
   fChain->SetBranchAddress("mu_muid_EFCB_eta", &mu_muid_EFCB_eta, &b_mu_muid_EFCB_eta);
   fChain->SetBranchAddress("mu_muid_EFCB_phi", &mu_muid_EFCB_phi, &b_mu_muid_EFCB_phi);
   fChain->SetBranchAddress("mu_muid_EFCB_hasCB", &mu_muid_EFCB_hasCB, &b_mu_muid_EFCB_hasCB);
   fChain->SetBranchAddress("mu_muid_EFCB_matched", &mu_muid_EFCB_matched, &b_mu_muid_EFCB_matched);
   fChain->SetBranchAddress("mu_muid_EFMG_dr", &mu_muid_EFMG_dr, &b_mu_muid_EFMG_dr);
   fChain->SetBranchAddress("mu_muid_EFMG_n", &mu_muid_EFMG_n, &b_mu_muid_EFMG_n);
   fChain->SetBranchAddress("mu_muid_EFMG_MuonType", &mu_muid_EFMG_MuonType, &b_mu_muid_EFMG_MuonType);
   fChain->SetBranchAddress("mu_muid_EFMG_pt", &mu_muid_EFMG_pt, &b_mu_muid_EFMG_pt);
   fChain->SetBranchAddress("mu_muid_EFMG_eta", &mu_muid_EFMG_eta, &b_mu_muid_EFMG_eta);
   fChain->SetBranchAddress("mu_muid_EFMG_phi", &mu_muid_EFMG_phi, &b_mu_muid_EFMG_phi);
   fChain->SetBranchAddress("mu_muid_EFMG_hasMG", &mu_muid_EFMG_hasMG, &b_mu_muid_EFMG_hasMG);
   fChain->SetBranchAddress("mu_muid_EFMG_matched", &mu_muid_EFMG_matched, &b_mu_muid_EFMG_matched);
   fChain->SetBranchAddress("mu_muid_EFME_dr", &mu_muid_EFME_dr, &b_mu_muid_EFME_dr);
   fChain->SetBranchAddress("mu_muid_EFME_n", &mu_muid_EFME_n, &b_mu_muid_EFME_n);
   fChain->SetBranchAddress("mu_muid_EFME_MuonType", &mu_muid_EFME_MuonType, &b_mu_muid_EFME_MuonType);
   fChain->SetBranchAddress("mu_muid_EFME_pt", &mu_muid_EFME_pt, &b_mu_muid_EFME_pt);
   fChain->SetBranchAddress("mu_muid_EFME_eta", &mu_muid_EFME_eta, &b_mu_muid_EFME_eta);
   fChain->SetBranchAddress("mu_muid_EFME_phi", &mu_muid_EFME_phi, &b_mu_muid_EFME_phi);
   fChain->SetBranchAddress("mu_muid_EFME_hasME", &mu_muid_EFME_hasME, &b_mu_muid_EFME_hasME);
   fChain->SetBranchAddress("mu_muid_EFME_matched", &mu_muid_EFME_matched, &b_mu_muid_EFME_matched);
   fChain->SetBranchAddress("mu_muid_L2CB_dr", &mu_muid_L2CB_dr, &b_mu_muid_L2CB_dr);
   fChain->SetBranchAddress("mu_muid_L2CB_pt", &mu_muid_L2CB_pt, &b_mu_muid_L2CB_pt);
   fChain->SetBranchAddress("mu_muid_L2CB_eta", &mu_muid_L2CB_eta, &b_mu_muid_L2CB_eta);
   fChain->SetBranchAddress("mu_muid_L2CB_phi", &mu_muid_L2CB_phi, &b_mu_muid_L2CB_phi);
   fChain->SetBranchAddress("mu_muid_L2CB_id_pt", &mu_muid_L2CB_id_pt, &b_mu_muid_L2CB_id_pt);
   fChain->SetBranchAddress("mu_muid_L2CB_ms_pt", &mu_muid_L2CB_ms_pt, &b_mu_muid_L2CB_ms_pt);
   fChain->SetBranchAddress("mu_muid_L2CB_nPixHits", &mu_muid_L2CB_nPixHits, &b_mu_muid_L2CB_nPixHits);
   fChain->SetBranchAddress("mu_muid_L2CB_nSCTHits", &mu_muid_L2CB_nSCTHits, &b_mu_muid_L2CB_nSCTHits);
   fChain->SetBranchAddress("mu_muid_L2CB_nTRTHits", &mu_muid_L2CB_nTRTHits, &b_mu_muid_L2CB_nTRTHits);
   fChain->SetBranchAddress("mu_muid_L2CB_nTRTHighTHits", &mu_muid_L2CB_nTRTHighTHits, &b_mu_muid_L2CB_nTRTHighTHits);
   fChain->SetBranchAddress("mu_muid_L2CB_matched", &mu_muid_L2CB_matched, &b_mu_muid_L2CB_matched);
   fChain->SetBranchAddress("mu_muid_L1_dr", &mu_muid_L1_dr, &b_mu_muid_L1_dr);
   fChain->SetBranchAddress("mu_muid_L1_pt", &mu_muid_L1_pt, &b_mu_muid_L1_pt);
   fChain->SetBranchAddress("mu_muid_L1_eta", &mu_muid_L1_eta, &b_mu_muid_L1_eta);
   fChain->SetBranchAddress("mu_muid_L1_phi", &mu_muid_L1_phi, &b_mu_muid_L1_phi);
   fChain->SetBranchAddress("mu_muid_L1_thrNumber", &mu_muid_L1_thrNumber, &b_mu_muid_L1_thrNumber);
   fChain->SetBranchAddress("mu_muid_L1_RoINumber", &mu_muid_L1_RoINumber, &b_mu_muid_L1_RoINumber);
   fChain->SetBranchAddress("mu_muid_L1_sectorAddress", &mu_muid_L1_sectorAddress, &b_mu_muid_L1_sectorAddress);
   fChain->SetBranchAddress("mu_muid_L1_firstCandidate", &mu_muid_L1_firstCandidate, &b_mu_muid_L1_firstCandidate);
   fChain->SetBranchAddress("mu_muid_L1_moreCandInRoI", &mu_muid_L1_moreCandInRoI, &b_mu_muid_L1_moreCandInRoI);
   fChain->SetBranchAddress("mu_muid_L1_moreCandInSector", &mu_muid_L1_moreCandInSector, &b_mu_muid_L1_moreCandInSector);
   fChain->SetBranchAddress("mu_muid_L1_source", &mu_muid_L1_source, &b_mu_muid_L1_source);
   fChain->SetBranchAddress("mu_muid_L1_hemisphere", &mu_muid_L1_hemisphere, &b_mu_muid_L1_hemisphere);
   fChain->SetBranchAddress("mu_muid_L1_charge", &mu_muid_L1_charge, &b_mu_muid_L1_charge);
   fChain->SetBranchAddress("mu_muid_L1_vetoed", &mu_muid_L1_vetoed, &b_mu_muid_L1_vetoed);
   fChain->SetBranchAddress("mu_muid_L1_matched", &mu_muid_L1_matched, &b_mu_muid_L1_matched);
   fChain->SetBranchAddress("mu_staco_n", &mu_staco_n, &b_mu_staco_n);
   fChain->SetBranchAddress("mu_staco_E", &mu_staco_E, &b_mu_staco_E);
   fChain->SetBranchAddress("mu_staco_pt", &mu_staco_pt, &b_mu_staco_pt);
   fChain->SetBranchAddress("mu_staco_m", &mu_staco_m, &b_mu_staco_m);
   fChain->SetBranchAddress("mu_staco_eta", &mu_staco_eta, &b_mu_staco_eta);
   fChain->SetBranchAddress("mu_staco_phi", &mu_staco_phi, &b_mu_staco_phi);
   fChain->SetBranchAddress("mu_staco_px", &mu_staco_px, &b_mu_staco_px);
   fChain->SetBranchAddress("mu_staco_py", &mu_staco_py, &b_mu_staco_py);
   fChain->SetBranchAddress("mu_staco_pz", &mu_staco_pz, &b_mu_staco_pz);
   fChain->SetBranchAddress("mu_staco_charge", &mu_staco_charge, &b_mu_staco_charge);
   fChain->SetBranchAddress("mu_staco_allauthor", &mu_staco_allauthor, &b_mu_staco_allauthor);
   fChain->SetBranchAddress("mu_staco_author", &mu_staco_author, &b_mu_staco_author);
   fChain->SetBranchAddress("mu_staco_beta", &mu_staco_beta, &b_mu_staco_beta);
   fChain->SetBranchAddress("mu_staco_isMuonLikelihood", &mu_staco_isMuonLikelihood, &b_mu_staco_isMuonLikelihood);
   fChain->SetBranchAddress("mu_staco_matchchi2", &mu_staco_matchchi2, &b_mu_staco_matchchi2);
   fChain->SetBranchAddress("mu_staco_matchndof", &mu_staco_matchndof, &b_mu_staco_matchndof);
   fChain->SetBranchAddress("mu_staco_etcone20", &mu_staco_etcone20, &b_mu_staco_etcone20);
   fChain->SetBranchAddress("mu_staco_etcone30", &mu_staco_etcone30, &b_mu_staco_etcone30);
   fChain->SetBranchAddress("mu_staco_etcone40", &mu_staco_etcone40, &b_mu_staco_etcone40);
   fChain->SetBranchAddress("mu_staco_nucone20", &mu_staco_nucone20, &b_mu_staco_nucone20);
   fChain->SetBranchAddress("mu_staco_nucone30", &mu_staco_nucone30, &b_mu_staco_nucone30);
   fChain->SetBranchAddress("mu_staco_nucone40", &mu_staco_nucone40, &b_mu_staco_nucone40);
   fChain->SetBranchAddress("mu_staco_ptcone20", &mu_staco_ptcone20, &b_mu_staco_ptcone20);
   fChain->SetBranchAddress("mu_staco_ptcone30", &mu_staco_ptcone30, &b_mu_staco_ptcone30);
   fChain->SetBranchAddress("mu_staco_ptcone40", &mu_staco_ptcone40, &b_mu_staco_ptcone40);
   fChain->SetBranchAddress("mu_staco_etconeNoEm10", &mu_staco_etconeNoEm10, &b_mu_staco_etconeNoEm10);
   fChain->SetBranchAddress("mu_staco_etconeNoEm20", &mu_staco_etconeNoEm20, &b_mu_staco_etconeNoEm20);
   fChain->SetBranchAddress("mu_staco_etconeNoEm30", &mu_staco_etconeNoEm30, &b_mu_staco_etconeNoEm30);
   fChain->SetBranchAddress("mu_staco_etconeNoEm40", &mu_staco_etconeNoEm40, &b_mu_staco_etconeNoEm40);
   fChain->SetBranchAddress("mu_staco_scatteringCurvatureSignificance", &mu_staco_scatteringCurvatureSignificance, &b_mu_staco_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("mu_staco_scatteringNeighbourSignificance", &mu_staco_scatteringNeighbourSignificance, &b_mu_staco_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("mu_staco_momentumBalanceSignificance", &mu_staco_momentumBalanceSignificance, &b_mu_staco_momentumBalanceSignificance);
   fChain->SetBranchAddress("mu_staco_energyLossPar", &mu_staco_energyLossPar, &b_mu_staco_energyLossPar);
   fChain->SetBranchAddress("mu_staco_energyLossErr", &mu_staco_energyLossErr, &b_mu_staco_energyLossErr);
   fChain->SetBranchAddress("mu_staco_etCore", &mu_staco_etCore, &b_mu_staco_etCore);
   fChain->SetBranchAddress("mu_staco_energyLossType", &mu_staco_energyLossType, &b_mu_staco_energyLossType);
   fChain->SetBranchAddress("mu_staco_caloMuonIdTag", &mu_staco_caloMuonIdTag, &b_mu_staco_caloMuonIdTag);
   fChain->SetBranchAddress("mu_staco_caloLRLikelihood", &mu_staco_caloLRLikelihood, &b_mu_staco_caloLRLikelihood);
   fChain->SetBranchAddress("mu_staco_bestMatch", &mu_staco_bestMatch, &b_mu_staco_bestMatch);
   fChain->SetBranchAddress("mu_staco_isStandAloneMuon", &mu_staco_isStandAloneMuon, &b_mu_staco_isStandAloneMuon);
   fChain->SetBranchAddress("mu_staco_isCombinedMuon", &mu_staco_isCombinedMuon, &b_mu_staco_isCombinedMuon);
   fChain->SetBranchAddress("mu_staco_isLowPtReconstructedMuon", &mu_staco_isLowPtReconstructedMuon, &b_mu_staco_isLowPtReconstructedMuon);
   fChain->SetBranchAddress("mu_staco_isSegmentTaggedMuon", &mu_staco_isSegmentTaggedMuon, &b_mu_staco_isSegmentTaggedMuon);
   fChain->SetBranchAddress("mu_staco_isCaloMuonId", &mu_staco_isCaloMuonId, &b_mu_staco_isCaloMuonId);
   fChain->SetBranchAddress("mu_staco_alsoFoundByLowPt", &mu_staco_alsoFoundByLowPt, &b_mu_staco_alsoFoundByLowPt);
   fChain->SetBranchAddress("mu_staco_alsoFoundByCaloMuonId", &mu_staco_alsoFoundByCaloMuonId, &b_mu_staco_alsoFoundByCaloMuonId);
   fChain->SetBranchAddress("mu_staco_isSiliconAssociatedForwardMuon", &mu_staco_isSiliconAssociatedForwardMuon, &b_mu_staco_isSiliconAssociatedForwardMuon);
   fChain->SetBranchAddress("mu_staco_loose", &mu_staco_loose, &b_mu_staco_loose);
   fChain->SetBranchAddress("mu_staco_medium", &mu_staco_medium, &b_mu_staco_medium);
   fChain->SetBranchAddress("mu_staco_tight", &mu_staco_tight, &b_mu_staco_tight);
   fChain->SetBranchAddress("mu_staco_d0_exPV", &mu_staco_d0_exPV, &b_mu_staco_d0_exPV);
   fChain->SetBranchAddress("mu_staco_z0_exPV", &mu_staco_z0_exPV, &b_mu_staco_z0_exPV);
   fChain->SetBranchAddress("mu_staco_phi_exPV", &mu_staco_phi_exPV, &b_mu_staco_phi_exPV);
   fChain->SetBranchAddress("mu_staco_theta_exPV", &mu_staco_theta_exPV, &b_mu_staco_theta_exPV);
   fChain->SetBranchAddress("mu_staco_qoverp_exPV", &mu_staco_qoverp_exPV, &b_mu_staco_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_cb_d0_exPV", &mu_staco_cb_d0_exPV, &b_mu_staco_cb_d0_exPV);
   fChain->SetBranchAddress("mu_staco_cb_z0_exPV", &mu_staco_cb_z0_exPV, &b_mu_staco_cb_z0_exPV);
   fChain->SetBranchAddress("mu_staco_cb_phi_exPV", &mu_staco_cb_phi_exPV, &b_mu_staco_cb_phi_exPV);
   fChain->SetBranchAddress("mu_staco_cb_theta_exPV", &mu_staco_cb_theta_exPV, &b_mu_staco_cb_theta_exPV);
   fChain->SetBranchAddress("mu_staco_cb_qoverp_exPV", &mu_staco_cb_qoverp_exPV, &b_mu_staco_cb_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_d0_exPV", &mu_staco_id_d0_exPV, &b_mu_staco_id_d0_exPV);
   fChain->SetBranchAddress("mu_staco_id_z0_exPV", &mu_staco_id_z0_exPV, &b_mu_staco_id_z0_exPV);
   fChain->SetBranchAddress("mu_staco_id_phi_exPV", &mu_staco_id_phi_exPV, &b_mu_staco_id_phi_exPV);
   fChain->SetBranchAddress("mu_staco_id_theta_exPV", &mu_staco_id_theta_exPV, &b_mu_staco_id_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_qoverp_exPV", &mu_staco_id_qoverp_exPV, &b_mu_staco_id_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_d0_exPV", &mu_staco_me_d0_exPV, &b_mu_staco_me_d0_exPV);
   fChain->SetBranchAddress("mu_staco_me_z0_exPV", &mu_staco_me_z0_exPV, &b_mu_staco_me_z0_exPV);
   fChain->SetBranchAddress("mu_staco_me_phi_exPV", &mu_staco_me_phi_exPV, &b_mu_staco_me_phi_exPV);
   fChain->SetBranchAddress("mu_staco_me_theta_exPV", &mu_staco_me_theta_exPV, &b_mu_staco_me_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_qoverp_exPV", &mu_staco_me_qoverp_exPV, &b_mu_staco_me_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_ie_d0_exPV", &mu_staco_ie_d0_exPV, &b_mu_staco_ie_d0_exPV);
   fChain->SetBranchAddress("mu_staco_ie_z0_exPV", &mu_staco_ie_z0_exPV, &b_mu_staco_ie_z0_exPV);
   fChain->SetBranchAddress("mu_staco_ie_phi_exPV", &mu_staco_ie_phi_exPV, &b_mu_staco_ie_phi_exPV);
   fChain->SetBranchAddress("mu_staco_ie_theta_exPV", &mu_staco_ie_theta_exPV, &b_mu_staco_ie_theta_exPV);
   fChain->SetBranchAddress("mu_staco_ie_qoverp_exPV", &mu_staco_ie_qoverp_exPV, &b_mu_staco_ie_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_SpaceTime_detID", &mu_staco_SpaceTime_detID, &b_mu_staco_SpaceTime_detID);
   fChain->SetBranchAddress("mu_staco_SpaceTime_t", &mu_staco_SpaceTime_t, &b_mu_staco_SpaceTime_t);
   fChain->SetBranchAddress("mu_staco_SpaceTime_tError", &mu_staco_SpaceTime_tError, &b_mu_staco_SpaceTime_tError);
   fChain->SetBranchAddress("mu_staco_SpaceTime_weight", &mu_staco_SpaceTime_weight, &b_mu_staco_SpaceTime_weight);
   fChain->SetBranchAddress("mu_staco_SpaceTime_x", &mu_staco_SpaceTime_x, &b_mu_staco_SpaceTime_x);
   fChain->SetBranchAddress("mu_staco_SpaceTime_y", &mu_staco_SpaceTime_y, &b_mu_staco_SpaceTime_y);
   fChain->SetBranchAddress("mu_staco_SpaceTime_z", &mu_staco_SpaceTime_z, &b_mu_staco_SpaceTime_z);
   fChain->SetBranchAddress("mu_staco_cov_d0_exPV", &mu_staco_cov_d0_exPV, &b_mu_staco_cov_d0_exPV);
   fChain->SetBranchAddress("mu_staco_cov_z0_exPV", &mu_staco_cov_z0_exPV, &b_mu_staco_cov_z0_exPV);
   fChain->SetBranchAddress("mu_staco_cov_phi_exPV", &mu_staco_cov_phi_exPV, &b_mu_staco_cov_phi_exPV);
   fChain->SetBranchAddress("mu_staco_cov_theta_exPV", &mu_staco_cov_theta_exPV, &b_mu_staco_cov_theta_exPV);
   fChain->SetBranchAddress("mu_staco_cov_qoverp_exPV", &mu_staco_cov_qoverp_exPV, &b_mu_staco_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_cov_d0_z0_exPV", &mu_staco_cov_d0_z0_exPV, &b_mu_staco_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_staco_cov_d0_phi_exPV", &mu_staco_cov_d0_phi_exPV, &b_mu_staco_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_cov_d0_theta_exPV", &mu_staco_cov_d0_theta_exPV, &b_mu_staco_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_cov_d0_qoverp_exPV", &mu_staco_cov_d0_qoverp_exPV, &b_mu_staco_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_cov_z0_phi_exPV", &mu_staco_cov_z0_phi_exPV, &b_mu_staco_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_cov_z0_theta_exPV", &mu_staco_cov_z0_theta_exPV, &b_mu_staco_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_cov_z0_qoverp_exPV", &mu_staco_cov_z0_qoverp_exPV, &b_mu_staco_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_cov_phi_theta_exPV", &mu_staco_cov_phi_theta_exPV, &b_mu_staco_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_staco_cov_phi_qoverp_exPV", &mu_staco_cov_phi_qoverp_exPV, &b_mu_staco_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_cov_theta_qoverp_exPV", &mu_staco_cov_theta_qoverp_exPV, &b_mu_staco_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_d0_exPV", &mu_staco_id_cov_d0_exPV, &b_mu_staco_id_cov_d0_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_z0_exPV", &mu_staco_id_cov_z0_exPV, &b_mu_staco_id_cov_z0_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_phi_exPV", &mu_staco_id_cov_phi_exPV, &b_mu_staco_id_cov_phi_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_theta_exPV", &mu_staco_id_cov_theta_exPV, &b_mu_staco_id_cov_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_qoverp_exPV", &mu_staco_id_cov_qoverp_exPV, &b_mu_staco_id_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_d0_z0_exPV", &mu_staco_id_cov_d0_z0_exPV, &b_mu_staco_id_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_d0_phi_exPV", &mu_staco_id_cov_d0_phi_exPV, &b_mu_staco_id_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_d0_theta_exPV", &mu_staco_id_cov_d0_theta_exPV, &b_mu_staco_id_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_d0_qoverp_exPV", &mu_staco_id_cov_d0_qoverp_exPV, &b_mu_staco_id_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_z0_phi_exPV", &mu_staco_id_cov_z0_phi_exPV, &b_mu_staco_id_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_z0_theta_exPV", &mu_staco_id_cov_z0_theta_exPV, &b_mu_staco_id_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_z0_qoverp_exPV", &mu_staco_id_cov_z0_qoverp_exPV, &b_mu_staco_id_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_phi_theta_exPV", &mu_staco_id_cov_phi_theta_exPV, &b_mu_staco_id_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_phi_qoverp_exPV", &mu_staco_id_cov_phi_qoverp_exPV, &b_mu_staco_id_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_id_cov_theta_qoverp_exPV", &mu_staco_id_cov_theta_qoverp_exPV, &b_mu_staco_id_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_d0_exPV", &mu_staco_me_cov_d0_exPV, &b_mu_staco_me_cov_d0_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_z0_exPV", &mu_staco_me_cov_z0_exPV, &b_mu_staco_me_cov_z0_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_phi_exPV", &mu_staco_me_cov_phi_exPV, &b_mu_staco_me_cov_phi_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_theta_exPV", &mu_staco_me_cov_theta_exPV, &b_mu_staco_me_cov_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_qoverp_exPV", &mu_staco_me_cov_qoverp_exPV, &b_mu_staco_me_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_d0_z0_exPV", &mu_staco_me_cov_d0_z0_exPV, &b_mu_staco_me_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_d0_phi_exPV", &mu_staco_me_cov_d0_phi_exPV, &b_mu_staco_me_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_d0_theta_exPV", &mu_staco_me_cov_d0_theta_exPV, &b_mu_staco_me_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_d0_qoverp_exPV", &mu_staco_me_cov_d0_qoverp_exPV, &b_mu_staco_me_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_z0_phi_exPV", &mu_staco_me_cov_z0_phi_exPV, &b_mu_staco_me_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_z0_theta_exPV", &mu_staco_me_cov_z0_theta_exPV, &b_mu_staco_me_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_z0_qoverp_exPV", &mu_staco_me_cov_z0_qoverp_exPV, &b_mu_staco_me_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_phi_theta_exPV", &mu_staco_me_cov_phi_theta_exPV, &b_mu_staco_me_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_phi_qoverp_exPV", &mu_staco_me_cov_phi_qoverp_exPV, &b_mu_staco_me_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_me_cov_theta_qoverp_exPV", &mu_staco_me_cov_theta_qoverp_exPV, &b_mu_staco_me_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_staco_ms_d0", &mu_staco_ms_d0, &b_mu_staco_ms_d0);
   fChain->SetBranchAddress("mu_staco_ms_z0", &mu_staco_ms_z0, &b_mu_staco_ms_z0);
   fChain->SetBranchAddress("mu_staco_ms_phi", &mu_staco_ms_phi, &b_mu_staco_ms_phi);
   fChain->SetBranchAddress("mu_staco_ms_theta", &mu_staco_ms_theta, &b_mu_staco_ms_theta);
   fChain->SetBranchAddress("mu_staco_ms_qoverp", &mu_staco_ms_qoverp, &b_mu_staco_ms_qoverp);
   fChain->SetBranchAddress("mu_staco_id_d0", &mu_staco_id_d0, &b_mu_staco_id_d0);
   fChain->SetBranchAddress("mu_staco_id_z0", &mu_staco_id_z0, &b_mu_staco_id_z0);
   fChain->SetBranchAddress("mu_staco_id_phi", &mu_staco_id_phi, &b_mu_staco_id_phi);
   fChain->SetBranchAddress("mu_staco_id_theta", &mu_staco_id_theta, &b_mu_staco_id_theta);
   fChain->SetBranchAddress("mu_staco_id_qoverp", &mu_staco_id_qoverp, &b_mu_staco_id_qoverp);
   fChain->SetBranchAddress("mu_staco_me_d0", &mu_staco_me_d0, &b_mu_staco_me_d0);
   fChain->SetBranchAddress("mu_staco_me_z0", &mu_staco_me_z0, &b_mu_staco_me_z0);
   fChain->SetBranchAddress("mu_staco_me_phi", &mu_staco_me_phi, &b_mu_staco_me_phi);
   fChain->SetBranchAddress("mu_staco_me_theta", &mu_staco_me_theta, &b_mu_staco_me_theta);
   fChain->SetBranchAddress("mu_staco_me_qoverp", &mu_staco_me_qoverp, &b_mu_staco_me_qoverp);
   fChain->SetBranchAddress("mu_staco_ie_d0", &mu_staco_ie_d0, &b_mu_staco_ie_d0);
   fChain->SetBranchAddress("mu_staco_ie_z0", &mu_staco_ie_z0, &b_mu_staco_ie_z0);
   fChain->SetBranchAddress("mu_staco_ie_phi", &mu_staco_ie_phi, &b_mu_staco_ie_phi);
   fChain->SetBranchAddress("mu_staco_ie_theta", &mu_staco_ie_theta, &b_mu_staco_ie_theta);
   fChain->SetBranchAddress("mu_staco_ie_qoverp", &mu_staco_ie_qoverp, &b_mu_staco_ie_qoverp);
   fChain->SetBranchAddress("mu_staco_nOutliersOnTrack", &mu_staco_nOutliersOnTrack, &b_mu_staco_nOutliersOnTrack);
   fChain->SetBranchAddress("mu_staco_nBLHits", &mu_staco_nBLHits, &b_mu_staco_nBLHits);
   fChain->SetBranchAddress("mu_staco_nPixHits", &mu_staco_nPixHits, &b_mu_staco_nPixHits);
   fChain->SetBranchAddress("mu_staco_nSCTHits", &mu_staco_nSCTHits, &b_mu_staco_nSCTHits);
   fChain->SetBranchAddress("mu_staco_nTRTHits", &mu_staco_nTRTHits, &b_mu_staco_nTRTHits);
   fChain->SetBranchAddress("mu_staco_nTRTHighTHits", &mu_staco_nTRTHighTHits, &b_mu_staco_nTRTHighTHits);
   fChain->SetBranchAddress("mu_staco_nBLSharedHits", &mu_staco_nBLSharedHits, &b_mu_staco_nBLSharedHits);
   fChain->SetBranchAddress("mu_staco_nPixSharedHits", &mu_staco_nPixSharedHits, &b_mu_staco_nPixSharedHits);
   fChain->SetBranchAddress("mu_staco_nPixHoles", &mu_staco_nPixHoles, &b_mu_staco_nPixHoles);
   fChain->SetBranchAddress("mu_staco_nSCTSharedHits", &mu_staco_nSCTSharedHits, &b_mu_staco_nSCTSharedHits);
   fChain->SetBranchAddress("mu_staco_nSCTHoles", &mu_staco_nSCTHoles, &b_mu_staco_nSCTHoles);
   fChain->SetBranchAddress("mu_staco_nTRTOutliers", &mu_staco_nTRTOutliers, &b_mu_staco_nTRTOutliers);
   fChain->SetBranchAddress("mu_staco_nTRTHighTOutliers", &mu_staco_nTRTHighTOutliers, &b_mu_staco_nTRTHighTOutliers);
   fChain->SetBranchAddress("mu_staco_nGangedPixels", &mu_staco_nGangedPixels, &b_mu_staco_nGangedPixels);
   fChain->SetBranchAddress("mu_staco_nPixelDeadSensors", &mu_staco_nPixelDeadSensors, &b_mu_staco_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_staco_nSCTDeadSensors", &mu_staco_nSCTDeadSensors, &b_mu_staco_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_staco_nTRTDeadStraws", &mu_staco_nTRTDeadStraws, &b_mu_staco_nTRTDeadStraws);
   fChain->SetBranchAddress("mu_staco_expectBLayerHit", &mu_staco_expectBLayerHit, &b_mu_staco_expectBLayerHit);
   fChain->SetBranchAddress("mu_staco_nMDTHits", &mu_staco_nMDTHits, &b_mu_staco_nMDTHits);
   fChain->SetBranchAddress("mu_staco_nMDTHoles", &mu_staco_nMDTHoles, &b_mu_staco_nMDTHoles);
   fChain->SetBranchAddress("mu_staco_nCSCEtaHits", &mu_staco_nCSCEtaHits, &b_mu_staco_nCSCEtaHits);
   fChain->SetBranchAddress("mu_staco_nCSCEtaHoles", &mu_staco_nCSCEtaHoles, &b_mu_staco_nCSCEtaHoles);
   fChain->SetBranchAddress("mu_staco_nCSCUnspoiledEtaHits", &mu_staco_nCSCUnspoiledEtaHits, &b_mu_staco_nCSCUnspoiledEtaHits);
   fChain->SetBranchAddress("mu_staco_nCSCPhiHits", &mu_staco_nCSCPhiHits, &b_mu_staco_nCSCPhiHits);
   fChain->SetBranchAddress("mu_staco_nCSCPhiHoles", &mu_staco_nCSCPhiHoles, &b_mu_staco_nCSCPhiHoles);
   fChain->SetBranchAddress("mu_staco_nRPCEtaHits", &mu_staco_nRPCEtaHits, &b_mu_staco_nRPCEtaHits);
   fChain->SetBranchAddress("mu_staco_nRPCEtaHoles", &mu_staco_nRPCEtaHoles, &b_mu_staco_nRPCEtaHoles);
   fChain->SetBranchAddress("mu_staco_nRPCPhiHits", &mu_staco_nRPCPhiHits, &b_mu_staco_nRPCPhiHits);
   fChain->SetBranchAddress("mu_staco_nRPCPhiHoles", &mu_staco_nRPCPhiHoles, &b_mu_staco_nRPCPhiHoles);
   fChain->SetBranchAddress("mu_staco_nTGCEtaHits", &mu_staco_nTGCEtaHits, &b_mu_staco_nTGCEtaHits);
   fChain->SetBranchAddress("mu_staco_nTGCEtaHoles", &mu_staco_nTGCEtaHoles, &b_mu_staco_nTGCEtaHoles);
   fChain->SetBranchAddress("mu_staco_nTGCPhiHits", &mu_staco_nTGCPhiHits, &b_mu_staco_nTGCPhiHits);
   fChain->SetBranchAddress("mu_staco_nTGCPhiHoles", &mu_staco_nTGCPhiHoles, &b_mu_staco_nTGCPhiHoles);
   fChain->SetBranchAddress("mu_staco_nprecisionLayers", &mu_staco_nprecisionLayers, &b_mu_staco_nprecisionLayers);
   fChain->SetBranchAddress("mu_staco_nprecisionHoleLayers", &mu_staco_nprecisionHoleLayers, &b_mu_staco_nprecisionHoleLayers);
   fChain->SetBranchAddress("mu_staco_nphiLayers", &mu_staco_nphiLayers, &b_mu_staco_nphiLayers);
   fChain->SetBranchAddress("mu_staco_ntrigEtaLayers", &mu_staco_ntrigEtaLayers, &b_mu_staco_ntrigEtaLayers);
   fChain->SetBranchAddress("mu_staco_nphiHoleLayers", &mu_staco_nphiHoleLayers, &b_mu_staco_nphiHoleLayers);
   fChain->SetBranchAddress("mu_staco_ntrigEtaHoleLayers", &mu_staco_ntrigEtaHoleLayers, &b_mu_staco_ntrigEtaHoleLayers);
   fChain->SetBranchAddress("mu_staco_nMDTBIHits", &mu_staco_nMDTBIHits, &b_mu_staco_nMDTBIHits);
   fChain->SetBranchAddress("mu_staco_nMDTBMHits", &mu_staco_nMDTBMHits, &b_mu_staco_nMDTBMHits);
   fChain->SetBranchAddress("mu_staco_nMDTBOHits", &mu_staco_nMDTBOHits, &b_mu_staco_nMDTBOHits);
   fChain->SetBranchAddress("mu_staco_nMDTBEEHits", &mu_staco_nMDTBEEHits, &b_mu_staco_nMDTBEEHits);
   fChain->SetBranchAddress("mu_staco_nMDTBIS78Hits", &mu_staco_nMDTBIS78Hits, &b_mu_staco_nMDTBIS78Hits);
   fChain->SetBranchAddress("mu_staco_nMDTEIHits", &mu_staco_nMDTEIHits, &b_mu_staco_nMDTEIHits);
   fChain->SetBranchAddress("mu_staco_nMDTEMHits", &mu_staco_nMDTEMHits, &b_mu_staco_nMDTEMHits);
   fChain->SetBranchAddress("mu_staco_nMDTEOHits", &mu_staco_nMDTEOHits, &b_mu_staco_nMDTEOHits);
   fChain->SetBranchAddress("mu_staco_nMDTEEHits", &mu_staco_nMDTEEHits, &b_mu_staco_nMDTEEHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer1EtaHits", &mu_staco_nRPCLayer1EtaHits, &b_mu_staco_nRPCLayer1EtaHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer2EtaHits", &mu_staco_nRPCLayer2EtaHits, &b_mu_staco_nRPCLayer2EtaHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer3EtaHits", &mu_staco_nRPCLayer3EtaHits, &b_mu_staco_nRPCLayer3EtaHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer1PhiHits", &mu_staco_nRPCLayer1PhiHits, &b_mu_staco_nRPCLayer1PhiHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer2PhiHits", &mu_staco_nRPCLayer2PhiHits, &b_mu_staco_nRPCLayer2PhiHits);
   fChain->SetBranchAddress("mu_staco_nRPCLayer3PhiHits", &mu_staco_nRPCLayer3PhiHits, &b_mu_staco_nRPCLayer3PhiHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer1EtaHits", &mu_staco_nTGCLayer1EtaHits, &b_mu_staco_nTGCLayer1EtaHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer2EtaHits", &mu_staco_nTGCLayer2EtaHits, &b_mu_staco_nTGCLayer2EtaHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer3EtaHits", &mu_staco_nTGCLayer3EtaHits, &b_mu_staco_nTGCLayer3EtaHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer4EtaHits", &mu_staco_nTGCLayer4EtaHits, &b_mu_staco_nTGCLayer4EtaHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer1PhiHits", &mu_staco_nTGCLayer1PhiHits, &b_mu_staco_nTGCLayer1PhiHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer2PhiHits", &mu_staco_nTGCLayer2PhiHits, &b_mu_staco_nTGCLayer2PhiHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer3PhiHits", &mu_staco_nTGCLayer3PhiHits, &b_mu_staco_nTGCLayer3PhiHits);
   fChain->SetBranchAddress("mu_staco_nTGCLayer4PhiHits", &mu_staco_nTGCLayer4PhiHits, &b_mu_staco_nTGCLayer4PhiHits);
   fChain->SetBranchAddress("mu_staco_barrelSectors", &mu_staco_barrelSectors, &b_mu_staco_barrelSectors);
   fChain->SetBranchAddress("mu_staco_endcapSectors", &mu_staco_endcapSectors, &b_mu_staco_endcapSectors);
   fChain->SetBranchAddress("mu_staco_spec_surf_px", &mu_staco_spec_surf_px, &b_mu_staco_spec_surf_px);
   fChain->SetBranchAddress("mu_staco_spec_surf_py", &mu_staco_spec_surf_py, &b_mu_staco_spec_surf_py);
   fChain->SetBranchAddress("mu_staco_spec_surf_pz", &mu_staco_spec_surf_pz, &b_mu_staco_spec_surf_pz);
   fChain->SetBranchAddress("mu_staco_spec_surf_x", &mu_staco_spec_surf_x, &b_mu_staco_spec_surf_x);
   fChain->SetBranchAddress("mu_staco_spec_surf_y", &mu_staco_spec_surf_y, &b_mu_staco_spec_surf_y);
   fChain->SetBranchAddress("mu_staco_spec_surf_z", &mu_staco_spec_surf_z, &b_mu_staco_spec_surf_z);
   fChain->SetBranchAddress("mu_staco_trackd0", &mu_staco_trackd0, &b_mu_staco_trackd0);
   fChain->SetBranchAddress("mu_staco_trackz0", &mu_staco_trackz0, &b_mu_staco_trackz0);
   fChain->SetBranchAddress("mu_staco_trackphi", &mu_staco_trackphi, &b_mu_staco_trackphi);
   fChain->SetBranchAddress("mu_staco_tracktheta", &mu_staco_tracktheta, &b_mu_staco_tracktheta);
   fChain->SetBranchAddress("mu_staco_trackqoverp", &mu_staco_trackqoverp, &b_mu_staco_trackqoverp);
   fChain->SetBranchAddress("mu_staco_trackcov_d0", &mu_staco_trackcov_d0, &b_mu_staco_trackcov_d0);
   fChain->SetBranchAddress("mu_staco_trackcov_z0", &mu_staco_trackcov_z0, &b_mu_staco_trackcov_z0);
   fChain->SetBranchAddress("mu_staco_trackcov_phi", &mu_staco_trackcov_phi, &b_mu_staco_trackcov_phi);
   fChain->SetBranchAddress("mu_staco_trackcov_theta", &mu_staco_trackcov_theta, &b_mu_staco_trackcov_theta);
   fChain->SetBranchAddress("mu_staco_trackcov_qoverp", &mu_staco_trackcov_qoverp, &b_mu_staco_trackcov_qoverp);
   fChain->SetBranchAddress("mu_staco_trackcov_d0_z0", &mu_staco_trackcov_d0_z0, &b_mu_staco_trackcov_d0_z0);
   fChain->SetBranchAddress("mu_staco_trackcov_d0_phi", &mu_staco_trackcov_d0_phi, &b_mu_staco_trackcov_d0_phi);
   fChain->SetBranchAddress("mu_staco_trackcov_d0_theta", &mu_staco_trackcov_d0_theta, &b_mu_staco_trackcov_d0_theta);
   fChain->SetBranchAddress("mu_staco_trackcov_d0_qoverp", &mu_staco_trackcov_d0_qoverp, &b_mu_staco_trackcov_d0_qoverp);
   fChain->SetBranchAddress("mu_staco_trackcov_z0_phi", &mu_staco_trackcov_z0_phi, &b_mu_staco_trackcov_z0_phi);
   fChain->SetBranchAddress("mu_staco_trackcov_z0_theta", &mu_staco_trackcov_z0_theta, &b_mu_staco_trackcov_z0_theta);
   fChain->SetBranchAddress("mu_staco_trackcov_z0_qoverp", &mu_staco_trackcov_z0_qoverp, &b_mu_staco_trackcov_z0_qoverp);
   fChain->SetBranchAddress("mu_staco_trackcov_phi_theta", &mu_staco_trackcov_phi_theta, &b_mu_staco_trackcov_phi_theta);
   fChain->SetBranchAddress("mu_staco_trackcov_phi_qoverp", &mu_staco_trackcov_phi_qoverp, &b_mu_staco_trackcov_phi_qoverp);
   fChain->SetBranchAddress("mu_staco_trackcov_theta_qoverp", &mu_staco_trackcov_theta_qoverp, &b_mu_staco_trackcov_theta_qoverp);
   fChain->SetBranchAddress("mu_staco_trackfitchi2", &mu_staco_trackfitchi2, &b_mu_staco_trackfitchi2);
   fChain->SetBranchAddress("mu_staco_trackfitndof", &mu_staco_trackfitndof, &b_mu_staco_trackfitndof);
   fChain->SetBranchAddress("mu_staco_hastrack", &mu_staco_hastrack, &b_mu_staco_hastrack);
   fChain->SetBranchAddress("mu_staco_trackd0beam", &mu_staco_trackd0beam, &b_mu_staco_trackd0beam);
   fChain->SetBranchAddress("mu_staco_trackz0beam", &mu_staco_trackz0beam, &b_mu_staco_trackz0beam);
   fChain->SetBranchAddress("mu_staco_tracksigd0beam", &mu_staco_tracksigd0beam, &b_mu_staco_tracksigd0beam);
   fChain->SetBranchAddress("mu_staco_tracksigz0beam", &mu_staco_tracksigz0beam, &b_mu_staco_tracksigz0beam);
   fChain->SetBranchAddress("mu_staco_trackd0pv", &mu_staco_trackd0pv, &b_mu_staco_trackd0pv);
   fChain->SetBranchAddress("mu_staco_trackz0pv", &mu_staco_trackz0pv, &b_mu_staco_trackz0pv);
   fChain->SetBranchAddress("mu_staco_tracksigd0pv", &mu_staco_tracksigd0pv, &b_mu_staco_tracksigd0pv);
   fChain->SetBranchAddress("mu_staco_tracksigz0pv", &mu_staco_tracksigz0pv, &b_mu_staco_tracksigz0pv);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_d0_biasedpvunbiased", &mu_staco_trackIPEstimate_d0_biasedpvunbiased, &b_mu_staco_trackIPEstimate_d0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_z0_biasedpvunbiased", &mu_staco_trackIPEstimate_z0_biasedpvunbiased, &b_mu_staco_trackIPEstimate_z0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_sigd0_biasedpvunbiased", &mu_staco_trackIPEstimate_sigd0_biasedpvunbiased, &b_mu_staco_trackIPEstimate_sigd0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_sigz0_biasedpvunbiased", &mu_staco_trackIPEstimate_sigz0_biasedpvunbiased, &b_mu_staco_trackIPEstimate_sigz0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_d0_unbiasedpvunbiased", &mu_staco_trackIPEstimate_d0_unbiasedpvunbiased, &b_mu_staco_trackIPEstimate_d0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_z0_unbiasedpvunbiased", &mu_staco_trackIPEstimate_z0_unbiasedpvunbiased, &b_mu_staco_trackIPEstimate_z0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased", &mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased, &b_mu_staco_trackIPEstimate_sigd0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased", &mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased, &b_mu_staco_trackIPEstimate_sigz0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_staco_trackd0pvunbiased", &mu_staco_trackd0pvunbiased, &b_mu_staco_trackd0pvunbiased);
   fChain->SetBranchAddress("mu_staco_trackz0pvunbiased", &mu_staco_trackz0pvunbiased, &b_mu_staco_trackz0pvunbiased);
   fChain->SetBranchAddress("mu_staco_tracksigd0pvunbiased", &mu_staco_tracksigd0pvunbiased, &b_mu_staco_tracksigd0pvunbiased);
   fChain->SetBranchAddress("mu_staco_tracksigz0pvunbiased", &mu_staco_tracksigz0pvunbiased, &b_mu_staco_tracksigz0pvunbiased);
   fChain->SetBranchAddress("mu_staco_type", &mu_staco_type, &b_mu_staco_type);
   fChain->SetBranchAddress("mu_staco_origin", &mu_staco_origin, &b_mu_staco_origin);
   fChain->SetBranchAddress("mu_staco_truth_dr", &mu_staco_truth_dr, &b_mu_staco_truth_dr);
   fChain->SetBranchAddress("mu_staco_truth_E", &mu_staco_truth_E, &b_mu_staco_truth_E);
   fChain->SetBranchAddress("mu_staco_truth_pt", &mu_staco_truth_pt, &b_mu_staco_truth_pt);
   fChain->SetBranchAddress("mu_staco_truth_eta", &mu_staco_truth_eta, &b_mu_staco_truth_eta);
   fChain->SetBranchAddress("mu_staco_truth_phi", &mu_staco_truth_phi, &b_mu_staco_truth_phi);
   fChain->SetBranchAddress("mu_staco_truth_type", &mu_staco_truth_type, &b_mu_staco_truth_type);
   fChain->SetBranchAddress("mu_staco_truth_status", &mu_staco_truth_status, &b_mu_staco_truth_status);
   fChain->SetBranchAddress("mu_staco_truth_barcode", &mu_staco_truth_barcode, &b_mu_staco_truth_barcode);
   fChain->SetBranchAddress("mu_staco_truth_mothertype", &mu_staco_truth_mothertype, &b_mu_staco_truth_mothertype);
   fChain->SetBranchAddress("mu_staco_truth_motherbarcode", &mu_staco_truth_motherbarcode, &b_mu_staco_truth_motherbarcode);
   fChain->SetBranchAddress("mu_staco_truth_matched", &mu_staco_truth_matched, &b_mu_staco_truth_matched);
   fChain->SetBranchAddress("mu_staco_EFCB_dr", &mu_staco_EFCB_dr, &b_mu_staco_EFCB_dr);
   fChain->SetBranchAddress("mu_staco_EFCB_n", &mu_staco_EFCB_n, &b_mu_staco_EFCB_n);
   fChain->SetBranchAddress("mu_staco_EFCB_MuonType", &mu_staco_EFCB_MuonType, &b_mu_staco_EFCB_MuonType);
   fChain->SetBranchAddress("mu_staco_EFCB_pt", &mu_staco_EFCB_pt, &b_mu_staco_EFCB_pt);
   fChain->SetBranchAddress("mu_staco_EFCB_eta", &mu_staco_EFCB_eta, &b_mu_staco_EFCB_eta);
   fChain->SetBranchAddress("mu_staco_EFCB_phi", &mu_staco_EFCB_phi, &b_mu_staco_EFCB_phi);
   fChain->SetBranchAddress("mu_staco_EFCB_hasCB", &mu_staco_EFCB_hasCB, &b_mu_staco_EFCB_hasCB);
   fChain->SetBranchAddress("mu_staco_EFCB_matched", &mu_staco_EFCB_matched, &b_mu_staco_EFCB_matched);
   fChain->SetBranchAddress("mu_staco_EFMG_dr", &mu_staco_EFMG_dr, &b_mu_staco_EFMG_dr);
   fChain->SetBranchAddress("mu_staco_EFMG_n", &mu_staco_EFMG_n, &b_mu_staco_EFMG_n);
   fChain->SetBranchAddress("mu_staco_EFMG_MuonType", &mu_staco_EFMG_MuonType, &b_mu_staco_EFMG_MuonType);
   fChain->SetBranchAddress("mu_staco_EFMG_pt", &mu_staco_EFMG_pt, &b_mu_staco_EFMG_pt);
   fChain->SetBranchAddress("mu_staco_EFMG_eta", &mu_staco_EFMG_eta, &b_mu_staco_EFMG_eta);
   fChain->SetBranchAddress("mu_staco_EFMG_phi", &mu_staco_EFMG_phi, &b_mu_staco_EFMG_phi);
   fChain->SetBranchAddress("mu_staco_EFMG_hasMG", &mu_staco_EFMG_hasMG, &b_mu_staco_EFMG_hasMG);
   fChain->SetBranchAddress("mu_staco_EFMG_matched", &mu_staco_EFMG_matched, &b_mu_staco_EFMG_matched);
   fChain->SetBranchAddress("mu_staco_EFME_dr", &mu_staco_EFME_dr, &b_mu_staco_EFME_dr);
   fChain->SetBranchAddress("mu_staco_EFME_n", &mu_staco_EFME_n, &b_mu_staco_EFME_n);
   fChain->SetBranchAddress("mu_staco_EFME_MuonType", &mu_staco_EFME_MuonType, &b_mu_staco_EFME_MuonType);
   fChain->SetBranchAddress("mu_staco_EFME_pt", &mu_staco_EFME_pt, &b_mu_staco_EFME_pt);
   fChain->SetBranchAddress("mu_staco_EFME_eta", &mu_staco_EFME_eta, &b_mu_staco_EFME_eta);
   fChain->SetBranchAddress("mu_staco_EFME_phi", &mu_staco_EFME_phi, &b_mu_staco_EFME_phi);
   fChain->SetBranchAddress("mu_staco_EFME_hasME", &mu_staco_EFME_hasME, &b_mu_staco_EFME_hasME);
   fChain->SetBranchAddress("mu_staco_EFME_matched", &mu_staco_EFME_matched, &b_mu_staco_EFME_matched);
   fChain->SetBranchAddress("mu_staco_L2CB_dr", &mu_staco_L2CB_dr, &b_mu_staco_L2CB_dr);
   fChain->SetBranchAddress("mu_staco_L2CB_pt", &mu_staco_L2CB_pt, &b_mu_staco_L2CB_pt);
   fChain->SetBranchAddress("mu_staco_L2CB_eta", &mu_staco_L2CB_eta, &b_mu_staco_L2CB_eta);
   fChain->SetBranchAddress("mu_staco_L2CB_phi", &mu_staco_L2CB_phi, &b_mu_staco_L2CB_phi);
   fChain->SetBranchAddress("mu_staco_L2CB_id_pt", &mu_staco_L2CB_id_pt, &b_mu_staco_L2CB_id_pt);
   fChain->SetBranchAddress("mu_staco_L2CB_ms_pt", &mu_staco_L2CB_ms_pt, &b_mu_staco_L2CB_ms_pt);
   fChain->SetBranchAddress("mu_staco_L2CB_nPixHits", &mu_staco_L2CB_nPixHits, &b_mu_staco_L2CB_nPixHits);
   fChain->SetBranchAddress("mu_staco_L2CB_nSCTHits", &mu_staco_L2CB_nSCTHits, &b_mu_staco_L2CB_nSCTHits);
   fChain->SetBranchAddress("mu_staco_L2CB_nTRTHits", &mu_staco_L2CB_nTRTHits, &b_mu_staco_L2CB_nTRTHits);
   fChain->SetBranchAddress("mu_staco_L2CB_nTRTHighTHits", &mu_staco_L2CB_nTRTHighTHits, &b_mu_staco_L2CB_nTRTHighTHits);
   fChain->SetBranchAddress("mu_staco_L2CB_matched", &mu_staco_L2CB_matched, &b_mu_staco_L2CB_matched);
   fChain->SetBranchAddress("mu_staco_L1_dr", &mu_staco_L1_dr, &b_mu_staco_L1_dr);
   fChain->SetBranchAddress("mu_staco_L1_pt", &mu_staco_L1_pt, &b_mu_staco_L1_pt);
   fChain->SetBranchAddress("mu_staco_L1_eta", &mu_staco_L1_eta, &b_mu_staco_L1_eta);
   fChain->SetBranchAddress("mu_staco_L1_phi", &mu_staco_L1_phi, &b_mu_staco_L1_phi);
   fChain->SetBranchAddress("mu_staco_L1_thrNumber", &mu_staco_L1_thrNumber, &b_mu_staco_L1_thrNumber);
   fChain->SetBranchAddress("mu_staco_L1_RoINumber", &mu_staco_L1_RoINumber, &b_mu_staco_L1_RoINumber);
   fChain->SetBranchAddress("mu_staco_L1_sectorAddress", &mu_staco_L1_sectorAddress, &b_mu_staco_L1_sectorAddress);
   fChain->SetBranchAddress("mu_staco_L1_firstCandidate", &mu_staco_L1_firstCandidate, &b_mu_staco_L1_firstCandidate);
   fChain->SetBranchAddress("mu_staco_L1_moreCandInRoI", &mu_staco_L1_moreCandInRoI, &b_mu_staco_L1_moreCandInRoI);
   fChain->SetBranchAddress("mu_staco_L1_moreCandInSector", &mu_staco_L1_moreCandInSector, &b_mu_staco_L1_moreCandInSector);
   fChain->SetBranchAddress("mu_staco_L1_source", &mu_staco_L1_source, &b_mu_staco_L1_source);
   fChain->SetBranchAddress("mu_staco_L1_hemisphere", &mu_staco_L1_hemisphere, &b_mu_staco_L1_hemisphere);
   fChain->SetBranchAddress("mu_staco_L1_charge", &mu_staco_L1_charge, &b_mu_staco_L1_charge);
   fChain->SetBranchAddress("mu_staco_L1_vetoed", &mu_staco_L1_vetoed, &b_mu_staco_L1_vetoed);
   fChain->SetBranchAddress("mu_staco_L1_matched", &mu_staco_L1_matched, &b_mu_staco_L1_matched);
   fChain->SetBranchAddress("mu_calo_n", &mu_calo_n, &b_mu_calo_n);
   fChain->SetBranchAddress("mu_calo_E", &mu_calo_E, &b_mu_calo_E);
   fChain->SetBranchAddress("mu_calo_pt", &mu_calo_pt, &b_mu_calo_pt);
   fChain->SetBranchAddress("mu_calo_m", &mu_calo_m, &b_mu_calo_m);
   fChain->SetBranchAddress("mu_calo_eta", &mu_calo_eta, &b_mu_calo_eta);
   fChain->SetBranchAddress("mu_calo_phi", &mu_calo_phi, &b_mu_calo_phi);
   fChain->SetBranchAddress("mu_calo_px", &mu_calo_px, &b_mu_calo_px);
   fChain->SetBranchAddress("mu_calo_py", &mu_calo_py, &b_mu_calo_py);
   fChain->SetBranchAddress("mu_calo_pz", &mu_calo_pz, &b_mu_calo_pz);
   fChain->SetBranchAddress("mu_calo_charge", &mu_calo_charge, &b_mu_calo_charge);
   fChain->SetBranchAddress("mu_calo_allauthor", &mu_calo_allauthor, &b_mu_calo_allauthor);
   fChain->SetBranchAddress("mu_calo_author", &mu_calo_author, &b_mu_calo_author);
   fChain->SetBranchAddress("mu_calo_beta", &mu_calo_beta, &b_mu_calo_beta);
   fChain->SetBranchAddress("mu_calo_isMuonLikelihood", &mu_calo_isMuonLikelihood, &b_mu_calo_isMuonLikelihood);
   fChain->SetBranchAddress("mu_calo_matchchi2", &mu_calo_matchchi2, &b_mu_calo_matchchi2);
   fChain->SetBranchAddress("mu_calo_matchndof", &mu_calo_matchndof, &b_mu_calo_matchndof);
   fChain->SetBranchAddress("mu_calo_etcone20", &mu_calo_etcone20, &b_mu_calo_etcone20);
   fChain->SetBranchAddress("mu_calo_etcone30", &mu_calo_etcone30, &b_mu_calo_etcone30);
   fChain->SetBranchAddress("mu_calo_etcone40", &mu_calo_etcone40, &b_mu_calo_etcone40);
   fChain->SetBranchAddress("mu_calo_nucone20", &mu_calo_nucone20, &b_mu_calo_nucone20);
   fChain->SetBranchAddress("mu_calo_nucone30", &mu_calo_nucone30, &b_mu_calo_nucone30);
   fChain->SetBranchAddress("mu_calo_nucone40", &mu_calo_nucone40, &b_mu_calo_nucone40);
   fChain->SetBranchAddress("mu_calo_ptcone20", &mu_calo_ptcone20, &b_mu_calo_ptcone20);
   fChain->SetBranchAddress("mu_calo_ptcone30", &mu_calo_ptcone30, &b_mu_calo_ptcone30);
   fChain->SetBranchAddress("mu_calo_ptcone40", &mu_calo_ptcone40, &b_mu_calo_ptcone40);
   fChain->SetBranchAddress("mu_calo_etconeNoEm10", &mu_calo_etconeNoEm10, &b_mu_calo_etconeNoEm10);
   fChain->SetBranchAddress("mu_calo_etconeNoEm20", &mu_calo_etconeNoEm20, &b_mu_calo_etconeNoEm20);
   fChain->SetBranchAddress("mu_calo_etconeNoEm30", &mu_calo_etconeNoEm30, &b_mu_calo_etconeNoEm30);
   fChain->SetBranchAddress("mu_calo_etconeNoEm40", &mu_calo_etconeNoEm40, &b_mu_calo_etconeNoEm40);
   fChain->SetBranchAddress("mu_calo_scatteringCurvatureSignificance", &mu_calo_scatteringCurvatureSignificance, &b_mu_calo_scatteringCurvatureSignificance);
   fChain->SetBranchAddress("mu_calo_scatteringNeighbourSignificance", &mu_calo_scatteringNeighbourSignificance, &b_mu_calo_scatteringNeighbourSignificance);
   fChain->SetBranchAddress("mu_calo_momentumBalanceSignificance", &mu_calo_momentumBalanceSignificance, &b_mu_calo_momentumBalanceSignificance);
   fChain->SetBranchAddress("mu_calo_energyLossPar", &mu_calo_energyLossPar, &b_mu_calo_energyLossPar);
   fChain->SetBranchAddress("mu_calo_energyLossErr", &mu_calo_energyLossErr, &b_mu_calo_energyLossErr);
   fChain->SetBranchAddress("mu_calo_etCore", &mu_calo_etCore, &b_mu_calo_etCore);
   fChain->SetBranchAddress("mu_calo_energyLossType", &mu_calo_energyLossType, &b_mu_calo_energyLossType);
   fChain->SetBranchAddress("mu_calo_caloMuonIdTag", &mu_calo_caloMuonIdTag, &b_mu_calo_caloMuonIdTag);
   fChain->SetBranchAddress("mu_calo_caloLRLikelihood", &mu_calo_caloLRLikelihood, &b_mu_calo_caloLRLikelihood);
   fChain->SetBranchAddress("mu_calo_bestMatch", &mu_calo_bestMatch, &b_mu_calo_bestMatch);
   fChain->SetBranchAddress("mu_calo_isStandAloneMuon", &mu_calo_isStandAloneMuon, &b_mu_calo_isStandAloneMuon);
   fChain->SetBranchAddress("mu_calo_isCombinedMuon", &mu_calo_isCombinedMuon, &b_mu_calo_isCombinedMuon);
   fChain->SetBranchAddress("mu_calo_isLowPtReconstructedMuon", &mu_calo_isLowPtReconstructedMuon, &b_mu_calo_isLowPtReconstructedMuon);
   fChain->SetBranchAddress("mu_calo_isSegmentTaggedMuon", &mu_calo_isSegmentTaggedMuon, &b_mu_calo_isSegmentTaggedMuon);
   fChain->SetBranchAddress("mu_calo_isCaloMuonId", &mu_calo_isCaloMuonId, &b_mu_calo_isCaloMuonId);
   fChain->SetBranchAddress("mu_calo_alsoFoundByLowPt", &mu_calo_alsoFoundByLowPt, &b_mu_calo_alsoFoundByLowPt);
   fChain->SetBranchAddress("mu_calo_alsoFoundByCaloMuonId", &mu_calo_alsoFoundByCaloMuonId, &b_mu_calo_alsoFoundByCaloMuonId);
   fChain->SetBranchAddress("mu_calo_isSiliconAssociatedForwardMuon", &mu_calo_isSiliconAssociatedForwardMuon, &b_mu_calo_isSiliconAssociatedForwardMuon);
   fChain->SetBranchAddress("mu_calo_loose", &mu_calo_loose, &b_mu_calo_loose);
   fChain->SetBranchAddress("mu_calo_medium", &mu_calo_medium, &b_mu_calo_medium);
   fChain->SetBranchAddress("mu_calo_tight", &mu_calo_tight, &b_mu_calo_tight);
   fChain->SetBranchAddress("mu_calo_d0_exPV", &mu_calo_d0_exPV, &b_mu_calo_d0_exPV);
   fChain->SetBranchAddress("mu_calo_z0_exPV", &mu_calo_z0_exPV, &b_mu_calo_z0_exPV);
   fChain->SetBranchAddress("mu_calo_phi_exPV", &mu_calo_phi_exPV, &b_mu_calo_phi_exPV);
   fChain->SetBranchAddress("mu_calo_theta_exPV", &mu_calo_theta_exPV, &b_mu_calo_theta_exPV);
   fChain->SetBranchAddress("mu_calo_qoverp_exPV", &mu_calo_qoverp_exPV, &b_mu_calo_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_cb_d0_exPV", &mu_calo_cb_d0_exPV, &b_mu_calo_cb_d0_exPV);
   fChain->SetBranchAddress("mu_calo_cb_z0_exPV", &mu_calo_cb_z0_exPV, &b_mu_calo_cb_z0_exPV);
   fChain->SetBranchAddress("mu_calo_cb_phi_exPV", &mu_calo_cb_phi_exPV, &b_mu_calo_cb_phi_exPV);
   fChain->SetBranchAddress("mu_calo_cb_theta_exPV", &mu_calo_cb_theta_exPV, &b_mu_calo_cb_theta_exPV);
   fChain->SetBranchAddress("mu_calo_cb_qoverp_exPV", &mu_calo_cb_qoverp_exPV, &b_mu_calo_cb_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_d0_exPV", &mu_calo_id_d0_exPV, &b_mu_calo_id_d0_exPV);
   fChain->SetBranchAddress("mu_calo_id_z0_exPV", &mu_calo_id_z0_exPV, &b_mu_calo_id_z0_exPV);
   fChain->SetBranchAddress("mu_calo_id_phi_exPV", &mu_calo_id_phi_exPV, &b_mu_calo_id_phi_exPV);
   fChain->SetBranchAddress("mu_calo_id_theta_exPV", &mu_calo_id_theta_exPV, &b_mu_calo_id_theta_exPV);
   fChain->SetBranchAddress("mu_calo_id_qoverp_exPV", &mu_calo_id_qoverp_exPV, &b_mu_calo_id_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_d0_exPV", &mu_calo_me_d0_exPV, &b_mu_calo_me_d0_exPV);
   fChain->SetBranchAddress("mu_calo_me_z0_exPV", &mu_calo_me_z0_exPV, &b_mu_calo_me_z0_exPV);
   fChain->SetBranchAddress("mu_calo_me_phi_exPV", &mu_calo_me_phi_exPV, &b_mu_calo_me_phi_exPV);
   fChain->SetBranchAddress("mu_calo_me_theta_exPV", &mu_calo_me_theta_exPV, &b_mu_calo_me_theta_exPV);
   fChain->SetBranchAddress("mu_calo_me_qoverp_exPV", &mu_calo_me_qoverp_exPV, &b_mu_calo_me_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_ie_d0_exPV", &mu_calo_ie_d0_exPV, &b_mu_calo_ie_d0_exPV);
   fChain->SetBranchAddress("mu_calo_ie_z0_exPV", &mu_calo_ie_z0_exPV, &b_mu_calo_ie_z0_exPV);
   fChain->SetBranchAddress("mu_calo_ie_phi_exPV", &mu_calo_ie_phi_exPV, &b_mu_calo_ie_phi_exPV);
   fChain->SetBranchAddress("mu_calo_ie_theta_exPV", &mu_calo_ie_theta_exPV, &b_mu_calo_ie_theta_exPV);
   fChain->SetBranchAddress("mu_calo_ie_qoverp_exPV", &mu_calo_ie_qoverp_exPV, &b_mu_calo_ie_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_SpaceTime_detID", &mu_calo_SpaceTime_detID, &b_mu_calo_SpaceTime_detID);
   fChain->SetBranchAddress("mu_calo_SpaceTime_t", &mu_calo_SpaceTime_t, &b_mu_calo_SpaceTime_t);
   fChain->SetBranchAddress("mu_calo_SpaceTime_tError", &mu_calo_SpaceTime_tError, &b_mu_calo_SpaceTime_tError);
   fChain->SetBranchAddress("mu_calo_SpaceTime_weight", &mu_calo_SpaceTime_weight, &b_mu_calo_SpaceTime_weight);
   fChain->SetBranchAddress("mu_calo_SpaceTime_x", &mu_calo_SpaceTime_x, &b_mu_calo_SpaceTime_x);
   fChain->SetBranchAddress("mu_calo_SpaceTime_y", &mu_calo_SpaceTime_y, &b_mu_calo_SpaceTime_y);
   fChain->SetBranchAddress("mu_calo_SpaceTime_z", &mu_calo_SpaceTime_z, &b_mu_calo_SpaceTime_z);
   fChain->SetBranchAddress("mu_calo_cov_d0_exPV", &mu_calo_cov_d0_exPV, &b_mu_calo_cov_d0_exPV);
   fChain->SetBranchAddress("mu_calo_cov_z0_exPV", &mu_calo_cov_z0_exPV, &b_mu_calo_cov_z0_exPV);
   fChain->SetBranchAddress("mu_calo_cov_phi_exPV", &mu_calo_cov_phi_exPV, &b_mu_calo_cov_phi_exPV);
   fChain->SetBranchAddress("mu_calo_cov_theta_exPV", &mu_calo_cov_theta_exPV, &b_mu_calo_cov_theta_exPV);
   fChain->SetBranchAddress("mu_calo_cov_qoverp_exPV", &mu_calo_cov_qoverp_exPV, &b_mu_calo_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_cov_d0_z0_exPV", &mu_calo_cov_d0_z0_exPV, &b_mu_calo_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_calo_cov_d0_phi_exPV", &mu_calo_cov_d0_phi_exPV, &b_mu_calo_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_cov_d0_theta_exPV", &mu_calo_cov_d0_theta_exPV, &b_mu_calo_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_cov_d0_qoverp_exPV", &mu_calo_cov_d0_qoverp_exPV, &b_mu_calo_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_cov_z0_phi_exPV", &mu_calo_cov_z0_phi_exPV, &b_mu_calo_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_cov_z0_theta_exPV", &mu_calo_cov_z0_theta_exPV, &b_mu_calo_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_cov_z0_qoverp_exPV", &mu_calo_cov_z0_qoverp_exPV, &b_mu_calo_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_cov_phi_theta_exPV", &mu_calo_cov_phi_theta_exPV, &b_mu_calo_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_calo_cov_phi_qoverp_exPV", &mu_calo_cov_phi_qoverp_exPV, &b_mu_calo_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_cov_theta_qoverp_exPV", &mu_calo_cov_theta_qoverp_exPV, &b_mu_calo_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_d0_exPV", &mu_calo_id_cov_d0_exPV, &b_mu_calo_id_cov_d0_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_z0_exPV", &mu_calo_id_cov_z0_exPV, &b_mu_calo_id_cov_z0_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_phi_exPV", &mu_calo_id_cov_phi_exPV, &b_mu_calo_id_cov_phi_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_theta_exPV", &mu_calo_id_cov_theta_exPV, &b_mu_calo_id_cov_theta_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_qoverp_exPV", &mu_calo_id_cov_qoverp_exPV, &b_mu_calo_id_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_d0_z0_exPV", &mu_calo_id_cov_d0_z0_exPV, &b_mu_calo_id_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_d0_phi_exPV", &mu_calo_id_cov_d0_phi_exPV, &b_mu_calo_id_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_d0_theta_exPV", &mu_calo_id_cov_d0_theta_exPV, &b_mu_calo_id_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_d0_qoverp_exPV", &mu_calo_id_cov_d0_qoverp_exPV, &b_mu_calo_id_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_z0_phi_exPV", &mu_calo_id_cov_z0_phi_exPV, &b_mu_calo_id_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_z0_theta_exPV", &mu_calo_id_cov_z0_theta_exPV, &b_mu_calo_id_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_z0_qoverp_exPV", &mu_calo_id_cov_z0_qoverp_exPV, &b_mu_calo_id_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_phi_theta_exPV", &mu_calo_id_cov_phi_theta_exPV, &b_mu_calo_id_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_phi_qoverp_exPV", &mu_calo_id_cov_phi_qoverp_exPV, &b_mu_calo_id_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_id_cov_theta_qoverp_exPV", &mu_calo_id_cov_theta_qoverp_exPV, &b_mu_calo_id_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_d0_exPV", &mu_calo_me_cov_d0_exPV, &b_mu_calo_me_cov_d0_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_z0_exPV", &mu_calo_me_cov_z0_exPV, &b_mu_calo_me_cov_z0_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_phi_exPV", &mu_calo_me_cov_phi_exPV, &b_mu_calo_me_cov_phi_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_theta_exPV", &mu_calo_me_cov_theta_exPV, &b_mu_calo_me_cov_theta_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_qoverp_exPV", &mu_calo_me_cov_qoverp_exPV, &b_mu_calo_me_cov_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_d0_z0_exPV", &mu_calo_me_cov_d0_z0_exPV, &b_mu_calo_me_cov_d0_z0_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_d0_phi_exPV", &mu_calo_me_cov_d0_phi_exPV, &b_mu_calo_me_cov_d0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_d0_theta_exPV", &mu_calo_me_cov_d0_theta_exPV, &b_mu_calo_me_cov_d0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_d0_qoverp_exPV", &mu_calo_me_cov_d0_qoverp_exPV, &b_mu_calo_me_cov_d0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_z0_phi_exPV", &mu_calo_me_cov_z0_phi_exPV, &b_mu_calo_me_cov_z0_phi_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_z0_theta_exPV", &mu_calo_me_cov_z0_theta_exPV, &b_mu_calo_me_cov_z0_theta_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_z0_qoverp_exPV", &mu_calo_me_cov_z0_qoverp_exPV, &b_mu_calo_me_cov_z0_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_phi_theta_exPV", &mu_calo_me_cov_phi_theta_exPV, &b_mu_calo_me_cov_phi_theta_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_phi_qoverp_exPV", &mu_calo_me_cov_phi_qoverp_exPV, &b_mu_calo_me_cov_phi_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_me_cov_theta_qoverp_exPV", &mu_calo_me_cov_theta_qoverp_exPV, &b_mu_calo_me_cov_theta_qoverp_exPV);
   fChain->SetBranchAddress("mu_calo_ms_d0", &mu_calo_ms_d0, &b_mu_calo_ms_d0);
   fChain->SetBranchAddress("mu_calo_ms_z0", &mu_calo_ms_z0, &b_mu_calo_ms_z0);
   fChain->SetBranchAddress("mu_calo_ms_phi", &mu_calo_ms_phi, &b_mu_calo_ms_phi);
   fChain->SetBranchAddress("mu_calo_ms_theta", &mu_calo_ms_theta, &b_mu_calo_ms_theta);
   fChain->SetBranchAddress("mu_calo_ms_qoverp", &mu_calo_ms_qoverp, &b_mu_calo_ms_qoverp);
   fChain->SetBranchAddress("mu_calo_id_d0", &mu_calo_id_d0, &b_mu_calo_id_d0);
   fChain->SetBranchAddress("mu_calo_id_z0", &mu_calo_id_z0, &b_mu_calo_id_z0);
   fChain->SetBranchAddress("mu_calo_id_phi", &mu_calo_id_phi, &b_mu_calo_id_phi);
   fChain->SetBranchAddress("mu_calo_id_theta", &mu_calo_id_theta, &b_mu_calo_id_theta);
   fChain->SetBranchAddress("mu_calo_id_qoverp", &mu_calo_id_qoverp, &b_mu_calo_id_qoverp);
   fChain->SetBranchAddress("mu_calo_me_d0", &mu_calo_me_d0, &b_mu_calo_me_d0);
   fChain->SetBranchAddress("mu_calo_me_z0", &mu_calo_me_z0, &b_mu_calo_me_z0);
   fChain->SetBranchAddress("mu_calo_me_phi", &mu_calo_me_phi, &b_mu_calo_me_phi);
   fChain->SetBranchAddress("mu_calo_me_theta", &mu_calo_me_theta, &b_mu_calo_me_theta);
   fChain->SetBranchAddress("mu_calo_me_qoverp", &mu_calo_me_qoverp, &b_mu_calo_me_qoverp);
   fChain->SetBranchAddress("mu_calo_ie_d0", &mu_calo_ie_d0, &b_mu_calo_ie_d0);
   fChain->SetBranchAddress("mu_calo_ie_z0", &mu_calo_ie_z0, &b_mu_calo_ie_z0);
   fChain->SetBranchAddress("mu_calo_ie_phi", &mu_calo_ie_phi, &b_mu_calo_ie_phi);
   fChain->SetBranchAddress("mu_calo_ie_theta", &mu_calo_ie_theta, &b_mu_calo_ie_theta);
   fChain->SetBranchAddress("mu_calo_ie_qoverp", &mu_calo_ie_qoverp, &b_mu_calo_ie_qoverp);
   fChain->SetBranchAddress("mu_calo_nOutliersOnTrack", &mu_calo_nOutliersOnTrack, &b_mu_calo_nOutliersOnTrack);
   fChain->SetBranchAddress("mu_calo_nBLHits", &mu_calo_nBLHits, &b_mu_calo_nBLHits);
   fChain->SetBranchAddress("mu_calo_nPixHits", &mu_calo_nPixHits, &b_mu_calo_nPixHits);
   fChain->SetBranchAddress("mu_calo_nSCTHits", &mu_calo_nSCTHits, &b_mu_calo_nSCTHits);
   fChain->SetBranchAddress("mu_calo_nTRTHits", &mu_calo_nTRTHits, &b_mu_calo_nTRTHits);
   fChain->SetBranchAddress("mu_calo_nTRTHighTHits", &mu_calo_nTRTHighTHits, &b_mu_calo_nTRTHighTHits);
   fChain->SetBranchAddress("mu_calo_nBLSharedHits", &mu_calo_nBLSharedHits, &b_mu_calo_nBLSharedHits);
   fChain->SetBranchAddress("mu_calo_nPixSharedHits", &mu_calo_nPixSharedHits, &b_mu_calo_nPixSharedHits);
   fChain->SetBranchAddress("mu_calo_nPixHoles", &mu_calo_nPixHoles, &b_mu_calo_nPixHoles);
   fChain->SetBranchAddress("mu_calo_nSCTSharedHits", &mu_calo_nSCTSharedHits, &b_mu_calo_nSCTSharedHits);
   fChain->SetBranchAddress("mu_calo_nSCTHoles", &mu_calo_nSCTHoles, &b_mu_calo_nSCTHoles);
   fChain->SetBranchAddress("mu_calo_nTRTOutliers", &mu_calo_nTRTOutliers, &b_mu_calo_nTRTOutliers);
   fChain->SetBranchAddress("mu_calo_nTRTHighTOutliers", &mu_calo_nTRTHighTOutliers, &b_mu_calo_nTRTHighTOutliers);
   fChain->SetBranchAddress("mu_calo_nGangedPixels", &mu_calo_nGangedPixels, &b_mu_calo_nGangedPixels);
   fChain->SetBranchAddress("mu_calo_nPixelDeadSensors", &mu_calo_nPixelDeadSensors, &b_mu_calo_nPixelDeadSensors);
   fChain->SetBranchAddress("mu_calo_nSCTDeadSensors", &mu_calo_nSCTDeadSensors, &b_mu_calo_nSCTDeadSensors);
   fChain->SetBranchAddress("mu_calo_nTRTDeadStraws", &mu_calo_nTRTDeadStraws, &b_mu_calo_nTRTDeadStraws);
   fChain->SetBranchAddress("mu_calo_expectBLayerHit", &mu_calo_expectBLayerHit, &b_mu_calo_expectBLayerHit);
   fChain->SetBranchAddress("mu_calo_nMDTHits", &mu_calo_nMDTHits, &b_mu_calo_nMDTHits);
   fChain->SetBranchAddress("mu_calo_nMDTHoles", &mu_calo_nMDTHoles, &b_mu_calo_nMDTHoles);
   fChain->SetBranchAddress("mu_calo_nCSCEtaHits", &mu_calo_nCSCEtaHits, &b_mu_calo_nCSCEtaHits);
   fChain->SetBranchAddress("mu_calo_nCSCEtaHoles", &mu_calo_nCSCEtaHoles, &b_mu_calo_nCSCEtaHoles);
   fChain->SetBranchAddress("mu_calo_nCSCUnspoiledEtaHits", &mu_calo_nCSCUnspoiledEtaHits, &b_mu_calo_nCSCUnspoiledEtaHits);
   fChain->SetBranchAddress("mu_calo_nCSCPhiHits", &mu_calo_nCSCPhiHits, &b_mu_calo_nCSCPhiHits);
   fChain->SetBranchAddress("mu_calo_nCSCPhiHoles", &mu_calo_nCSCPhiHoles, &b_mu_calo_nCSCPhiHoles);
   fChain->SetBranchAddress("mu_calo_nRPCEtaHits", &mu_calo_nRPCEtaHits, &b_mu_calo_nRPCEtaHits);
   fChain->SetBranchAddress("mu_calo_nRPCEtaHoles", &mu_calo_nRPCEtaHoles, &b_mu_calo_nRPCEtaHoles);
   fChain->SetBranchAddress("mu_calo_nRPCPhiHits", &mu_calo_nRPCPhiHits, &b_mu_calo_nRPCPhiHits);
   fChain->SetBranchAddress("mu_calo_nRPCPhiHoles", &mu_calo_nRPCPhiHoles, &b_mu_calo_nRPCPhiHoles);
   fChain->SetBranchAddress("mu_calo_nTGCEtaHits", &mu_calo_nTGCEtaHits, &b_mu_calo_nTGCEtaHits);
   fChain->SetBranchAddress("mu_calo_nTGCEtaHoles", &mu_calo_nTGCEtaHoles, &b_mu_calo_nTGCEtaHoles);
   fChain->SetBranchAddress("mu_calo_nTGCPhiHits", &mu_calo_nTGCPhiHits, &b_mu_calo_nTGCPhiHits);
   fChain->SetBranchAddress("mu_calo_nTGCPhiHoles", &mu_calo_nTGCPhiHoles, &b_mu_calo_nTGCPhiHoles);
   fChain->SetBranchAddress("mu_calo_nprecisionLayers", &mu_calo_nprecisionLayers, &b_mu_calo_nprecisionLayers);
   fChain->SetBranchAddress("mu_calo_nprecisionHoleLayers", &mu_calo_nprecisionHoleLayers, &b_mu_calo_nprecisionHoleLayers);
   fChain->SetBranchAddress("mu_calo_nphiLayers", &mu_calo_nphiLayers, &b_mu_calo_nphiLayers);
   fChain->SetBranchAddress("mu_calo_ntrigEtaLayers", &mu_calo_ntrigEtaLayers, &b_mu_calo_ntrigEtaLayers);
   fChain->SetBranchAddress("mu_calo_nphiHoleLayers", &mu_calo_nphiHoleLayers, &b_mu_calo_nphiHoleLayers);
   fChain->SetBranchAddress("mu_calo_ntrigEtaHoleLayers", &mu_calo_ntrigEtaHoleLayers, &b_mu_calo_ntrigEtaHoleLayers);
   fChain->SetBranchAddress("mu_calo_nMDTBIHits", &mu_calo_nMDTBIHits, &b_mu_calo_nMDTBIHits);
   fChain->SetBranchAddress("mu_calo_nMDTBMHits", &mu_calo_nMDTBMHits, &b_mu_calo_nMDTBMHits);
   fChain->SetBranchAddress("mu_calo_nMDTBOHits", &mu_calo_nMDTBOHits, &b_mu_calo_nMDTBOHits);
   fChain->SetBranchAddress("mu_calo_nMDTBEEHits", &mu_calo_nMDTBEEHits, &b_mu_calo_nMDTBEEHits);
   fChain->SetBranchAddress("mu_calo_nMDTBIS78Hits", &mu_calo_nMDTBIS78Hits, &b_mu_calo_nMDTBIS78Hits);
   fChain->SetBranchAddress("mu_calo_nMDTEIHits", &mu_calo_nMDTEIHits, &b_mu_calo_nMDTEIHits);
   fChain->SetBranchAddress("mu_calo_nMDTEMHits", &mu_calo_nMDTEMHits, &b_mu_calo_nMDTEMHits);
   fChain->SetBranchAddress("mu_calo_nMDTEOHits", &mu_calo_nMDTEOHits, &b_mu_calo_nMDTEOHits);
   fChain->SetBranchAddress("mu_calo_nMDTEEHits", &mu_calo_nMDTEEHits, &b_mu_calo_nMDTEEHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer1EtaHits", &mu_calo_nRPCLayer1EtaHits, &b_mu_calo_nRPCLayer1EtaHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer2EtaHits", &mu_calo_nRPCLayer2EtaHits, &b_mu_calo_nRPCLayer2EtaHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer3EtaHits", &mu_calo_nRPCLayer3EtaHits, &b_mu_calo_nRPCLayer3EtaHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer1PhiHits", &mu_calo_nRPCLayer1PhiHits, &b_mu_calo_nRPCLayer1PhiHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer2PhiHits", &mu_calo_nRPCLayer2PhiHits, &b_mu_calo_nRPCLayer2PhiHits);
   fChain->SetBranchAddress("mu_calo_nRPCLayer3PhiHits", &mu_calo_nRPCLayer3PhiHits, &b_mu_calo_nRPCLayer3PhiHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer1EtaHits", &mu_calo_nTGCLayer1EtaHits, &b_mu_calo_nTGCLayer1EtaHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer2EtaHits", &mu_calo_nTGCLayer2EtaHits, &b_mu_calo_nTGCLayer2EtaHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer3EtaHits", &mu_calo_nTGCLayer3EtaHits, &b_mu_calo_nTGCLayer3EtaHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer4EtaHits", &mu_calo_nTGCLayer4EtaHits, &b_mu_calo_nTGCLayer4EtaHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer1PhiHits", &mu_calo_nTGCLayer1PhiHits, &b_mu_calo_nTGCLayer1PhiHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer2PhiHits", &mu_calo_nTGCLayer2PhiHits, &b_mu_calo_nTGCLayer2PhiHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer3PhiHits", &mu_calo_nTGCLayer3PhiHits, &b_mu_calo_nTGCLayer3PhiHits);
   fChain->SetBranchAddress("mu_calo_nTGCLayer4PhiHits", &mu_calo_nTGCLayer4PhiHits, &b_mu_calo_nTGCLayer4PhiHits);
   fChain->SetBranchAddress("mu_calo_barrelSectors", &mu_calo_barrelSectors, &b_mu_calo_barrelSectors);
   fChain->SetBranchAddress("mu_calo_endcapSectors", &mu_calo_endcapSectors, &b_mu_calo_endcapSectors);
   fChain->SetBranchAddress("mu_calo_spec_surf_px", &mu_calo_spec_surf_px, &b_mu_calo_spec_surf_px);
   fChain->SetBranchAddress("mu_calo_spec_surf_py", &mu_calo_spec_surf_py, &b_mu_calo_spec_surf_py);
   fChain->SetBranchAddress("mu_calo_spec_surf_pz", &mu_calo_spec_surf_pz, &b_mu_calo_spec_surf_pz);
   fChain->SetBranchAddress("mu_calo_spec_surf_x", &mu_calo_spec_surf_x, &b_mu_calo_spec_surf_x);
   fChain->SetBranchAddress("mu_calo_spec_surf_y", &mu_calo_spec_surf_y, &b_mu_calo_spec_surf_y);
   fChain->SetBranchAddress("mu_calo_spec_surf_z", &mu_calo_spec_surf_z, &b_mu_calo_spec_surf_z);
   fChain->SetBranchAddress("mu_calo_trackd0", &mu_calo_trackd0, &b_mu_calo_trackd0);
   fChain->SetBranchAddress("mu_calo_trackz0", &mu_calo_trackz0, &b_mu_calo_trackz0);
   fChain->SetBranchAddress("mu_calo_trackphi", &mu_calo_trackphi, &b_mu_calo_trackphi);
   fChain->SetBranchAddress("mu_calo_tracktheta", &mu_calo_tracktheta, &b_mu_calo_tracktheta);
   fChain->SetBranchAddress("mu_calo_trackqoverp", &mu_calo_trackqoverp, &b_mu_calo_trackqoverp);
   fChain->SetBranchAddress("mu_calo_trackcov_d0", &mu_calo_trackcov_d0, &b_mu_calo_trackcov_d0);
   fChain->SetBranchAddress("mu_calo_trackcov_z0", &mu_calo_trackcov_z0, &b_mu_calo_trackcov_z0);
   fChain->SetBranchAddress("mu_calo_trackcov_phi", &mu_calo_trackcov_phi, &b_mu_calo_trackcov_phi);
   fChain->SetBranchAddress("mu_calo_trackcov_theta", &mu_calo_trackcov_theta, &b_mu_calo_trackcov_theta);
   fChain->SetBranchAddress("mu_calo_trackcov_qoverp", &mu_calo_trackcov_qoverp, &b_mu_calo_trackcov_qoverp);
   fChain->SetBranchAddress("mu_calo_trackcov_d0_z0", &mu_calo_trackcov_d0_z0, &b_mu_calo_trackcov_d0_z0);
   fChain->SetBranchAddress("mu_calo_trackcov_d0_phi", &mu_calo_trackcov_d0_phi, &b_mu_calo_trackcov_d0_phi);
   fChain->SetBranchAddress("mu_calo_trackcov_d0_theta", &mu_calo_trackcov_d0_theta, &b_mu_calo_trackcov_d0_theta);
   fChain->SetBranchAddress("mu_calo_trackcov_d0_qoverp", &mu_calo_trackcov_d0_qoverp, &b_mu_calo_trackcov_d0_qoverp);
   fChain->SetBranchAddress("mu_calo_trackcov_z0_phi", &mu_calo_trackcov_z0_phi, &b_mu_calo_trackcov_z0_phi);
   fChain->SetBranchAddress("mu_calo_trackcov_z0_theta", &mu_calo_trackcov_z0_theta, &b_mu_calo_trackcov_z0_theta);
   fChain->SetBranchAddress("mu_calo_trackcov_z0_qoverp", &mu_calo_trackcov_z0_qoverp, &b_mu_calo_trackcov_z0_qoverp);
   fChain->SetBranchAddress("mu_calo_trackcov_phi_theta", &mu_calo_trackcov_phi_theta, &b_mu_calo_trackcov_phi_theta);
   fChain->SetBranchAddress("mu_calo_trackcov_phi_qoverp", &mu_calo_trackcov_phi_qoverp, &b_mu_calo_trackcov_phi_qoverp);
   fChain->SetBranchAddress("mu_calo_trackcov_theta_qoverp", &mu_calo_trackcov_theta_qoverp, &b_mu_calo_trackcov_theta_qoverp);
   fChain->SetBranchAddress("mu_calo_trackfitchi2", &mu_calo_trackfitchi2, &b_mu_calo_trackfitchi2);
   fChain->SetBranchAddress("mu_calo_trackfitndof", &mu_calo_trackfitndof, &b_mu_calo_trackfitndof);
   fChain->SetBranchAddress("mu_calo_hastrack", &mu_calo_hastrack, &b_mu_calo_hastrack);
   fChain->SetBranchAddress("mu_calo_trackd0beam", &mu_calo_trackd0beam, &b_mu_calo_trackd0beam);
   fChain->SetBranchAddress("mu_calo_trackz0beam", &mu_calo_trackz0beam, &b_mu_calo_trackz0beam);
   fChain->SetBranchAddress("mu_calo_tracksigd0beam", &mu_calo_tracksigd0beam, &b_mu_calo_tracksigd0beam);
   fChain->SetBranchAddress("mu_calo_tracksigz0beam", &mu_calo_tracksigz0beam, &b_mu_calo_tracksigz0beam);
   fChain->SetBranchAddress("mu_calo_trackd0pv", &mu_calo_trackd0pv, &b_mu_calo_trackd0pv);
   fChain->SetBranchAddress("mu_calo_trackz0pv", &mu_calo_trackz0pv, &b_mu_calo_trackz0pv);
   fChain->SetBranchAddress("mu_calo_tracksigd0pv", &mu_calo_tracksigd0pv, &b_mu_calo_tracksigd0pv);
   fChain->SetBranchAddress("mu_calo_tracksigz0pv", &mu_calo_tracksigz0pv, &b_mu_calo_tracksigz0pv);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_d0_biasedpvunbiased", &mu_calo_trackIPEstimate_d0_biasedpvunbiased, &b_mu_calo_trackIPEstimate_d0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_z0_biasedpvunbiased", &mu_calo_trackIPEstimate_z0_biasedpvunbiased, &b_mu_calo_trackIPEstimate_z0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_sigd0_biasedpvunbiased", &mu_calo_trackIPEstimate_sigd0_biasedpvunbiased, &b_mu_calo_trackIPEstimate_sigd0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_sigz0_biasedpvunbiased", &mu_calo_trackIPEstimate_sigz0_biasedpvunbiased, &b_mu_calo_trackIPEstimate_sigz0_biasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_d0_unbiasedpvunbiased", &mu_calo_trackIPEstimate_d0_unbiasedpvunbiased, &b_mu_calo_trackIPEstimate_d0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_z0_unbiasedpvunbiased", &mu_calo_trackIPEstimate_z0_unbiasedpvunbiased, &b_mu_calo_trackIPEstimate_z0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased", &mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased, &b_mu_calo_trackIPEstimate_sigd0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased", &mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased, &b_mu_calo_trackIPEstimate_sigz0_unbiasedpvunbiased);
   fChain->SetBranchAddress("mu_calo_trackd0pvunbiased", &mu_calo_trackd0pvunbiased, &b_mu_calo_trackd0pvunbiased);
   fChain->SetBranchAddress("mu_calo_trackz0pvunbiased", &mu_calo_trackz0pvunbiased, &b_mu_calo_trackz0pvunbiased);
   fChain->SetBranchAddress("mu_calo_tracksigd0pvunbiased", &mu_calo_tracksigd0pvunbiased, &b_mu_calo_tracksigd0pvunbiased);
   fChain->SetBranchAddress("mu_calo_tracksigz0pvunbiased", &mu_calo_tracksigz0pvunbiased, &b_mu_calo_tracksigz0pvunbiased);
   fChain->SetBranchAddress("mu_calo_type", &mu_calo_type, &b_mu_calo_type);
   fChain->SetBranchAddress("mu_calo_origin", &mu_calo_origin, &b_mu_calo_origin);
   fChain->SetBranchAddress("mu_calo_truth_dr", &mu_calo_truth_dr, &b_mu_calo_truth_dr);
   fChain->SetBranchAddress("mu_calo_truth_E", &mu_calo_truth_E, &b_mu_calo_truth_E);
   fChain->SetBranchAddress("mu_calo_truth_pt", &mu_calo_truth_pt, &b_mu_calo_truth_pt);
   fChain->SetBranchAddress("mu_calo_truth_eta", &mu_calo_truth_eta, &b_mu_calo_truth_eta);
   fChain->SetBranchAddress("mu_calo_truth_phi", &mu_calo_truth_phi, &b_mu_calo_truth_phi);
   fChain->SetBranchAddress("mu_calo_truth_type", &mu_calo_truth_type, &b_mu_calo_truth_type);
   fChain->SetBranchAddress("mu_calo_truth_status", &mu_calo_truth_status, &b_mu_calo_truth_status);
   fChain->SetBranchAddress("mu_calo_truth_barcode", &mu_calo_truth_barcode, &b_mu_calo_truth_barcode);
   fChain->SetBranchAddress("mu_calo_truth_mothertype", &mu_calo_truth_mothertype, &b_mu_calo_truth_mothertype);
   fChain->SetBranchAddress("mu_calo_truth_motherbarcode", &mu_calo_truth_motherbarcode, &b_mu_calo_truth_motherbarcode);
   fChain->SetBranchAddress("mu_calo_truth_matched", &mu_calo_truth_matched, &b_mu_calo_truth_matched);
   fChain->SetBranchAddress("mu_calo_EFCB_dr", &mu_calo_EFCB_dr, &b_mu_calo_EFCB_dr);
   fChain->SetBranchAddress("mu_calo_EFCB_n", &mu_calo_EFCB_n, &b_mu_calo_EFCB_n);
   fChain->SetBranchAddress("mu_calo_EFCB_MuonType", &mu_calo_EFCB_MuonType, &b_mu_calo_EFCB_MuonType);
   fChain->SetBranchAddress("mu_calo_EFCB_pt", &mu_calo_EFCB_pt, &b_mu_calo_EFCB_pt);
   fChain->SetBranchAddress("mu_calo_EFCB_eta", &mu_calo_EFCB_eta, &b_mu_calo_EFCB_eta);
   fChain->SetBranchAddress("mu_calo_EFCB_phi", &mu_calo_EFCB_phi, &b_mu_calo_EFCB_phi);
   fChain->SetBranchAddress("mu_calo_EFCB_hasCB", &mu_calo_EFCB_hasCB, &b_mu_calo_EFCB_hasCB);
   fChain->SetBranchAddress("mu_calo_EFCB_matched", &mu_calo_EFCB_matched, &b_mu_calo_EFCB_matched);
   fChain->SetBranchAddress("mu_calo_EFMG_dr", &mu_calo_EFMG_dr, &b_mu_calo_EFMG_dr);
   fChain->SetBranchAddress("mu_calo_EFMG_n", &mu_calo_EFMG_n, &b_mu_calo_EFMG_n);
   fChain->SetBranchAddress("mu_calo_EFMG_MuonType", &mu_calo_EFMG_MuonType, &b_mu_calo_EFMG_MuonType);
   fChain->SetBranchAddress("mu_calo_EFMG_pt", &mu_calo_EFMG_pt, &b_mu_calo_EFMG_pt);
   fChain->SetBranchAddress("mu_calo_EFMG_eta", &mu_calo_EFMG_eta, &b_mu_calo_EFMG_eta);
   fChain->SetBranchAddress("mu_calo_EFMG_phi", &mu_calo_EFMG_phi, &b_mu_calo_EFMG_phi);
   fChain->SetBranchAddress("mu_calo_EFMG_hasMG", &mu_calo_EFMG_hasMG, &b_mu_calo_EFMG_hasMG);
   fChain->SetBranchAddress("mu_calo_EFMG_matched", &mu_calo_EFMG_matched, &b_mu_calo_EFMG_matched);
   fChain->SetBranchAddress("mu_calo_EFME_dr", &mu_calo_EFME_dr, &b_mu_calo_EFME_dr);
   fChain->SetBranchAddress("mu_calo_EFME_n", &mu_calo_EFME_n, &b_mu_calo_EFME_n);
   fChain->SetBranchAddress("mu_calo_EFME_MuonType", &mu_calo_EFME_MuonType, &b_mu_calo_EFME_MuonType);
   fChain->SetBranchAddress("mu_calo_EFME_pt", &mu_calo_EFME_pt, &b_mu_calo_EFME_pt);
   fChain->SetBranchAddress("mu_calo_EFME_eta", &mu_calo_EFME_eta, &b_mu_calo_EFME_eta);
   fChain->SetBranchAddress("mu_calo_EFME_phi", &mu_calo_EFME_phi, &b_mu_calo_EFME_phi);
   fChain->SetBranchAddress("mu_calo_EFME_hasME", &mu_calo_EFME_hasME, &b_mu_calo_EFME_hasME);
   fChain->SetBranchAddress("mu_calo_EFME_matched", &mu_calo_EFME_matched, &b_mu_calo_EFME_matched);
   fChain->SetBranchAddress("mu_calo_L2CB_dr", &mu_calo_L2CB_dr, &b_mu_calo_L2CB_dr);
   fChain->SetBranchAddress("mu_calo_L2CB_pt", &mu_calo_L2CB_pt, &b_mu_calo_L2CB_pt);
   fChain->SetBranchAddress("mu_calo_L2CB_eta", &mu_calo_L2CB_eta, &b_mu_calo_L2CB_eta);
   fChain->SetBranchAddress("mu_calo_L2CB_phi", &mu_calo_L2CB_phi, &b_mu_calo_L2CB_phi);
   fChain->SetBranchAddress("mu_calo_L2CB_id_pt", &mu_calo_L2CB_id_pt, &b_mu_calo_L2CB_id_pt);
   fChain->SetBranchAddress("mu_calo_L2CB_ms_pt", &mu_calo_L2CB_ms_pt, &b_mu_calo_L2CB_ms_pt);
   fChain->SetBranchAddress("mu_calo_L2CB_nPixHits", &mu_calo_L2CB_nPixHits, &b_mu_calo_L2CB_nPixHits);
   fChain->SetBranchAddress("mu_calo_L2CB_nSCTHits", &mu_calo_L2CB_nSCTHits, &b_mu_calo_L2CB_nSCTHits);
   fChain->SetBranchAddress("mu_calo_L2CB_nTRTHits", &mu_calo_L2CB_nTRTHits, &b_mu_calo_L2CB_nTRTHits);
   fChain->SetBranchAddress("mu_calo_L2CB_nTRTHighTHits", &mu_calo_L2CB_nTRTHighTHits, &b_mu_calo_L2CB_nTRTHighTHits);
   fChain->SetBranchAddress("mu_calo_L2CB_matched", &mu_calo_L2CB_matched, &b_mu_calo_L2CB_matched);
   fChain->SetBranchAddress("mu_calo_L1_dr", &mu_calo_L1_dr, &b_mu_calo_L1_dr);
   fChain->SetBranchAddress("mu_calo_L1_pt", &mu_calo_L1_pt, &b_mu_calo_L1_pt);
   fChain->SetBranchAddress("mu_calo_L1_eta", &mu_calo_L1_eta, &b_mu_calo_L1_eta);
   fChain->SetBranchAddress("mu_calo_L1_phi", &mu_calo_L1_phi, &b_mu_calo_L1_phi);
   fChain->SetBranchAddress("mu_calo_L1_thrNumber", &mu_calo_L1_thrNumber, &b_mu_calo_L1_thrNumber);
   fChain->SetBranchAddress("mu_calo_L1_RoINumber", &mu_calo_L1_RoINumber, &b_mu_calo_L1_RoINumber);
   fChain->SetBranchAddress("mu_calo_L1_sectorAddress", &mu_calo_L1_sectorAddress, &b_mu_calo_L1_sectorAddress);
   fChain->SetBranchAddress("mu_calo_L1_firstCandidate", &mu_calo_L1_firstCandidate, &b_mu_calo_L1_firstCandidate);
   fChain->SetBranchAddress("mu_calo_L1_moreCandInRoI", &mu_calo_L1_moreCandInRoI, &b_mu_calo_L1_moreCandInRoI);
   fChain->SetBranchAddress("mu_calo_L1_moreCandInSector", &mu_calo_L1_moreCandInSector, &b_mu_calo_L1_moreCandInSector);
   fChain->SetBranchAddress("mu_calo_L1_source", &mu_calo_L1_source, &b_mu_calo_L1_source);
   fChain->SetBranchAddress("mu_calo_L1_hemisphere", &mu_calo_L1_hemisphere, &b_mu_calo_L1_hemisphere);
   fChain->SetBranchAddress("mu_calo_L1_charge", &mu_calo_L1_charge, &b_mu_calo_L1_charge);
   fChain->SetBranchAddress("mu_calo_L1_vetoed", &mu_calo_L1_vetoed, &b_mu_calo_L1_vetoed);
   fChain->SetBranchAddress("mu_calo_L1_matched", &mu_calo_L1_matched, &b_mu_calo_L1_matched);
   fChain->SetBranchAddress("mooreseg_n", &mooreseg_n, &b_mooreseg_n);
   fChain->SetBranchAddress("mooreseg_x", &mooreseg_x, &b_mooreseg_x);
   fChain->SetBranchAddress("mooreseg_y", &mooreseg_y, &b_mooreseg_y);
   fChain->SetBranchAddress("mooreseg_z", &mooreseg_z, &b_mooreseg_z);
   fChain->SetBranchAddress("mooreseg_phi", &mooreseg_phi, &b_mooreseg_phi);
   fChain->SetBranchAddress("mooreseg_theta", &mooreseg_theta, &b_mooreseg_theta);
   fChain->SetBranchAddress("mooreseg_locX", &mooreseg_locX, &b_mooreseg_locX);
   fChain->SetBranchAddress("mooreseg_locY", &mooreseg_locY, &b_mooreseg_locY);
   fChain->SetBranchAddress("mooreseg_locAngleXZ", &mooreseg_locAngleXZ, &b_mooreseg_locAngleXZ);
   fChain->SetBranchAddress("mooreseg_locAngleYZ", &mooreseg_locAngleYZ, &b_mooreseg_locAngleYZ);
   fChain->SetBranchAddress("mooreseg_sector", &mooreseg_sector, &b_mooreseg_sector);
   fChain->SetBranchAddress("mooreseg_stationEta", &mooreseg_stationEta, &b_mooreseg_stationEta);
   fChain->SetBranchAddress("mooreseg_isEndcap", &mooreseg_isEndcap, &b_mooreseg_isEndcap);
   fChain->SetBranchAddress("mooreseg_stationName", &mooreseg_stationName, &b_mooreseg_stationName);
   fChain->SetBranchAddress("mooreseg_author", &mooreseg_author, &b_mooreseg_author);
   fChain->SetBranchAddress("mooreseg_chi2", &mooreseg_chi2, &b_mooreseg_chi2);
   fChain->SetBranchAddress("mooreseg_ndof", &mooreseg_ndof, &b_mooreseg_ndof);
   fChain->SetBranchAddress("mooreseg_t0", &mooreseg_t0, &b_mooreseg_t0);
   fChain->SetBranchAddress("mooreseg_t0err", &mooreseg_t0err, &b_mooreseg_t0err);
   fChain->SetBranchAddress("mboyseg_n", &mboyseg_n, &b_mboyseg_n);
   fChain->SetBranchAddress("mboyseg_x", &mboyseg_x, &b_mboyseg_x);
   fChain->SetBranchAddress("mboyseg_y", &mboyseg_y, &b_mboyseg_y);
   fChain->SetBranchAddress("mboyseg_z", &mboyseg_z, &b_mboyseg_z);
   fChain->SetBranchAddress("mboyseg_phi", &mboyseg_phi, &b_mboyseg_phi);
   fChain->SetBranchAddress("mboyseg_theta", &mboyseg_theta, &b_mboyseg_theta);
   fChain->SetBranchAddress("mboyseg_locX", &mboyseg_locX, &b_mboyseg_locX);
   fChain->SetBranchAddress("mboyseg_locY", &mboyseg_locY, &b_mboyseg_locY);
   fChain->SetBranchAddress("mboyseg_locAngleXZ", &mboyseg_locAngleXZ, &b_mboyseg_locAngleXZ);
   fChain->SetBranchAddress("mboyseg_locAngleYZ", &mboyseg_locAngleYZ, &b_mboyseg_locAngleYZ);
   fChain->SetBranchAddress("mboyseg_sector", &mboyseg_sector, &b_mboyseg_sector);
   fChain->SetBranchAddress("mboyseg_stationEta", &mboyseg_stationEta, &b_mboyseg_stationEta);
   fChain->SetBranchAddress("mboyseg_isEndcap", &mboyseg_isEndcap, &b_mboyseg_isEndcap);
   fChain->SetBranchAddress("mboyseg_stationName", &mboyseg_stationName, &b_mboyseg_stationName);
   fChain->SetBranchAddress("mboyseg_author", &mboyseg_author, &b_mboyseg_author);
   fChain->SetBranchAddress("mboyseg_chi2", &mboyseg_chi2, &b_mboyseg_chi2);
   fChain->SetBranchAddress("mboyseg_ndof", &mboyseg_ndof, &b_mboyseg_ndof);
   fChain->SetBranchAddress("mboyseg_t0", &mboyseg_t0, &b_mboyseg_t0);
   fChain->SetBranchAddress("mboyseg_t0err", &mboyseg_t0err, &b_mboyseg_t0err);
   fChain->SetBranchAddress("mgseg_n", &mgseg_n, &b_mgseg_n);
   fChain->SetBranchAddress("mgseg_x", &mgseg_x, &b_mgseg_x);
   fChain->SetBranchAddress("mgseg_y", &mgseg_y, &b_mgseg_y);
   fChain->SetBranchAddress("mgseg_z", &mgseg_z, &b_mgseg_z);
   fChain->SetBranchAddress("mgseg_phi", &mgseg_phi, &b_mgseg_phi);
   fChain->SetBranchAddress("mgseg_theta", &mgseg_theta, &b_mgseg_theta);
   fChain->SetBranchAddress("mgseg_locX", &mgseg_locX, &b_mgseg_locX);
   fChain->SetBranchAddress("mgseg_locY", &mgseg_locY, &b_mgseg_locY);
   fChain->SetBranchAddress("mgseg_locAngleXZ", &mgseg_locAngleXZ, &b_mgseg_locAngleXZ);
   fChain->SetBranchAddress("mgseg_locAngleYZ", &mgseg_locAngleYZ, &b_mgseg_locAngleYZ);
   fChain->SetBranchAddress("mgseg_sector", &mgseg_sector, &b_mgseg_sector);
   fChain->SetBranchAddress("mgseg_stationEta", &mgseg_stationEta, &b_mgseg_stationEta);
   fChain->SetBranchAddress("mgseg_isEndcap", &mgseg_isEndcap, &b_mgseg_isEndcap);
   fChain->SetBranchAddress("mgseg_stationName", &mgseg_stationName, &b_mgseg_stationName);
   fChain->SetBranchAddress("mgseg_author", &mgseg_author, &b_mgseg_author);
   fChain->SetBranchAddress("mgseg_chi2", &mgseg_chi2, &b_mgseg_chi2);
   fChain->SetBranchAddress("mgseg_ndof", &mgseg_ndof, &b_mgseg_ndof);
   fChain->SetBranchAddress("mgseg_t0", &mgseg_t0, &b_mgseg_t0);
   fChain->SetBranchAddress("mgseg_t0err", &mgseg_t0err, &b_mgseg_t0err);
   fChain->SetBranchAddress("muonTruth_n", &muonTruth_n, &b_muonTruth_n);
   fChain->SetBranchAddress("muonTruth_pt", &muonTruth_pt, &b_muonTruth_pt);
   fChain->SetBranchAddress("muonTruth_m", &muonTruth_m, &b_muonTruth_m);
   fChain->SetBranchAddress("muonTruth_eta", &muonTruth_eta, &b_muonTruth_eta);
   fChain->SetBranchAddress("muonTruth_phi", &muonTruth_phi, &b_muonTruth_phi);
   fChain->SetBranchAddress("muonTruth_charge", &muonTruth_charge, &b_muonTruth_charge);
   fChain->SetBranchAddress("muonTruth_PDGID", &muonTruth_PDGID, &b_muonTruth_PDGID);
   fChain->SetBranchAddress("muonTruth_barcode", &muonTruth_barcode, &b_muonTruth_barcode);
   fChain->SetBranchAddress("muonTruth_type", &muonTruth_type, &b_muonTruth_type);
   fChain->SetBranchAddress("muonTruth_origin", &muonTruth_origin, &b_muonTruth_origin);
   fChain->SetBranchAddress("mcevt_n", &mcevt_n, &b_mcevt_n);
   fChain->SetBranchAddress("mcevt_signal_process_id", &mcevt_signal_process_id, &b_mcevt_signal_process_id);
   fChain->SetBranchAddress("mcevt_event_number", &mcevt_event_number, &b_mcevt_event_number);
   fChain->SetBranchAddress("mcevt_event_scale", &mcevt_event_scale, &b_mcevt_event_scale);
   fChain->SetBranchAddress("mcevt_alphaQCD", &mcevt_alphaQCD, &b_mcevt_alphaQCD);
   fChain->SetBranchAddress("mcevt_alphaQED", &mcevt_alphaQED, &b_mcevt_alphaQED);
   fChain->SetBranchAddress("mcevt_pdf_id1", &mcevt_pdf_id1, &b_mcevt_pdf_id1);
   fChain->SetBranchAddress("mcevt_pdf_id2", &mcevt_pdf_id2, &b_mcevt_pdf_id2);
   fChain->SetBranchAddress("mcevt_pdf_x1", &mcevt_pdf_x1, &b_mcevt_pdf_x1);
   fChain->SetBranchAddress("mcevt_pdf_x2", &mcevt_pdf_x2, &b_mcevt_pdf_x2);
   fChain->SetBranchAddress("mcevt_pdf_scale", &mcevt_pdf_scale, &b_mcevt_pdf_scale);
   fChain->SetBranchAddress("mcevt_pdf1", &mcevt_pdf1, &b_mcevt_pdf1);
   fChain->SetBranchAddress("mcevt_pdf2", &mcevt_pdf2, &b_mcevt_pdf2);
   fChain->SetBranchAddress("mcevt_weight", &mcevt_weight, &b_mcevt_weight);
   fChain->SetBranchAddress("mcevt_nparticle", &mcevt_nparticle, &b_mcevt_nparticle);
   fChain->SetBranchAddress("mcevt_pileUpType", &mcevt_pileUpType, &b_mcevt_pileUpType);
   fChain->SetBranchAddress("mc_n", &mc_n, &b_mc_n);
   fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
   fChain->SetBranchAddress("mc_m", &mc_m, &b_mc_m);
   fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
   fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
   fChain->SetBranchAddress("mc_status", &mc_status, &b_mc_status);
   fChain->SetBranchAddress("mc_barcode", &mc_barcode, &b_mc_barcode);
   fChain->SetBranchAddress("mc_pdgId", &mc_pdgId, &b_mc_pdgId);
   fChain->SetBranchAddress("mc_charge", &mc_charge, &b_mc_charge);
   fChain->SetBranchAddress("mc_parents", &mc_parents, &b_mc_parents);
   fChain->SetBranchAddress("mc_children", &mc_children, &b_mc_children);
   fChain->SetBranchAddress("mc_vx_x", &mc_vx_x, &b_mc_vx_x);
   fChain->SetBranchAddress("mc_vx_y", &mc_vx_y, &b_mc_vx_y);
   fChain->SetBranchAddress("mc_vx_z", &mc_vx_z, &b_mc_vx_z);
   fChain->SetBranchAddress("mc_vx_barcode", &mc_vx_barcode, &b_mc_vx_barcode);
   fChain->SetBranchAddress("mc_child_index", &mc_child_index, &b_mc_child_index);
   fChain->SetBranchAddress("mc_parent_index", &mc_parent_index, &b_mc_parent_index);
   fChain->SetBranchAddress("trig_bgCode", &trig_bgCode, &b_trig_bgCode);
   fChain->SetBranchAddress("trig_L1_mu_n", &trig_L1_mu_n, &b_trig_L1_mu_n);
   fChain->SetBranchAddress("trig_L1_mu_pt", &trig_L1_mu_pt, &b_trig_L1_mu_pt);
   fChain->SetBranchAddress("trig_L1_mu_eta", &trig_L1_mu_eta, &b_trig_L1_mu_eta);
   fChain->SetBranchAddress("trig_L1_mu_phi", &trig_L1_mu_phi, &b_trig_L1_mu_phi);
   fChain->SetBranchAddress("trig_L1_mu_thrName", &trig_L1_mu_thrName, &b_trig_L1_mu_thrName);
   fChain->SetBranchAddress("trig_L1_mu_thrNumber", &trig_L1_mu_thrNumber, &b_trig_L1_mu_thrNumber);
   fChain->SetBranchAddress("trig_L1_mu_RoINumber", &trig_L1_mu_RoINumber, &b_trig_L1_mu_RoINumber);
   fChain->SetBranchAddress("trig_L1_mu_sectorAddress", &trig_L1_mu_sectorAddress, &b_trig_L1_mu_sectorAddress);
   fChain->SetBranchAddress("trig_L1_mu_firstCandidate", &trig_L1_mu_firstCandidate, &b_trig_L1_mu_firstCandidate);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInRoI", &trig_L1_mu_moreCandInRoI, &b_trig_L1_mu_moreCandInRoI);
   fChain->SetBranchAddress("trig_L1_mu_moreCandInSector", &trig_L1_mu_moreCandInSector, &b_trig_L1_mu_moreCandInSector);
   fChain->SetBranchAddress("trig_L1_mu_source", &trig_L1_mu_source, &b_trig_L1_mu_source);
   fChain->SetBranchAddress("trig_L1_mu_hemisphere", &trig_L1_mu_hemisphere, &b_trig_L1_mu_hemisphere);
   fChain->SetBranchAddress("trig_L1_mu_charge", &trig_L1_mu_charge, &b_trig_L1_mu_charge);
   fChain->SetBranchAddress("trig_L1_mu_vetoed", &trig_L1_mu_vetoed, &b_trig_L1_mu_vetoed);
   fChain->SetBranchAddress("trig_L1_mu_RoIWord", &trig_L1_mu_RoIWord, &b_trig_L1_mu_RoIWord);
   fChain->SetBranchAddress("muctpi_candMultiplicity", &muctpi_candMultiplicity, &b_muctpi_candMultiplicity);
   fChain->SetBranchAddress("muctpi_nDataWords", &muctpi_nDataWords, &b_muctpi_nDataWords);
   fChain->SetBranchAddress("muctpi_dataWords", &muctpi_dataWords, &b_muctpi_dataWords);
   fChain->SetBranchAddress("muctpi_dw_eta", &muctpi_dw_eta, &b_muctpi_dw_eta);
   fChain->SetBranchAddress("muctpi_dw_phi", &muctpi_dw_phi, &b_muctpi_dw_phi);
   fChain->SetBranchAddress("muctpi_dw_source", &muctpi_dw_source, &b_muctpi_dw_source);
   fChain->SetBranchAddress("muctpi_dw_hemisphere", &muctpi_dw_hemisphere, &b_muctpi_dw_hemisphere);
   fChain->SetBranchAddress("muctpi_dw_bcid", &muctpi_dw_bcid, &b_muctpi_dw_bcid);
   fChain->SetBranchAddress("muctpi_dw_sectorID", &muctpi_dw_sectorID, &b_muctpi_dw_sectorID);
   fChain->SetBranchAddress("muctpi_dw_thrNumber", &muctpi_dw_thrNumber, &b_muctpi_dw_thrNumber);
   fChain->SetBranchAddress("muctpi_dw_RoINumber", &muctpi_dw_RoINumber, &b_muctpi_dw_RoINumber);
   fChain->SetBranchAddress("muctpi_dw_overlapFlags", &muctpi_dw_overlapFlags, &b_muctpi_dw_overlapFlags);
   fChain->SetBranchAddress("muctpi_dw_firstCandidate", &muctpi_dw_firstCandidate, &b_muctpi_dw_firstCandidate);
   fChain->SetBranchAddress("muctpi_dw_moreCandInRoI", &muctpi_dw_moreCandInRoI, &b_muctpi_dw_moreCandInRoI);
   fChain->SetBranchAddress("muctpi_dw_moreCandInSector", &muctpi_dw_moreCandInSector, &b_muctpi_dw_moreCandInSector);
   fChain->SetBranchAddress("muctpi_dw_charge", &muctpi_dw_charge, &b_muctpi_dw_charge);
   fChain->SetBranchAddress("muctpi_dw_vetoed", &muctpi_dw_vetoed, &b_muctpi_dw_vetoed);
   fChain->SetBranchAddress("TGC_prd_x", &TGC_prd_x, &b_TGC_prd_x);
   fChain->SetBranchAddress("TGC_prd_y", &TGC_prd_y, &b_TGC_prd_y);
   fChain->SetBranchAddress("TGC_prd_z", &TGC_prd_z, &b_TGC_prd_z);
   fChain->SetBranchAddress("TGC_prd_shortWidth", &TGC_prd_shortWidth, &b_TGC_prd_shortWidth);
   fChain->SetBranchAddress("TGC_prd_longWidth", &TGC_prd_longWidth, &b_TGC_prd_longWidth);
   fChain->SetBranchAddress("TGC_prd_length", &TGC_prd_length, &b_TGC_prd_length);
   fChain->SetBranchAddress("TGC_prd_isStrip", &TGC_prd_isStrip, &b_TGC_prd_isStrip);
   fChain->SetBranchAddress("TGC_prd_gasGap", &TGC_prd_gasGap, &b_TGC_prd_gasGap);
   fChain->SetBranchAddress("TGC_prd_channel", &TGC_prd_channel, &b_TGC_prd_channel);
   fChain->SetBranchAddress("TGC_prd_eta", &TGC_prd_eta, &b_TGC_prd_eta);
   fChain->SetBranchAddress("TGC_prd_phi", &TGC_prd_phi, &b_TGC_prd_phi);
   fChain->SetBranchAddress("TGC_prd_station", &TGC_prd_station, &b_TGC_prd_station);
   fChain->SetBranchAddress("TGC_prd_bunch", &TGC_prd_bunch, &b_TGC_prd_bunch);
   fChain->SetBranchAddress("TGC_coin_x_In", &TGC_coin_x_In, &b_TGC_coin_x_In);
   fChain->SetBranchAddress("TGC_coin_y_In", &TGC_coin_y_In, &b_TGC_coin_y_In);
   fChain->SetBranchAddress("TGC_coin_z_In", &TGC_coin_z_In, &b_TGC_coin_z_In);
   fChain->SetBranchAddress("TGC_coin_x_Out", &TGC_coin_x_Out, &b_TGC_coin_x_Out);
   fChain->SetBranchAddress("TGC_coin_y_Out", &TGC_coin_y_Out, &b_TGC_coin_y_Out);
   fChain->SetBranchAddress("TGC_coin_z_Out", &TGC_coin_z_Out, &b_TGC_coin_z_Out);
   fChain->SetBranchAddress("TGC_coin_width_In", &TGC_coin_width_In, &b_TGC_coin_width_In);
   fChain->SetBranchAddress("TGC_coin_width_Out", &TGC_coin_width_Out, &b_TGC_coin_width_Out);
   fChain->SetBranchAddress("TGC_coin_width_R", &TGC_coin_width_R, &b_TGC_coin_width_R);
   fChain->SetBranchAddress("TGC_coin_width_Phi", &TGC_coin_width_Phi, &b_TGC_coin_width_Phi);
   fChain->SetBranchAddress("TGC_coin_isAside", &TGC_coin_isAside, &b_TGC_coin_isAside);
   fChain->SetBranchAddress("TGC_coin_isForward", &TGC_coin_isForward, &b_TGC_coin_isForward);
   fChain->SetBranchAddress("TGC_coin_isStrip", &TGC_coin_isStrip, &b_TGC_coin_isStrip);
   fChain->SetBranchAddress("TGC_coin_isPositiveDeltaR", &TGC_coin_isPositiveDeltaR, &b_TGC_coin_isPositiveDeltaR);
   fChain->SetBranchAddress("TGC_coin_type", &TGC_coin_type, &b_TGC_coin_type);
   fChain->SetBranchAddress("TGC_coin_trackletId", &TGC_coin_trackletId, &b_TGC_coin_trackletId);
   fChain->SetBranchAddress("TGC_coin_trackletIdStrip", &TGC_coin_trackletIdStrip, &b_TGC_coin_trackletIdStrip);
   fChain->SetBranchAddress("TGC_coin_phi", &TGC_coin_phi, &b_TGC_coin_phi);
   fChain->SetBranchAddress("TGC_coin_roi", &TGC_coin_roi, &b_TGC_coin_roi);
   fChain->SetBranchAddress("TGC_coin_pt", &TGC_coin_pt, &b_TGC_coin_pt);
   fChain->SetBranchAddress("TGC_coin_delta", &TGC_coin_delta, &b_TGC_coin_delta);
   fChain->SetBranchAddress("TGC_coin_sub", &TGC_coin_sub, &b_TGC_coin_sub);
   fChain->SetBranchAddress("TGC_coin_bunch", &TGC_coin_bunch, &b_TGC_coin_bunch);
   fChain->SetBranchAddress("TGC_hierarchy_index", &TGC_hierarchy_index, &b_TGC_hierarchy_index);
   fChain->SetBranchAddress("TGC_hierarchy_dR_hiPt", &TGC_hierarchy_dR_hiPt, &b_TGC_hierarchy_dR_hiPt);
   fChain->SetBranchAddress("TGC_hierarchy_dPhi_hiPt", &TGC_hierarchy_dPhi_hiPt, &b_TGC_hierarchy_dPhi_hiPt);
   fChain->SetBranchAddress("TGC_hierarchy_dR_tracklet", &TGC_hierarchy_dR_tracklet, &b_TGC_hierarchy_dR_tracklet);
   fChain->SetBranchAddress("TGC_hierarchy_dPhi_tracklet", &TGC_hierarchy_dPhi_tracklet, &b_TGC_hierarchy_dPhi_tracklet);
   fChain->SetBranchAddress("TGC_hierarchy_isChamberBoundary", &TGC_hierarchy_isChamberBoundary, &b_TGC_hierarchy_isChamberBoundary);
   fChain->SetBranchAddress("RPC_prd_x", &RPC_prd_x, &b_RPC_prd_x);
   fChain->SetBranchAddress("RPC_prd_y", &RPC_prd_y, &b_RPC_prd_y);
   fChain->SetBranchAddress("RPC_prd_z", &RPC_prd_z, &b_RPC_prd_z);
   fChain->SetBranchAddress("RPC_prd_time", &RPC_prd_time, &b_RPC_prd_time);
   fChain->SetBranchAddress("RPC_prd_triggerInfo", &RPC_prd_triggerInfo, &b_RPC_prd_triggerInfo);
   fChain->SetBranchAddress("RPC_prd_ambiguityFlag", &RPC_prd_ambiguityFlag, &b_RPC_prd_ambiguityFlag);
   fChain->SetBranchAddress("RPC_prd_measuresPhi", &RPC_prd_measuresPhi, &b_RPC_prd_measuresPhi);
   fChain->SetBranchAddress("RPC_prd_inRibs", &RPC_prd_inRibs, &b_RPC_prd_inRibs);
   fChain->SetBranchAddress("RPC_prd_station", &RPC_prd_station, &b_RPC_prd_station);
   fChain->SetBranchAddress("RPC_prd_stationEta", &RPC_prd_stationEta, &b_RPC_prd_stationEta);
   fChain->SetBranchAddress("RPC_prd_stationPhi", &RPC_prd_stationPhi, &b_RPC_prd_stationPhi);
   fChain->SetBranchAddress("RPC_prd_doubletR", &RPC_prd_doubletR, &b_RPC_prd_doubletR);
   fChain->SetBranchAddress("RPC_prd_doubletZ", &RPC_prd_doubletZ, &b_RPC_prd_doubletZ);
   fChain->SetBranchAddress("RPC_prd_stripWidth", &RPC_prd_stripWidth, &b_RPC_prd_stripWidth);
   fChain->SetBranchAddress("RPC_prd_stripLength", &RPC_prd_stripLength, &b_RPC_prd_stripLength);
   fChain->SetBranchAddress("RPC_prd_stripPitch", &RPC_prd_stripPitch, &b_RPC_prd_stripPitch);
   fChain->SetBranchAddress("MDT_prd_x", &MDT_prd_x, &b_MDT_prd_x);
   fChain->SetBranchAddress("MDT_prd_y", &MDT_prd_y, &b_MDT_prd_y);
   fChain->SetBranchAddress("MDT_prd_z", &MDT_prd_z, &b_MDT_prd_z);
   fChain->SetBranchAddress("MDT_prd_adc", &MDT_prd_adc, &b_MDT_prd_adc);
   fChain->SetBranchAddress("MDT_prd_tdc", &MDT_prd_tdc, &b_MDT_prd_tdc);
   fChain->SetBranchAddress("MDT_prd_status", &MDT_prd_status, &b_MDT_prd_status);
   fChain->SetBranchAddress("MDT_prd_drift_radius", &MDT_prd_drift_radius, &b_MDT_prd_drift_radius);
   fChain->SetBranchAddress("MDT_prd_drift_radius_error", &MDT_prd_drift_radius_error, &b_MDT_prd_drift_radius_error);
   fChain->SetBranchAddress("ext_staco_ubias_type", &ext_staco_ubias_type, &b_ext_staco_ubias_type);
   fChain->SetBranchAddress("ext_staco_ubias_index", &ext_staco_ubias_index, &b_ext_staco_ubias_index);
   fChain->SetBranchAddress("ext_staco_ubias_size", &ext_staco_ubias_size, &b_ext_staco_ubias_size);
   fChain->SetBranchAddress("ext_staco_ubias_targetVec", &ext_staco_ubias_targetVec, &b_ext_staco_ubias_targetVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetDistanceVec", &ext_staco_ubias_targetDistanceVec, &b_ext_staco_ubias_targetDistanceVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetEtaVec", &ext_staco_ubias_targetEtaVec, &b_ext_staco_ubias_targetEtaVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetPhiVec", &ext_staco_ubias_targetPhiVec, &b_ext_staco_ubias_targetPhiVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetDeltaEtaVec", &ext_staco_ubias_targetDeltaEtaVec, &b_ext_staco_ubias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetDeltaPhiVec", &ext_staco_ubias_targetDeltaPhiVec, &b_ext_staco_ubias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetPxVec", &ext_staco_ubias_targetPxVec, &b_ext_staco_ubias_targetPxVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetPyVec", &ext_staco_ubias_targetPyVec, &b_ext_staco_ubias_targetPyVec);
   fChain->SetBranchAddress("ext_staco_ubias_targetPzVec", &ext_staco_ubias_targetPzVec, &b_ext_staco_ubias_targetPzVec);
   fChain->SetBranchAddress("ext_staco_bias_type", &ext_staco_bias_type, &b_ext_staco_bias_type);
   fChain->SetBranchAddress("ext_staco_bias_index", &ext_staco_bias_index, &b_ext_staco_bias_index);
   fChain->SetBranchAddress("ext_staco_bias_size", &ext_staco_bias_size, &b_ext_staco_bias_size);
   fChain->SetBranchAddress("ext_staco_bias_targetVec", &ext_staco_bias_targetVec, &b_ext_staco_bias_targetVec);
   fChain->SetBranchAddress("ext_staco_bias_targetDistanceVec", &ext_staco_bias_targetDistanceVec, &b_ext_staco_bias_targetDistanceVec);
   fChain->SetBranchAddress("ext_staco_bias_targetEtaVec", &ext_staco_bias_targetEtaVec, &b_ext_staco_bias_targetEtaVec);
   fChain->SetBranchAddress("ext_staco_bias_targetPhiVec", &ext_staco_bias_targetPhiVec, &b_ext_staco_bias_targetPhiVec);
   fChain->SetBranchAddress("ext_staco_bias_targetDeltaEtaVec", &ext_staco_bias_targetDeltaEtaVec, &b_ext_staco_bias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_staco_bias_targetDeltaPhiVec", &ext_staco_bias_targetDeltaPhiVec, &b_ext_staco_bias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_staco_bias_targetPxVec", &ext_staco_bias_targetPxVec, &b_ext_staco_bias_targetPxVec);
   fChain->SetBranchAddress("ext_staco_bias_targetPyVec", &ext_staco_bias_targetPyVec, &b_ext_staco_bias_targetPyVec);
   fChain->SetBranchAddress("ext_staco_bias_targetPzVec", &ext_staco_bias_targetPzVec, &b_ext_staco_bias_targetPzVec);
   fChain->SetBranchAddress("ext_muid_ubias_type", &ext_muid_ubias_type, &b_ext_muid_ubias_type);
   fChain->SetBranchAddress("ext_muid_ubias_index", &ext_muid_ubias_index, &b_ext_muid_ubias_index);
   fChain->SetBranchAddress("ext_muid_ubias_size", &ext_muid_ubias_size, &b_ext_muid_ubias_size);
   fChain->SetBranchAddress("ext_muid_ubias_targetVec", &ext_muid_ubias_targetVec, &b_ext_muid_ubias_targetVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetDistanceVec", &ext_muid_ubias_targetDistanceVec, &b_ext_muid_ubias_targetDistanceVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetEtaVec", &ext_muid_ubias_targetEtaVec, &b_ext_muid_ubias_targetEtaVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetPhiVec", &ext_muid_ubias_targetPhiVec, &b_ext_muid_ubias_targetPhiVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetDeltaEtaVec", &ext_muid_ubias_targetDeltaEtaVec, &b_ext_muid_ubias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetDeltaPhiVec", &ext_muid_ubias_targetDeltaPhiVec, &b_ext_muid_ubias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetPxVec", &ext_muid_ubias_targetPxVec, &b_ext_muid_ubias_targetPxVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetPyVec", &ext_muid_ubias_targetPyVec, &b_ext_muid_ubias_targetPyVec);
   fChain->SetBranchAddress("ext_muid_ubias_targetPzVec", &ext_muid_ubias_targetPzVec, &b_ext_muid_ubias_targetPzVec);
   fChain->SetBranchAddress("ext_muid_bias_type", &ext_muid_bias_type, &b_ext_muid_bias_type);
   fChain->SetBranchAddress("ext_muid_bias_index", &ext_muid_bias_index, &b_ext_muid_bias_index);
   fChain->SetBranchAddress("ext_muid_bias_size", &ext_muid_bias_size, &b_ext_muid_bias_size);
   fChain->SetBranchAddress("ext_muid_bias_targetVec", &ext_muid_bias_targetVec, &b_ext_muid_bias_targetVec);
   fChain->SetBranchAddress("ext_muid_bias_targetDistanceVec", &ext_muid_bias_targetDistanceVec, &b_ext_muid_bias_targetDistanceVec);
   fChain->SetBranchAddress("ext_muid_bias_targetEtaVec", &ext_muid_bias_targetEtaVec, &b_ext_muid_bias_targetEtaVec);
   fChain->SetBranchAddress("ext_muid_bias_targetPhiVec", &ext_muid_bias_targetPhiVec, &b_ext_muid_bias_targetPhiVec);
   fChain->SetBranchAddress("ext_muid_bias_targetDeltaEtaVec", &ext_muid_bias_targetDeltaEtaVec, &b_ext_muid_bias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_muid_bias_targetDeltaPhiVec", &ext_muid_bias_targetDeltaPhiVec, &b_ext_muid_bias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_muid_bias_targetPxVec", &ext_muid_bias_targetPxVec, &b_ext_muid_bias_targetPxVec);
   fChain->SetBranchAddress("ext_muid_bias_targetPyVec", &ext_muid_bias_targetPyVec, &b_ext_muid_bias_targetPyVec);
   fChain->SetBranchAddress("ext_muid_bias_targetPzVec", &ext_muid_bias_targetPzVec, &b_ext_muid_bias_targetPzVec);
   fChain->SetBranchAddress("ext_calo_ubias_type", &ext_calo_ubias_type, &b_ext_calo_ubias_type);
   fChain->SetBranchAddress("ext_calo_ubias_index", &ext_calo_ubias_index, &b_ext_calo_ubias_index);
   fChain->SetBranchAddress("ext_calo_ubias_size", &ext_calo_ubias_size, &b_ext_calo_ubias_size);
   fChain->SetBranchAddress("ext_calo_ubias_targetVec", &ext_calo_ubias_targetVec, &b_ext_calo_ubias_targetVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetDistanceVec", &ext_calo_ubias_targetDistanceVec, &b_ext_calo_ubias_targetDistanceVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetEtaVec", &ext_calo_ubias_targetEtaVec, &b_ext_calo_ubias_targetEtaVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetPhiVec", &ext_calo_ubias_targetPhiVec, &b_ext_calo_ubias_targetPhiVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetDeltaEtaVec", &ext_calo_ubias_targetDeltaEtaVec, &b_ext_calo_ubias_targetDeltaEtaVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetDeltaPhiVec", &ext_calo_ubias_targetDeltaPhiVec, &b_ext_calo_ubias_targetDeltaPhiVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetPxVec", &ext_calo_ubias_targetPxVec, &b_ext_calo_ubias_targetPxVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetPyVec", &ext_calo_ubias_targetPyVec, &b_ext_calo_ubias_targetPyVec);
   fChain->SetBranchAddress("ext_calo_ubias_targetPzVec", &ext_calo_ubias_targetPzVec, &b_ext_calo_ubias_targetPzVec);
   fChain->SetBranchAddress("trigger_info_chain", &trigger_info_chain, &b_trigger_info_chain);
   fChain->SetBranchAddress("trigger_info_isPassed", &trigger_info_isPassed, &b_trigger_info_isPassed);
   fChain->SetBranchAddress("trigger_info_nTracks", &trigger_info_nTracks, &b_trigger_info_nTracks);
   fChain->SetBranchAddress("trigger_info_typeVec", &trigger_info_typeVec, &b_trigger_info_typeVec);
   fChain->SetBranchAddress("trigger_info_ptVec", &trigger_info_ptVec, &b_trigger_info_ptVec);
   fChain->SetBranchAddress("trigger_info_etaVec", &trigger_info_etaVec, &b_trigger_info_etaVec);
   fChain->SetBranchAddress("trigger_info_phiVec", &trigger_info_phiVec, &b_trigger_info_phiVec);
   fChain->SetBranchAddress("trigger_info_chargeVec", &trigger_info_chargeVec, &b_trigger_info_chargeVec);
   fChain->SetBranchAddress("trigger_info_l1RoiWordVec", &trigger_info_l1RoiWordVec, &b_trigger_info_l1RoiWordVec);
   fChain->SetBranchAddress("tandp_staco_tag_index", &tandp_staco_tag_index, &b_tandp_staco_tag_index);
   fChain->SetBranchAddress("tandp_staco_probe_index", &tandp_staco_probe_index, &b_tandp_staco_probe_index);
   fChain->SetBranchAddress("tandp_staco_vertex_ndof", &tandp_staco_vertex_ndof, &b_tandp_staco_vertex_ndof);
   fChain->SetBranchAddress("tandp_staco_vertex_chi2", &tandp_staco_vertex_chi2, &b_tandp_staco_vertex_chi2);
   fChain->SetBranchAddress("tandp_staco_vertex_posX", &tandp_staco_vertex_posX, &b_tandp_staco_vertex_posX);
   fChain->SetBranchAddress("tandp_staco_vertex_posY", &tandp_staco_vertex_posY, &b_tandp_staco_vertex_posY);
   fChain->SetBranchAddress("tandp_staco_vertex_posZ", &tandp_staco_vertex_posZ, &b_tandp_staco_vertex_posZ);
   fChain->SetBranchAddress("tandp_staco_vertex_probability", &tandp_staco_vertex_probability, &b_tandp_staco_vertex_probability);
   fChain->SetBranchAddress("tandp_staco_v0_mass", &tandp_staco_v0_mass, &b_tandp_staco_v0_mass);
   fChain->SetBranchAddress("tandp_staco_v0_mass_error", &tandp_staco_v0_mass_error, &b_tandp_staco_v0_mass_error);
   fChain->SetBranchAddress("tandp_staco_v0_mass_probability", &tandp_staco_v0_mass_probability, &b_tandp_staco_v0_mass_probability);
   fChain->SetBranchAddress("tandp_staco_v0_px", &tandp_staco_v0_px, &b_tandp_staco_v0_px);
   fChain->SetBranchAddress("tandp_staco_v0_py", &tandp_staco_v0_py, &b_tandp_staco_v0_py);
   fChain->SetBranchAddress("tandp_staco_v0_pz", &tandp_staco_v0_pz, &b_tandp_staco_v0_pz);
   fChain->SetBranchAddress("tandp_staco_v0_pt", &tandp_staco_v0_pt, &b_tandp_staco_v0_pt);
   fChain->SetBranchAddress("tandp_staco_v0_pt_error", &tandp_staco_v0_pt_error, &b_tandp_staco_v0_pt_error);
   fChain->SetBranchAddress("tandp_staco_num_verticies", &tandp_staco_num_verticies, &b_tandp_staco_num_verticies);
   fChain->SetBranchAddress("tandp_staco_vertex_index", &tandp_staco_vertex_index, &b_tandp_staco_vertex_index);
   fChain->SetBranchAddress("tandp_staco_lxyVec", &tandp_staco_lxyVec, &b_tandp_staco_lxyVec);
   fChain->SetBranchAddress("tandp_staco_lxy_errorVec", &tandp_staco_lxy_errorVec, &b_tandp_staco_lxy_errorVec);
   fChain->SetBranchAddress("tandp_staco_tauVec", &tandp_staco_tauVec, &b_tandp_staco_tauVec);
   fChain->SetBranchAddress("tandp_staco_tau_errorVec", &tandp_staco_tau_errorVec, &b_tandp_staco_tau_errorVec);
   fChain->SetBranchAddress("tandp_muid_tag_index", &tandp_muid_tag_index, &b_tandp_muid_tag_index);
   fChain->SetBranchAddress("tandp_muid_probe_index", &tandp_muid_probe_index, &b_tandp_muid_probe_index);
   fChain->SetBranchAddress("tandp_muid_vertex_ndof", &tandp_muid_vertex_ndof, &b_tandp_muid_vertex_ndof);
   fChain->SetBranchAddress("tandp_muid_vertex_chi2", &tandp_muid_vertex_chi2, &b_tandp_muid_vertex_chi2);
   fChain->SetBranchAddress("tandp_muid_vertex_posX", &tandp_muid_vertex_posX, &b_tandp_muid_vertex_posX);
   fChain->SetBranchAddress("tandp_muid_vertex_posY", &tandp_muid_vertex_posY, &b_tandp_muid_vertex_posY);
   fChain->SetBranchAddress("tandp_muid_vertex_posZ", &tandp_muid_vertex_posZ, &b_tandp_muid_vertex_posZ);
   fChain->SetBranchAddress("tandp_muid_vertex_probability", &tandp_muid_vertex_probability, &b_tandp_muid_vertex_probability);
   fChain->SetBranchAddress("tandp_muid_v0_mass", &tandp_muid_v0_mass, &b_tandp_muid_v0_mass);
   fChain->SetBranchAddress("tandp_muid_v0_mass_error", &tandp_muid_v0_mass_error, &b_tandp_muid_v0_mass_error);
   fChain->SetBranchAddress("tandp_muid_v0_mass_probability", &tandp_muid_v0_mass_probability, &b_tandp_muid_v0_mass_probability);
   fChain->SetBranchAddress("tandp_muid_v0_px", &tandp_muid_v0_px, &b_tandp_muid_v0_px);
   fChain->SetBranchAddress("tandp_muid_v0_py", &tandp_muid_v0_py, &b_tandp_muid_v0_py);
   fChain->SetBranchAddress("tandp_muid_v0_pz", &tandp_muid_v0_pz, &b_tandp_muid_v0_pz);
   fChain->SetBranchAddress("tandp_muid_v0_pt", &tandp_muid_v0_pt, &b_tandp_muid_v0_pt);
   fChain->SetBranchAddress("tandp_muid_v0_pt_error", &tandp_muid_v0_pt_error, &b_tandp_muid_v0_pt_error);
   fChain->SetBranchAddress("tandp_muid_num_verticies", &tandp_muid_num_verticies, &b_tandp_muid_num_verticies);
   fChain->SetBranchAddress("tandp_muid_vertex_index", &tandp_muid_vertex_index, &b_tandp_muid_vertex_index);
   fChain->SetBranchAddress("tandp_muid_lxyVec", &tandp_muid_lxyVec, &b_tandp_muid_lxyVec);
   fChain->SetBranchAddress("tandp_muid_lxy_errorVec", &tandp_muid_lxy_errorVec, &b_tandp_muid_lxy_errorVec);
   fChain->SetBranchAddress("tandp_muid_tauVec", &tandp_muid_tauVec, &b_tandp_muid_tauVec);
   fChain->SetBranchAddress("tandp_muid_tau_errorVec", &tandp_muid_tau_errorVec, &b_tandp_muid_tau_errorVec);
   Notify();
}

Bool_t physics::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void physics::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t physics::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef physics_cxx
