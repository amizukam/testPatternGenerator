#!/bin/sh -f

# void obtain_hits(const char *output_name, const int PT, const int ISFORWARD, const int PHI, const int ROI)
#
# PT: to be compared with TGC_coin_pt
# ISFORWARD: to be compared with TGC_coin_isForward
# PHI: to be compared with TGC_coin_phi
# ROI: to be compared with TGC_coin_roi

root -l -b 'obtain_hits.C++("output.txt", 6, 0, 2, 20)'
