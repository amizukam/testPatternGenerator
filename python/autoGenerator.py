import subprocess as sp
from optparse import OptionParser
import os
import shutil

parser = OptionParser()

parser.add_option("-r", "--region", action="store", type="string", dest="region", help="F:Forward region, E:Endcap region", default="F")
parser.add_option("-s", "--side", action="store", type="string", dest="side", help="A: A-side, C: C-side", default="A")
parser.add_option("-c", "--sector", action="store", type="string", dest="sector", help="01, 02, ..., 11, 12", default="01")
parser.add_option("-i", "--inputFile", action="store", type="string", dest="inputFile", help="The file name for the STEP1 input")


(options, args) = parser.parse_args()

side = options.side
sector = options.sector
inputFile = options.inputFile
Region = options.region

if Region == "E":
    Dir="../TGCTrackPatternMaker/STEP1_Endcap/"
elif Region == "F":
    Dir="../TGCTrackPatternMaker/STEP1_Forward/"
else:
    print "Wrong argument. F for Forward or E for Endcap is available"

outputFileName="outputs/autoGenerate.out" # Would it be better to get the name from inputFile?
#Get the inputFile from STEP0
inputFileName = "../STEP0/" + inputFile

os.chdir(Dir)
f = open("Execute.sh", "w")
f.write("#!/bin/bash\n")
f.write("./ana " + inputFileName + " " + outputFileName)
f.close()
os.system("./Execute.sh")


# Step2 start from here
STEP2 = "../STEP2"
os.chdir(STEP2)
#sp.Popen(cmd, shell=True)

os.environ["TRACK_PATTERN_DIR"] = "../STEP2"
ENV = os.environ.get("TRACK_PATTERN_DIR")
#print ("TRACK_PATTERN_DIR : ") + ENV

os.environ["SETUP_DIR"] = ENV + "/Setup/"
S = os.environ.get("SETUP_DIR")
#print ("SETUP_DIR : ") + S


os.system("Setup/preparation.csh")

if options.region == "E":
    Dir = "../STEP1_Endcap/"
elif options.region == "F":
    Dir = "../STEP1_Forward/"

path = Dir
files = path + outputFileName
#fin = open("files", "r")

shutil.copyfile(files, "./Setup/Tpp.txt.tmp")

read_file = open(files, "r")
write_file = open("./Setup/Tpp.txt", "w")
#print "Open " + files
for line in read_file:
    if line.find("#SIDE  SEC  PHI  EoF  WoS  LAY  rID  CH") != -1:
        line = line.replace("#SIDE  SEC  PHI  EoF  WoS  LAY  rID  CH", "BIT_DEFINITION 1")
    write_file.write(line)
read_file.close()
write_file.close()

os.remove("./Setup/Tpp.txt.tmp")

#sp.Popen("pwd", shell=True)

cmd = "Packages/TgcParameterGenerator/bin/GenerateTgcParameters -f Slb/Tpp"
cmd += " -s " + side
cmd += " -c " + sector
cmd += " -m Setup/config_tpp_on.txt"
cmd += " -n " + "test_ON.prm"

cmd2 = "Packages/TgcParameterGenerator/bin/GenerateTgcParameters -f Slb/Tpp"
cmd2 += " -s " + side
cmd2 += " -c " + sector
cmd2 += " -m Setup/config_tpp_off.txt"
cmd2 += " -n " + "test_OFF.prm"


#print cmd
sp.call(cmd, shell=True)
sp.call(cmd2, shell= True)

print "Done!"
