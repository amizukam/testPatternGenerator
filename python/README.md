## How to use the auto generator script
Before you execute the auto script, you need to put the input file(s) in the STEP1 inputfiles directory.

You should modify 
```
TRACK_PATTERN_DIR
```
to your environment before using the automatic script.

0) setup the environment
```
source setup.sh
```

1) run the script
```
python autoGenerator.py -r [REGION] -s [SIDE] -c [SECTOR] -i [INPUTFILE]
```

ex) If you want to generate the pulse pattern at the end-cap reagion, C05, and the input file name is the "example.in" in STEP0/, 
```
python autoGenerator.py -r E -s C -c 05 -i example.in
```

The output file (test_ON.prm and test_OFF.prm) are created at STEP2/