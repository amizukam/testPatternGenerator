## Getting Started

This project is created for the TGC track pattern generator tool.

```
mkdir workArea; cd workArea;
```

Get trackPatternGenerator with:

```
git clone https://:@gitlab.cern.ch:8443/amizukam/testPatternGenerator.git
```

To generate the test pulse pattern, go to STEP1 and read the instruction(README).

This tool can work only for the endcaps now.  
Need to update to generate at forward region.

Branch description
```
master : Original version
develop : Compilable version for the forward
Endcap : Same as original version
Forward : Mainly developing version for forward. If this version cannot work, go back to develop version
```


## Convert the offline parameters (TGC_prd_...) into online parameters at STEP1
Copied the README from STEP1.

0) Type 'make clean' and 'make' to compile codes.

1) Prepare an input file int the directory "inputfiles".  
   Example file is exist: inputfiles/example.in.  
   The actual values should not be in the file line.  
   The file should consists of side, phi, station, eta, layer, wire/strip and channel.  
   Exaple of a line of the actual values: "A 1 M1 4 1 1 4"  

   - side: "A" or "C", can be derived from the sign of TGC_prd_eta(+: A, -: C).  
   - phi: 1-48 (TGC_prd_phi).  
   - station: "M1", "M2", or "M3", can be derived from TGC_prd_station (e.g "42" -> "M1").  
   - eta: 1-5 (magnitude of TGC_prd_eta).  
   - layer: 1-3 (TGC_prd_gasGap).  
   - channel: 1-122 (TGC_prd_channel).  

2) Edit the file "Execute.sh" (file names of input and output should be edited), and execute the following command.  
   The output file consists of side, sector, phi, endcap/forward, wire/strip, layer, rID, and channel.  
   The first line shows the list of contents.  
   The actual values are shown from the second line.  

   ./Execute.sh

## Go to STEP2
Copied the README from STEP2.  

0) TRACK_PATTERN_DIR should be set.  
   [Directory location] should be .../TGCTrackPatternMaker/STEP2  
   ("..." depends on the actual location of the package.)  

   > export TRACK_PATTERN_DIR=[Directory location]

1) Environmental setup.

   > source $TRACK_PATTERN_DIR/Setup/setup.sh

2) Compilation is needed for the following packages in the initial use.  
   Complilation should be done at vm-tdq-build-02(?)  
   This step can be skipped if compilation has already been done(most cases).  

   $PACKAGE_DIR/TgcDeadChannelAnalysis
   $PACKAGE_DIR/TgcMappingSvc
   $PACKAGE_DIR/TgcParameterGenerator

3) Create a setup file based on the output from STEP1: $SETUP_DIR/Tpp.txt
   An example is at $SEETUP_DIR/Tpp.txt.example
   Note that the first line should be "BIT_DEFINITION 1".

4) Prepare config file for parameter generation by following command.  
   
   > $SETUP_DIR/preparation.csh

5) Execute the following commands (one for the list of "ON", the other for "OFF").  
   [Side] should be A or C
   [Sector] should be one of 01, 02, ....., 12

   > $PACKAGES_DIR/TgcParameterGenerator/bin/GenerateTgcParameters -f Slb/Tpp -s [Side] -c[Sector] -m $SETUP_DIR/config_tpp_on.txt -n Tpp_ON.prm

   > $PACKAGES_DIR/TgcParameterGenerator/bin/GenerateTgcParameters -f Slb/Tpp -s [Side] -c[Sector] -m $SETUP_DIR/config_tpp_off.txt -n Tpp_OFF.prm

Note: -n Tpp_ON.prm is created file.

6) Check the contents of the output file Tpp_ON.prm and Tpp_OFF.prm.  
   Single channel in Tpp.txt can create multiple channels in Tpp_ON.prm;  
   some channels are separated and input into two SLB ASICs.  
   All bits should be zero for Tpp_OFF.prm.

7) Use the created files for test pulse runs.  
   Locations of the tools for test pulse runs with arbitrary track test pattern are shown in the following.  

   - Track test pulse run GUI: /det/muon/TGC/StandalonePartition/FEtest/guiForArbTPP  
   - Scripts: /det/muon/TGC/StandalonePartition/FEtest/scriptsForArbTPP  
   - Parameter files: /det/muon/TGC/StandalonePartition/FEtest/param/Arbtrack  

All procedure are tested at lxplus.

